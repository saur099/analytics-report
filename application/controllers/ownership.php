<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ownership extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('ownership_model');
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		
		//$data['res'] = $this->ownership_model->get_support_details($useruid);
		//$data['resr'] = $this->support_model->get_prod_serial();
		//print_r($data['res']); exit();
		$this->load->view('services/ownership_view');
	}
	
	function search()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$serial = trim(stripslashes($_POST['serial']));
		$mobile	 = trim(stripslashes($_POST['mobile']));
		// echo "Serial : ".$serial."<br/>";
		// echo "Mobile : ".$mobile."<br/>";
		
		if(empty($serial) && empty($mobile))
		{
			//die("no");
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Please provide anyone either mobile no. or serial number..')
				window.location.href='".base_url()."index.php/ownership';
				</SCRIPT>");
		}
		else
		{
			//die("Hello");
			if(empty($serial)){ $serial =''; }
			if(empty($mobile)){ $mobile =''; }
				
			$user_uuid = $get_session_data['user_uuid'];
			//$data['mob'] = $this->ownership_model->get_ownership_mob($mobile);
			
			if(!empty($mobile) || $mobile != '')
			{
				$data['mob'] = $this->ownership_model->get_ownership_mob($mobile);
				if(empty($data['mob']))
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! Mobile number not found. Please create customer.')
						window.location.href='".base_url()."index.php/service_cases/new_consumer';
						</SCRIPT>");
				}
				else
				{
					$this->load->view('services/owner_mob_view', $data);
				}	
			}
			else
			{
				$resSer = $this->ownership_model->get_product_detail($serial);
				//print_r($resSer); die("Product");
				$row = $resSer[0]->num; 
				if($row > 0)
				{
						$data['ser'] = $this->ownership_model->get_ownership_serial($serial);
						//echo "<pre>"; print_r($data['ser']); //die("Hello");
						if(empty($data['ser']))
						{
							//echo " hii";
						/* 	echo ("<SCRIPT LANGUAGE='JavaScript'>
								window.alert('Sorry ! Please contact admin. We couldn't find this product.')
								window.location.href='".base_url()."index.php/ownership';
								</SCRIPT>"); */
								
								echo ("<SCRIPT LANGUAGE='JavaScript'>
								window.alert('Sorry ! We could not find this product. Please contact Mr Vineet Kumar Upadhyay.')
								window.location.href='".base_url()."index.php/ownership';
								</SCRIPT>");
						}
						else
						{
							$this->load->view('services/owner_serial_view', $data);
							//$this->load->view('testing/testing_mode', $data);
						}	    
				}
				else
				{
							echo ("<SCRIPT LANGUAGE='JavaScript'>
								window.alert('Sorry ! The product is not created yet.')
								window.location.href='".base_url()."index.php/ownership';
								</SCRIPT>");
				}	
			}
				
		}	
	}
	
	/* 
	function search()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$userT = $get_session_data['user_type'];
		$this->load->model('support_model');
		$data['res'] = $this->support_model->get_support_admin();
		//$data['resr'] = $this->support_model->get_prod_serial();
		//print_r($data['res']); exit();
		$this->load->view('', $data);
	} 
	*/
	
	function confirmation($id)
	{
		$id = base64_decode($id);
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		//echo $id; die("Hello");
		$data = $this->ownership_model->confirm_new_owner($useruid, $id); 
		if($data > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Success ! You have taken ownership.')
						window.location.href='".base_url()."index.php/ownership/successes';
						</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! You cant take ownership at this time. ')
						window.location.href='".base_url()."index.php/failureship';
						</SCRIPT>");
		}
		
	}
	function serial_cnfrm($id)
	{
		$id = base64_decode($id);
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		//echo $id; die("Hello");
		$date = date('d-m-Y');
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$prCst = $this->ownership_model->get_prod_cust($id);  
		//echo "<pre>"; print_r($prCst); //die("Hello");
		if(!empty($prCst))
		{
			$new_cons = array(
								 'fname' => $prCst[0]->fname,
								 'lname' => $prCst[0]->lname,
								 'mobile' => $prCst[0]->mobile,
								 'address' => $prCst[0]->address,
								 'state' => $prCst[0]->state,
								 'city' => $prCst[0]->city,
								 'area' => $prCst[0]->area,
								 'pincode' => $prCst[0]->pincode,
								 'cust_code' => $prCst[0]->cust_code,
								 'cust_product_id' => $prCst[0]->cust_product_id,
								 'custAssetGuid' => $prCst[0]->productGuid,
								 'prod_pri_key' => $prCst[0]->productId,
								 'ownership_taken_on' => $date,
								 'owned_by_ip' => $ip,
								 'custPurchaseDate' => $prCst[0]->custPurchaseDate,
								 'warrantyStatus' => $prCst[0]->warrantyStatus,
								 'productType' => $prCst[0]->productType,
								 'model' => $prCst[0]->model,
								 'previous_owner_guid' => 'N.A.',
								 'createdBy' => $useruid,
							  );
			//echo "<pre>"; print_r($new_cons); die;
			$data = $this->ownership_model->insert_new_owner($new_cons); 
			if($data > 0)
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Success ! You have taken ownership.')
							window.location.href='".base_url()."index.php/ownership/successes';
							</SCRIPT>");
			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! You can't take ownership at this time. Try again later.')
							window.location.href='".base_url()."index.php/failureship';
							</SCRIPT>");
			}
		}
		else
		{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! You can't take ownership at this time. Try again later.')
						window.location.href='".base_url()."index.php/failureship';
						</SCRIPT>");
		}
		
		
	}
	function successes() 
	{
		//$this->load->view(('services/success');
		$this->load->view('services/owner_success');
	}
	function failureship()
	{
		//$this->load->view(('services/success');
		$this->load->view('services/owner_failure');
	}
	
	function change_cust()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$create = trim($_POST['create']);
		$mobile = trim($_POST['mobile']);
		$Serial = trim($_POST['productSerialNo']);
		$productGuid = trim($_POST['productGuid']);
		
		$date = date("Y-m-d H:i:s");
		$ip = $_SERVER['REMOTE_ADDR']; 
		$arr = array(
		              'createdBy' => $useruid,
		              'ownership_taken_on' => $date,
		              'previous_owner_guid' => $create,
		              'owned_by_ip' => $ip
					  );
					
		// echo "<pre>"; print_r($arr1); 			
		// echo "<pre>"; print_r($arr); die("Hello");  			
		 
		//$this->load->model('support_model');
		
			if(!empty($Serial))
			{
				$arr1 = array(
		              'createdBy' => $useruid,
		              'ownership_taken_on' => $date,
		              'previous_owner_guid' => $create,
		              'owned_by_ip' => $ip
					  );
				$arr2 = array(
						'account_id_c' => $useruid
						);	
				/* echo "<pre>"; print_r($arr1); 
				echo "<pre>"; print_r($arr2); 
				die("hello");	 */	
				$upd = $this->ownership_model->update_mob_product($arr1, $arr2, $create, $productGuid, $mobile);
				if($upd > 0)
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Successful ! You are now the owner of this customer & product. ')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
				}
				else
				{
					
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! Some problem occurred.. ')
							window.location.href='".base_url()."index.php/ownership';
							</SCRIPT>");
				}
			}
			else
			{
				$upd = $this->ownership_model->insert_data_owner($arr, $create, $mobile);
				// echo "<pre>"; print_r($arr); 
				// die("hello");
				if($upd > 0)
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Successful ! You are now the owner of this customer. ')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
				}
				else
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! Some problem occurred.. ')
							window.location.href='".base_url()."index.php/ownership';
							</SCRIPT>");
				}
			}
			
			
			
			//print_r($upd); die("Hello");
		
		
	}
	
	
	
	function update_product()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$createdBy = trim($_POST['createdBy']);
		$mobile = trim($_POST['mobile']);
		$Serial = trim($_POST['productSerialNo']);
		$productGuid = trim($_POST['productGuid']);
		$userId = trim($_POST['userId']);
		/* 
		echo "Mobile : ".$mobile."<br/>";
		echo "Serial : ".$Serial."<br/>";
		echo "Product : ".$productGuid."<br/>";
		echo "UserId : ".$userId."<br/>";
		echo "useruid : ".$useruid."<br/>";
		die;
		 */
		if(empty($Serial))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Serial Number can't be empty.. ')
					window.location.href='".base_url()."index.php/ownership';
					</SCRIPT>");
		}
		else
		{
			  $date = date("Y-m-d H:i:s");
				$ip = $_SERVER['REMOTE_ADDR']; 
				
				$arr1 = array(
							  'createdBy' => $useruid,
							  'ownership_taken_on' => $date,
							  'previous_owner_guid' => $createdBy,
							  'owned_by_ip' => $ip
							  );
							  
				$arr2  = array(
								 'owned_on' => $date,
								 'owned_ip' => $ip,
								 'previous_owner' => $userId,
								 'userId'=>$useruid
							  );			
				$arr3 = array(
							   'account_id_c' => $useruid
							 );			  
				// echo "<pre>"; print_r($arr1); 			
				// echo "<pre>"; print_r($arr); die("Hello");  			
				 
				//$this->load->model('support_model');
				$upd = $this->ownership_model->insert_data_serial($arr1, $arr2, $arr3, $Serial, $productGuid, $mobile);
				/* print_r($upd); die("Hello");
				print_r($upd); die("Hello"); */
				if($upd > 0)
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Successful ! You are now the owner of this customer. ')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
				}
				else
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! Some problem occurred.. ')
							window.location.href='".base_url()."index.php/ownership';
							</SCRIPT>");
				}
		}
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */