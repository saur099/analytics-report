<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fault_parts extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
             $this->load->model('fault_parts_model');
             $this->load->model('cc_customer_model');
		//$this->load->library('../models/service_case_model');
		//  $this->load->model('models/service_case_model');
		 // $this->load->model('service_case_model');
           
    }

/******************************Shivshankar method changes ***********************************************************************/	
public function views_challan(){
  $get_session_data = $this->session->userdata('logged_in');
  $user = $get_session_data['user_uuid'];
  
  
  if($_POST['submit']){
   $data['challan_month'] = $this->input->post('pname');
   $data['product_type'] = $this->input->post('product_type');
   //echo "<pre>";
   //print_r($data); die;
   $this->load->view('services/views_challan_details', $data);
  }else{
  $this->load->model('fault_parts_model');
  $data['res'] = $this->fault_parts_model->get_challan_list($user);
  //echo "<pre>";
  //print_r($data); die;
  $this->load->view('services/views_challan_details', $data);
  }
 }
/******************************Shivshankar method changes ends ******************************************************************/	
	
    
	function view_challan()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		$this->load->model('fault_parts_model');
		$data['res'] = $this->fault_parts_model->get_challan_list($user);
		 $data['pname'] = $this->fault_parts_model->products_detail();
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/view_challan_details', $data);
	}
	
	
	public function index($id)
	{
		$id = base64_decode($id);
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		$user_type = $get_session_data['user_type'];
		// echo $id; die; 
		/*
		$sql = "SELECT * FROM `tblservicecasetestreport` WHERE `caseId";
		//echo $sql; die;
		$r = $this->db->query($sql)->result();
		if($r[0]->caLL_case_status != '0')
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The test Report is already submitted. Go Back..')
					window.location.href='".base_url()."service_cases';
					</SCRIPT>");
		} 
		*/
		
		$this->load->model('fault_parts_model');
                if($user_type='SF')
                {
                    $data['res'] = $this->fault_parts_model->get_parts_details($id);
                }
                else 
                {
                    $data['res'] = $this->fault_parts_model->get_call_details($id);
                }
                
		
		if($user_type == 'SF')
                {
                    if(empty($data['res']))
                    {
                         echo ("<SCRIPT LANGUAGE='JavaScript'>
                                window.alert('Sorry ! Something went wrong with Complaint. Please contact admin.')
                                window.location.href='".base_url()."index.php/mod_call_center/complain_module_battery/YWxsb2NhdGU=';
                                </SCRIPT>");
                    }
                    $dealerCode=$data['res'][0]->dealerName;
                    $data['bak'] = $this->fault_parts_model->get_dealerName($dealerCode);
                    $this->load->view('services/fault_parts_view', $data);
                }
                else
                {
		 //echo "<pre>"; print_r($data['res']); die("Hello");
                    if(empty($data['res']))
                    {
                            echo ("<SCRIPT LANGUAGE='JavaScript'>
                                            window.alert('Sorry ! Something went wrong with Complaint. Please contact admin.')
                                            window.location.href='".base_url()."index.php/service_cases';
                                            </SCRIPT>");
                    }
                    $dealerCode=$data['res'][0]->dealerName;
		$data['bak'] = $this->fault_parts_model->get_dealerName($dealerCode);
		$this->load->view('services/fault_parts_view', $data);
                }
		
		
	}
	function batery_report()
	{
		//  echo "Hello jsbdjfbsdjbfjdsbfjhd"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$case = trim($_POST['caseId']);
		$arr = array(
					  'physicalCond' => trim($_POST['dremark']),
					  'condOfTerminal' => trim($_POST['brand']),
					  'electrolyteColor' => trim($_POST['sevisit']),
					  'ocv' => trim($_POST['ocv_bef']),
					  'caseId' => $case
				);
		//print_r($arr); die;
		$this->load->model('fault_parts_model');
		$dat = $this->fault_parts_model->insert_batt_report($arr);
		//print_r($dat)
		if($dat > 0)
		{
			echo $case;
		}
		else
		{
			return '';
		}
	}
	
	function fault_close_case()
	{
		 
		$get_session_data = $this->session->userdata('logged_in');
		$case  	  	  = 	trim(stripslashes($_POST['case']));
		$caseENC      =  	base64_encode($case);
		$user = $get_session_data['user_uuid'];
		$this->load->model('fault_parts_model');
		$st = $this->fault_parts_model->check_decision($case);
		// print_r($st[0]->caLL_case_status); die("Hii"); 
		$var = $st[0]->caLL_case_status;
		//echo $var; die;
		if(empty($_FILES['docUpload']['name']))
		{
			//$case = base64_encode($case);
				echo ("<SCRIPT LANGUAGE='JavaScript'>
                                        window.alert('Document not uploaded properly. Please upload it again. ')
                                        window.location.href='".base_url()."index.php/fault_parts/decision/$caseENC';
                                        </SCRIPT>"); 
		} 
                if($var > 0)
                {
			echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! The decision is already given, Please check status in my complaints.')
							window.location.href='cases';
							</SCRIPT>");
		}
		else
		{
				$user  	      = 	trim(stripslashes($_POST['user']));
				$symptom      = 	trim(stripslashes($_POST['symptom']));
				$defect       = 	trim(stripslashes($_POST['symptom']));
				$action  	  = 	trim(stripslashes($_POST['action']));
				$dist_remark  = 	trim(stripslashes($_POST['remark']));
				$docType  	  = 	trim(stripslashes($_POST['docType']));
				$docType2  	  = 	trim(stripslashes($_POST['docType2']));
				$docType3  	  = 	trim(stripslashes($_POST['docType3']));
				$docType4  	  = 	trim(stripslashes($_POST['docType4']));
				
				$count = 0;
				if(move_uploaded_file($_FILES['docUpload']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload']['name']))
				{ $count =1; }
			
				if(move_uploaded_file($_FILES['docUpload2']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload2']['name'])){}
				if(move_uploaded_file($_FILES['docUpload3']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload3']['name'])){}
				if(move_uploaded_file($_FILES['docUpload4']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload4']['name'])){}
				
				if(empty($dist_remark) || $dist_remark=='0') {
					$dist_remark = '11';
				}
				/* 
				$query 	= $this->db->query("SELECT * FROM `tbl_service_dist_remarks` WHERE `distRemarkId`='".$dist_remark."'");
				$row 	= $query->result();
				 $remarks = $row[0]->distRemarkName;
				 */
				/* 
				$sql 	= $this->db->query("SELECT * FROM `tbl_defect_symptoms` WHERE `id`='".$symptom."'");
				$rows 	= $sql->result();
				
				 $symptom = $rows[0]->Symptoms;
				 $defect   = $rows[0]->Defect;
				  */ 
				  $dt = date("d-m-Y H:i:s");
				$data = array(
								'callModifiedBy' => $user,
								'symptomCode' => $symptom,
								'defectCode' => $defect,
								'caLL_case_status' => $action,
								'uploadDocuments' => $_FILES['docUpload']['name'],
								'uploadDocs2' => $_FILES['docUpload2']['name'],
								'uploadDocs3' => $_FILES['docUpload3']['name'],
								'uploadDocs4' => $_FILES['docUpload4']['name'],
								'distributorRemark' => $dist_remark,
								'documentType' => $docType,
								'docType2' => $docType2,
								'docType3' => $docType3,
								'docType4' => $docType4,
								'modified_date_time' => $dt
							);
			// echo "<pre>"; print_r($data);
			if($count > 0)
			{
					$this->load->model('fault_parts_model');
					  $status = $this->fault_parts_model->update_decision($data, $case);
					  $serial_find = $this->fault_parts_model->get_serial_case($case);
					//echo "<pre>"; print_r($serial_find[0]->callProductSerialNo);  die;
					$sr_no = $serial_find[0]->callProductSerialNo;
					if(!empty($sr_no))
					{
						if($status == "1")
						{
							 $stage = $this->fault_parts_model->upd_dec($status, $case);
							 echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Great !! Please fill the replacement details..')
									window.location.href='".base_url()."index.php/fault_parts/replace_battery/$caseENC';
									</SCRIPT>");  	
						}
						
						
						else if($status == "2")
						{  
							$stage = $this->fault_parts_model->upd_dec($status, $case);			
							$blck = $this->fault_parts_model->block_serial($sr_no, $user);			
							echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Thank you !! Your Case has successfully closed with REJECTED.. ')
									window.location.href='".base_url()."index.php/fault_parts/service_cases_battery/$caseENC/UmVqZWN0ZWQ=';
									</SCRIPT>");    
							
						}
						else
						{    
							$stage = $this->fault_parts_model->upd_dec($status, $case); 
							echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Thank you !! Your Case has successfully closed with SAMEBACK.. ')
									window.location.href='".base_url()."index.php/fault_parts/service_cases_battery/$caseENC/U2FtZWJhY2s=';
									</SCRIPT>");    
							
						}
					}
					else
					{
						echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Sorry ! Some Problem Occurred.  Please contact administrator. ')
									window.location.href='".base_url()."index.php/service_cases/battery_complaints';
									</SCRIPT>");  
					}
			}
			else
			{
				$case = base64_encode($case);
				echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Document not uploaded properly. Please upload it again.')
									window.location.href='".base_url()."index.php/fault_parts/decision/$case';
									</SCRIPT>"); 
			}	
			
		}
	}
	function service_cases()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/success');
	}
        function service_cases_battery($case, $decision)
	{
	    $get_session_data = $this->session->userdata('logged_in');
            $js  =  base64_decode($case);
            $dec =  base64_decode($decision);
           // die;
            $data['arr'] = array(
                                    'jobsheet' => $js,
                                    'decision' => $dec
                                );
           // print_r($data['arr']); die("Hell");
	    $this->load->view('service_franchise/success_battery_view', $data);
	}
        function submit_final_decision()
        {
            $get_session_data = $this->session->userdata('logged_in');
            $user = $get_session_data['user_uuid'];
           // print_r($_POST); die("lsdngkjsd");
            $se_approval_c = trim(stripslashes($_POST['se_approval_c']));
            $caseId = trim(stripslashes($_POST['caseId']));
            $se_estd_date = trim(stripslashes($_POST['se_estd_date']));
            $decision = trim(stripslashes($_POST['decision']));
            $sf_decision = trim(stripslashes($_POST['sf_decision']));
            $se_remark_c = trim(stripslashes($_POST['se_remark_c']));
            $sub_dec = array(
                                'callStatus' => 2,
                                'callStatus' => 13,
                                'callModifiedBy' => $user,
                                'callSubmittedBy' => $user,
                                'closedByOn' => date('Y-m-d H:i:s'),
                                'se_approval' => $se_approval_c,
                                'se_decision' => $sf_decision,
                                'se_remarks' => $se_remark_c,
                                'se_estd_visit_date' => $se_estd_date
                             );
            $decision = $this->cc_customer_model->update_final_dec($sub_dec, $caseId);
            echo $decision;
            die;
        }
 
        function replacement_sf_decision()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/success');
	}
	function cases()
	{
			//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		//echo "hii"; die;
		$this->load->model('service_case_model');
		$data['res'] = $this->service_case_model->get_call_details();
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/service_mycases_view', $data);
	}
	
	function case_report()
	{
		$get_session_data = $this->session->userdata('logged_in');
		
		$caseId= trim($_POST['caseId']);
		$user = $get_session_data['user_uuid'];
		//$sess_id = $this->session->set_userdata('case', $caseId);
		//sss$case = $this->session->userdata('case');
		//echo "case report controller";
		$data  = array(
		         'caseId' => $caseId,
		         'submittedByDist' => $user,
		         'physicalCond' => $_POST['dremark'],
		         'condOfTerminal' => $_POST['brand'],
		         'electrolyteColor' => $_POST['sevisit'],
		         'ocv' => $_POST['ocv_bef'],
		         'loadTest' => $_POST['load_test'],
		         'hrd' => $_POST['hrd_bef'],
		         'backup' => $_POST['backup'],
		         'bCCAr' => $_POST['bcca_rated'],
		         'bCCAm' => $_POST['b_measured'],
		         'bVerdict' => $_POST['bverdict'],
		         'bef_c1' => $_POST['c1'],
		         'bef_c2' => $_POST['c2'],
		         'bef_c3' => $_POST['c3'],
		         'bef_c4' => $_POST['c4'],
		         'bef_c5' => $_POST['c5'],
		         'bef_c6' => $_POST['c6'],
		         'chrgeStDate' => $_POST['chrgStartDate'],
		         'chrgStTime' => $_POST['chrgStartTime'],
		         'ChrgeEndDate' => $_POST['chrgEndDate'],
		         'ChrgeEndTime' => $_POST['chrgEndTime'],
		         'chrgingAmpere' => $_POST['chrgEnd'],
		         'TOC' => $_POST['chrgEnd'],
		         'aCCAr' => $_POST['acca_rated'],
		         'aCCAm' => $_POST['ameasured'],
		         'aVerdict' => $_POST['averdict'],
		         'aft_c1' => $_POST['afterC1'],
		         'aft_c2' => $_POST['afterC2'],
		         'aft_c3' => $_POST['afterC3'],
		         'aft_c4' => $_POST['afterC4'],
		         'aft_c5' => $_POST['afterC5'],
		         'aft_c6' => $_POST['afterC6'],
		         'aft_loadtest' => $_POST['load_test'],
		         'aft_hrd' => $_POST['hrd_aftr'],
		         'vehicle_avail' => $_POST['purpose'],
		         'min_out_curr' => $_POST['v_min_out'],
		         'max_out_curr' => $_POST['v_max_out'],
		         'lower_cutoff_volt' => $_POST['v_l_cutoff'],
		         'upper_cutoff_volt' => $_POST['v_u_cutoff'],
		         'sys_leakage' => $_POST['sys_leak'],
		         'vd_cranking' => $_POST['v_drop'],
		         'cd_cranking' => $_POST['c_drawn'],
		         'erick_chrg_out' => $_POST['erick_chr_out'],
		         'vehicl_chrg_remark' => $_POST['chrgRemrk']
				 
		);
		$callStatus = "1";
		$callStage = "10";
		//echo "<pre>"; print_r($data); die;
		$dt = date("d-m-Y H:i:s");
		$arr = array(
		              'callStatus' => $callStatus,
		              'callStage' => $callStage,
		              'modified_date_time' => $dt
					  
					);
		
		
			$this->load->model('fault_parts_model');
			$dat = $this->fault_parts_model->case_report($data);
			//echo $dat; die;
			
			if($dat > 0)
			{
				$sta = $this->fault_parts_model->update_status_stage($arr, $caseId);
				if($sta>0)
				{
					$caseId = base64_encode($caseId);
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Test Report has successfully submitted.')
					window.location.href='decision/$caseId';
					</SCRIPT>");
				}
				else
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry, Test Report not submitted.. Please try again later !!')
							window.location.href='case_report';
							</SCRIPT>");
				}	
			}
			else
			{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry, Some problem occured.. Please try again later !!')
							window.location.href='case_report';
							</SCRIPT>");
			}
	}
	
	function decision($caseId)
	{
            $case =  base64_decode($caseId);
            $get_session_data = $this->session->userdata('logged_in');
            //$case = $this->session->userdata('case');
	    $user       =   $get_session_data['user_uuid'];
	    $user_type  =   $get_session_data['user_type'];
		//echo "<pre>"; print_r($_SESSION); die("hii");
            $data['res']= array(
                                    'case' => $case,
                                    'user' => $user
                                );
            if($user_type == 'SF')
            {
               
                $sym = $this->fault_parts_model->get_cas_status($case);
               // echo "<pre>"; print_r($sym); die; 
		if($sym > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The decision has already submitted.. Go Back..')
					window.location.href='".base_url()."service_cases';
					</SCRIPT>");
		}
		else
		{
			$data['sms'] = $this->cc_customer_model->get_product_wrrnt($case);
			$data['symptom'] = $this->fault_parts_model->get_symptom_code();
			//$data['br'] = $this->fault_parts_model->get_battery_report($case);
                      //  echo "<pre>"; print_r($data['sms']); 
                       // echo "<pre>"; print_r($data['symptom']); die;
			$this->load->view('services/decision_view', $data);
		}
            }
            else
            {
                $this->load->model('fault_parts_model');
                $sym = $this->fault_parts_model->get_cas_status($case);
		if($sym > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The decision has already submitted.. Go Back..')
					window.location.href='".base_url()."service_cases';
					</SCRIPT>");
		}
		else
		{
			$data['sms'] = $this->fault_parts_model->get_product_wrrnt($case);
			$data['symptom'] = $this->fault_parts_model->get_symptom_code();
			//$data['br'] = $this->fault_parts_model->get_battery_report($case);
			$this->load->view('services/decision_view', $data);
		}
            }    
	}
	
   function error()
   {
	   //echo "jindsnkjsdfa"; die;
		$this->load->view('services/error_view', $data);
   }
   function replace_battery_settlement()
   {
	   $mode = $_POST['row_dim'];
	   $others = $_POST['others'];
	   $date = $_POST['date'];
	   $case = $_POST['case'];
	   
	   if(empty($mode) || $mode == 2)
	   {
		   $mode = "Settle Through Other Mode";
	   }
	   /*
	   echo "Saurav : ".$case; 
	   echo "Mode : ".$mode; 
	   echo "Remarks : ".$others; 
	   echo "Date : ".$date; 
	   die;
	    if(empty($case))
	   {
		   echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some error occured.. Please try again..')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
	   } */
	   $get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		
		$this->load->model('fault_parts_model');
		$dat['set'] = $this->fault_parts_model->update_replace_settle($mode, $others, $date, $case, $user);
		//print_r($dat['set']);
		/* if(!empty($dat['sp']))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Success ! Your case has been successfully replaced with ')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
		} */
   }

	
	function replace_battery($caseId)
	{
            $get_session_data = $this->session->userdata('logged_in');
            $user      =    $get_session_data['user_uuid'];
            $user_type =    $get_session_data['user_type'];
           //  echo $user_type; die;
               if($user_type == 'SF')
               {
                    $case =  base64_decode($caseId);
                    //echo $case; die;
                    
                    //$dat = $this->fault_parts_model->update_replace_mod($rep, $case);
                    $dat = $this->fault_parts_model->get_comp_date($case);
                    //print_r($dat); exit;
                    $cmpDt=date('d-m-Y',strtotime($dat[0]->callRegDate));

                    $data['s'] = array('case'=>$case,'cmpdt'=>$cmpDt);
                    $this->load->view('service_franchise/replace_battery_sf_view', $data);
               }
               else
               {
                   $case =  base64_decode($caseId);
		//echo $case; die;
                    //$dat = $this->fault_parts_model->update_replace_mod($rep, $case);
                    $dat = $this->fault_parts_model->get_comp_date($case);
                    //print_r($dat); exit;
                    $cmpDt=date('d-m-Y',strtotime($dat[0]->callRegDate));

                    $data['s'] = array('case'=>$case,'cmpdt'=>$cmpDt);
                    $this->load->view('services/replace_battery_view', $data);
                    $this->load->view('service_franchise/replace_battery_sf_view', $data);
               }
		
	}
	
	/* function battery_replace_details()
	{
		$get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];
		 $case = $this->input->post('case');
	
	$battery = array(
						'submittedByDist' => $user,
						'caseId' => $case,
						'custId' => $this->input->post('custId'),
						'custName' => $this->input->post('custname'),
						'jobId' => $this->input->post('jobId'),
						'dist_settle_type' => $this->input->post('setime'),
						'dist_remark' => $this->input->post('contact'),
						'brand' => $this->input->post('brand'),
						'productType' => $this->input->post('prType'),
						'productSegment' => $this->input->post('prseg'),
						'model' => $this->input->post('modelcode'),
						'modelSerialNo' => $this->input->post('modser'),
						'primarySaleDate' => $this->input->post('prsale'),
						'primarySaleInvoice' => $this->input->post('pr_sale_invoic'),
						'aging_days' => $this->input->post('aging'),
						'date_of_replace' => $this->input->post('dor'),
						'comany_settlement' => $this->input->post('comset')
					);
		$mode = "Settled_through_battery_replacement";			
		$date = date("Y-m-d");
		$rep = array('replacementMode'=>$mode, 'dateOfReplacementRemark'=> $date);
		//ech
		//echo "<pre>"; print_r($battery); die;
		//echo "hii"; die;
		$this->load->model('fault_parts_model');
		//$dat = $this->fault_parts_model->update_replace_mod($rep, $case);
		$dat = $this->fault_parts_model->insert_replace_details($battery, $rep, $case, $user);
		$case = base64_encode($this->input->post('case'));
		if($dat > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Success !! Go ahead to next step..  ')
				window.location.href='battery_charge_report/$case';
				</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry, Some problem occured.. Please try again later !!')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
		}
		//$this->load->view('services/replace_details_view', $data);
	}
	 */
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	
	function battery_replace_details()
	{
		
	 //print_r($_POST);
	 $get_session_data = $this->session->userdata('logged_in');  
	 $user 		  =  	$get_session_data['user_uuid'];
	 $usern 		  =  	$get_session_data['username'];
	 $case 		  = 	trim($this->input->post('case'));
	 $prod_serial = 	trim($_REQUEST['prod_sr']);
	 $ageing 		= trim($this->input->post('aging'));
	 
	 // echo "Product Serial :".$prod_serial."<br/>"; // die(" - product sr");
	 // echo "User :".$user."<br/>"; // die(" - product sr");
	 // echo "Username :".$usern."<br/>"; // die(" - product sr");
	 // echo "Case :".$case."<br/>"; // die(" - product sr");
	 // echo "Age :".$ageing."<br/>";  
	 // die(" - End sr");
	 // exit;
	 // echo "Case :".$case."<br/>"; die("Case");
	 // die;
	 
	 if(empty($case))
	 {
		 $case = base64_encode($case);
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Case can't be empty..')
					window.location.href='replace_battery/$case';
					</SCRIPT>");
	 }
	 if(empty($case))
	 {
		 $case = base64_encode($case);
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Case can't be empty..')
					window.location.href='replace_battery/$case';
					</SCRIPT>");
	 }
	 if(empty($this->input->post('dor')))
	 {
		 $case = base64_encode($case);
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Replacement Date can't be empty.')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
	 }
	 
	 $this->load->model('fault_parts_model');
	 $dat = $this->fault_parts_model->check_replcaement($case, $user);
	 $agng_pr = $this->fault_parts_model->check_ageing($prod_serial); 
	 $ag_pr_vl = $agng_pr[0]->AGING_LIMIT;
	 
	 // print_r($agng_pr[0]->AGING_LIMIT); die("jsdbfjasbnghjad");
	 // $agng_pr 
	 
	 $dat1 = $dat[0]->replacementMode;
	 if(!empty($dat1))
	 {
		//echo "Helloo";  die("sdjbj");
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! The decision is already given. Go back..')
				window.location.href='".base_url()."index.php/service_cases';
				</SCRIPT>");
	 }
	 else
	 {
		 if($ag_pr_vl > $ageing)
		 {
			 			// die("bhai");
			 $dat11 = $this->fault_parts_model->check_replcaement_ag($case, $user);
			//print_r($dat11); die("Hello tbl replace");
			 $dat11 = $dat11[0]->modelSerialNo;
			 if(!empty($dat11))
			 {
				 echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! The decision is already given. Go back..')
						window.location.href='".base_url()."service_cases';
						</SCRIPT>");
			 }
			 else
			 {
			 /* $qry = $this->fault_parts_model->check_blocking($prod_serial);
			 print_r($qry); die(" - Check blocking"); 
			 
			 if($qry > 0)
			 {
				 
			 } 
		     else
			 {
				 */
			 function getGUID()
			 {
						if (function_exists('com_create_guid'))
						{
							return com_create_guid();
						}
						else
						{
							mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$uuid = substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12);
							return $uuid;
						}
			}
			$guid = getGUID();
			function getSecGUID()
			 {
						if (function_exists('com_create_guid'))
						{
							return com_create_guid();
						}
						else
						{
							mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$uuid = substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12);
							return $uuid;
						}
			}
			$guid_sec = getSecGUID();
					
			
					$battery = array(
										'submittedByDist' => $user,
										'caseId' => $case,
										'custId' => $this->input->post('custId'),
										'custName' => $this->input->post('custname'),
										'jobId' => $this->input->post('jobId'),
										'dist_settle_type' => $this->input->post('setime'),
										'dist_remark' => $this->input->post('contact'),
										'brand' => $this->input->post('brand'),
										'productType' => $this->input->post('prType'),
										'productSegment' => $this->input->post('prseg'),
										'model' => $this->input->post('modelcode'),
										'productSerialNo' => $prod_serial,
										'primarySaleDate' => $this->input->post('prsale'),
										'primarySaleInvoice' => $this->input->post('pr_sale_invoic'),
										'aging_days' => $ageing,
										'date_of_replace' => $this->input->post('dor'),
										'comany_settlement' => $this->input->post('comset')
									);
									
							
									
						//$mode = "Settled_through_battery_replacement";			
						$mode = "Settled_through_battery_replacement";			
						$date = date("Y-m-d");
						$dt = date("d-m-Y H:i:s");
						$rep = array('replacementMode'=>$mode, 'dateOfReplacementRemark'=> $date, 'modified_date_time'=>$dt);
						//ech
						//echo "<pre>"; print_r($battery); die;
						//echo "hii"; die;
						
						
							  // echo "Not Blocked"; die(" Yes");
							  
							 $serial = $this->fault_parts_model->get_serial_data($case, $user);
							 
							  // print_r($serial); die("Gotia");
							 
							 $complaint_date = $serial[0]->callRegDate;
							  $callAssetId = $serial[0]->callAssetId;             
							  $us = $serial[0]->callCreatedBy;
							  $consumerId = $serial[0]->callCustId;
							  $old_sr = $serial[0]->callProductSerialNo;
							  $model = $serial[0]->model;
						  
					
						$newAsset = $this->fault_parts_model->get_new_asset($old_sr);
						//  echo "<pre>"; print_r($newAsset); // die("Hello");
						
						
						
						
						  $productSegment_n 	= $newAsset[0]->productSegment;
						  $brand_n 				= $newAsset[0]->brand;
						  $batteryType_n 		= $newAsset[0]->batteryType;
						  $custPurchaseDate_n   = $newAsset[0]->custPurchaseDate;
						  $warrantyStatus_n     = $newAsset[0]->warrantyStatus;
						  $warrantyStartDate_n  = $newAsset[0]->warrantyStartDate;
						  $warrantyEndDate_n 	= $newAsset[0]->warrantyEndDate;
						  $proRateDisc_n 		= $newAsset[0]->proRateDisc;
						  $proRateWarrStDate_n 	= $newAsset[0]->proRateWarrStDate;
						  $proRateWarrEndDate_n = $newAsset[0]->proRateWarrEndDate;
						  $dealerName_n 		= $newAsset[0]->dealerName;
						  $productAppFitment_n 	= $newAsset[0]->productAppFitment;
						  $productVehicleMake_n = $newAsset[0]->productVehicleMake;
						  $productVehicleMfg_n 	= $newAsset[0]->productVehicleMfg;
						  $vehicle_reg_n 		= $newAsset[0]->vehicle_reg;
						  $dateOfCreation_n 	= $newAsset[0]->dateOfCreation;
						  $model_n 				= $newAsset[0]->model;
						  $primary_sale_date_n 	= $newAsset[0]->primary_sale_date;
						  $invoice_no_n 		= $newAsset[0]->invoice_no;
						  $product_n 			= $newAsset[0]->product;
						  
						  
						  
/***********************************Product Category and Aos product Id****************************************/
$queries = $this->db->query("SELECT a.id AS product_id, d.id AS category_id
											FROM aos_products a
											LEFT JOIN aos_product_categories d ON a.aos_product_category_id = d.id
											WHERE a.deleted =0
											AND a.part_number = '".$this->input->post('modelcode')."'
											LIMIT 1");

						//echo $this->db->last_query();
						$row_pr = $queries->result();
						//print_r($row_pr); 
						$pr_product_id = $row_pr[0]->product_id;
						$pr_category_id = $row_pr[0]->category_id;
/*********************************New Asset INSERTION****************************************/

					$dataServiceProducts = array(
												'productGuid' => $guid,
												'consumerId' => $this->input->post('custId'),
												'userId' => $user,
												'productSerialNo' => $prod_serial,
												'batteryType' => $batteryType_n,  
												'productSegment' => $productSegment_n,  
												'Brand' => $brand_n,
												'productType' => $product_n,
												'model' => $this->input->post('modelcode'),
												'warrantyStartDate' => $warrantyStartDate_n,
												'warrantyEndDate' => $warrantyEndDate_n,
												'proRateWarrStDate' => $proRateWarrStDate_n,
												'proRateWarrEndDate' => $proRateWarrEndDate_n,
												'custPurchaseDate' => $custPurchaseDate_n,  
												'letpl_invoice_no' => $this->input->post('pr_sale_invoic'), 
												'letpl_invoice_date' => $this->input->post('prsale'), 
												'proRateDisc' => $proRateDisc_n, 
												'warrantyStatus' => $warrantyStatus_n, 
												'dealerName' => $dealerName_n,									  
												'productAppFitment' => $productAppFitment_n,									  
												'productVehicleMake' => $productVehicleMake_n,									  
												'productVehicleMfg' => $productVehicleMfg_n,									  
												'vehicle_reg' => $vehicle_reg_n,													
												'datetime' => date('d-m-Y h:i:s'),
												'dateOfCreation'=> $dateOfCreation_n		
											);	
						//echo "<pre>>"; print_r($dataServiceProducts); 
						
						//////////////////////sar_asset
						 $dataAsset = array(
										  'id' => $guid,//need to generate guid
										  'name' => $product_n,//name of asset 
										  'assigned_user_id' => "1",
										  'date_entered' => date('Y-m-d h:i:s')
										 ); 
						//////////////////////sar_asset_cstm
						
						
						
						 $dataAssetCstm = array(
											  'id_c' => $guid,//guid that will be same as guid of sar_asset
											  'product_serial_no_c' => $prod_serial,
											  'battery_type_c' => $batteryType_n,  
											  'product_segment_c' => $productSegment_n,
											  'brand_c' => $brand_n, 
											  'sap_code_c' => $this->input->post('modelcode'),
											  'warranty_start_date_c' => date('Y-m-d',strtotime($warrantyStartDate_n)),     
											  'warranty_end_date_c' => date('Y-m-d',strtotime($warrantyEndDate_n)), 
											  'proratewarr_strtdate_c' => date('Y-m-d',strtotime($proRateWarrStDate_n)), 
											  'proratewarr_enddate_c' => date('Y-m-d',strtotime($proRateWarrEndDate_n)), 
											  'purchase_date_c' => date('Y-m-d',strtotime($custPurchaseDate_n)), 
											  'letpl_invoice_no_c' => $this->input->post('pr_sale_invoic'), 
											  'letpl_invoice_date_c' =>  $this->input->post('prsale'), 
											  'pro_rate_disc_c' => $proRateDisc_n, 
											  'warranty_status_c' => $warrantyStatus_n, 
											  'application_c' => $productAppFitment_n, 
											  'manufacturer_c' => $productVehicleMake_n, 
											  'vehicle_model_c' => $productVehicleMfg_n, 
											  'vehicle_registration_no_c' => $vehicle_reg_n, 
											  'aos_products_id_c' => $pr_product_id, 
											  'aos_product_categories_id_c' => $pr_category_id, 
											  'product_type_c' => $product_n, 
											  'parent_id' => $dealerName_n, 
											  'parent_type' => "SAR_dealer", 
											  'account_id_c' => $user
											 );						
											 //////////////////////contacts_sar_asset_1_c
						 $contactAsset = array(
											  'id' =>$guid_sec,//need to generate guid
											  'contacts_sar_asset_1contacts_ida' => $this->input->post('custId'),//ifofconsumer 
											  'contacts_sar_asset_1sar_asset_idb ' => $guid //idofasset
											 );
						
														
/******************************************************************************************************************/									
								/* 	
									

						 echo "<pre>"; print_r($dataServiceProducts); 
						 echo "<pre>"; print_r($dataAsset);
						 echo "<pre>"; print_r($dataAssetCstm);
						 echo "<pre>"; print_r($contactAsset); die("Hello Checking");
						   */
						 $blck = $this->fault_parts_model->block_old_sr($old_sr, $guid, $user);
						if(empty($blck))
						{
							//echo "Blocked"; die(" Yes");
							//$finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Some Error Occured. Please contact administrator.');
							echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Sorry ! Please contact administrator. Couldn't block old serial.')
									window.location.href='replace_battery/$case';
									</SCRIPT>");
						}
						else
						{
							
							$dat = $this->fault_parts_model->insert_replace_details($battery);
							if($dat > 0)
							{ 
										
							   $replce = $this->fault_parts_model->replace_serial_old($dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset, $prod_serial, $callAssetId, $consumerId, $old_sr, $user, $case, $model);
								//print_r($replace); die("hello");
								/* $stg = array(
									   'callStatus' => 5,
									   'callStage' =>  8
								); */
								if($replce > 0)
								{
									//$stsStg = $this->fault_parts_model->update_st_stage($stg, $case);
										$case = base64_encode($case);
										echo ("<SCRIPT LANGUAGE='JavaScript'>
												window.alert('Replacement Successful !! TAT-1 Closed & SE decision pending. ')
												window.location.href='battery_charge_report/$case';
												</SCRIPT>");
								}
								else
								{
									$case = base64_encode($case);
									echo ("<SCRIPT LANGUAGE='JavaScript'>
										window.alert('Sorry, Some problem occured.. Replacement can't be done.. Please contact admin..!!')
										window.location.href='replace_battery/$case';
										</SCRIPT>");
								}
									//$this->load->view('services/replace_details_view', $data);
							}
							else
							{
								$case = base64_encode($case);
								echo ("<SCRIPT LANGUAGE='JavaScript'>
										window.alert('Replacement Unsuccessful.. Please raise support ticket or contact admin !!')
										window.location.href='replace_battery/$case';
										</SCRIPT>");
							}				
						}
				// }		
		 }
		 }
		 else
		 {
			 $case = base64_encode($case);
			  echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Aging Of Replacement Battery is ".$ageing." Days, It Should Be <=".$ag_pr_vl." Days. Please Contact Service Engineer/ Customer Care.')
					window.location.href='".base_url()."index.php/fault_parts/replace_battery/".$case."';
					</SCRIPT>");
		 }
		 

	}
}
	
	function battry_serial_verify()
	{
		$get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];
		 $usern = $get_session_data['username'];
		 $prod_serial = $this->input->post('prod_serial');
		 $caseId = $this->input->post('caseId');
		//echo $caseId; exit;
			$this->load->model('fault_parts_model');
			$chk = $this->fault_parts_model->check_status($caseId);
			//print_r($chk); die("hii");
			$var = $chk[0]->replacementMode;
			if($var == 1)
			{
				$finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Replacement already given.');
			}
			else
			{
				 function getGUID(){
				if (function_exists('com_create_guid')){
					return com_create_guid();
				}else{
					mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
					$charid = strtoupper(md5(uniqid(rand(), true)));
					$hyphen = chr(45);// "-"
					$uuid = substr($charid, 0, 8).$hyphen
						.substr($charid, 8, 4).$hyphen
						.substr($charid,12, 4).$hyphen
						.substr($charid,16, 4).$hyphen
						.substr($charid,20,12);
					return $uuid;
				}
			}
			$guid = getGUID();
			
				//echo "Hello";
				  
				  $serial = $this->fault_parts_model->get_serial_info($prod_serial);
				  
				  $exist = $this->fault_parts_model->get_product_exist($prod_serial);
				  $isDist = $this->fault_parts_model->get_dist_serial($prod_serial, $usern);
				  $isBlock = $this->fault_parts_model->get_claim_serBlock($caseId);
				 // print_r($isBlock);  die("verify serial");
				 $isBLK = $isBlock[0]->callProductSerialNo;
				 
				if(!empty($isBLK))
				{	
                       $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! The Claim Serial no. is already blocked. Please contact administrator..');  
				}
				else
				{
					if($exist == 0)
					{	
						   $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Search not successful. Please contact administrator..');  
					}
					else
					{
						if($isDist == 0)
						{
							$finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Please Raise Issue or contact service admin.');
						}
						else
						{
							  if($serial > 0)
							  {
								  $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! The Serial Number is already assigned to a customer. Please try another.');
							  }
							  else
							  {
								 
								 //echo $serial;  die("verify serial"); // In this loop, Serial No. doesnt exist in any product creation. So, can go ahead.
								 
								 
								 $qry = $this->fault_parts_model->check_blocking($prod_serial);
								 $chkng = $qry[0]->name;
								 if(empty($chkng))
								 {
									  $serial = $this->fault_parts_model->get_serial_data($caseId, $user);
									//print_r($serial); die("Gotia");
									  $complaint_date = $serial[0]->callRegDate;
									  $callAssetId = $serial[0]->callAssetId;
									  $us = $serial[0]->callCreatedBy;
									  $consumerId = $serial[0]->callCustId;
									  $old_sr = $serial[0]->callProductSerialNo;
									  
									  
									  //echo $complaint_date; die(" - callDate");
									  $asd = array(
													'complaint_date' => $complaint_date,
													'callAssetId' => $callAssetId,
													'distguid' => $us,
													'consumerId' => $consumerId,
													'serialNo' => $old_sr
												   ); 
												  
									//echo "<pre>"; print_r($asd);
									$old_sr = trim(stripslashes($old_sr));								
									$prod_serial = trim(stripslashes($prod_serial));								
									 $oprt = $this->fault_parts_model->get_product_type($old_sr);  // oprt = old product type
									 $nprt = $this->fault_parts_model->get_new_product($prod_serial);   // nprt = new product type
									/*  print_r($oprt);
									 print_r($nprt);
									 die("jhbsdj"); */
									 $opr_type = trim($oprt[0]->product);
									 $npr_type = trim($nprt[0]->product);
									 // die("Serial Wait");
									 // echo "OPR".$opr_type."<br/>";
									   // echo "NPR".$npr_type."<br/>";
									  // die; 
									  
									  if($opr_type == "CAR & UV" && $npr_type =="3-W")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "3-W" && $npr_type =="CAR & UV")
									  {
										  //ECHO "HII"; die;
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "CV" && $npr_type =="CAR & UV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else if($opr_type == "CAR & UV" && $npr_type =="CV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "TRACTOR" && $npr_type =="CAR & UV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "CAR & UV" && $npr_type =="TRACTOR")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else if($opr_type == "CAR & UV" && $npr_type =="CAR & UV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "TRACTOR" && $npr_type =="CV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "CV" && $npr_type =="TRACTOR")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else if($opr_type == $npr_type)
									  {
										  // echo "kjdbjkdzbkjzbbbbbbbbbbbbbbbbbbbbjdjjsdkjbfdkjfkjdbkbdskhfbdkjfbds";
										  //$finalOutcome = array('status'=>'success', 'msg'=>'Great ! The replacement battery is of same type. Go ahead');
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										
											$finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else
									  {
										  
										  $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Please replace battery with same product type.');
									  }
									  
									
									 /*  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										$data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										
										print_r($data);
										print_r($data1);
										die("Hendfjx");
										$finalOutcome=array_merge($data,$data1); */
								 }
								 else
								 {
									  $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! The battery serial number is already blocked.');
								 }
							  }						
						}		// print_r($data); die;
					}	
			    }
			}
			echo json_encode($finalOutcome);
	}
	
        function battry_serial_verify_sf()
	{
		$get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];
		 $usern = $get_session_data['username'];
		 $prod_serial = $this->input->post('prod_serial');
		 $caseId = $this->input->post('caseId');
		//echo $caseId; exit;
			$this->load->model('fault_parts_model');
			$chk = $this->fault_parts_model->check_status($caseId);
			//print_r($chk); die("hii");
			$var = $chk[0]->replacementMode;
			if($var == 1)
			{
				$finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Replacement already given.');
			}
			else
			{
				 function getGUID(){
				if (function_exists('com_create_guid')){
					return com_create_guid();
				}else{
					mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
					$charid = strtoupper(md5(uniqid(rand(), true)));
					$hyphen = chr(45);// "-"
					$uuid = substr($charid, 0, 8).$hyphen
						.substr($charid, 8, 4).$hyphen
						.substr($charid,12, 4).$hyphen
						.substr($charid,16, 4).$hyphen
						.substr($charid,20,12);
					return $uuid;
				}
			}
			$guid = getGUID();
			
				//echo "Hello";
				  
				  $serial = $this->fault_parts_model->get_serial_info($prod_serial);
				  
				  //$exist = $this->fault_parts_model->get_product_exist($prod_serial);
				  $isDist = $this->fault_parts_model->get_dist_serial($prod_serial, $usern);
				  $callCentre = $this->cc_customer_model->verify_serial_no($prod_serial);
                                  
                                  //print_r($callCentre); die("akdnfa");
				  $isBlock = $this->fault_parts_model->get_claim_serBlock($caseId);
				 // print_r($isBlock);  die("verify serial");
				 $isBLK = $isBlock[0]->callProductSerialNo;
				 
				if(!empty($isBLK))
				{	
                                    $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! The Claim Serial no. is already blocked. Please contact administrator..');  
				}
				else
				{
                                        if($callCentre == 'x')
                                        {
                                            $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Search not successful. Please contact administrator (SF)..');  
                                        }
					
					else
					{
						
							  if($serial > 0)
							  {
								  $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! The Serial Number is already assigned to a customer. Please try another.');
							  }
							  else
							  {
								 
								 //echo $serial;  die("verify serial"); // In this loop, Serial No. doesnt exist in any product creation. So, can go ahead.
								 
								 
								 $qry = $this->fault_parts_model->check_blocking($prod_serial);
								 $chkng = $qry[0]->name;
								 if(empty($chkng))
								 {
									  $serial = $this->fault_parts_model->get_serial_data($caseId);
									//print_r($serial); //die("Gotia");
									  $complaint_date   =   $serial[0]->callRegDate;
									  $callAssetId      =   $serial[0]->callAssetId;
									  $us               =   $serial[0]->callCreatedBy;
									  $consumerId       =   $serial[0]->callCustId;
									  $old_sr           =   $serial[0]->callProductSerialNo;
									  
									  
									  //echo $complaint_date; die(" - callDate");
									  $asd = array(
													'complaint_date' => $complaint_date,
													'callAssetId' => $callAssetId,
													'distguid' => $us,
													'consumerId' => $consumerId,
													'serialNo' => $old_sr
												   ); 
												  
									$old_sr = trim(stripslashes($old_sr));	
									//echo "<pre>"; print_r($old_sr);	die("sdg6");						
									$prod_serial = trim(stripslashes($prod_serial));								
									 $oprt = $this->cc_customer_model->get_product_type($old_sr);  // oprt = old product type
									 $nprt = $this->cc_customer_model->get_new_product($prod_serial);   // nprt = new product type
									 /*
                                                                         print_r($oprt);
									 print_r($nprt);
									 die("jhbsdj"); */
									 $opr_type = trim($oprt[0]->product);
									 $npr_type = trim($nprt[0]->product);
									 // die("Serial Wait");
									 // echo "OPR".$opr_type."<br/>";
									   // echo "NPR".$npr_type."<br/>";
									  // die; 
									  
									  if($opr_type == "CAR & UV" && $npr_type =="3-W")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "3-W" && $npr_type =="CAR & UV")
									  {
										  //ECHO "HII"; die;
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "CV" && $npr_type =="CAR & UV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else if($opr_type == "CAR & UV" && $npr_type =="CV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "TRACTOR" && $npr_type =="CAR & UV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "CAR & UV" && $npr_type =="TRACTOR")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else if($opr_type == "CAR & UV" && $npr_type =="CAR & UV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "TRACTOR" && $npr_type =="CV")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									   else if($opr_type == "CV" && $npr_type =="TRACTOR")
									  {
										  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										  $finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else if($opr_type == $npr_type)
									  {
										  // echo "kjdbjkdzbkjzbbbbbbbbbbbbbbbbbbbbjdjjsdkjbfdkjfkjdbkbdskhfbdkjfbds";
										  //$finalOutcome = array('status'=>'success', 'msg'=>'Great ! The replacement battery is of same type. Go ahead');
										  $data = $this->cc_customer_model->serial_verify_data($prod_serial);
										  $data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										  //$cmpln_date = $this->fault_parts_model->get_cmpln_date($caseId);
										
											$finalOutcome = array('status'=>'sucesss', 'data'=>array_merge($data,$data1));
									  }
									  else
									  {
										  
										  $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! Please replace battery with same product type.');
									  }
									  
									
									 /*  $data = $this->fault_parts_model->serial_verify_data($prod_serial);
										$data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
										
										print_r($data);
										print_r($data1);
										die("Hendfjx");
										$finalOutcome=array_merge($data,$data1); */
								 }
								 else
								 {
									  $finalOutcome = array('status'=>'error', 'msg'=>'Sorry ! The battery serial number is already blocked.');
								 }
							  }						
								// print_r($data); die;
					}	
                                }
			}
			echo json_encode($finalOutcome);
	}
	
        function battery_charge_report($case)
	{
		$case = base64_decode($case);
		$get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];	
		$this->load->model('fault_parts_model');
		
		
		
		$data['ss'] = $this->fault_parts_model->getfault_detail($case,$user);
		if(!empty($data['ss']))
		{
			 $us  = $data['ss'][0]->submittedByDist;
			 $caseId  = $data['ss'][0]->caseId;
			 $consumerId  = $data['ss'][0]->custId;
			 
			$data['sas']  = $this->fault_parts_model->update_wrrnty_details($caseId);
			$data['cust'] = $this->fault_parts_model->get_cust_data($consumerId);
			$data['asset']= $this->fault_parts_model->get_cust_prod($consumerId);
			$data['test'] = $this->fault_parts_model->getTestData($us, $caseId);
			
			$this->load->view('services/replace_details_view', $data);
		}
		else
		{
			$case = base64_encode($case);
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some network problem occured. Please try again..')
				window.location.href='".base_url()."fault_parts/replace_battery/$case';
				</SCRIPT>");
		}
		
	}
	function replace_approve()
	{
		$this->load->view('services/replace_approve_view');
	}
	
	
	function replace_battery_new()
	{
		 $get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];
	     $case = $this->input->post('case');
	     $prod_serial = $this->input->post('prod_serial');
	   if(empty($case))
	   {
		    echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Network Problem Occurred. Try Sgain')
				window.location.href='".base_url()."service_cases';
				</SCRIPT>");
	   }
	   if(empty($prod_serial))
	   {
		    echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Please enter product serial number !!')
				window.location.href='".base_url()."service_cases';
				</SCRIPT>");
	   }
	   /*  echo $case."<br/>";
	    echo $prod_serial;
	    die("Hello"); */
	   
	  $this->load->model('fault_parts_model');
	  $chk = $this->fault_parts_model->check_status($case);
	  // print_r($chk[0]->replacementMode); die("sdkjnjdskn");
	  $var = $chk[0]->replacementMode;
	 // echo "Hiill ".$var; die;
	  if($var > 0)
	  {
		  $case = base64_encode($case);
		  echo $case; die("greater");
		  
		  echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! The Replacement is already given. Check progress on Complaint Dashboard.')
				window.location.href='".base_url()."serive_cases';
				</SCRIPT>");
	  }
	  else
	  {
		  echo $case; die("less");
		  function getGUID(){
				if (function_exists('com_create_guid')){
					return com_create_guid();
				}else{
					mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
					$charid = strtoupper(md5(uniqid(rand(), true)));
					$hyphen = chr(45);// "-"
					$uuid = substr($charid, 0, 8).$hyphen
						.substr($charid, 8, 4).$hyphen
						.substr($charid,12, 4).$hyphen
						.substr($charid,16, 4).$hyphen
						.substr($charid,20,12);
					return $uuid;
				}
			}
			$guid = getGUID();
			
		  //echo "Hello";
		  
		  $serial = $this->fault_parts_model->get_serial_data($case);
		  
		 /*  $complaint_date = $serial[0]->callRegDate;
		  $callAssetId = $serial[0]->callAssetId;
		  $us = $serial[0]->submittedByDist;
		  $consumerId = $serial[0]->custId;
		  
		  $asd = array(
		      'complaint_date' => $complaint_date,
		      'callAssetId' => $callAssetId,
		      'distName' => $us,
		      'consumerId' => $consumerId
			  
		  ); */
		  //echo "<pre>"; print_r($asd)
		  die;
		  
		 //print_r($serial[0]->callProductSerialNo);

		  $old_sr = $serial[0]->callProductSerialNo;
		  $serial = $this->fault_parts_model->block_old_sr($old_sr, $guid, $user);
		  if($serial > 0)
		  {
			  $chkNewBlck = $this->fault_parts_model->check_new_serial($prod_serial);
			  echo "Chk new : ".$chkNewBlck; 
			  if($chkNewBlck < 1)
			  {
				  //$data['asset']= $this->fault_parts_model->get_cust_prod($consumerId);
					$pr_st = $this->fault_parts_model->replace_new_sr($old_sr, $case, $prod_serial);
					if($pr_st > 0)
					{
						$data['sas']  = $this->fault_parts_model->update_wrrnty_details($caseId);
						$data['cust'] = $this->fault_parts_model->get_cust_data($consumerId);
						$data['asset']= $this->fault_parts_model->get_cust_prod($consumerId);
						$data['test'] = $this->fault_parts_model->getTestData($us, $caseId);
						$this->load->view('services/replaced_battery_view', $data);
					}
					else
					{
						$case = base64_encode($case);
				  echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry, Please contact admin. Replacement of serial number failed.')
						window.location.href='".base_url()."fault_parts/replace_battery/$case';
						</SCRIPT>");
					}
					
			  }
			  else
			  {
				  $case = base64_encode($case);
				  echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry, Please contact admin. Can't replace the new serial number.')
						window.location.href='".base_url()."fault_parts/replace_battery/$case';
						</SCRIPT>");
			  }
		  }
		  else
		  {
			  $case = base64_encode($case);
			  echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Some Problem Occurred. Please try again later.')
					window.location.href='".base_url()."fault_parts/replace_battery/$case';
					</SCRIPT>");
		  }
	  }
	     
	}

	
}


?>
