<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require_once('application/libraries/vendor/autoload.php'); 
// reference the Dompdf namespace
//use Dompdf\Dompdf;
class Cc_customer extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('cc_customer_model');
			$this->load->model('call_center_model');
			
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('sap_api/credit_note_view');
	}
	
	function add_new_consumer()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$user_name = $get_session_data['user_name'];
		$user_type = $get_session_data['user_type'];
		//echo "Custmer submit"; die;
		//print_r($get_session_data['user_id']); die("Hello");
		$fname 		= stripslashes(trim($_REQUEST['fname']));
		$lname 		= stripslashes(trim($_REQUEST['lname']));
		$category 	= stripslashes(trim($_REQUEST['category']));
		$priority 	= stripslashes(trim($_REQUEST['priority']));
		$mobile_no 	= stripslashes(trim($_REQUEST['mobile_no']));
		$alt_mobile = stripslashes(trim($_REQUEST['alt_mobile']));
		$std_code = stripslashes(trim($_REQUEST['std_code']));
		
		$std_no = stripslashes(trim($_REQUEST['std_number']));
		
		$landline_number = $std_code.'-'.$std_no;
		$email_id 	= stripslashes(trim($_REQUEST['email_id']));
		$address 	= stripslashes(trim($_REQUEST['address']));
		$landmark 	= stripslashes(trim($_REQUEST['landmark']));
		$pincode 	= stripslashes(trim($_REQUEST['pincode']));
		$area 		= stripslashes(trim($_REQUEST['area']));
		$city 		= stripslashes(trim($_REQUEST['city']));
		$district 	= stripslashes(trim($_REQUEST['district']));
		$state 		= stripslashes(trim($_REQUEST['state']));
		$consumer_code = stripslashes(trim($_REQUEST['consumer_code']));
		
				function getGUID()
				{
					if (function_exists('com_create_guid')){
						return com_create_guid();
					}else{
						mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
		
				function getsecGUID()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
				function getsecGUIDth()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
				function getsecGUIDfr()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
		
		$tid   = getGUID();
		$secid = getsecGUID();
		$thrid = getsecGUIDth();
		$forid = getsecGUIDfr();
		 	
		//echo "Custmer submit2"; 
		
		$ser_cust= array(
							'fname' 			=> 		$fname,	
							'lname' 			=> 		$lname,	
							'mobile' 			=> 		$mobile_no,	
							'cust_type' 		=> 		$category,	
							'cust_priority_type'=> 		$priority,	
							'alt_mobile'		=> 		$alt_mobile,	
							'email' 			=> 		$email_id,	
							'address' 			=> 		$address,	
							'landmark' 			=> 		$landmark,	
							'pincode' 			=> 		$pincode,	
							'area' 				=> 		$area,	
							'city' 				=> 		$city,	
							'district' 			=> 		$district,	
							'createdBy' 		=> 		$get_session_data['user_uuid'],	
							'consumer_code' 	=> 		$consumer_code,	
							'cust_code' 		=> 		$tid,	
							'cust_status' 		=> 		1
						);
						
		
		//echo "Custmer submit22"; 				
		$cc_cust= array(
							'fname' 				=> 		$fname,	
							'lname' 				=> 		$lname,	
							'mobile' 				=> 		$mobile_no,	
							'cust_type' 			=> 		$category,	
							'cust_priority_type'	=> 		$priority,	
							'alt_mobile'			=> 		$alt_mobile,	
							'std_no'			    => 		$landline_number,	
							'email' 				=> 		$email_id,	
							'address' 				=> 		$address,	
							'landmark' 				=> 		$landmark,	
							'pincode' 				=> 		$pincode,	
							'area' 					=> 		$area,	
							'city' 					=> 		$city,	
							'state' 				=> 		$state,	
							'district' 				=> 		$district,	
							'consumer_code' 		=> 		$consumer_code,
							'cust_code' 			=> 			$tid,
							'createdBy' 	    	=> 		$get_session_data['user_uuid'],	
							'created_type' 	    	=> 		$get_session_data['user_type'],	
							'cust_status' 			=> 		1,	
							'customer_activated_on' => 		date('d-m-Y'),	
							'date_modified' 		=> 		date('Y-m-d H:i:s')	
						);		
          // echo "Custmer submit222";                                                                         
		$dat = 	  array(
		                 'id' => $tid,
		                 'first_name' => $fname,
		                 'last_name' => $lname,
		                 'date_entered' => date('Y-m-d H:i:s'),
		                 'date_modified' => date('Y-m-d H:i:s'),
		                 'phone_mobile' => $mobile_no,
		                 'phone_work' => $alt_mobile,
		                 'primary_address_state' => $state,
		                 'primary_address_city' => $city,
		                 'primary_address_postalcode' => $pincode
		    			);
						
		$d_email   = array(
							'id' => $thrid,
							'email_address' => $email_id,
							'date_created' => date('Y-m-d H:i:s'),
							'date_modified' => date('Y-m-d H:i:s')
						); 	
          				
		$data_cstm = array(
							'id_c' => $tid,
							'primary_address_area_c' => $area,
							'primary_address_1_c' => $address,
							'primary_address_district_c' => $landmark
						   );
							   
		$em_rel =   array(
							'id' => $secid,
							'bean_id' => $tid,
							'bean_module' => 'Contacts',
							'email_address_id' => $thrid
						);
		
	 /* 
		echo"<pre>"; print_r($ser_cust); 
		echo"<pre>"; print_r($cc_cust); 
		echo"<pre>"; print_r($dat); 
		echo"<pre>"; print_r($d_email); 
		echo"<pre>"; print_r($data_cstm); 
		echo"<pre>"; print_r($em_rel); 
		die;
		echo "Custmer submit3"; 
		  */
		$data = $this->cc_customer_model->add_new_consumer($ser_cust, $cc_cust, $dat, $data_cstm, $d_email, $em_rel);
		echo base64_encode($data);
		//echo "Custmer submit33"; die;
		//echo "<pre>";   print_r($fname); die("another"); 
	}
	
	public function updateConsumerInfo($mobile=null)
	{
		if($mobile==null)
		{
			redirect('mod_call_center');
		}
		
		$data['res'] = $this->call_center_model->get_states_names();
		$data['info'] = $this->cc_customer_model->consumerInfo(base64_decode($mobile));
		//echo '<pre>';print_r($data['info']);exit;
		$this->load->view('includes/top.php');
		$this->load->view('call_center/customer/update_info', $data);
		$this->load->view('includes/sidebar.php');
		$this->load->view('includes/call_center_asset.php');
		
	}
	
	public function saveAddress()
	{
		//echo '<pre>';print_r($_POST);
		
		$data = array(
			'mobile' => $this->input->post('mobile'),
			'cust_id' => $this->input->post('cust_id'),
			'alt_mobile' => $this->input->post('alt_mobile'),
			'email' => $this->input->post('email_id'),
			'address' => $this->input->post('address'),
			'landmark' => $this->input->post('landmark'),
			'state' => $this->input->post('state'),
			'city' => $this->input->post('city'),
			'district' => $this->input->post('district'),
			'area' => $this->input->post('area'),
			'pincode' => $this->input->post('pincode'),
			'createdBy' => $this->session->userdata['logged_in']['user_uuid']
		);
		$res = $this->cc_customer_model->updateAddress($data);
		if($res){
			$this->session->set_flashdata('message','Updated successfully. Thank You...!');
		}else{
			$this->session->set_flashdata('message','NOt updated please try agian');
		}
		redirect('mod_call_center');
	}
}
?>

