<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fault_parts extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
		//$this->load->library('../models/service_case_model');
		//  $this->load->model('models/service_case_model');
		 // $this->load->model('service_case_model');
           
    }
    
	function view_challan()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('fault_parts_model');
		$data['res'] = $this->fault_parts_model->get_challan_list();
		 $data['pname'] = $this->fault_parts_model->products_detail();
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/view_challan_details', $data);
	}
	
	
	public function index($id)
	{
		$id = base64_decode($id);
		// echo $id; die; 
		
		/* $sql = "SELECT * FROM `tblservicecasetestreport` WHERE `caseId";
		//echo $sql; die;
		$r = $this->db->query($sql)->result();
		if($r[0]->caLL_case_status != '0')
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The test Report is already submitted. Go Back..')
					window.location.href='http://13.228.144.245/livguard_dms/index.php/service_cases';
					</SCRIPT>");
		} */
		
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('fault_parts_model');
		$data['res'] = $this->fault_parts_model->get_parts_details($id);
		 $dealerCode=$data['res'][0]->dealerName;
		 $data['bak'] = $this->fault_parts_model->get_dealerName($dealerCode);
		$this->load->view('services/fault_parts_view', $data);
		
	}
	function batery_report()
	{
		//  echo "Hello jsbdjfbsdjbfjdsbfjhd"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$case = trim($_POST['caseId']);
		$arr = array(
					  'physicalCond' => trim($_POST['dremark']),
					  'condOfTerminal' => trim($_POST['brand']),
					  'electrolyteColor' => trim($_POST['sevisit']),
					  'ocv' => trim($_POST['ocv_bef']),
					  'caseId' => $case
				);
		//print_r($arr); die;
		$this->load->model('fault_parts_model');
		$dat = $this->fault_parts_model->insert_batt_report($arr);
		//print_r($dat)
		if($dat > 0)
		{
			echo $case;
		}
		else
		{
			return '';
		}
	}
	
	function fault_close_case()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$case  	  	  = 	trim(stripslashes($_POST['case']));
		$user  	      = 	trim(stripslashes($_POST['user']));
		$symptom      = 	trim(stripslashes($_POST['symptom']));
		//$defect     = 	trim(stripslashes($_POST['defect']));
		$action  	  = 	trim(stripslashes($_POST['action']));
		$dist_remark  = 	trim(stripslashes($_POST['remark']));
		$docType  	  = 	trim(stripslashes($_POST['docType']));
		$docType2  	  = 	trim(stripslashes($_POST['docType2']));
		$docType3  	  = 	trim(stripslashes($_POST['docType3']));
		$docType4  	  = 	trim(stripslashes($_POST['docType4']));
		
		if(move_uploaded_file($_FILES['docUpload']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload']['name'])){}
		if(move_uploaded_file($_FILES['docUpload2']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload2']['name'])){}
		if(move_uploaded_file($_FILES['docUpload3']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload3']['name'])){}
		if(move_uploaded_file($_FILES['docUpload4']['tmp_name'], './docUploadsCases/' . $_FILES['docUpload4']['name'])){}
		
		if(empty($dist_remark) || $dist_remark=='0') {
			$dist_remark = '11';
		}
		$query 	= $this->db->query("SELECT * FROM `tbl_service_dist_remarks` WHERE `distRemarkId`='".$dist_remark."'");
		$row 	= $query->result();
		//print_r(); die;
		//if(empty($caseid)) {  redirect('fault_parts'); }
		$remarks = $row[0]->distRemarkName;
		
		
		$sql 	= $this->db->query("SELECT * FROM `tbl_defect_symptoms` WHERE `id`='".$symptom."'");
		$rows 	= $sql->result();
		//print_r(); die;
		//if(empty($caseid)) {  redirect('fault_parts'); }
		
		$symptom = $rows[0]->Symptoms;
		$defect   = $rows[0]->Defect;
		
		/* echo "Symptom : ".$symptom."<br/>";
		echo "Defect : ".$defect."<br/>";
		die; */
		
		$data = array(
			'callModifiedBy' => $user,
			'symptomCode' => $symptom,
			'defectCode' => $defect,
			'caLL_case_status' => $action,
			'uploadDocuments' => $_FILES['docUpload']['name'],
			'uploadDocs2' => $_FILES['docUpload2']['name'],
			'uploadDocs3' => $_FILES['docUpload3']['name'],
			'uploadDocs4' => $_FILES['docUpload4']['name'],
			'distributorRemark' => $remarks,
			'documentType' => $docType,
			'docType2' => $docType2,
			'docType3' => $docType3,
			'docType4' => $docType4
		);
		//echo "<pre>"; print_r($data); die;
		//die("123");
		$this->load->model('fault_parts_model');
		$data = $this->fault_parts_model->update_decision($data, $case);
		//echo "<pre>"; print_r($data); die;
		$case   =  base64_encode($case);
		//if(empty($data)) { redirect('error'); }
		
		//echo "Hello : ".$data; die;
		
		if($data == "1")
		{
             echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Great !! Please fill the replacement details..')
					window.location.href='replace_battery/$case';
					</SCRIPT>");  	
		}
		else if($data == "2")
		{    
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Thank you !! Your Case has successfully closed with REJECTED.. ')
					window.location.href='service_cases';
					</SCRIPT>");    
			
		}
		else
		{    
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Thank you !! Your Case has successfully closed with SAMEBACK.. ')
					window.location.href='service_cases';
					</SCRIPT>");    
			
		}
		
	}
	function service_cases()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/success');
	}
	
	function case_report()
	{
		$get_session_data = $this->session->userdata('logged_in');
		//echo "<pre>"; print_r($get_session_data); die;
		$caseId=$_POST['caseId'];
		$user = $get_session_data['user_uuid'];
		//$sess_id = $this->session->set_userdata('case', $caseId);
		//sss$case = $this->session->userdata('case');
		//echo "case report controller";
		$data  = array(
		         'caseId' => $caseId,
		         'submittedByDist' => $user,
		         'physicalCond' => $_POST['dremark'],
		         'condOfTerminal' => $_POST['brand'],
		         'electrolyteColor' => $_POST['sevisit'],
		         'ocv' => $_POST['ocv_bef'],
		         'loadTest' => $_POST['load_test'],
		         'hrd' => $_POST['hrd_bef'],
		         'backup' => $_POST['backup'],
		         'bCCAr' => $_POST['bcca_rated'],
		         'bCCAm' => $_POST['b_measured'],
		         'bVerdict' => $_POST['bverdict'],
		         'bef_c1' => $_POST['c1'],
		         'bef_c2' => $_POST['c2'],
		         'bef_c3' => $_POST['c3'],
		         'bef_c4' => $_POST['c4'],
		         'bef_c5' => $_POST['c5'],
		         'bef_c6' => $_POST['c6'],
		         'chrgeStDate' => $_POST['chrgStartDate'],
		         'chrgStTime' => $_POST['chrgStartTime'],
		         'ChrgeEndDate' => $_POST['chrgEndDate'],
		         'ChrgeEndTime' => $_POST['chrgEndTime'],
		         'chrgingAmpere' => $_POST['chrgEnd'],
		         'TOC' => $_POST['chrgEnd'],
		         'aCCAr' => $_POST['acca_rated'],
		         'aCCAm' => $_POST['ameasured'],
		         'aVerdict' => $_POST['averdict'],
		         'aft_c1' => $_POST['afterC1'],
		         'aft_c2' => $_POST['afterC2'],
		         'aft_c3' => $_POST['afterC3'],
		         'aft_c4' => $_POST['afterC4'],
		         'aft_c5' => $_POST['afterC5'],
		         'aft_c6' => $_POST['afterC6'],
		         'aft_loadtest' => $_POST['load_test'],
		         'aft_hrd' => $_POST['hrd_bef'],
		         'vehicle_avail' => $_POST['purpose'],
		         'min_out_curr' => $_POST['v_min_out'],
		         'max_out_curr' => $_POST['v_max_out'],
		         'lower_cutoff_volt' => $_POST['v_l_cutoff'],
		         'upper_cutoff_volt' => $_POST['v_u_cutoff'],
		         'sys_leakage' => $_POST['sys_leak'],
		         'vd_cranking' => $_POST['v_drop'],
		         'cd_cranking' => $_POST['c_drawn'],
		         'erick_chrg_out' => $_POST['erick_chr_out'],
		         'vehicl_chrg_remark' => $_POST['chrgRemrk']
				 
		);
		//echo "<pre>"; print_r($data); die;
		
			$this->load->model('fault_parts_model');
			$dat = $this->fault_parts_model->case_report($data);
			//echo $dat; die;
			$caseId = base64_encode($caseId);
			if($dat > 0)
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Test Report has successfully submitted.')
				window.location.href='decision/$caseId';
				</SCRIPT>");
			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry, Some problem occured.. Please try again later !!')
				window.location.href='case_report';
				</SCRIPT>");
			}
	}
	
	function decision($caseId)
	{
		$case =  base64_decode($caseId);
		$get_session_data = $this->session->userdata('logged_in');
		//$case = $this->session->userdata('case');
	    $user = $get_session_data['user_uuid'];
		//echo "<pre>"; print_r($_SESSION); die("hii");
		$data['res'] = array(
							 'case' => $case,
							 'user' => $user
							);
		
		
		$sql = "SELECT caLL_case_status FROM `tbl_service_calls` where call_id='".$case."'";
		//echo $sql; die;
		$r = $this->db->query($sql)->result();
		if($r[0]->caLL_case_status != '0')
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The decision has already submitted.. Go Back..')
					window.location.href='http://13.228.144.245/livguard_dms/index.php/service_cases';
					</SCRIPT>");
		}
		//die("hii");
		
		/* $query = $this->db->query("SELECT caLL_case_status FROM `tbl_service_calls` where call_id='".$case."'");
		//echo $this->db->last_query();
		$row = $this->db->result();
		$row = $this->db->get();
		$ret = $query->row(); */
		/* print_r($ret);
		print_r($ret->callStage); */
		//print_r($row->callStage);
		
		/*  die("hello");
		
		if($de != 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The decision has already submitted.. Go Back..')
					window.location.href='http://13.228.144.245/livguard_dms/index.php/service_cases';
					</SCRIPT>");

		}	
            */
         		
		$this->load->model('fault_parts_model');
		$data['symptom'] = $this->fault_parts_model->get_symptom_code();
		$this->load->view('services/decision_view', $data);
	}
	
   function error()
   {
	   //echo "jindsnkjsdfa"; die;
		$this->load->view('services/error_view', $data);
   }
   function replace_battery_settlement()
   {
	   $mode = $_POST['row_dim'];
	   $others = $_POST['others'];
	   $date = $_POST['date'];
	   $case = $_POST['case'];
	   
	   if(empty($mode) || $mode == 2)
	   {
		   $mode = "Settle Through Other Mode";
	   }
	   /*
	   echo "Saurav : ".$case; 
	   echo "Mode : ".$mode; 
	   echo "Remarks : ".$others; 
	   echo "Date : ".$date; 
	   die;
	    if(empty($case))
	   {
		   echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some error occured.. Please try again..')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
	   } */
	   $get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		
		$this->load->model('fault_parts_model');
		$dat['set'] = $this->fault_parts_model->update_replace_settle($mode, $others, $date, $case, $user);
		print_r($dat['set']);
		/* if(!empty($dat['sp']))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Success ! Your case has been successfully replaced with ')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
		} */
   }

	
	function replace_battery($caseId)
	{
		$case =  base64_decode($caseId);
		//echo $case; die;
		$this->load->model('fault_parts_model');
		//$dat = $this->fault_parts_model->update_replace_mod($rep, $case);
		$dat = $this->fault_parts_model->get_comp_date($case);
		$cmpDt=date('d-m-Y',strtotime($dat[0]->callRegDate));
		$data['s'] = array('case'=>$case,'cmpdt'=>$cmpDt);
		$this->load->view('services/replace_battery_view', $data);
	}
	
	function battery_replace_details()
	{
		$get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];
		 $case = $this->input->post('case');
	
	$battery = array(
						'submittedByDist' => $user,
						'caseId' => $case,
						'custId' => $this->input->post('custId'),
						'custName' => $this->input->post('custname'),
						'jobId' => $this->input->post('jobId'),
						'dist_settle_type' => $this->input->post('setime'),
						'dist_remark' => $this->input->post('contact'),
						'brand' => $this->input->post('brand'),
						'productType' => $this->input->post('prType'),
						'productSegment' => $this->input->post('prseg'),
						'model' => $this->input->post('modelcode'),
						'modelSerialNo' => $this->input->post('modser'),
						'primarySaleDate' => $this->input->post('prsale'),
						'primarySaleInvoice' => $this->input->post('pr_sale_invoic'),
						'aging_days' => $this->input->post('aging'),
						'date_of_replace' => $this->input->post('dor'),
						'comany_settlement' => $this->input->post('comset')
					);
		$mode = "Settled_through_battery_replacement";			
		$date = date("Y-m-d");
		$rep = array('replacementMode'=>$mode, 'dateOfReplacementRemark'=> $date);
		//ech
		//echo "<pre>"; print_r($battery); die;
		//echo "hii"; die;
		$this->load->model('fault_parts_model');
		//$dat = $this->fault_parts_model->update_replace_mod($rep, $case);
		$dat = $this->fault_parts_model->insert_replace_details($battery, $rep, $case, $user);
		$case = base64_encode($this->input->post('case'));
		if($dat > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Success !! Go ahead to next step..  ')
				window.location.href='battery_charge_report/$case';
				</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry, Some problem occured.. Please try again later !!')
				window.location.href='replace_battery/$case';
				</SCRIPT>");
		}
		//$this->load->view('services/replace_details_view', $data);
	}
	function battry_serial_verify()
	{
		 $prod_serial = $this->input->post('prod_serial');
		$caseId = $this->input->post('caseId');
		//echo $caseId; exit;
			$this->load->model('fault_parts_model');
			 $data = $this->fault_parts_model->serial_verify_data($prod_serial);
			 //$data0 = $this->fault_parts_model->serial_verify_status($prod_serial);
			$data1 = $this->fault_parts_model->serial_verify_data_case($caseId);
			$finalOutcome=array_merge($data,$data1);
			echo json_encode($finalOutcome); 
		// print_r($data); die;
	}
	function battery_charge_report($case)
	{
		$case = base64_decode($case);
		$get_session_data = $this->session->userdata('logged_in');  
		 $user = $get_session_data['user_uuid'];	
		$this->load->model('fault_parts_model');
		
		
		
		$data['ss'] = $this->fault_parts_model->getfault_detail($case,$user);
		if(!empty($data['ss']))
		{
			 $us  = $data['ss'][0]->submittedByDist;
			 $caseId  = $data['ss'][0]->caseId;
			 $consumerId  = $data['ss'][0]->custId;
			 
			$data['sas']  = $this->fault_parts_model->update_wrrnty_details($caseId);
			$data['cust'] = $this->fault_parts_model->get_cust_data($consumerId);
			$data['asset']= $this->fault_parts_model->get_cust_prod($consumerId);
			$data['test'] = $this->fault_parts_model->getTestData($us, $caseId);
			
			$this->load->view('services/replace_details_view', $data);
		}
		else
		{
			$case = base64_encode($case);
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some network problem occured. Please try again..')
				window.location.href='http://13.228.144.245/livguard_dms/index.php/fault_parts/replace_battery/$case';
				</SCRIPT>");
		}
		
	}
	function replace_approve()
	{
		$this->load->view('services/replace_approve_view');
	}
}


?>
