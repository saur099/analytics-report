<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cc_product extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
		if(!$this->session->userdata['logged_in']['username']) 
			return redirect('login', 'refresh'); 
		$this->load->model('cc_product_model');
		$this->load->model('cc_customer_model');
        $this->load->model('service_case_model');
        $this->load->model('call_center_model');
		$this->load->helper('sendsms_helper');
		$this->load->library('form_validation');
    }
	
	public function add_more_prod()
	{
		$var = stripslashes(trim($_POST['ck_string']));
		$var_ex = explode('~', $var);
		$js_id = time().rand(0,9);
		
		foreach($var_ex as $d) {
			$proData = $this->cc_product_model->find_prod_data($d);
			$newProd=array(
				'cust_id' 				=> 		$proData[0]->cust_id,
				'job_sheet' 			=> 		"JS-".date('Ymdhis').rand(0,99),
				'js_case_id' 			=> 		$js_id,
				'product_type'  		=> 		$proData[0]->product_type,
				'product_type_id'     	=> 		$proData[0]->product_type_id,
				'product_sub_type_id' 	=> 		$proData[0]->product_sub_type_id,
				'quantity' 	          	=> 		$proData[0]->quantity,
				'purchase_date' 		=> 		$proData[0]->purchase_date,
				'warranty_start_date' 	=> 		$proData[0]->warranty_start_date,
				'warranty_end_date' 	=> 		$proData[0]->warranty_end_date,
				'warranty_status' 		=> 		$proData[0]->warranty_status,
				'product_sap_code' 		=> 		$proData[0]->product_sap_code,
				'asset_serial_no' 		=> 		$proData[0]->asset_serial_no,
				'dealer_id' 	   		=> 		$proData[0]->dealer_id,
				'js_status' 	   		=> 		$jsStatus,
				'js_final_closure' 		=> 		'Open',
				'created_on' 			=> 		date('Y-m-d H:i:s'),
				'created_by' 			=> 		$proData[0]->created_by,
				'user_type' 			=> 		$proData[0]->user_type,
				'repeat_claim' 			=> 		($proData[0]->repeat_claim )+1,
			);
			$newProdStatus = $this->cc_product_model->insert_new_product($newProd);
		}
		
		if($newProdStatus > 0) {
			$responseSt = base64_encode($js_id);
		}
		$consData = $this->cc_customer_model->getConsumerDetailForProd($proData[0]->cust_id);	
			
		if(!empty($consData)) {
			$pinCode = $consData[0]->pincode;
			$assignResult = $this->cc_product_model->assignJobSheet($pincode, base64_decode($js_id), $product_type);	
			$responseSt = base64_encode($js_id);
		}
		else {
			$responseSt = '';
		}
		echo $responseSt; die;
	}
	
	public function product($id = null)
	{
		if(empty($id)) {
			redirect('mod_call_center');exit;
		}
		$data['cust_info'] = $this->cc_product_model->getCustomerInfo(base64_decode($id));
		$this->load->view('includes/top.php');
		$this->load->view('call_center/product/create_product', $data);
		$this->load->view('includes/sidebar.php');
		//$this->load->view('includes/call_center_asset.php');
	}
	
	public function get_product()
	{
		$type = trim($this->input->post('prd_type'));
		switch ($type) {
			case 1: //STABILIZER
				$product_types = "'LIVGUARD STABILIZER ML','LIVGUARD STABILIZER REF','LIVGUARD STABILIZER AC','LIVGUARD STABILIZER TV'";
				break;
			case 2: //INVERTER
				$product_types = "'IB','INV'";
				break;
			case 3: //BATTERY
				$product_types = "'CV','2-W','3-W','CAR & UV','TRACTOR'";
				break;
			case 4: //INVERTER+BATTERY
				$product_types = "'IB','INV','CV','2-W','3-W','CAR & UV','TRACTOR'";
				break;
			case 5: //SOLAR
				$product_types = "'LIVGUARD SOLAR STREET LIGHT','SOLAR CHARGE CONTROLLER','LIVFAST SOLAR PANEL','SOLAR BATTERY','LIVFAST SOLAR STREET LIGHT','LIVGUARD SOLAR PANEL','SOLAR SMU','SOLAR UPS'";
				break;
			case 6: //SOLAR+INVERTER+BATTERY
				$product_types = "'LIVGUARD SOLAR STREET LIGHT','SOLAR CHARGE CONTROLLER','LIVFAST SOLAR PANEL','SOLAR BATTERY','LIVFAST SOLAR STREET LIGHT','LIVGUARD SOLAR PANEL','SOLAR SMU','SOLAR UPS','IB','INV','CV','2-W','3-W','CAR & UV','TRACTOR'";
				break;
			case 7:
				$product_types = "'other'";
				break;
			default:
				$product_types = null;
		}
		if(!empty($product_types)) {
			$option = $this->cc_product_model->get_category($product_types);
			echo '<option value="">Select</option>';
			for($i=0;$i<count($option);$i++) {
				echo '<option value="'.$option[$i]->id.'">'.$option[$i]->name.'</option>';
			}
			exit;
		}
		echo '<option>None</option>';exit;
	}
	
	public function get_productname()
	{
		$cate_id = trim($this->input->post('cate_id'));
		$option = $this->cc_product_model->get_product($cate_id);
		if(!empty($option)) {
			echo '<option value="">Select</option>';
			for($i=0;$i<count($option);$i++) {
				echo '<option value="'.$option[$i]->id.'" part="'.$option[$i]->part_number.'" wmonth="'.$option[$i]->warranty_c.'">'.$option[$i]->name.'</option>';
			}
			exit;
		}
		echo '<option>None</option>';exit;
	 }
	
	public function saveJobSheet()
	{
		if($this->input->post()) {
			$cust_id 	= $this->input->post('cust_id');
			$js_case_id = time().rand(0,9);
			for($i=0;$i<count($this->input->post('prd_cate'));$i++) {
				$date_now = date("d-m-Y");

				$wed = str_replace('/','-',$this->input->post('wed')[$i]);
				if ($date_now < $wed) {
						echo $warranty_status = 'IW';
					}else{
						echo $warranty_status = 'OW';
				}
				$qunty = $this->input->post('quantity')[$i];
				for($q=0;$q<$qunty;$q++) {
					$job_sheet = 'JS-'.date("Ymdhis").rand(00,99);
					$data = array(
						'cust_id'				=> $cust_id,
						'job_sheet' 			=> $job_sheet,
						'js_case_id' 			=> $js_case_id,
						'product_type' 			=> $this->input->post('product_type')[$i],
						'product_type_id' 		=> $this->input->post('prd_cate')[$i],
						'product_sub_type_id' 	=> $this->input->post('product')[$i],
						'quantity' 				=> '1',
						'purchase_date' 		=> $this->input->post('dop')[$i],
						'warranty_start_date' 	=> $this->input->post('wsd')[$i],
						'warranty_end_date' 	=> $wed,
						'warranty_status' 		=> $warranty_status,
						'product_sap_code' 		=> $this->input->post('prd_sap')[$i],
						'asset_serial_no' 		=> $this->input->post('asset_no')[$i],
						'dealer_id' 			=> $this->input->post('dealer')[$i],
						'created_by'			=> $this->session->userdata['logged_in']['user_uuid'],
						'user_type'				=> $this->session->userdata['logged_in']['user_type'],
						'user_id'				=> $this->session->userdata['logged_in']['user_id']
					);
					$this->cc_product_model->save_jobsheet($data);
				}
			}
			redirect('cc_product/complaint/'.base64_encode($js_case_id));
		}
	}
	
	public function updateJobsheet()
	{
		//echo '<pre>';print_r($_POST);exit;
		$documentarr = array();
		if(!empty($_FILES['document']['name'])){
			$targetDir = "complain_docs/";
			$allowTypes = array('jpg','png','jpeg','gif');
    		foreach($_FILES['document']['name'] as $key=>$val){
				if($_FILES["document"]["tmp_name"][$key])
				{
					$fileName = time().basename($_FILES['document']['name'][$key]);
					$targetFilePath = $targetDir . $fileName;
					$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
					if(in_array($fileType, $allowTypes)){
						if(move_uploaded_file($_FILES["document"]["tmp_name"][$key], $targetFilePath)){
							$documentarr[$key] = $fileName;
						}
					}
				}
			}
		}

		$cc_product_id 	= base64_decode($this->input->post('cc_product_id'));
		
		if($this->input->post('complain_type')!='Repair')
		{
			$js_update = array(
				'js_status' 		=> 'Reschedule',
				'rescheduled_date' 	=> $this->input->post('rescheduled_date'),
				'complain_type' 	=> 'Non productive',
				'rescheduled_reson' => $this->input->post('complain_type')
			);

			$this->db->where('cc_product_id',$cc_product_id); 
			$this->db->update('tbl_cc_products', $js_update);
			//$this->callCRMClosure($documentarr);
			redirect('mod_call_center/complain_module');
		}

		$date_now 		= date("d-m-Y");
		$wed 			= $this->input->post('wed');
		if($wed > $date_now && $this->input->post('warr_void')=='No'){
			$warranty_status = 'IW';
		} else {
			$warranty_status = 'OW';
		}

		$js_status = "Closed";
		$complain_type = 'Productive';
		if(count($this->input->post('symptoms'))>0)
		{
			for($i=0;$i<count($this->input->post('symptoms'));$i++)
			{
				if($this->input->post('symptoms')[$i]=='Part Pending')
				{
					$js_status 		= "Part Pending";
					$complain_type 	= 'Non Productive';
				}
				if($this->input->post('symptoms')[$i]=='Part Replacement Pending')
				{
					$js_status 		= "Part Replacement Pending";
					$complain_type 	= 'Non Productive';
				}
				$sdrdata = array(
					'josheet_status' 	=> $js_status,
					'jobsheet_no' 		=> $this->input->post('job_sheet'),
					'complain_no' 		=> $this->input->post('cc_complain_no'),
					'cust_id' 			=> $this->input->post('cust_id'),
					'symptoms' 			=> $this->input->post('symptoms')[$i],
					'defects' 			=> $this->input->post('defects')[$i],
					'repaire' 			=> explode("|", $this->input->post('repaire')[$i])[0],
					'product' 			=> $this->input->post('product')[$i],
					'sap_code' 			=> $this->input->post('sap_code')[$i],
					'serial_no' 		=> $this->input->post('serial_no')[$i],
					'bom_qty' 			=> $this->input->post('bom_qty')[$i],
					'comsumed_qty' 		=> $this->input->post('consume_qty')[$i],
					'eng_stock' 		=> $this->input->post('eng_stock')[$i],
					'return_product' 	=> $this->input->post('return_prod')[$i],
					'ret_prod_ser_no' 	=> $this->input->post('retrun_serial_no')[$i],
					'created_by' 		=> $this->session->userdata['logged_in']['user_id']
				);
				$this->cc_product_model->saveSDR($sdrdata);
			}

			$updatedata = array (
                'happycode' 			=> $this->input->post('happycode'),
                'purchase_date' 		=> $this->input->post('dop'),
                'warranty_start_date'   => $this->input->post('dop'),
                'warranty_end_date'     => $wed,
                'warranty_status' 		=> $warranty_status,
                'warranty_avoid' 		=> $this->input->post('warr_void'),
                'product_sap_code' 		=> $this->input->post('psc'),
                'asset_serial_no' 		=> $this->input->post('asn'),
                'product_type_id' 		=> $this->input->post('json_product_type_id'),
                'product_sub_type_id' 	=> $this->input->post('json_product_sub_type_id'),
                'js_status' 			=> $js_status,
                'dealer_id' 			=> $this->input->post('dealer'),
                'payment_mode' 			=> $this->input->post('mode_of_payment_c'),
                'bank_name' 			=> $this->input->post('bank_name_c'),
                'cheq_ref_no' 			=> $this->input->post('checque_no_c'),
                'csr_number' 			=> $this->input->post('csr_number_c'),
				'distName' 				=> $this->input->post('distName'),
				'complain_type' 		=> $complain_type,
				'rescheduled_reson' 	=> $this->input->post('complain_type'),
                'js_modified_by' 		=> $this->session->userdata['logged_in']['user_id'],
                'js_modified_date' 		=> date("Y-m-d h:i:s"),
                'js_end_date' 			=> date("Y-m-d h:i:s"),
                'js_closed_by' 			=> $this->session->userdata['logged_in']['user_id']
        	);
			
			if(!empty($documentarr[0]))
				$updatedata['purchage_invoice'] = $documentarr[0];
			if(!empty($documentarr[1]))
				$updatedata['warranty_card'] = $documentarr[1];
			if(!empty($documentarr[2]))
				$updatedata['product_img1'] = $documentarr[2];
			if(!empty($documentarr[3]))
				$updatedata['product_img2'] = $documentarr[3];
			if(!empty($documentarr[4]))
				$updatedata['product_img3'] = $documentarr[4];
			if(!empty($documentarr[5]))
				$updatedata['csr_img1'] = $documentarr[5];
			if(!empty($documentarr[6]))
				$updatedata['csr_img2'] = $documentarr[6];
			if(!empty($documentarr[7]))
				$updatedata['csr_img3'] = $documentarr[7];


			$res = $this->cc_product_model->updateJobSheet($cc_product_id, $updatedata);
			if($js_status == 'Closed')
			{
				$savedata = array (
					'josheet_status' 		=> $js_status,
					'jobsheet_no' 			=> $this->input->post('job_sheet'),
					'complain_no' 			=> $this->input->post('cc_complain_no'),
					'cust_id' 				=> $this->input->post('cust_id'),
					'customer_name' 		=> $this->input->post('cust_name'),
					'customer_mobile' 		=> $this->input->post('mobile')
				);
				$this->cc_product_model->saveFeedback($savedata);
			}
			redirect('mod_call_center/feedback_module/');
			//$this->callCRMClosure($documentarr);
		}
		redirect('mod_call_center/complain_module');
	}

	public function callCRMClosure($documentarr)
	{
		$username  					= $this->session->userdata['logged_in']['username'];
		$user_uuid 					= $this->session->userdata['logged_in']['user_uuid'];
		$json_prd_type 				= $this->input->post('json_prd_type');
		$json_product_type_id 		= $this->input->post('json_product_type_id');
		$json_product_sub_type_id 	= $this->input->post('json_product_sub_type_id');
		$dop 						= $this->input->post('dop');
		$wsd 						= $this->input->post('wsd');
		$wed 						= $this->input->post('wed');
		$warrSt 					= $this->input->post('warrSt');
		$asn 						= $this->input->post('asn');
		$psc 						= $this->input->post('psc');
		$dealer 					= $this->input->post('dealer');
		$cc_complain_no 			= $this->input->post('cc_complain_no');
		$job_sheet 					= $this->input->post('job_sheet');
		$complain_type 				= $this->input->post('complain_type');
		$warr_void 					= $this->input->post('warr_void');
		$happycode 					= $this->input->post('happycode');
		$rescheduled_reason_c 		= $this->input->post('rescheduled_reason_c');
		$rescheduled_date 			= $this->input->post('rescheduled_date');
		$part_pending 				= $this->input->post('part_pending');
		$sdr_info 					= '';
		for($i=0;$i<count($this->input->post('symptoms'));$i++)
		{
			$sdr_info = $sdr_info.'{
				"symptom": "'.$this->input->post('symptoms')[$i].'",
				"defect": "'.$this->input->post('defects')[$i].'",
				"repair": "'.$this->input->post('repaire')[$i].'",
				"product_name": "'.$this->input->post('product_name')[$i].'",
				"quantity": "'.$this->input->post('quantity')[$i].'",
				"stock": "'.$this->input->post('stock')[$i].'",
				"price": "'.$this->input->post('price')[$i].'"
			},';
		}
		
		$jsonData  = '{
			"job_type": "complain_close",
				"employee_details": {
					"user_name": "'.$username.'",
					"user_id": "'.$user_uuid.'"
				},
			"asset_details": {
				"product_type": "'.$json_prd_type.'",
				"product_category": "'.$json_product_type_id.'",
				"product_sub_category": "'.$json_product_sub_type_id.'",
				"date_of_purchase": "'.$dop ? $dop : ''.'",
				"warranty_start_date": "'.$wsd ? $wsd : ''.'",
				"warranty_end_date":"'.$wed ? $wed : ''.'",
				"warranty_status":"'.$warrSt.'",
				"serial_number": "'.$asn.'",
				"sap_code":"'.$psc.'",
				"distributor_code":"'.$dealer.'"
			  },
			  "complain_details"  : {
				"complaint_number" : "'.$cc_complain_no.'",
				"job_sheet_number" : "'.$job_sheet.'",
				"complain_type" : "'.$complain_type.'",
				"is_warranty_void" : "'.$warr_void.'",
				"happy_code" : "'.$happycode.'",
				"reschedule_reason" : "'.$rescheduled_reason_c.'",
				"reschedule_date" : "'.$rescheduled_date.'",
				"part_pending" : "'.$part_pending.'",
				"sdr_details" : [ '.rtrim($sdr_info, ',').' ],
				"image_url1" : "complain_docs/'.$documentarr[0].'",
				"image_url2" : "complain_docs/'.$documentarr[1].'",
				"image_url3" : "complain_docs/'.$documentarr[2].'",
				"image_url4" : "complain_docs/'.$documentarr[3].'"
			  }
		}';
		$url = CASE_URL;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function complaint($js_id = null)
	{
		if($this->input->post()) { 
			$this->saveComplaint($js_id);
		}
		
		if(empty($js_id)) {
			redirect('mod_call_center');exit;
		}
		$data['js_info'] 	= $this->cc_product_model->getJSInfo(base64_decode($js_id));
		$data['cust_info'] 	= $this->cc_product_model->getCustomerInfo($data['js_info'][0]->cust_id);
		$data['res'] 		= $this->call_center_model->get_states_names();
		$data['voc_list'] 	= $this->cc_product_model->get_voc(base64_decode($js_id));
		$this->load->view('includes/top.php');
		$this->load->view('call_center/complaint/create_complaint', $data);
		$this->load->view('includes/sidebar.php');
		$this->load->view('includes/call_center_asset.php');
	}
        
    public function getSerialDetails()
	{
		$serial = trim(stripslashes($_POST['serial']));
		$dats = $this->cc_product_model->getSerialDetails($serial);
		print_r(json_encode($dats[0]));
	}
	
	public function saveComplaint($js_id)
	{
		//$this->callCRM($cc_complain_no);exit;
		//echo '<pre>';print_r($_POST);exit;
		$this->form_validation->set_rules('source', 'source', 'required');
		$this->form_validation->set_rules('originator', 'Originator', 'required|xss_trim');
		$this->form_validation->set_rules('call_type', 'Call Type', 'required|xss_trim');
		$this->form_validation->set_rules('sub_type', 'Sub Type', 'required|xss_trim');
		$this->form_validation->set_rules('location', 'Location', 'required|xss_trim');
		$this->form_validation->set_rules('voc', 'VOC', 'required|xss_trim');
		
		if ($this->form_validation->run() == FALSE) {
			return false;
		}
		$cc_complain_no = "C".date('Ymdhis').rand(00,99);
		$preferd_datetime = $this->input->post('preferd_date').' '.$this->input->post('preferd_time_from').'-'.$this->input->post('preferd_time_to');
		$data = array(
			'cc_complain_no' 		=> $cc_complain_no,
			'product_random_id' 	=> base64_decode($js_id),
			'cust_id' 				=> trim($this->input->post('cust_id')),
			'contact_person' 		=> trim($this->input->post('contact_person')),
			'preferd_datetime' 		=> $preferd_datetime,
			'complain_source' 		=> trim($this->input->post('source')),
			'call_originator' 		=> trim($this->input->post('originator')),
			'call_type' 			=> trim($this->input->post('call_type')),
			'complain_sub_type' 	=> trim($this->input->post('sub_type')),
			'location_type' 		=> trim($this->input->post('location')),
			'voc_type' 				=> trim($this->input->post('voc')),
			'complain_address' 		=> trim($this->input->post('address')),
			'complain_landmark' 	=> trim($this->input->post('landmark')),
			'complain_pincode' 		=> trim($this->input->post('pincode')),
			'complain_area' 		=> trim($this->input->post('area')),
			'complain_city' 		=> trim($this->input->post('city')),
			'complain_district' 	=> trim($this->input->post('district')),
			'complain_state' 		=> trim($this->input->post('state')),
			'complain_modified_by' 	=> $this->session->userdata['logged_in']['user_id'],
			'complain_created_by' 	=> $this->session->userdata['logged_in']['user_uuid']
		);
		
		$res = $this->cc_product_model->save_complaint($data);
		if($res>0) {
			$physical_varification = $this->input->post('physical_varification');
			for($i=0;$i<count($physical_varification);$i++) {
				$service_data = array(
					'callCustId'          =>   $this->input->post('cust_id'),
					'isCallCentreCall'    =>   1,
					'callCreatedBy'       =>   $this->session->userdata['logged_in']['user_uuid'],
					'isDefective'         =>   0,
					'callProductSerialNo' =>   NULL,
					'jobsheet_no'         =>   $this->input->post('physical_varification')[$i],
					'cc_call_ref_id'      =>   base64_decode($js_id),
					'callType'        	  =>   $this->input->post('call_type'),
					'callerType'          =>   $this->input->post('originator'),
					'callerMobile'        =>   $this->input->post('cmobile'),
					'callSource'          =>   $this->input->post('source'),
					'callStatus'          =>   1,
					'callStage'           =>   9,
					'call_id'             =>   $this->input->post('physical_varification')[$i],
					'cur_date_entered'    =>   date("Y-m-d  h:i:s")
				);
				$this->db->insert('tbl_service_calls',$service_data);
			}
			if($this->input->post('call_type')=="Walk-in") {
				$this->physical_verification();
			}
			$this->callCRM($cc_complain_no);
			$product_type 	= $this->input->post('product_type');
			$pincode 		= $this->input->post('pincode');
			$this->cc_product_model->assignJobSheet($pincode, base64_decode($js_id), $product_type);
			//$send_se = sendsms(9818540681,  "Dear  has confirmed secondary order of  Dealer  and ready to dispatch by courier or hand etc Qty . Team Livguard");
			redirect('cc_product/complaintView/'.base64_encode($cc_complain_no));
		}
	}
	
	public function physical_verification($js_id)
	{
		$physical_varification = $this->input->post('physical_varification');
		for($i=0;$i<count($physical_varification);$i++) {
			$data = array(
				'job_sheet' 		=> $this->input->post('physical_varification')[$i],
				'cust_fname' 		=> $this->input->post($physical_varification[$i])[0],
				'cust_lname' 		=> $this->input->post($physical_varification[$i])[1],
				'cust_mobile' 		=> $this->input->post($physical_varification[$i])[2],
				'cust_email' 		=> $this->input->post($physical_varification[$i])[3],
				'address' 			=> $this->input->post($physical_varification[$i])[4],
				'landmark' 			=> $this->input->post($physical_varification[$i])[5],
				'pincode' 			=> $this->input->post($physical_varification[$i])[6],
				'state' 			=> $this->input->post($physical_varification[$i])[7],
				'district' 			=> $this->input->post($physical_varification[$i])[8],
				'city' 				=> $this->input->post($physical_varification[$i])[9],
				'area' 				=> $this->input->post($physical_varification[$i])[10],
				'container' 		=> $this->input->post($physical_varification[$i])[11],
				'top_lid' 			=> $this->input->post($physical_varification[$i])[12],
				'plus_terminal' 	=> $this->input->post($physical_varification[$i])[13],
				'minus_terminal	' 	=> $this->input->post($physical_varification[$i])[14],
				'bat_serial_no' 	=> $this->input->post($physical_varification[$i])[15],
				'electrol_level' 	=> $this->input->post($physical_varification[$i])[16],
				'electrol_color' 	=> $this->input->post($physical_varification[$i])[17],
				'application' 		=> $this->input->post($physical_varification[$i])[18],
				'heat_seal' 		=> $this->input->post($physical_varification[$i])[19],
				'document' 			=> $this->input->post($physical_varification[$i])[20],
				'serial_no' 		=> $this->input->post($physical_varification[$i])[21],
				'dop' 				=> $this->input->post($physical_varification[$i])[22],
				'warranty_start' 	=> $this->input->post($physical_varification[$i])[23],
				'warranty_end' 		=> $this->input->post($physical_varification[$i])[24]
			);
			$this->db->insert('tbl_cc_physical_verification',$data);			
		}
	}
	
	public function callCRM($cc_complain_no)
	{
		$type = $this->input->post('jsonproduct_type');
		switch ($type) {
			case 1: //STABILIZER
				$product_types = "STABILIZER";
				break;
			case 2: //INVERTER
				$product_types = "INVERTER";
				break;
			case 3: //BATTERY
				$product_types = "BATTERY";
				break;
			
			case 5: //SOLAR
				$product_types = "SOLAR";
				break;
			
			case 7: //SOLAR
				$product_types = "Other";
				break;
			default:
				$product_types = null;
		} 
		$preferd_datetime = $this->input->post('preferd_date').' '.$this->input->post('preferd_time_from').'-'.$this->input->post('preferd_time_to');
		$username = $this->session->userdata['logged_in']['username'];
		$user_uuid = $this->session->userdata['logged_in']['user_uuid'];
		$prd_subinfo = '';
		for($i=0;$i<count($this->input->post('jsoncate_name'));$i++) {
			$prd_subinfo = $prd_subinfo.'{
				"product_category": "'.$type.'",
				"product_sub_category": "'.$this->input->post('jsoncate_name')[$i].'",
				"product_description": "'.$this->input->post('jsonsub_cate_name')[$i].'",
				"quantity": "1",
				"job_sheetno": "'.$this->input->post('jsonjob_sheet')[$i].'",
				"date_of_purchase": "'.date("Y-m-d", strtotime($this->input->post('jsonpurchase_date')[$i])).'",
				"warranty_start": "'.date("Y-m-d", strtotime($this->input->post('jsonwarranty_start_date')[$i])).'",
				"warranty_end": "'.date("Y-m-d", strtotime($this->input->post('jsonwarranty_end_date')[$i])).'",
				"warranty_status": "'.$this->input->post('jsonwarranty_status')[$i].'",
				"job_sheet_assigned_to": "'.$this->input->post('jsonassigned_to')[$i].'"
			},';
		}

		$jsonData = '{ 
			"job_type": "create_complain",
			"employee_details": {
				"user_name": "'.$username.'",
				"user_id": "'.$user_uuid.'"
			},
			"customer_details": {
				"first_name": "'.$this->input->post('jsonfname').'",
				"last_name": "'.$this->input->post('jsonlname').'",
				"consumer_category": "'.$this->input->post('jsoncust_type').'",
				"consumer_priority": "'.$this->input->post('jsoncust_priority_type').'",
				"phone_mobile": "'.$this->input->post('jsonmobile').'",
				"alt_mobile": "'.$this->input->post('jsonalt_mobile').'",
				"cli_mobile": "'.$this->input->post('jsoncli').'",
				"email_address": "'.$this->input->post('jsonemail').'",
				"consumer_code": "'.$this->input->post('jsonconsumer_code').'",
				"country": "INDIA",
				"consumer_pincode": "'.$this->input->post('jsonpincode').'",
				"consumer_state": "'.$this->input->post('jsonstate').'",
				"consumer_district": "'.$this->input->post('jsondistrict').'",
				"consumer_city": "'.$this->input->post('jsoncity').'",
				"consumer_area": "'.$this->input->post('jsonarea').'",
				"consumer_address1": "'.$this->input->post('jsonaddress').'",
				"consumer_address2": "'.$this->input->post('jsonlandmark').'"
			},
			"product_details": {
				"product_sub_details": 
					[ '.rtrim($prd_subinfo, ',').' ]
			},
			"complaint_details": {
				"product_type": "'.$type.'",
				"complaint_number": "'.$cc_complain_no.'",
				"source_of_complaint": "'.$this->input->post('source').'",
				"call_originator": "'.$this->input->post('originator').'",
				"type_of_call": "'.$this->input->post('call_type').'",
				"product_status": "'.$this->input->post('sub_type').'",
				"service_location": "'.$this->input->post('location').'",
				"preferd_datetime": "'.$preferd_datetime.'",
				"service_location_address": [{
					"location_pincode": "'.$this->input->post('pincode').'",
					"location_state": "'.$this->input->post('state').'",
					"location_district": "'.$this->input->post('district').'",
					"location_city": "'.$this->input->post('city').'",
					"location_area": "'.$this->input->post('area').'",
					"location_address1": "'.$this->input->post('address').'",
					"location_address2": "'.$this->input->post('landmark').'"
				}],
				"voc": "'.$this->input->post('voc').'"
			}
		}';
		$url = CASE_URL;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function complaintView($cc_complain_no)
	{
		$data['cust_info'] = $this->cc_product_model->getComplaintInfo(base64_decode($cc_complain_no));
		$data['js_info'] = $this->cc_product_model->getJSInfo($data['cust_info'][0]->product_random_id);
		$data['cc_complain_no'] = base64_decode($cc_complain_no);
		$this->load->view('includes/top.php');
		$this->load->view('includes/sidebar.php');
		$this->load->view('call_center/complaint/view_complaint', $data);
	}
	    
    public function jobSheetView($comp_id=null, $prod_id=null)
	{
		$data['js_info'] 	=   $this->cc_product_model->getJS(base64_decode($prod_id));
		$prod_cat   		=   $data['js_info'][0]->cate_name;
		$cust_code  		=   $data['js_info'][0]->cust_code;
		$job_sheet  		=   base64_encode($data['js_info'][0]->job_sheet);
		$cat 				= array('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV');
		if (in_array($prod_cat , $cat)) {
			echo "<SCRIPT LANGUAGE='JavaScript'>window.location.href='".base_url()."index.php/service_cases/add_product/$cust_code/$job_sheet';</SCRIPT>"; 
		}
		else 
        {
        	$prod_id = trim($_REQUEST['prod_id']);
	        $engg_det_id = trim($_REQUEST['engg_det_id']);
	        
			$data['comp_info'] 		= $this->cc_product_model->comp_info(base64_decode($comp_id));
			$data['warranty_c']     = $this->cc_product_model->getWarranty($data['js_info'][0]->product_sub_type_id);
			//$data['symptems'] 		= $this->cc_product_model->getSymptemList();
			$data['prd_list'] 	= $this->cc_product_model->prd_list($data['js_info'][0]->product_sub_type_id);
			$engg_info 				= $data['js_info'][0]->engg_reassigned_to;
			$data['sdr_list'] 		= $this->cc_product_model->get_sdr($data['js_info'][0]->job_sheet);
			$data['engg_det'] 		= $this->cc_product_model->get_engg_detail($engg_info);
			$data['js_id'] 			= base64_decode($prod_id);
			$data['comp_id'] 		= base64_decode($comp_id);
			$data['symptems'] 		= $this->cc_product_model->get_sdr_details_m($data['js_info'][0]->product_sub_type_id, $engg_info);
        
			//echo '<pre>';print_r($data);exit;
			$this->load->view('includes/top.php');
			$this->load->view('includes/sidebar.php');
			$this->load->view('call_center/product/job_sheet_view', $data);
		}
	}
    public function getProductList()
    {
    	$product_id = $this->input->post('product_id');
    	$list = $this->cc_product_model->prd_list($product_id);
    	echo json_encode($list);
    }
    public function get_sdr_details()
    {
        $prod_id = trim($_REQUEST['prod_id']);
        $engg_det_id = trim($_REQUEST['engg_det_id']);
        $getSdr = $this->cc_product_model->get_sdr_details_m($prod_id, $engg_det_id);
        echo json_encode($getSdr);
    }
        
    public function get_DR()
	{
		$sym_id = trim($this->input->post('sym_id'));
		$option = $this->cc_product_model->getDRList($sym_id);
		if(!empty($option)) {
			for($i=0;$i<count($option);$i++) {
				$data['defect_c'] =  '<option value="" >Please Select Defect</option><option value="'.$option[$i]->defect_c.'" >'.$option[$i]->defect_c.'</option>';
			}
		}
		echo json_encode($data);exit;
	}

	public function get_repair()
    {
        $defect_code = trim($this->input->post('defect_code'));
        $symptems = trim($this->input->post('symptems'));

        $option = $this->cc_product_model->getRepairDetails($defect_code, $symptems);
        if(!empty($option)) {
            for($i=0;$i<count($option);$i++) {
                $data['repair_c'] =  '<option value="">Please Select Repair</option><option value="'.$option[$i]->id.'|'.$option[$i]->type_c.'" >'.$option[$i]->repair_c.'</option>';
            }
        }
    	echo json_encode($data);exit;
    }

    public function verify_serial_no()
    {
        $asn = trim(stripslashes($_POST['asn']));
        $verRes = $this->cc_product_model->verify_serial_no($asn);
       	if($verRes == 'x')
			    $dats['error'] = "Sorry ! The product serial doesn't exist. Please try another.";
        	else if($verRes == 'y')
                $dats['error'] = "Sorry ! The product is already created. Please try another.";
        	else if($verRes =='z')
                $dats['error'] = "Sorry ! The Serial Number is blocked in our system.";
        else
				$dats = $this->cc_product_model->getSerialDetails($asn);
        print_r(json_encode($dats));
    }
  
    public function get_eng_stock()
    {
    	$prod_id 		= trim($this->input->post('prod_id'));
    	$engg_det_id 	= trim($this->input->post('engg_det_id'));
    	$result 		= $this->cc_product_model->getStock($prod_id, $engg_det_id);
    	if($result[0]->hand_stock_c > 0){
			$main_array = array("qty" => $result[0]->hand_stock_c);
		} else {
			$main_array = array("qty" => 0);
		}
		echo json_encode($main_array);exit;
    }
	
	public function printCha()
	{
		$type 							= $this->input->post('type');
		$job_id 						= $this->input->post('job_id');
		$data['js_info'] 				= $this->cc_product_model->getJS($job_id);
		$data['complain_info'] 			= $this->cc_product_model->getComplainDetails($data['js_info'][0]->js_case_id);
		$data['physical_varification'] 	= $this->cc_product_model->getPhysicalVarification($data['js_info'][0]->job_sheet);
		if(empty($data['js_info'][0]->btr_no)) {
			$btr = $data['js_info'][0]->cc_product_id;
			$this->db->where('job_sheet', $data['js_info'][0]->job_sheet);
			$this->db->update('tbl_cc_products', array('btr_no' => $btr));
		}
		echo $this->load->view('call_center/product/print_challan', $data, true);
	}
	
	public function checkStatus()
	{
		if($_POST){
			$type 				= $this->input->post('type');
			$keyword 			= $this->input->post('keyword');
			$data['js_info'] 	= $this->cc_product_model->check_status($type, $keyword);
		}
		$this->load->view('call_center/product/complaint_status', $data);
	}
	
	public function getSpare()
	{
		$prod_id 	= $this->input->post('getSpare');
		$getSdr 	= $this->cc_product_model->get_spare($prod_id);
        echo json_encode($getSdr);
	}
	
	public function allJobSheet()
	{
		$data['res'] = $this->cc_product_model->get_alljobsheet();
		$this->load->view('call_center/complaint/all_job_sheet', $data);
	}

	public function checkWarrntyMonth()
	{
		$date 	= $this->input->post('dop');
		$dop 	= date("Y-m-d", strtotime($date));
		$sub_id = $this->input->post('sub_id');
		$res = $this->db->query("SELECT warranty_c FROM `sar_warranty` LEFT join sar_warranty_cstm on id=id_c LEFT JOIN aos_products_sar_warranty_1_c as wp on wp.aos_products_sar_warranty_1sar_warranty_idb=sar_warranty.id WHERE wp.aos_products_sar_warranty_1aos_products_ida='".$sub_id."' and start_from_c <= '".$dop."'and end_to_c >= '".$dop."'")->result();
		echo $res[0]->warranty_c;
	}
	
	public function editJobSheet($job_id)
	{
		$data['job_sheet'] = $job_id;
		$this->load->view('call_center/product/editJobSheet', $data);

	}

	public function manualClosed()
	{
		$job_id = $this->input->post('job_sheet');
		$data = array(
			'status_remark' => $this->input->post('remark'),
			'complain_type' => $this->input->post('getCallTypes'),
			'accept_call' 	=> 2,
			'js_status' 	=> 'Closed'
		);
		$this->db->where('job_sheet', $job_id);
		$this->db->update('tbl_cc_products', $data);
		redirect('cc_product/allJobSheet');
	}

	public function physicalVerificationForm()
	{
		$this->input->post('job_sheet');
		echo $this->view->load('call_center/product/verification_form', '', true);
	}

	public function test()
	{
		print_r($this->session->all_userdata());
	}
}
?>