<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting('E_ALL');
class Mod_call_center extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('call_center_model');
	}

	function get_consumer_detail()
	{ 
		 $mobile 	= 	trim(stripslashes($_POST['mobile'])); 
		 $cons_code  = 	trim(stripslashes($_POST['cons_code']));
		//$mobile = '9012892128';
		/*  echo $cons_code;
		die; */

		if(!empty($mobile) || !empty($cons_code))
		{
			 $data['res'] = $this->call_center_model->get_consum_detail($mobile ,$cons_code);
			// $data['c_prod'] = 	$this->call_center_model->get_cons_product($data['res']->cust_id);
			 $this->load->view('call_center/consumer_detail_view',$data);
			// print_r($data); die;
		}
		else
		{
			return false;
		}
		
	   
		/*if($data != 0)
		{
			$result['customer_details'] 	= 	$this->call_center_model->get_customer_details($mobile);
			$result['res'] 	= 	$this->call_center_model->get_consumer_comp($data->cust_id);
			$result['c_prod'] = 	$this->call_center_model->get_cons_product($data->cust_id);
			//echo "<pre>"; print_r($c_prod); die;
			$this->load->view('call_center/consumer_detail_view',$result);
		}
		else
		{
			return false;
		}*/

	}
	function add_new_consumer()
	{
		 print_r($_REQUEST); die("Hello");
	}

	public function index()
	{

		$data['res'] = $this->call_center_model->get_states_names();
		// echo "<pre>"; print_r($data); die;
		$this->load->view('call_center/create_complain_view',$data);

		//$this->load->helper('url');
		// $this->load->view('call_center/create_complain_view');

	}

	function create_consumer()
	{
		$this->load->view('call_center/create_consumer_view');
	} 
	function create_product()
	{
		$this->load->view('call_center/create_product_view');
	}
	function create_case()
	{
		$this->load->view('call_center/create_case_view');
	}
	function create_case2()
	{
		$this->load->view('call_center/create_case_view2');
	}
	function success()
	{
		$this->load->view('call_center/success_view');
	}
	function view_jobsheet()
	{
		$this->load->view('call_center/job_sheet_view');
	}
	function js_feedback()
	{
		$this->load->view('call_center/js_feedback_view');
	}
	function complain_module($parameter)
	{
		$parameter = base64_decode($parameter);
		//echo $parameter; die;
		$user_uuid 	= 	$this->session->userdata['logged_in']['user_uuid'];
		$user_name 	= 	$this->session->userdata['logged_in']['user_name'];
		$user_id 	= 	$this->session->userdata['logged_in']['user_id'];
		$user_type 	= 	$this->session->userdata['logged_in']['user_type'];
		$SF_id 		= 	'df109cbf-9c3b-fa40-4472-5bb44c737b3e';
		$data['res'] = 	$this->call_center_model->get_complain_details($user_uuid, $complain_no, $user_type, $parameter);
		$data['eng'] = 	$this->call_center_model->get_engg_details($SF_id);
	   // print_r($data); die;
		$this->load->view('call_center/complaint/case_js_view', $data);
	}
	function job_sheet_details()
	{
		$this->load->view('call_center/job_sheet_view');
	}
	function feedback_module()
	{
		$user_id = $this->session->userdata['logged_in']['user_id'];
		$data['res'] = $this->call_center_model->get_closed_js($user_id);
		$this->load->view('call_center/feedback/js_feedback_view', $data);
	}
	public function get_districts()
	{
		 $state = $this->input->post('state');
		$district = $this->call_center_model->get_district_names($state);
		echo json_encode($district);
	}
	public function get_city()
	{
		 $district = $this->input->post('district');
		$city = $this->call_center_model->get_city_names($district);
		echo json_encode($city);
	}
	public function get_area()
	{
		 $city = $this->input->post('city');
		$area = $this->call_center_model->get_area_names($city);
		echo json_encode($area);
	}
	public function get_pincode()
	{
		 $area = $this->input->post('area');
		$pincode = $this->call_center_model->get_pincode($area);
		echo json_encode($pincode);
	// echo "<pre>"; print_r($data['district']); die;
	}

}
?>