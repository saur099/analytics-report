<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class invoice extends CI_Controller {
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('invoice_model');
    }

	
	public function index()
	{
		$invoice['res']  = $this->invoice_model->get_invoices_list();
		$this->load->view('services/invoice_list_view',$invoice);
	}
	public function challans()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid		  = $get_session_data['user_uuid'];
		$invoice['res']   = $this->invoice_model->get_dist_invoices($user_uuid);
		$this->load->view('services/invoice_dist_view',$invoice);
	}
	 public function update_complain()
	 {
		$this->load->view('services/update_complain_detail');
	 } 
	 public function update_complain_detail()
	 {
		$this->load->view('services/serialno_updated_view');
	 } 
	 
	 public function views_challan(){
  $get_session_data = $this->session->userdata('logged_in');
  $user = $get_session_data['user_uuid'];
  
  
  if($_POST['submit']){
   $data['challan_month'] = $this->input->post('pname');
   $data['product_type'] = $this->input->post('product_type');
   //echo "<pre>";
   //print_r($data); die;
   $this->load->view('services/views_challan_details_new', $data);
  }else{
  $this->load->model('fault_parts_model');
  $data['res'] = $this->fault_parts_model->get_challan_list($user);
  //echo "<pre>";
  //print_r($data); die;
  $this->load->view('services/views_challan_details_new', $data);
  }
 }
	
}
