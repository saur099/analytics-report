<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		$usern = $get_session_data['username'];
		$this->load->view('edit_profile_view');
		  
	}
	
	public function edit()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		$usern = $get_session_data['username'];
		$this->load->view('change_pass_view');
		
	}
	public function edit_profile_process()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
	    $login_type = $get_session_data['username'];
		$usern = $get_session_data['username'];
		$old_pass = $_POST['old_pass'];
		$new_pass = $_POST['new_pass'];
		$cnf_pass = $_POST['cnf_pass'];
		$date = date("Y-m-d H:i:s");
		$ip = $_SERVER['REMOTE_ADDR'];
		if(empty($old_pass))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Please enter Old Password !! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
		}
		if(empty($new_pass))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Please enter New Password !! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
		}
		if(empty($cnf_pass))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Please enter Confirm Password !! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
		}
		if($new_pass != $cnf_pass)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Your New Password and Confirm Password didn't match. !! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
		}
		$this->load->model('users');
		$data = $this->users->get_dist_username($usern, $old_pass);
		
		//print_r($data);die("Hello");
		
		if(!empty($data))
		{
			$old = $data[0]->pass;
			$times = $data[0]->changed_times;
			$email = $data[0]->email_address;
			$dist = $data[0]->distributor_name;
			$sum = $times+1;
			if($old == $old_pass)
			{
				$chng = array(
								'pass' => $new_pass,
								'chng_prev_passwrd' => $old_pass,
								'changed_datetime' => $date,
								'changed_ip' => $ip,
								'changed_times' => $sum,
								'prev_change_by' => $usern
				);
				$data = $this->users->change_old_password($usern, $chng);
				if($data > 0)
				{
					$this->load->library('email');
					$this->email->from('saurav.verma@livpure.in', 'LIVGUARD DMS');
					$this->email->to($email);
					$this->email->cc('ashish.pathak@livpure.in');
				//	$this->email->bcc('them@their-example.com');

					$this->email->subject('Password Changed Successfully.');
					$this->email->message('Dear '.$dist.', Your Password has changed successfully on DMS portal from '.$old_pass.' to '.$new_pass);
					$this->email->send();
					
					
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Success ! Your password has successfully changed.. ')
					window.location.href='".base_url()."index.php/dashboard';
					</SCRIPT>");
				}
				else
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Some Problem Occurred. Please contact Admin! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
				}
			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Your Old Password didnot match with our record!! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
			}
		
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Username / Password is Incorrect !! ')
					window.location.href='".base_url()."index.php/dashboard/change_pass';
					</SCRIPT>");
		}
		
		
		$this->load->view('change_pass_view');
		
	}
	
	public function user_profile()
	
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('users');
		$data = $this->users->get_current_user_details($get_session_data['username']);
		$this->load->view('user_profile', array('data' => $data));
	}
	
	public function user_profile_update()
	{
		 $this->load->model('users');
		 $distributor_name=$this->input->post('distributor_name');
		 $billing_address_city=$this->input->post('billing_address_city');
		 $billing_address_state=$this->input->post('billing_address_state');
		 $billing_address_postalcode=$this->input->post('billing_address_postalcode');
		 $mobile=$this->input->post('mobile');
		 $first_name=$this->input->post('first_name');
		 $last_name=$this->input->post('last_name');
		 $email_address=$this->input->post('email_address');
		 $hub_name=$this->input->post('hub_name');
		 $password=$this->input->post('password');
		 $user_id=$this->input->post('user_id');
		 $data = $this->users->update_current_user_details($distributor_name,$billing_address_city,$billing_address_state,$billing_address_postalcode,$mobile,$first_name,$last_name,$email_address,$hub_name,$password,$user_id);
		 
	}
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */