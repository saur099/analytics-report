<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cust_details extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('cust_details_model');
		$data['res'] = $this->cust_details_model->get_cust_details();
		//print_r(); die;
		$this->load->view('services/cust_details_view', $data);
	}
		
	public function create_case($id)
    {
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('cust_details_model');
		$data['res'] = $this->cust_details_model->get_cust_product($id);
		$this->load->view('services/create_cust_view', $data);
	}
	
	public function verify_consumer()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$con_id 	= 	trim($_POST['con_id']);
		$pr_serial  = 	trim($_POST['pr_serial']);
		if(empty($con_id)){ echo ("<SCRIPT LANGUAGE='JavaScript'> window.alert('Succesfully Updated')  window.location.href='index'; </SCRIPT>"); }
		if(empty($pr_serial)){ echo ("<SCRIPT LANGUAGE='JavaScript'> window.alert('Succesfully Updated')  window.location.href='index'; </SCRIPT>"); }
		
		$this->load->model('cust_details_model');
		$data = $this->cust_details_model->verify_case_model($con_id, $pr_serial);
		//print_r($data);
		if(empty($data))
		{
			//echo "Ifbajsfn";
			echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Success ! Proeed to create the case..')
    window.location.href='create_case/$con_id';
    </SCRIPT>");
		}
		
		$var = $data[0]->caLL_case_status; 
		//echo $var;
		
		//print_r($data); echo "<br/>".$data[0]->caLL_case_status; die;
		//echo "Hello".$data; die;
		//$var = $data[0]->caLL_case_status;
		//echo $var; die("Hii");
		if($var == 2 or $var == 3)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Success ! The previous cases  are closed. Please proceed..')
    window.location.href='create_case/$con_id';
    </SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('So Sorry ! You have already a pending case of this product & consumer.')
    window.location.href='index';
    </SCRIPT>");

			
		}
		
		
		//$this->load->view('services/create_cust_view', $data);
		
		
		
	}


    public function new_case()
    {	
	   $get_session_data = $this->session->userdata('logged_in');
		$consumerid = trim(stripslashes($_POST['consumerid']));
		$productserial = trim(stripslashes($_POST['productserial']));
		$callType = trim(stripslashes($_POST['callType']));
		$sevisit = trim(stripslashes($_POST['sevisit']));
		$setime = trim(stripslashes($_POST['setime']));
		$custremark = trim(stripslashes($_POST['custremark']));
		$remark = trim(stripslashes($_POST['remark']));
		$caller_type = trim(stripslashes($_POST['caller_type']));
		$mobile = trim(stripslashes($_POST['mobile']));
		$callsource = trim(stripslashes($_POST['callsource']));
		$contact = trim(stripslashes($_POST['contact']));
		$dealer = trim(stripslashes($_POST['dealer']));
		$fd = date(Ymdhis);
		 
		$caseId = "CS".$fd; 
		//echo $caseId;   die;
		$date = date("Y/m/d H:i:s");
		
		$data = array(
		                'callCustId' => $consumerid,
		                'callProductSerialNo' => $productserial,
		                'callType' => $callType,
		                'se_estd_visit_date' => $sevisit,
		                'se_estd_visit_time' => $setime,
		                'customerRemarks' => $custremark,
		                'Remarks' => $remark,
		                'callerType' => $caller_type,
		                'callerMobile' => $mobile,
		                'callSource' => $callsource,
		                'contactPerson' => $contact,
		                'dealerName' => $dealer,
						'call_id' => $caseId
		);
		
	    $this->load->model('cust_details_model');
		$data = $this->cust_details_model->add_new_case($data);
		//print_r($data); die;
		//print_r($data); die("Hello");
		
		
		if(empty($data))
		{
			
			redirect('index');                 
		}
		else
		{
			redirect('fault_parts/index/'.$data);
		}
	  
	  // print_r($data); die;
	}
}
