<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting('E_ALL');
class Order_punch extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			
    }
	
	public function indexes()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		
		
		$this->load->view('foc_view', $data);
		  
	}
	function index($fosid){
		$get_session_data = $this->session->userdata('logged_in');
	    $user = $get_session_data['username'];
	    $data['distributor_name'] = $get_session_data['distributor_name'];
		//print_r($distributor_name['distributor_name']); die;
		$this->load->model('order_punch_model');
		$fosid = base64_decode($fosid);
		
		$data['editFos'] = $this->order_punch_model->editFosManagement($user, $fosid); 
		$data['fos'] = $this->order_punch_model->get_fos($user);
		$data['state'] = $this->order_punch_model->get_state();
		$data['city'] = $this->order_punch_model->get_city();
		$this->load->view('foc_manager_view', $data);
	}
	
	public function updateFoc(){
		$fosid = base64_decode($fosid);
		$get_session_data = $this->session->userdata('logged_in');
	    $user = $get_session_data['username'];
		
		$this->load->model('order_punch_model', 'Model');
		$fossts = array(
			'fosName' => $_POST['fosn'],
			'designation' => $_POST['designation'],
			'email' => $_POST['email'],
			'state' => $_POST['state'],
			'city' => $_POST['city'],
			'mobile' => $_POST['mobile'],
			'address' => $_POST['address'],
			'remarks' => $_POST['remarks'],
		);
		$data['update'] = $this->Model->updateFocs($fosid, $user, $fossts);
		if($data['update'] == 'TRUE'){
			$this->session->set_flashdata('feedback', 'Your data has been updated. Thank You!');
		}else{
			$this->session->set_flashdata('feedback', 'Your data not updated. Please try again!');
		}
		return redirect('order_punch');
	}
	
	function dash()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $user = $get_session_data['username'];
		$this->load->view('punching_dash', $data);
	}	
	
	function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);
        return $uuid;
    }
}
	
	function punching()
	{
		//echo "Hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$data['distributor_code'] = $get_session_data['user_uuid'];
		
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$data['arr'] = array('user' => $user);
		
		//$data[''] = array('user' => $user);
		
		$this->load->model('order_punch_model');
		$this->load->model('service_case_model');
		$data['res'] = $this->order_punch_model->get_product_type();
		$data['dist'] = $this->service_case_model->get_dist_dealer($user_uuid);
		$data['fos'] = $this->order_punch_model->get_fos_dtls($user);
		// $data['prod'] = $this->order_punch_model->get_prod_model();
		//	echo "<pre>"; print_r($data['fos']);  die;
		$this->load->view('add_punch_view', $data);
	}
	
	function get_model_master(){
		$get_session_data = $this->session->userdata('logged_in');
		$distributor_code = $get_session_data['user_uuid'];
		$id = $_POST['id']; 
		// echo $id; die;
		$this->load->model('order_punch_model');
		//$data['distributor_check'] = $this->order_punch_model->get_distributor_name($distributor_code);
		$data['segment_id'] = $this->order_punch_model->get_distributor_segmentid($distributor_code);
		$segment_id = $data['segment_id'][0]->segment_id;
		$data['deal'] = $this->order_punch_model->get_product_det($id, $segment_id);
		$var = json_encode($data['deal']);
		 print_r($var); 
	}
	function get_model_data($prod_type)
	{
		//$id = $_POST['id']; 
		// echo $id; die;
		$this->load->model('order_punch_model');
		$data['deal'] = $this->order_punch_model->get_product_data($prod_type);
		print_r($data['res']);
		$this->load->view('add_punch_model_view', $data);
	}
	function insert_model()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$quantity = $_POST['quantity'];
		$moc_val = $_POST['moc_val'];
		$mod_val = $_POST['mod_val'];
		$prod_type = $_POST['prod_type'];
	    
		//echo $prod_type; die; 
		
		$this->load->model('order_punch_model');
		$data = $this->order_punch_model->insert_punch_comp($quantity, $moc_val, $mod_val, $user_uuid, $prod_type);
		//print_r($data); die;
		if(empty($data))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! Some problem occured.. Try Again.')
						</SCRIPT>");
			
		}
		else
		{
				echo'
						<div class="alert alert-success alert-dismissible fade show" role="alert">
						<strong>Success ! </strong> You have punched <b>'
						.$quantity.'</b> order quantity of model <b>'.$mod_val.'</b> of product type <b>'.$prod_type.
						'</b>. <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						</div>
					';
				$query = $this->db->query("SELECT * FROM `tbl_model_master` WHERE `PRODUCT` ='".$prod_type."'");
			    $test['deal'] = $query->result();
				//echo "<pre>test"; print_r($test); 
				$this->load->view('add_punch_model_view', $test);
		}
	}
	function insert_model_dist()
	{
		//echo "Hello";
		
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$quantity = $_POST['quantity'];
		$moc_val = $_POST['moc_val'];
		$mod_val = $_POST['mod_val'];
		$prod_type = $_POST['prod_type'];
	    
		// echo "<br/>Product :".$prod_type."<br/>".$user."<br/>".$user_uuid."<br/>".$quantity."<br/>".$moc_val."<br/> Model Desc: ".$mod_val; die; 
		
		$this->load->model('order_punch_model');
		$data = $this->order_punch_model->insert_punch_dist($quantity, $moc_val, $mod_val, $user_uuid, $prod_type);
		//$data['deal'] = $this->order_punch_model->get_product_data($prod_type);
		//print_r($data); die;
		print_r($data);
	}
	
	function get_model_masters()
	{
		$id = $_POST['id']; 
		//print_r($id); die;
		$this->load->model('order_punch_model');
		$data['deal'] = $this->order_punch_model->get_product_det($id);
		//print_r($data['res']);
		$this->load->view('add_punch_model_comp_view', $data);
	}
	
	function punching_comp()
	{
		//echo "Hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$data['arr'] = array('user' => $user);
		//$data[''] = array('user' => $user);
		//print_r($data); 
		$this->load->model('order_punch_model');
		$this->load->model('service_case_model');
		$data['res'] = $this->order_punch_model->get_product_type();
		///shiv
		$data['dist'] = $this->service_case_model->get_dist_dealer($user_uuid);
		//echo "<pre>";print_r($data['res']); exit;
		$this->load->view('add_punch_company_view', $data);
	}
	
	function add_order_punch()                                                                                                                                                                                                                                                                                                                                                                                            
	{
		$get_session_data = $this->session->userdata('logged_in');
         //echo "<pre>"; print_r($_POST); die;
		 //$mdcd=$_POST['modelCode'];
		/* 		
		function getGUID(){
						if (function_exists('com_create_guid')){
							return com_create_guid();
						}else{
							mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$uuid = substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12);
							return $uuid;
						}
					}
			$guid = getGUID(); 
*/
			
		$data = array(
		              'distUUID' => $_POST['dist_uuid'],
		              'distName' => $_POST['dist_name'],
		              'dealerName' => $_POST['dealer'],
		              'dsrCallType' => $_POST['callType'],
		              'modeOfOrder' => $_POST['mode'],
		              'paymentCollection' => $_POST['paycollect'],
		              'payOption' => $_POST['payoption'],
					  'totalOrderQty' => $_POST['ordrQty'],
		              'paymentSerialNo' => $_POST['cash'],
					  'remarks' => $_POST['remark'],
		              'productCategory' => $_POST['productCategory'],
					  'modelCode' => implode(',',$_POST['modelCode']),
					  'OrderQty' => implode(',',$_POST['ibox'])	              
		);

///////////////////////////////////////////////////////////////////////
		/* $dataSec = array(
		              'distuuid' => $_POST['dist_uuid'],
		              'dist_name' => $_POST['dist_name'],
		              'dealer_name' => $_POST['dealer'],
		              'call_type' => $_POST['callType'],
		              'order_mode' => $_POST['mode'],
		              'payment_collection' => $_POST['paycollect'],
		              'pay_option' => $_POST['payoption'],
					  'total_ord_qty' => $_POST['ordrQty'],
		              'payment_sno' => $_POST['cash'],
					  'remarks' => $_POST['remark'],
		              'prod_cat' => $_POST['productCategory'],
					  'mdl_code' => implode(',',$_POST['modelCode']),
					  'ord_qty' => implode(',',$_POST['ibox']),	              
					  'order_type' => 'Secondary'	              
		); */
/////////////////////////////////////////////////////////////////////		
		$this->load->model('order_punch_model');
		$dataisrt = $this->order_punch_model->insert_order_punch($data);		
		//$dataSecOrd = $this->order_punch_model->insert_secorder_punch($dataSec);		
		if($dataisrt>0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Success ! The Order has successfully punched....')
			window.location.href='http://13.228.144.245/livguard_dms/index.php/secondary';
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! Some Problem Occured. Please try again later.')
			window.location.href='punching';
			</SCRIPT>");
		}

/* 
		if($dataSecOrd>0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Success ! The secondary Order has successfully punched....')
			window.location.href='http://13.228.144.245/livguard_dms/index.php/secondary';
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! Some Problem Occured. Please try again later.')
			window.location.href='punching';
			</SCRIPT>");
		}	 */	
	}
	
	

	function add_order_punching(){ 
        //print_r($_POST); die("dihfkeasbfjbs");		
		$get_session_data = $this->session->userdata('logged_in');
	    $user_uuid = $get_session_data['user_uuid']; 
	    $user_name = $get_session_data['username']; 
		$cur_date = date('Y-m-d H:i:s');
		$day_in_date = date("Y-m-d H:i:s",strtotime("-5 hour -30 minutes",strtotime($cur_date)));
		//echo $user_name ."hello"; die;
		$date = date("Y-m-d");
		/* $modelCode = implode(',',$_POST['modelCode']);
		$qty = implode(',',$_POST['ibox']);
		// echo $date; die;
		$modelcode = explode(',', modelCode); */
		 $order_rand = $this->getGUID();
		 
		 $today = date("Ymdhis");
		$rand = rand(0,99);
		$reference_number = "DMS-".$today.$rand;
	    $dn = $_POST['dist_name'] .'_SO(DMS)';
		$did = $_POST['dist_uuid'];
		$dealer = $_POST['dealer'];
		
		 $fos = $_POST['fos'];
		 //$callType = $_POST['callType'];
		 $mode = $_POST['mode'];
		 $paymentTerm = $_POST['paymentTerm'];
		 $ordrQty = $_POST['ordrQty'];
		 $remark = $_POST['remark'];
		 //$remark = $_POST['productCategory'];
		 $modelCode = $_POST['modelCode'];
		
		 $ibox = $_POST['ibox'];
		
		 //$order_rands = date('Y-m-d H:i:s', $order_rand);
		
		//$sql = "SELECT * FROM sar_dealer  as a LEFT JOIN sar_dealer_cstm as b on b.id_c = a.id WHERE b.customer_number_c ='".$dealer."'";
		$sql = "SELECT a.id,b.customer_number_c,b.billing_address_address1_c,b.last_name_c,b.base_type_c,b.billing_address_district_c,a.name as dealer_name,a.phone_office,a.billing_address_city,a.billing_address_street,a.billing_address_state,d.name as hub_name,e.hub_code_c FROM `sar_dealer` a left join sar_dealer_cstm b on a.id = b.id_c left join sar_dealer_sar_branches_1_c c on a.id = c.sar_dealer_sar_branches_1sar_dealer_ida left join sar_branches d on c.sar_dealer_sar_branches_1sar_branches_idb = d.id left join sar_branches_cstm e on d.id = e.id_c where a.deleted = 0 and c.deleted = 0 and b.customer_number_c = '".$dealer."'";
		
		
		//echo "Query : ".$sql; die;
		$query = $this->db->query($sql);
		//echo $this->db->last_query(); die("kadfuian");
		
        $row = $query->result();
		$hub_name_c = $row[0]->hub_name;
		$hub_code_c = $row[0]->hub_code_c;
		
		
		$city_c = $row[0]->billing_address_city;
		$district_c = $row[0]->billing_address_district_c;
		$state_c = $row[0]->billing_address_state;
		$dealer_reference_no = $row[0]->customer_number_c;
		$dealer_name = $row[0]->dealer_name;
		$order_name = $dealer_name.' -SO';
		$dealer_guid = $row[0]->id;
		
		
		$dname = $row[0]->dealer_name; 
		//$billing_address = $row[0]->billing_address_street;
		$billing_address_address1_c = $row[0]->billing_address_address1_c;
		$city = $row[0]->billing_address_city;
		$state = $row[0]->billing_address_state;
		$employee_code = $row[0]->customer_number_c;
		$dmobile = $row[0]->phone_office;
		$dempname =  $row[0]->last_name_c;
		$dtype =  $row[0]->base_type_c;
       
		for($i=0;$i<count($modelCode);$i++)
		{
			$mcode= $_POST['modelCode'][$i];
			$mcodeparent = explode('@@',$mcode);
			$product_category = $_POST['product_category'][$i];
			$proCat= $mcodeparent[0];
			$modelName= $mcodeparent[1];
			$ibox= $_POST['ibox'][$i];
			$ordrtype ="Secondary"; 
			$aos_product_id= $mcodeparent[0];
			
			//print_r($mcodeparent); die;
			$query = $this->db->query("SELECT * FROM `aos_products` WHERE `part_number` ='".$modelName."' and deleted = 0");
			//echo $this->db->last_query(); die("kadfuian");
			//$row = $query->result();
			$guid = $this->getGUID();
			$guid_realtion = $this->getGUID();
			$guid_realtion2 = $this->getGUID();
			$rows = $query->result();
			$prod_name = $rows[0]->name;
			$prod_guid = $rows[0]->id;
			
			$data = array(
		              'order_name' => $dn,
		              'order_date_entered' => $date,
		              'modeOfOrder' => 'secondary dms',
		              'product_description' => $prod_name,
		              'model_code' => $modelName,
		              //'callType' => $_POST['callType'],
		              'product_name' => $prod_name,
		              'dealer_code' => $dealer,
		              'dealer_name' => $dname,
		              'city' => $city,
		              'state' => $state,
		              'billing_address' => $billing_address_address1_c,
					  'dealer_mobile' => $dmobile,
		              'employee_code' => $employee_code,
					  //'emp_name' => $dempname,
		              'emp_mobile' => $dmobile,
					  'order_type' => $ordrtype,
					  'distributor_code' => $did,	              
					  'order_random' => $reference_number,	              
					  'fareye_id' => $order_rand,	              
					  'product_category' => $product_category,	              
					  'quantity' => $ibox,
					  'hub_name' => $hub_name_c,
					  'hub_code' => $hub_code_c,
					  'distributor_reference_no' => $user_name,
					  'emp_name' => $fos,
					);	
					
				$data_crm = array(
				'id_c' => $guid,
				'aos_products_id_c' => $prod_guid,
				'quantity_c' => $ibox,
				'order_status_c' => 'Secondary',
				'account_id1_c' => $did,
				//'distributor_code_c' => $distributor_reference_no,
				'distributor_code_c' => $user_name,
				'hub_name_c' => $hub_name_c,
				'hub_code_c' => $hub_code_c,
				'city_c' => $city_c,
				'district_c' => $district_c,
				'state_c' => $state_c,
				'sync_to_dms_c' => 1,
                'fe_ref_num_c' => $reference_number,
                'dealer_code_c'=> $dealer_reference_no,
                'sar_dealer_id_c' => $dealer_guid			
			);
			
			$data_dms = array( //day_in_date
				'id' => $guid,
				'name' => $order_name,
				'date_entered' => $day_in_date,
				'date_modified' => $day_in_date,
				'description' => $remark      
				      
			);
			$data_crm_realtion = array(
				'id' => $guid_realtion,
				'date_modified' => $day_in_date,
				'accounts_sar_secondary_order_1accounts_ida' => $did,
				'accounts_sar_secondary_order_1sar_secondary_order_idb' => $guid      
				      
			);
			
			
			$data_crm_realtion1 = array(
				'id' => $guid_realtion2,
				'date_modified' => $day_in_date,
				'sar_dealer_sar_secondary_order_1sar_dealer_ida' => $dealer_guid,
				'sar_dealer_sar_secondary_order_1sar_secondary_order_idb' => $guid      
				      
			);
			
					
		$this->load->model('order_punch_model');
		//$dataisrt = $this->order_punch_model->insert_order_punch($data);		
		$dataSecOrd = $this->order_punch_model->insert_order_punch($data);	
		$dataPrimary = $this->order_punch_model->insert_order_punch_dc($data_crm);
		$dataPrimary_dms = $this->order_punch_model->insert_order_punch_dms($data_dms,$data_crm_realtion);
		$dataPrimary_crm_relation = $this->order_punch_model->insert_order_punch_crm_dealer_realtion($data_crm_realtion1);
		
		//print_r($dataSecOrd); 	die;
/* 		if($dataSecOrd>0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Success ! The Order has successfully punched....')
			window.location.href='http://13.228.144.245/livguard_dms/index.php/order_punch/punching';
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! Some Problem Occured. Please try again later.')
			window.location.href='http://13.228.144.245/livguard_dms/index.php/order_punch/punching';
			</SCRIPT>");
		}  */
	}

		

/* 
		if($dataSecOrd>0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Success ! The secondary Order has successfully punched....')
			window.location.href='http://13.228.144.245/livguard_dms/index.php/secondary';
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! Some Problem Occured. Please try again later.')
			window.location.href='punching';
			</SCRIPT>");
		}	 */	
	}
	
	function add_order_punch_dc(){
		$get_session_data = $this->session->userdata('logged_in');
		//echo "<pre>"; print_r($_POST); die;
		$dist_uuid = $_POST['dist_uuid'];
		$distributor_name = $_POST['dist_name'];
		$remark = $_POST['remark'];
		$dealer = $_POST['dealer'];
		$cur_date = date('Y-m-d H:i:s');
		$day_in_date = date("Y-m-d H:i:s",strtotime("-5 hour -30 minutes",strtotime($cur_date)));
		
		
		$ibox = $_POST['ibox'];
		$modelCode = $_POST['modelCode']; 
		$today = date("Ymdhis");
		$rand = rand(0,99);
		$reference_number = "DMS-".$today.$rand;
		$sql = "SELECT a.id,a.name as distrbutor_name,b.billing_address_district_c,a.billing_address_city,a.billing_address_state,d.name as hub_name,e.hub_code_c,b.customer_number_c FROM `accounts` a left join accounts_cstm b on a.id = b.id_c left join accounts_sar_branches_1_c c on a.id = c.accounts_sar_branches_1accounts_ida left join sar_branches d on c.accounts_sar_branches_1sar_branches_idb = d.id left join sar_branches_cstm e on d.id = e.id_c where a.deleted = 0 and c.deleted = 0 and a.id = '".$dist_uuid."'";
		$query = $this->db->query($sql);
		$rows = $query->result();
		$hub_name_c = $rows[0]->hub_name;
		$hub_code_c = $rows[0]->hub_code_c;
		$city_c = $rows[0]->billing_address_city;
		$district_c = $rows[0]->billing_address_district_c;
		$state_c = $rows[0]->billing_address_state;
		$distributor_reference_no = $rows[0]->customer_number_c;
		$distrbutor_name = $rows[0]->distrbutor_name;
		$order_name = $distrbutor_name.' -SO';
		for($i=0;$i<count($modelCode);$i++){
			$mcode= $_POST['modelCode'][$i];
			$mcodeparent = explode('@@',$mcode);
			$product_category = $_POST['product_category'][$i];
			$aos_product_id= $mcodeparent[0];
			$modelName= $mcodeparent[1];
			$ibox= $_POST['ibox'][$i];
			$ordrtype ="Primary";
			$guid = $this->getGUID();
			$guid_realtion = $this->getGUID();
			//print_r($proCat); die;
			
			$data = array(
				'id_c' => $guid,
				'aos_products_id_c' => $aos_product_id,
				'quantity_c' => $ibox,
				'order_status_c' => 'Primary',
				'account_id1_c' => $dist_uuid,
				//'distributor_code_c' => $distributor_reference_no,
				'distributor_code_c' => $distributor_name,
				'hub_name_c' => $hub_name_c,
				'hub_code_c' => $hub_code_c,
				'city_c' => $city_c,
				'district_c' => $district_c,
				'state_c' => $state_c,
				'sync_to_dms_c' => 1,
                'fe_ref_num_c' => $reference_number				
			);
			
			$data_dms = array(
				'id' => $guid,
				'name' => $order_name,
				'date_entered' => $day_in_date,
				'date_modified' => $day_in_date,
				'description' => $remark      
				      
			);
			$data_crm_realtion = array(
				'id' => $guid_realtion,
				'date_modified' => $day_in_date,
				'accounts_sar_secondary_order_1accounts_ida' => $dist_uuid,
				'accounts_sar_secondary_order_1sar_secondary_order_idb' => $guid      
				      
			);
			//echo "<pre>"; print_r($data); die;
			$this->load->model('order_punch_model');
			$dataPrimary = $this->order_punch_model->insert_order_punch_dc($data);
			$dataPrimary_dms = $this->order_punch_model->insert_order_punch_dms($data_dms,$data_crm_realtion);
			
			
			
		}
		
	}	
	function punch_list()
	{
		$get_session_data = $this->session->userdata('logged_in');
		//print_r($get_session_data);
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('order_punch_model');
		//echo "Hekdfkjxgn";
		//echo $user_uuid; die;
		$data['res'] = $this->order_punch_model->get_punch_list($user_uuid);
		
	//	echo "<pre>"; print_r($data); die;
		
		$this->load->view('punch_list_view', $data);
	}
	function view_punch()
	{
		$get_session_data = $this->session->userdata('logged_in');
		//print_r($get_session_data);
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('order_punch_model');
		//echo "Hekdfkjxgn";
		//echo $user_uuid; die;
		$data['res'] = $this->order_punch_model->view_punch_detail($user_uuid);
		$this->load->view('punchdetails_view', $data);
	}
	function punch_illustrate()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('order_punch_model');
		$data['res'] = $this->order_punch_model->get_punch_list_dc($user_uuid);
		$this->load->view('punch_list_dc_view', $data);
	}	
	function add_fos(){
		$get_session_data = $this->session->userdata('logged_in');
		$data = array(
		              'dept' => $_POST['dept'],
		              'createdBy' => $get_session_data['username'],
		              'loginId' => $_POST['loginid'],
		              'fosCreatedOn' => date("d-m-Y H:i:s"),
		              'fosCode' => $_POST['fosc'],
		              //'password' => $_POST['pass'],
		              'fosName' => $_POST['fosn'],
		              'manager' => $_POST['manager'],
		              'designation' => $_POST['designation'],
		              //'zone' => $_POST['zone'],
		              'country' => $_POST['country'],
		              'state' => $_POST['state'],
		              'city' => $_POST['city'],
		              //'hub' => $_POST['hub'],
		              'address' => $_POST['address'],
		              //'phone' => $_POST['phone'],
		              'mobile' => $_POST['mobile'],
		              'email' => $_POST['email'],
		              'remarks' => $_POST['remarks'],
		              'status' => $_POST['status']
		);
		//echo "<pre>"; print_r($data); die;
		$this->load->model('order_punch_model');
		$data = $this->order_punch_model->insert_fos($data);
		if($data>0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Success ! The FOS has created successfully....')
			window.location.href='index';
			</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! The FOS can't be ctreated. Please try again.')
			window.location.href='order_punch';
			</SCRIPT>");

		}
		
	}
	
	public function foschangestatus(){
		$fosid = $this->input->post('fosid');
		$fossts1 = $this->input->post('fossts');
		if($fossts1 == 1){
			$stsval = 0;
		}else{
			$stsval = 1;
		}
		$fossts = array(
			'status' => $stsval,
		);
		
		$this->load->model('order_punch_model', 'Model');
		$this->Model->changestatus($fosid, $fossts);
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
