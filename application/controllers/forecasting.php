<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forecasting extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }

	
	public function index(){
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$this->load->model('forecast_report_model');
		$data['for'] = $this->forecast_report_model->get_forecast_details($useruid);
		$data['prodType'] = $this->forecast_report_model->get_product_type();
		$this->load->view('forecasting_view', $data);

	}
	
	function add_forecast_report(){
	    $get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		//echo $dist;
		//echo $user; die;
		$date = date('m-y', strtotime('+1 month'));
		/* 
			$arr = explode("-",$date);
			$year = $arr[0];
			$mon  = $arr[1]; 
		*/
		
		$product_type = explode('##',$_POST['product_type']);
		$data = array(
		    'product_type' => $product_type[1],
		    'f_sku_code' => $_POST['sku_code'],
		    'f_sku_name' => $_POST['sku_code'],
		    'f_target_qty' => $_POST['target'],
		    'forecast_qty' => $_POST['forecast'],
		    //'actual_qty' => $_POST['actual'],
		    'abailable_stock' => $_POST['abailable_stock'],
		    'lst_mth_avg' => $_POST['lst_3mth_avg'],
		    'forecast_month' => $date,
		    'f_user' => $useruid
		);
		//echo "<pre>"; print_r($data); exit;
		$this->load->model('forecast_report_model');
		$data1 = $this->forecast_report_model->insert_forecast_report($data);
		print_r($data1);
/* 		 if(empty($data))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Succesfully Updated')
			window.location.href='success';
			</SCRIPT>");
		}
		else
		{
			$this->load->view("forecasting_view");
		} */ 
	}
	
	function success()
	{
		$this->load->view("forecasting_view");
	}

	function forecast_report()
	{
		$date = date('Y-m-d');
		//echo $date; die;
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
	    $m1 = date("m-y",strtotime("-1 Months")); 
	    $m2 = date("m-y",strtotime("-2 Months")); 
	    $m3 = date("m-y",strtotime("-3 Months"));
		// echo $m1." hi ".$m2." hw ".$m3; die;
		$this->load->model('forecast_report_model');
		$data['res'] = $this->forecast_report_model->get_forecast_report($m1, $m2, $m3, $useruid);
		$this->load->view('forecast_report_view', $data);
	}
	
	public function get_sku_code(){
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$this->load->model('order_punch_model');
		$product_type = $this->input->post('product_type');
		$this->load->model('forecast_report_model');
		$data['segment_id'] = $this->order_punch_model->get_distributor_segmentid($useruid);
		$segment_id = $data['segment_id'][0]->segment_id;
		$aos_prodcut = $this->forecast_report_model->aos_prodcuts($product_type,$segment_id);
		
		//echo "<pre>"; print_r($aos_prodcut); die;
		if(count($aos_prodcut)>0){
			?>
			<option value="">Please select SKU name</option>
			<?php
			foreach($aos_prodcut as $v){ //print_r($v); die;
				?>
				<option value="<?php echo $v->part_number;?>"><?php echo $v->name; ?></option>
				<?php
			}
		}else{
			?>
			<option value="">Sorry, No SKU Code found</option>
			<?php
		}
	}
	
	public function get_available_stock(){
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$part_number = $this->input->post('partNo');
		//echo $part_number; die;
		$this->load->model('forecast_report_model');
		$available_stock = $this->forecast_report_model->available_stock($part_number, $user);
		 //print_r($available_stock); 
		 echo json_encode($available_stock);
		/*foreach($available_stock as $v){
			?>
			<option value="<?php echo $v->con;?>"><?php echo $v->con;?></option>
			<?php
		}*/
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */