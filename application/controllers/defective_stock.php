<?php 
error_reporting('E_ALL');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Defective_stock extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
		//$this->load->library('../models/service_case_model');
		 $this->load->model('defective_stock_model');
		 // $this->load->model('service_case_model');
           
    }
    
	function upload_docs_manually($case)
	{
		$case = base64_decode($case);
		if(empty($case))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Please try again uploading documents!!')
					window.location.href='".base_url()."index.php/defective_stock/';
					</SCRIPT>");
		}
		else
		{
			//echo "Hii"; die;
			$data['array'] = $case;
			 $this->load->view('services/upload_docs_manually_view', $data);
		}
	}
	
	function isDocUploaded()
	{
		
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		$case = trim($_POST['case']);
		//echo $case; die("hey");
		$date = date("d-m-Y H:i:s");
		$rand1 = date('ymdhis').rand(0000,9999);
		$docType  	  = 	trim(stripslashes($_POST['MissedDocType']));
		$upload_rand1 = $rand1.'_'.$_FILES['MissedDocUpload']['name'];
		if(empty($case))
		{
		   echo "Empty data"; 
			die;		   
		}
		else
		{
				
				if(move_uploaded_file($_FILES['MissedDocUpload']['tmp_name'], './docUploadsCases/' .$upload_rand1)){}
				
				$data =array(
								'missedDocUploadTime' => $date,
								'missedUploadDocs' =>  	 $upload_rand1,
								'missedDocType' =>  	 $docType, 
								'missedDocUploadedBy' => $user 
							);
								
				//echo "<pre>"; print_r($ins_arr); DIE("Hello");				
			
			  //echo $user; die;
				$this->load->model('defective_stock_model');
				$val = $this->defective_stock_model->update_missed_document($data, $case);
				//$val = $this->defective_stock_model->uploader_success($user, $case, $ins_arr);
				//print_r($val); die("Hello");
				if($val>0)
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Success ! Your missed document uploaded! Check it now!')
							window.location.href='".base_url()."index.php/defective_stock/view_cases/".base64_encode($case)."';
							</SCRIPT>");
				}
				else
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry, Some problem ocurred. Please try again!!')
							window.location.href='".base_url()."index.php/defective_stock';
							</SCRIPT>");
				}
		}		
	}
	
	
  function index(){ 
	  $get_session_data = $this->session->userdata('logged_in');
	  $user = $get_session_data['user_uuid'];
	  //echo $user; die;
	  $this->load->model('defective_stock_model', 'df_sm');
	  $val = $this->df_sm->defective_complain_details($user);
	  //print_r($val); die;
	  if($val){
		  $data['defective_complain_detail'] = $val;
	  }
	  $this->load->view('services/defective_complaint_view', $data);
	  //return redirect('login', 'refresh');
  }
  
  function manual_search()
	{
		$this->load->view('services/manual_search_view');
	}
	
	public function manual_search_result(){
		//print_r('sssssssssssssss'); die;
		 $val = $this->input->post('serial_num');
		 //echo $val; die();
		 $get_session_data = $this->session->userdata('logged_in');
		 $us = $get_session_data['username'];
		 //$us_uuid = $get_session_data['user_uuid'];
		 $this->load->model('defective_stock_model');
		 
		 $data['sas'] = $this->defective_stock_model->manual_get_serial($val);
		 //echo "<pre>"; print_r($data['sas']); die;
		 if(empty($data['sas']))
		 {
			 
			 echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry, Serial Number not found. Please contact admin!!')
					window.location.href='".base_url()."index.php/defective_stock/manual_search';
					</SCRIPT>");
				 //$this->load->view('services/manual_search_view', $data); */
		 }
		 else
		 {
			$this->load->view('services/manual_search_view', $data);
		 }
	}
	
  public function view_cases($caseId)
  {
		$caseId = base64_decode($caseId);
		//echo $caseId; exit;
		
		$get_session_data = $this->session->userdata('logged_in');
		$us = $get_session_data['user_uuid'];
		if(empty($us))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry, Session expired.. Please login again!!')
					window.location.href='".base_url()."index.php/livguard_dms';
					</SCRIPT>");
		}
		$this->load->model('defective_stock_model');
		$claimC = $this->defective_stock_model->check_claim($caseId);
		if($claimC > 0)
		{
			//echo "<pre>"; print_r($claimC); die("Jai Hind");
			$service_call_id = $claimC[0]->service_call_id; 
			$symptomCode = $claimC[0]->symptomCode; 
			$call_id = $claimC[0]->call_id; 
			$testR  = $this->defective_stock_model->check_test_report($caseId);
			if($testR > 0)
			{
				if(!empty($symptomCode))
				{
					$data['sas'] = $this->defective_stock_model->update_wrrnty_details($caseId, $us);
					 //echo "<pre>"; print_r($data['sas']); die;
					 $serial = $data['sas'][0]->callProductSerialNo;
					$data['asset'] = $this->defective_stock_model->getDefectiveSerial($serial);
					//echo "<pre>"; print_r($data['asset']); die;
					//$data['test'] = $this->defective_stock_model->getAllDefective($user, $caseId);
					$this->load->view('services/defective_detailed_view', $data);
				}
				else
				{
					$call_id = base64_encode($call_id);
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Please submit the symptom, defect code and distributor remark to see detials.!!')
					window.location.href='".base_url()."index.php/defective_stock/decision/$call_id';
					</SCRIPT>");
				}
			}
			else
			{
				$service_call_id = base64_encode($service_call_id);
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Please Submit test report before viewing details!!')
					window.location.href='".base_url()."index.php/defective_stock/test_report/$service_call_id';
					</SCRIPT>");
			}
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry, Some Problem Occurred. Please try again later!!')
					window.location.href='".base_url()."index.php/defective_stock';
					</SCRIPT>");
		}
		
		
	}
	
  function batery_report_defective()
  {
		//  echo "Hello jsbdjfbsdjbfjdsbfjhd"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$case = trim($_POST['caseId']);
		$arr = array(
					  'physicalCond' => trim($_POST['dremark']),
					  'condOfTerminal' => trim($_POST['brand']),
					  'electrolyteColor' => trim($_POST['sevisit']),
					  'ocv' => trim($_POST['ocv_bef']),
					  'caseId' => $case
				);
		//print_r($arr); die;
		$this->load->model('fault_parts_model');
		$dat = $this->fault_parts_model->insert_batt_report($arr);
		//print_r($dat)
		if($dat > 0)
		{
			echo $case;
		}
		else
		{
			return '';
		}
	}
	
  function test_report_defective()
  {
	 // echo "Helohhhhh"; die;
		$get_session_data = $this->session->userdata('logged_in');
		//echo "<pre>"; print_r($get_session_data); die;
		$caseId=$_POST['caseId'];
		$user = $get_session_data['user_uuid'];
		//$sess_id = $this->session->set_userdata('case', $caseId);
		//sss$case = $this->session->userdata('case');
		//echo "case report controller";
		$data  = array(
		         'caseId' => $caseId,
		         'submittedByDist' => $user,
		         'physicalCond' => $_POST['dremark'],
		         'condOfTerminal' => $_POST['brand'],
		         'electrolyteColor' => $_POST['sevisit'],
		         'ocv' => $_POST['ocv_bef'],
		         'loadTest' => $_POST['load_test'],
		         'hrd' => $_POST['hrd_bef'],
		         'backup' => $_POST['backup'],
		         'bCCAr' => $_POST['bcca_rated'],
		         'bCCAm' => $_POST['b_measured'],
		         'bVerdict' => $_POST['bverdict'],
		         'bef_c1' => $_POST['c1'],
		         'bef_c2' => $_POST['c2'],
		         'bef_c3' => $_POST['c3'],
		         'bef_c4' => $_POST['c4'],
		         'bef_c5' => $_POST['c5'],
		         'bef_c6' => $_POST['c6'],
		         'chrgeStDate' => $_POST['chrgStartDate'],
		         'chrgStTime' => $_POST['chrgStartTime'],
		         'ChrgeEndDate' => $_POST['chrgEndDate'],
		         'ChrgeEndTime' => $_POST['chrgEndTime'],
		         'chrgingAmpere' => $_POST['chrgEnd'],
		         'TOC' => $_POST['chrgEnd'],
		         'aCCAr' => $_POST['acca_rated'],
		         'aCCAm' => $_POST['ameasured'],
		         'aVerdict' => $_POST['averdict'],
		         'aft_c1' => $_POST['afterC1'],
		         'aft_c2' => $_POST['afterC2'],
		         'aft_c3' => $_POST['afterC3'],
		         'aft_c4' => $_POST['afterC4'],
		         'aft_c5' => $_POST['afterC5'],
		         'aft_c6' => $_POST['afterC6'],
		         'aft_loadtest' => $_POST['load_test'],
		         'aft_hrd' => $_POST['hrd_bef'],
		         'vehicle_avail' => $_POST['purpose'],
		         'min_out_curr' => $_POST['v_min_out'],
		         'max_out_curr' => $_POST['v_max_out'],
		         'lower_cutoff_volt' => $_POST['v_l_cutoff'],
		         'upper_cutoff_volt' => $_POST['v_u_cutoff'],
		         'sys_leakage' => $_POST['sys_leak'],
		         'vd_cranking' => $_POST['v_drop'],
		         'cd_cranking' => $_POST['c_drawn'],
		         'erick_chrg_out' => $_POST['erick_chr_out'],
		         'vehicl_chrg_remark' => $_POST['chrgRemrk']
				 
		);
		$callStatus = "1";
		$callStage = "10";
		//echo "<pre>"; print_r($data); die;
		$arr = array(
		              'callStatus' => $callStatus,
		              'callStage' => $callStage
					);
		
		
			$this->load->model('defective_stock_model');
			$dat = $this->defective_stock_model->case_report($data);
			
			//echo $dat; die;
			
			if($dat > 0)
			{
				//echo $caseId; exit;
				$sta = $this->defective_stock_model->update_status_stage($arr, $caseId);
				if($sta > 0)
				{
					$caseId = base64_encode($caseId);
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Test Report has successfully submitted. Please submit symptom & defect code.')
					window.location.href='".base_url()."index.php/defective_stock/decision/$caseId';
					</SCRIPT>");
				}
				else
				{ 
					echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry, Some problem occured.. Please try again later !!')
							window.location.href='".base_url()."index.php/service_cases/verify_serial';
							</SCRIPT>");
				}	
			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry, Some problem occured.. Please try again later !!')
				window.location.href='".base_url()."index.php/service_cases/verify_serial';
				</SCRIPT>");
			}
	}
	
  
	public function new_case()
	{	
	    $ag = $_POST['pr_ageing'];
	    $prd = $_POST['pr_sale_date'];
		//echo $prd."<br>";
		$date1 = date('Y-m-d');
	
		$date1 = new DateTime($date1);
		$date2 = $date1->diff(new DateTime($prd));
		$calculated_aging = $date2->days;
		
		$rand1 = date('ymdhis').rand(0000,9999);
		$rand2 = date('ymdhis').rand(0000,9999);
		$rand3 = date('ymdhis').rand(0000,9999);
		$rand4 = date('ymdhis').rand(0000,9999);
		
		$docType  	  = 	trim(stripslashes($_POST['docType']));
		$docType2  	  = 	trim(stripslashes($_POST['docType2']));
		$docType3  	  = 	trim(stripslashes($_POST['docType3']));
		$docType4  	  = 	trim(stripslashes($_POST['docType4']));
				
		 $upload_rand1 = $rand1.'_'.$_FILES['docUpload']['name'];
		 $upload_rand2 = $rand2.'_'.$_FILES['docUpload2']['name'];
		 $upload_rand3 = $rand3.'_'.$_FILES['docUpload3']['name'];
		 $upload_rand4 = $rand4.'_'.$_FILES['docUpload4']['name'];
				
				
		if(move_uploaded_file($_FILES['docUpload']['tmp_name'], './docUploadsCases/' .$upload_rand1)){}
		if(move_uploaded_file($_FILES['docUpload2']['tmp_name'], './docUploadsCases/' .$upload_rand2)){}
		if(move_uploaded_file($_FILES['docUpload3']['tmp_name'], './docUploadsCases/' .$upload_rand3)){}
		if(move_uploaded_file($_FILES['docUpload4']['tmp_name'], './docUploadsCases/' .$upload_rand4)){}

	    $get_session_data = $this->session->userdata('logged_in');
	    $user_uuid = $get_session_data['user_uuid'];
	    //$defective = $_POST['defective'];
		//if(empty($defective)) { $defective = "0"; }
		$cust_code = trim(stripslashes($_POST['cust_code']));
		$assetId = trim(stripslashes($_POST['assetId']));
		$productserial = trim(stripslashes($_POST['productserial']));
		$callType = trim(stripslashes($_POST['callType']));
		$sevisit = trim(stripslashes($_POST['sevisit']));
		$setime = trim(stripslashes($_POST['setime']));
		$custremark = trim(stripslashes($_POST['custremark']));
		$remark = trim(stripslashes($_POST['remark']));
		$caller_type = trim(stripslashes($_POST['caller_type']));
		$mobile = trim(stripslashes($_POST['mobile']));
		$callsource = trim(stripslashes($_POST['callsource']));
		$contact = trim(stripslashes($_POST['contact']));
		$dealer = trim(stripslashes($_POST['dealer']));
		$dealer_code = trim(stripslashes($_POST['dealer_code']));
		$fd = date("Ymdhis");
		$caseId = "C".$fd; 
		//echo $caseId;   die;
		$date = date("Y/m/d H:i:s");
		$callStatus = "1";
		$callStage = "9";
		$defective = "1";
		$data = array(
		                'callCustId' => $cust_code,
		                'callCreatedBy' => $user_uuid,
		                'isDefective' => $defective,
		                'callAssetId' => $assetId,
		                'claimAgeing' => $calculated_aging,
		                'defectiveAgeing' => $calculated_aging,
		                'callProductSerialNo' => $productserial,
		                'callType' => $callType,
		                'se_estd_visit_date' => $sevisit,
		                'se_estd_visit_time' => $setime,
		                'customerRemarks' => $custremark,
		                'Remarks' => $remark,
		                'callerType' => $caller_type,
		                'callerMobile' => $mobile,
		                'callSource' => $callsource,
		                'contactPerson' => $contact,
		                'dealerName' => $dealer,
		                'dealer_code' => $dealer_code,
		                'callStatus' => $callStatus,
		                'callStage' => $callStage,
						'call_id' => $caseId,
						'documentType' => $docType,
						'docType2' => $docType2,
						'docType3' => $docType3,
						'docType4' => $docType4,
						'uploadDocuments' => $upload_rand1,
						'uploadDocs2' => $upload_rand2,
						'uploadDocs3' => $upload_rand3,
						'uploadDocs4' => $upload_rand4,
					);
		// echo "<pre>"; print_r($data); die;
		
		
		
	    $this->load->model('defective_stock_model');
		$chkCase = $this->defective_stock_model->check_case($productserial);
		// print_r($chkCase[0]->ct);
		// die("Case Checl");
		$cse = $chkCase[0]->ct; 
		if($cse > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('So Sorry ! A complaint has already filed on this product serial.')
					window.location.href='".base_url()."index.php/service_cases/verify_serial';
					</SCRIPT>");
		}
		else
		{
			$dat = $this->defective_stock_model->add_new_case($data);
			//print_r($data); die;
			//print_r($data); die("Hello");
			
			
			if(empty($data))
			{
				redirect('index');                 
			}
			else
			{
				   // $caseId
				   $id = base64_encode($dat);
				  echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Success ! Please submit the defective battery test report..')
					window.location.href='".base_url()."index.php/defective_stock/test_report/$id';
					</SCRIPT>");
				   
			
			}
	    }
	  // print_r($data); die;
	}
	
	function check($srl)
	{
			//echo "hii"; die;
	    $srl = base64_decode($srl);
		$get_session_data = $this->session->userdata('logged_in');
		//echo "hii"; die;
		$this->load->model('defective_stock_model');
		$chk1 = $this->defective_stock_model->check_serial_one($srl);
		// print_r($chk1[0]->ct); 
		$var1 = $chk1[0]->ct;
		if($var1 > 0)
		{
			//$srl = base64_encode($srl);
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The battery is already consumed with a customer.')
					window.location.href='".base_url()."index.php/service_cases/verify_serial';
					</SCRIPT>");

		}
		else
		{
			$chk2 = $this->defective_stock_model->check_serial_two($srl);
			// print_r($chk2[0]->sr); die("Hello");
			$var2 = $chk2[0]->sr;
			if($var2 > 0)
			{
				//$srl = base64_encode($srl);
				echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! The battery is already blocked in our system.')
						window.location.href='".base_url()."index.php/service_cases/verify_serial';
						</SCRIPT>");
			}
			else
			{
				$data['asset'] = $this->defective_stock_model->get_serial_data($srl);
				//echo "<pre>"; print_r($data['asset']); die;
				if(empty($data['asset']))
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The battery is already consumed with a customer.')
					window.location.href='".base_url()."index.php/service_cases/verify_serial';
					</SCRIPT>");
				}
				{
					$this->load->view('services/defective_stocks', $data);
				}	
			}	
		}
		
	}
	
	
	
	function test_report($srl)
	{
			//echo "hii"; die;
	    $srl = base64_decode($srl);
		//echo $srl; die;
		$get_session_data = $this->session->userdata('logged_in');
		//echo "hii"; die;
		$this->load->model('defective_stock_model');
		
		//$chk1 = $this->defective_stock_model->check_serial_one($srl);
		// print_r($chk1[0]->ct); 
		//$var1 = $chk1[0]->ct;
		// if($var1 > 0)
		// {
			//$srl = base64_encode($srl);
			// echo ("<SCRIPT LANGUAGE='JavaScript'>
					// window.alert('Sorry ! A complaint is already running on this serial.')
					// window.location.href='".base_url()."index.php/service_cases/verify_serial';
					// </SCRIPT>");

		// }
		// else
		// {
			$chk2 = $this->defective_stock_model->check_serial_two($srl);
			// print_r($chk2[0]->sr); die("Hello");
			$var2 = $chk2[0]->sr;
			if($var2 > 0)
			{
				//$srl = base64_encode($srl);
				echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! The battery test report is already submitted.')
						window.location.href='".base_url()."index.php/service_cases/verify_serial';
						</SCRIPT>");
			}
			else
			{
				
				//$data['asset'] = $this->defective_stock_model->get_serial_data($srl);
				//$data['complainId'] = $srl;
				$data['asset'] = $this->defective_stock_model->get_serial_data1($srl);
				//echo "<pre>"; print_r($data['asset']); die;
				if(empty($data['asset']))
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The product serial has no data in system.')
					window.location.href='".base_url()."index.php/service_cases/verify_serial';
					</SCRIPT>");
				}
				{
					$this->load->view('services/defective_stocks_report', $data);
				}	
			}	
		//}
		
	}
	
	function decision($caseId)
	{
		$case =  base64_decode($caseId);
		//echo $case; die("Helllo");
		$get_session_data = $this->session->userdata('logged_in');
		//$case = $this->session->userdata('case');
	    $user = $get_session_data['user_uuid'];
		//echo "<pre>"; print_r($_SESSION); die("hii");
		$data['res'] = array(
							 'case' => $case,
							 'user' => $user
							);
		//echo "<pre>"; print_r($data['res']); die("hii");
		$this->load->model('fault_parts_model');
		$sym = $this->fault_parts_model->get_cas_status($case);
		/* echo "dsym ".$sym; die;
		//echo $r; die("Helllo");
		$rs = $this->db->query($sql)->result(); */
			//echo "<pre>"; print_r($sms); die("hii");
		if($sym > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! The decision has already submitted.. Go Back..')
					window.location.href='".base_url()."defective_stock';
					</SCRIPT>");
		}
		else
		{
		
			$data['sms'] = $this->fault_parts_model->get_product_wrrnt($case);
			$data['symptom'] = $this->fault_parts_model->get_symptom_code();
			//$data['br'] = $this->fault_parts_model->get_battery_report($case);
			/* echo "<pre>"; print_r($data['sms']);
			echo "<pre>"; print_r($data['symptom']);
			die("Hello"); */
			$this->load->view('services/defective_decision_view', $data);
		}
	}
	
	function close_case()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$case  	  	  = 	trim(stripslashes($_POST['case']));
		$caseENC      =  	base64_encode($case);
		$user = $get_session_data['user_uuid'];
		$this->load->model('defective_stock_model');
		$st = $this->defective_stock_model->check_decision($case);
		// print_r($st[0]->caLL_case_status); die("Hii"); 
		$var = $st[0]->caLL_case_status;
        if($var != "0")
        {
			echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! The decision is already submitted.')
							window.location.href='cases';
							</SCRIPT>");
		}
		else
		{
				$user  	      = 	trim(stripslashes($_POST['user']));
				$symptom      = 	trim(stripslashes($_POST['symptom']));
				$defect       = 	trim(stripslashes($_POST['symptom']));
				//$action  	  = 	trim(stripslashes($_POST['action']));
				$dist_remark  = 	trim(stripslashes($_POST['remark']));
				$defective = "1";
				
				if(empty($dist_remark) || $dist_remark=='0') 
				{
					$dist_remark = '11';
				}
				/* 
				$query 	= $this->db->query("SELECT * FROM `tbl_service_dist_remarks` WHERE `distRemarkId`='".$dist_remark."'");
				$row 	= $query->result();
				 $remarks = $row[0]->distRemarkName;
				 */
				/* 
				$sql 	= $this->db->query("SELECT * FROM `tbl_defect_symptoms` WHERE `id`='".$symptom."'");
				$rows 	= $sql->result();
				
				 $symptom = $rows[0]->Symptoms;
				 $defect   = $rows[0]->Defect;
				  */ 
				$data = array(
								'callCreatedBy' => $user,
								'callModifiedBy' => $user,
								'closedByOn' => date("Y-m-d H:i:s"),
								'callEndDate' => date("Y-m-d H:i:s"),
								'symptomCode' => $symptom,
								'defectCode' => $defect,
								'callStatus' => 5,
								'isDefective' => $defective,
								'callStage' => 8,
								'distributorRemark' => $dist_remark
							);
			//echo "<pre>"; print_r($data);
				
				  $status = $this->defective_stock_model->update_decision($data, $case);
				 // $serial_find = $this->defective_stock_model->get_serial_case($case);
				//echo "<pre>"; print_r($serial_find[0]->callProductSerialNo);  die;
				//echo "<pre>"; print_r($status); die("hello");
				$sr_no = $serial_find[0]->callProductSerialNo;
				if($status>0)
				{
						echo ("<SCRIPT LANGUAGE='JavaScript'>
								window.alert('Success ! Your battery has submitted for defective. Wait for SE decision.')
								window.location.href='".base_url()."index.php/defective_stock';
								</SCRIPT>");  
				}
				else
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
								window.alert('Sorry ! Some Problem Occurred. Serial Number not found for this complaint. Please contact customer care. ')
								window.location.href='".base_url()."index.php/defective_stock';
								</SCRIPT>");  
				}
		}
	}
	

	
}


?>
