<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class All_services extends CI_Controller {

	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('all_services_model');
    }
	
	public function index()
	{
		//echo "Hii";
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		
		$data['pcalls'] = $this->all_services_model->count_pending_calls($useruid); 
		$data['pchalan'] = $this->all_services_model->count_pending_challan($useruid); 
		$data['preplce'] = $this->all_services_model->count_pending_replace($useruid); 
		$data['charge'] = $this->all_services_model->service_handling_charge($useruid); 
		$data['apClaim'] = $this->all_services_model->count_approve_claim($useruid); 
		$data['cntQrys'] = $this->all_services_model->count_queries($useruid); 
		$data['cntQryAns'] = $this->all_services_model->count_query_ans($useruid); 
		
		$this->load->view('services/all_service_dashboard', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */