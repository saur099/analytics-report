<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Return_orders_dealer extends CI_Controller {
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	function get_model_data()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $users = $get_session_data['username'];
	    $user = $get_session_data['user_uuid'];
		$id = $_POST['id'];
		$this->load->model('return_dealer_model');
		$data = $this->return_dealer_model->get_model_data($id);
		print_r(json_encode($data));
	}
	
	function delete_return($id)
	{
		$id = base64_decode($id);
		$this->db->select('*');	
	    $this->db->from('tbl_return_stock');	
	    $this->db->where('id', $id);
		$query = $this->db->get();
		//    echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()<1)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some problem occurred .. Try Again..')
				</SCRIPT>");
				redirect('return_orders_dealer', 'refresh');
		}
		else
		{
			$this->db->delete('tbl_return_stock', array('id' => $id)); 
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Success! You have deleted the order for return..')
				</SCRIPT>");
			redirect('return_orders_dealer', 'refresh');
		}
	}
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $users = $get_session_data['username'];
	    $user = $get_session_data['user_uuid'];
		$this->load->model('return_dealer_model');
		$data['dealer'] = $this->return_dealer_model->get_dealer_list($user);
		$data['serial'] = $this->return_dealer_model->get_serial_list();
		$data['r'] = $this->return_dealer_model->get_defective_stock($user);
		
		//$data['serial'] = $this->return_dealer_model->get_();
		$data['remarks'] = $this->return_dealer_model->get_remarks();
		
		//echo "<pre>"; print_r($data); die;
		
		$this->load->view('return_dealer_view', $data);
	}
	function add_dealer_stock()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$usern = $get_session_data['username'];
		$user = $get_session_data['user_uuid'];
		$ip  = $_SERVER['REMOTE_ADDR'];
		$serial = $_POST['serial_no'];
		$this->load->model('return_dealer_model');
		$get_sr = $this->return_dealer_model->get_sr_data($data2);
		$data2=array(
						'return_serial_number' => $serial,
						'returnedTo' => $_POST['returntype'],
						'return_reason' => $_POST['remark'],
						'distName' => $user,
						'stock_return_by' => $usern,
						'post_to_defect' => $_POST['defect'],
						'returned_IP' => $ip
					);
		echo  "<pre>"; print_r($data2); die;
		
		
		
		$data['dealer'] = $this->return_dealer_model->insert_dealer_list($data2);
		print_r($data['dealer']); 
		
	}
	
	
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>