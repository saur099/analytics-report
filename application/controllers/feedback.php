<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends CI_Controller {

	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$us = $get_session_data['user_uuid'];
		$this->load->model('support_model');
		$data['res'] = $this->support_model->get_feedback_detail($user);
		//$data['resr'] = $this->support_model->get_prod_serial();
		//print_r($data['res']); exit();
		$this->load->view('feedback_form', $data);
	}
	
	function replies()
	{
		$this->load->view('replies_view');
	}	
	
	function add_feedback()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$ip = $_SERVER['REMOTE_ADDR'];

		//if(move_uploaded_file($_FILES['filename']['tmp_name'], './uploads/' . $_FILES['filename']['name'])){}
	
		$feed = array(
		     'feedback_tkt' => $_POST['ticket'],
		     'feedback_in' => $_POST['issue'],
		     'name' => $_POST['name'],
		     'mobile' => $_POST['mobile'],
		     'subject' => $_POST['subject'],
		     'attracted_more' => $_POST['attracted'],
		     'rate_exp' => $_POST['experience'],
		     'portal_val' => $_POST['portal_value'],
		     'fulfill_req' => $_POST['req'],
		     'reqs' => $_POST['reqs'],
		     'tat1' => $_POST['tat1'],
		     'like_best' => $_POST['like_best'],
		     'description' => $_POST['desc'],
		     'createdBy' => $user,
		     'createdUUID' => $useruid,
			 'createdIp' => $ip
		);
		//echo "yes<pre>"; print_r($data); exit;
		$this->load->model('support_model');
		$resf = $this->support_model->insert_feedback($feed);
		if($resf>0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Feedback has given successfully. Thank you..')
			window.location.href='".base_url()."index.php/feedback';
			</SCRIPT>");
			
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry! Some Problem occured.. Please Try Again Later.')
			window.location.href='".base_url()."index.php/feedback';
			</SCRIPT>");

		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */