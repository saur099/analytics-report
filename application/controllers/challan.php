<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('application/libraries/vendor/autoload.php'); 
// reference the Dompdf namespace
use Dompdf\Dompdf;
class Challan extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
		//$this->load->library('../models/service_case_model');
		//  $this->load->model('models/service_case_model');
		 // $this->load->model('service_case_model');
           
    }
	
	public function index($id)
	{
		$id = base64_decode($id);
		// echo $id; die; 
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('fault_parts_model');
		$data['res'] = $this->fault_parts_model->get_parts_details($id);
	//	$data['cname'] = $this->fault_parts_model->cust_name($id);
		//\$this->load->view('services/fault_parts_view', $data);
		
	}
	 
	public function generate()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		$usern = $get_session_data['username'];
		$pname = $_POST['pname'];
		/*  if(empty($pname))
		 {

		 } */
		$from = $_POST['date1'];
		$to = $_POST['date2'];
		$pname = $_POST['pname']; 
		$from = $_POST['date1']; 
		$to = $_POST['date2'];
		$this->load->model('fault_parts_model');
		if($pname == 2 )
		{
			$p1 ="3-W";
			$p2 ="CAR ";
			$p3 ="CV";
			$p4 ="TRACTOR";
			$data['challan_number'] = "CHL".date('YmdHis');
			//echo $data['challan_number']; exit;
			$data['res'] = $this->fault_parts_model->get_date_challan2($from, $to, $p1, $p2, $p3, $p4, $user);
			//echo "<pre>"; print_r($data); die;
			// $pr = 
			if(empty($data['res']))
			{
				
			}
			
			foreach($data['res'] as $v){
				$data1 = array(
					 'complaint_no' => $v->complaint_no_c,
					 'serial_number' => $v->battery_serial_num_c,
					 'brand' => strtoupper($v->brand_c),
					 'product' => $v->product_c,
					 'model' => $v->model_c,
					 'scrap_value' => $v->aprox_scrap_value_c,
					 'address' => $_POST['mobile_no'],
					 'state' => $v->billing_address_state,
					 'city' => $v->billing_address_city,
					 'pincode' => $v->billing_address_postalcode,
					 'contact_person' => 'Mr Saurav',
					 'contact_mobile' => '9818540685',
					 'dist_firm_name' => $v->name,
					 'GST' => date('YmdHis'),
					 'generated_date' => $v->invoice_date,
					 'generated_ip' => $_SERVER['REMOTE_ADDR'],
					 'challan_number' => $data['challan_number'],
					 'challan_date' => $v->invoice_date,
					 'submitted_by' => $usern,
					 'modified_by' => date('Y-m-d H:i:s'),
					 'sent_to' => '',
					 'others_detail' => '',
					 'from_date' => $from,
					 'to_date' => $to
				);
				//echo "yes<pre>"; print_r($data1); exit;
				$data['challan_view_print'] = $this->fault_parts_model->insert_generate_challan_view_print($data1);
			}
			
			
			// $data['cal']= $this->fault_parts_model->get_total_price()
			$this->load->view('services/generate_challan_view', $data);
		}
		else
		{
			//echo "Hello";
			$get_session_data = $this->session->userdata('logged_in');
			
			$data['res'] = $this->fault_parts_model->get_date_challan($from, $to, $pname, $user);
		//	echo "<pre>"; print_r($data); die;
		    if(empty($data['res']))
			{
				
			}
			foreach($data['res'] as $v){
				$data1 = array(
					 'complaint_no' => $v->complaint_no_c,
					 'serial_number' => $user,
					 'brand' => strtoupper($v->brand_c),
					 'product' => $v->product_c,
					 'model' => $v->model_c,
					 'scrap_value' => $v->aprox_scrap_value_c,
					 'address' => $_POST['mobile_no'],
					 'state' => $v->billing_address_state,
					 'city' => $v->billing_address_city,
					 'pincode' => $v->billing_address_postalcode,
					 'contact_person' => 'Mr Saurav',
					 'contact_mobile' => '9818540685',
					 'dist_firm_name' => $v->name,
					 'GST' => date('YmdHis'),
					 'generated_date' => $v->invoice_date,
					 'generated_ip' => $_SERVER['REMOTE_ADDR'],
					 'challan_number' => $data['challan_number'],
					 'challan_date' => $v->invoice_date,
					 'submitted_by' => $usern,
					 'modified_by' => date('Y-m-d H:i:s'),
					 'sent_to' => '',
					 'others_detail' => '',
					 'from_date' => $from,
					 'to_date' => $to
				);
				//echo "yes<pre>"; print_r($data); exit;
				$data['challan_view_print'] = $this->fault_parts_model->insert_generate_challan_view_print($data1);
			}
			$this->load->view('services/generate_challan_view', $data);
			// echo $id; die; 
		
		}
	} 
	 
	 public function reprint_challan(){
		 $get_session_data = $this->session->userdata('logged_in');
		 $user = $get_session_data['user_uuid'];
		 $usern = $get_session_data['username'];
		 
		 $this->load->model('fault_parts_model');
		 $data['preprintchallan'] = $this->fault_parts_model->challan_service_reprint();
		 //echo "<pre>";
		 //print_r($data['preprintchallan']); die;
		 $this->load->view('services/reprint_challan_view', $data);
	 }
	 
	 
	  public function re_print_challan($product_type){
		 //echo $date_range; die;
		$get_session_data = $this->session->userdata('logged_in');
		 $user = $get_session_data['user_uuid'];
		 $usern = $get_session_data['username'];
		 $challan_number = "CHL".date('YmdHis');
		 
		 //$from_date1 = explode('-', $date_range,4);
		 //$from_date = $from_date1[0];
		 //$to_date = $from_date1[1];
		 //$challan_date_mm = $from_date1[2];
		 //$product_type = $from_date1[3];
		 //echo $product_type; die;
		 
		 
		 //echo $challan_date_mm; die;
		 $this->load->model('fault_parts_model');
		 $data = $this->fault_parts_model->re_gets_challan_details_ash($product_type,$user);
		 //echo $data[0]['complaint_no']; die;
		 // echo "<pre>";
		 // print_r($data); die;
		 
		 $challan_start_date = $from_date .'-'. $challan_date_mm .'-2018';
		 $challan_lasr_date = $to_date .'-'. $challan_date_mm .'-2018';
		 
		 //print($challan_date1); die;
		 if(!empty($data)){
			$date = date("Y-m-d");
			$dates = date("Ymdhis");
			$inv = "INV".$dates;
			$timestamp = strtotime($datesss);			
			$dmy = date("d-m-Y", $timestamp);
			//echo $dmy;
			$challan_number_array = array();
			foreach($data as $challan_no){
				$challan_number_array[] = $challan_no['challan_number'];
		    }
			$uniquePids = array_unique($challan_number_array);
			$files = array();
			foreach($uniquePids as $chal){
				$sum = 0;
				$base_url = base_url();

				foreach($data as $challanlist){ 
					if($challanlist['challan_number']==$chal){
						$challan_info = $challanlist;
						break;
					}
					 
				}
				//for($i=1; $i<count($data); $i++){
				//$challanNr = $data[$i]['challan_number'];
					$ab_i = FCPATH."images/liv.png"; //echo $ab_i; die; //images/liv.png
					$ab_im = FCPATH."images/livfast.png";
						 $var = '<table><tr width="100%">
		<center>RETURN CHALLAN - DEFECTIVE BATTERIES</center>
		</tr>
		</table>
		<table style="width: 100%; font-size: 10px; font-family: sans-serif; text-align: center;" border="1" cellspacing="2" cellpadding="2">
		
            <tr padding: 0px 6px;> 			 
					<td style="  padding-top:3px; width: 50%;">
						<img src="'.$ab_i.'" alt="LIVGUARD LTD" height="40"/>
						<img src="'.$ab_im.'" alt="LIVGUARD LTD" height="40"/> 
					</td>
					<td style="  width: 50%;">
						<h4> Pre-Authenticated <br/>
						 Authorised Signatory</h4>
					</td>
			 
				</tr>
				<tbody style="text-align: left;">
				
				<tr style="text-align: left;">
				<td style="font-weight: bold; background-color: #b0c4de; padding: 0px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Consigner Name & Address :</td>
				<td style="font-weight: bold; background-color: #b0c4de; padding: 0px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Consignee Name & Address :</td>
				</tr>
				<tr style="text-align: left;">
				<td style="padding: 2px 6px; border-style: solid; border-width: 1px; width: 50%; vertical-align: top; text-align: left;">
					<div style="font-size:10;">'.strtoupper($challan_info['distributor_name']).'</div>
					<div style="font-size:10;">'.strtoupper($challan_info['distributor_address']).'</div>
					<div style="font-size:10;">'.strtoupper($challan_info['distributor_city']).', '.strtoupper($challan_info['distributor_state']).'</div>
					<div style="font-size:10;">Pin Code -  '.$challan_info['distributor_pincode'].'</div>
					<div style="font-size:10;">India</div>
					<div style="font-size:10;">Contact Person -  '.$challan_info['distributor_person_name'].'</div>
					<div style="font-size:10;">Contact No -  '.$challan_info['distributor_phone'].'</div>
					<div style="font-size:10;">GSTIN  -  '.$challan_info['distributor_gstin_no'].'</div>
				</td>
				<td style="padding: 2px 6px; border-style: solid; border-width: 1px; width: 50%; vertical-align: top; text-align: left;">
				<div style="font-size:10;">LIVGUARD ENERGY TECHNOLOGIES PVT LTD<br>'
				.strtoupper($challan_info['hub_address']).'<br>'
				.strtoupper($challan_info['hub_state']).'<br>Pin Code -'
				.strtoupper($challan_info['hub_pincode']).'<br>
				India<br>
				Contact Person : '.strtoupper($challan_info['hub_contact_person']).'<br>
				Contact No : '.strtoupper($challan_info['hub_contact_mobile']).'<br>
				Email Id : '.$challan_info['hub_email'].'<br>
				GSTIN : '.strtoupper($challan_info['hub_GST']).'<br>
				</div>
				</td>
				</tr>
				<tr style="text-align: left;">
				<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Challan Date</td>
				<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Challan Number</td>
				</tr>
				<tr style="text-align: left;">
				<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
				<div style="font-size:10;">'.$challan_info['generated_challan_date'].'</div>
				</td>
				<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
				<div style="font-size:10;"> '.$challan_info['challan_number'].'</div>
				</td>
				</tr>
				
				
				</tbody>
				</table>
				<table style="width: 100%; border: 1 px solid #000; border-spacing: 0pt;">
				<tbody>
				<tr>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Sr. No.</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Brand</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Product</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Model</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Battery Serial No</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Complaint No.</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Approximate Scrap Value</td>
				</tr>';
							$i=1;
							foreach($data as $challanlist){ 
								if($challanlist['challan_number']!=$chal) continue;
								$challanlist1 = $challanlist['total_amt'];
								 $var.= '<tr>
									<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$i.'</td>
									<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['brand'].'</td>
													<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['product'].'</td>
													<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['model'].'</td>
													<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['serial_number'].'</td>
													<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['complaint_no'].'</td>
													<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.'Rs. '.number_format($challanlist1,2, '.', '').'</td>
									</tr>';
								//$sum += $challanlist1;
								$sum +=number_format($challanlist1,2, '.', '');
								$i++;
							}
							
							$var .='<tr>
							<td colspan="5"> </td>
				
								<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align:center;">'.'Total Rs. </td>
								<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align:center;">'.'Rs. '.number_format($sum,2, '.', '').'</td>
							</tr>
							</tbody>
							</table>
							</table>
				<table style=" width: 100%; border: 0pt none; border-spacing: 0pt;">

		<tr>	
		<ul  style="border:1 px solid #000;">
		
			<li style=" list-style-type: none;"><b>Special Remarks:</b></li>
			<li>The value declared is only for transit and insurance purpose only</li>
			<li>Goods covered under this Challan must be examined & verified by the consignee for quality, Transit Damage, condition of packages & shortage if any, and
			in case the package is delivered in damage condition or with the seal broken or missing or with seal other than as stated in the shipping documents or any
			shortage than it should be mentioned in the carrier (GR) or any such carrier documents and also informed to the nearest LETPL office immediately within 24
			hours., LETPL should be immediately informed on the receipt of material.</li>
			<li style=" list-style-type: none;"><h4>Warranty replacement - Not for Sale 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E. & O.E</h4></li>
		</ul>
</tr>
</table>
<table style=" width: 100%; border: 0pt none; border-spacing: 0pt;">
<tr>
<td style="font-weight: bold;  padding: 0px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">
Customer Signature<br/>
Customer Name & Seal Date<br/>
</td>
<td style="font-weight: bold;  padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">FOR '.strtoupper($challan_info['distributor_name']).'<br/>
AUTHORISED SIGNATORY</td>
</tr>
</table>';
							
						//	echo  $var; die;
						// instantiate and use the dompdf class
							//print_r($var); die;
						$dompdf = new Dompdf();
						$dompdf->loadHtml($var);
					   // $pdf->Image('images/pdf-header.jpg',0,0);
						// (Optional) Setup the paper size and orientation
						$dompdf->setPaper('A4', 'landscape');

						// Render the HTML as PDF
						$dompdf->render();
						$output = $dompdf->output();
						$file = FCPATH.'pdfs/'.$chal.'.pdf';
    					file_put_contents($file, $output);
    					$files[] = $file;

						// Output the generated PDF to Browser
						//$dompdf->stream($chal);
				//}	
			}

			$zip_name = 'challan.zip';
			$file = FCPATH.'pdfs/'.$zip_name;
			@unlink($file);
			$zip = new ZipArchive();
			if ($zip->open($file, ZipArchive::CREATE) === TRUE) {
			    foreach ($files as $v) {
					$zip->addFile($v, basename($v));
				}
			    $zip->close();
			    foreach ($files as $v) {
					@unlink($v);
				}
			    header('Content-type: application/zip');
				header('Content-Disposition: attachment; filename="'.$zip_name.'"');
				readfile($file);
			} else {
			    echo 'failed';
			} 

		}
		else{
				?>
				<script>
				window.alert("Sorry ! No Invoice Found.");
				window.location.href='<?php echo base_url();?>index.php/fault_parts/views_challan';
				</script>
				<?php
			
		}
	 }
	 
	 
	 public function generates_challan($product_type){
			echo $date_range;
		$get_session_data = $this->session->userdata('logged_in');
		 $user = $get_session_data['user_uuid'];
		 $usern = $get_session_data['username'];
		 $date = date("Ymdhis");
         $rand = rand(0,99);
         $challan_number = "CHL".$date.$rand;

		// $challan_number = "CHL".date('YmdHis');
		 
		// $from_date1 = explode('-', $date_range,4);
		 //$from_date = $from_date1[0];
		 //$to_date = $from_date1[1];
		 //$challan_date_mm = $from_date1[2];
		 //$product_type = $from_date1[3];
		 //echo $challan_date_mm; die;
		 $this->load->model('fault_parts_model');
		 $data = $this->fault_parts_model->gets_challan_details_ash($challan_number, $product_type,$user);
		 //echo $data[0]['complaint_no']; die;
		
		 $challan_date = explode('-', $data[0]['to_date']);
		 $challan_yy = $challan_date[0];
		 $challan_mm = $challan_date[1];
		 $challan_dd = $challan_date[2];
		 
		 $last_challan_date = $from_date .'-'. $challan_mm .'-'. $challan_yy;
		 //print($challan_date1); die;
		 if(!empty($data)){
			$date = date("Y-m-d");
			$dates = date("Ymdhis");
			$inv = "INV".$dates;
			$timestamp = strtotime($datesss);			
			$dmy = date("d-m-Y", $timestamp);
			//echo $dmy;
			$ab_i = "".$base_url."images/liv.png";
			$ab_im = "".$base_url."images/livfast.png";
			 $var = '<table><tr width="100%">
		<center>RETURN CHALLAN - DEFECTIVE BATTERIES</center>
		</tr>
		</table>
		<table style="width: 100%; font-size: 10px; font-family: sans-serif; text-align: center;" border="1" cellspacing="2" cellpadding="2">
		
            <tr padding: 0px 6px;> 			 
					<td style="  padding-top:3px; width: 50%;">
						<img src="'.$ab_i.'" alt="LIVGUARD LTD" height="40"/>
						<img src="'.$ab_im.'" alt="LIVGUARD LTD" height="40"/> 
					</td>
					<td style="  width: 50%;">
						<h4> Pre-Authenticated <br/>
						 Authorised Signatory</h4>
					</td>
			 
				</tr>
				<tbody style="text-align: left;">
				
				<tr style="text-align: left;">
				<td style="font-weight: bold; background-color: #b0c4de; padding: 0px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Consigner Name & Address :</td>
				<td style="font-weight: bold; background-color: #b0c4de; padding: 0px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Consignee Name & Address :</td>
				</tr>
				<tr style="text-align: left;">
				<td style="padding: 2px 6px; border-style: solid; border-width: 1px; width: 50%; vertical-align: top; text-align: left;">
					<div style="font-size:10;">'.strtoupper($data[0]['distributor_name']).'</div>
					<div style="font-size:10;">'.strtoupper($data[0]['distributor_address']).'</div>
					<div style="font-size:10;">'.strtoupper($data[0]['distributor_city']).', '.strtoupper($data[0]['distributor_state']).'</div>
					<div style="font-size:10;">Pin Code -  '.$data[0]['distributor_pincode'].'</div>
					
					<div style="font-size:10;">India</div>
					<div style="font-size:10;">Contact Person -  '.$data[0]['distributor_person_name'].'</div>
					<div style="font-size:10;">Contact No -  '.$data[0]['distributor_phone'].'</div>
					<div style="font-size:10;">GSTIN  -  '.$data[0]['distributor_gstin_no'].'</div>
				</td>
				<td style="padding: 2px 6px; border-style: solid; border-width: 1px; width: 50%; vertical-align: top; text-align: left;">
				<div style="font-size:10;">LIVGUARD ENERGY TECHNOLOGIES PVT LTD<br>'
				.strtoupper($data[0]['hub_address']).'<br>'
				.strtoupper($data[0]['hub_state']).'<br>Pin Code -'
				.strtoupper($data[0]['hub_pincode']).'<br>
				India<br>
				Contact Person : '.strtoupper($data[0]['hub_contact_person']).'<br>
				Contact No : '.strtoupper($data[0]['hub_contact_mobile']).'<br>
				Email Id : '.$data[0]['hub_email'].'<br>
				GSTIN : '.strtoupper($data[0]['hub_GST']).'<br>
				</div>
				</td>
				</tr>
				<tr style="text-align: left;">
				<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Challan Date</td>
				<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Challan Number</td>
				</tr>
				<tr style="text-align: left;">
				<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
				<div style="font-size:10;">'.date("d-m-Y").'</div>
				</td>
				<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
				<div style="font-size:10;"> '.$challan_number.'</div>
				</td>
				</tr>
				
				
				</tbody>
				</table>
				<table style="width: 100%; border: 1 px solid #000; border-spacing: 0pt;">
				<tbody>
				<tr>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Sr. No.</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Brand</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Product</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Model</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Battery Serial No</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Complaint No.</td>
								<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Approximate Scrap Value</td>
				</tr>';
				$i=1;
				foreach($data as $challanlist){ 
					$challanlist1 = $challanlist['total_amt'];
					 $var.= '<tr>
						<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$i.'</td>
						<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['brand'].'</td>
										<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['product'].'</td>
										<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['model'].'</td>
										<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['serial_number'].'</td>
										<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$challanlist['complaint_no'].'</td>
										<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.'Rs. '.number_format($challanlist1,2, '.', '').'</td>
						</tr>';
					//$sum += $challanlist1;
					$sum +=number_format($challanlist1,2, '.', '');
					$i++;
				}
				
				$var .='<tr>
				<td colspan="5"> </td>
				
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align:center;">'.'Total Rs. </td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align:center;">'.'Rs. '.number_format($sum,2, '.', '').'</td>
				</tr>
				</tbody>
				</table>
				<table style=" width: 100%; border: 0pt none; border-spacing: 0pt;">

		<tr>	
		<ul  style="border:1 px solid #000;">
		
			<li style=" list-style-type: none;"><b>Special Remarks:</b></li>
			<li>The value declared is only for transit and insurance purpose only</li>
			<li>Goods covered under this Challan must be examined & verified by the consignee for quality, Transit Damage, condition of packages & shortage if any, and
			in case the package is delivered in damage condition or with the seal broken or missing or with seal other than as stated in the shipping documents or any
			shortage than it should be mentioned in the carrier (GR) or any such carrier documents and also informed to the nearest LETPL office immediately within 24
			hours., LETPL should be immediately informed on the receipt of material.</li>
			<li style=" list-style-type: none;"><h4>Warranty replacement - Not for Sale 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;E. & O.E</h4></li>
		</ul>
</tr>
</table>
<table style=" width: 100%; border: 0pt none; border-spacing: 0pt;">
<tr>
<td style="font-weight: bold;  padding: 0px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">
Customer Signature<br/>
Customer Name & Seal Date<br/>
</td>
<td style="font-weight: bold;  padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">FOR '.strtoupper($data[0]['distributor_name']).'<br/>
AUTHORISED SIGNATORY</td>
</tr>
</table>
				';
				//echo  $var; die;
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml($var);
           // $pdf->Image('images/pdf-header.jpg',0,0);
			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'landscape');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream($challan_number);
		}
		else{
				?>
				<script>
				window.alert("Sorry ! No Invoice Found.");
				window.location.href='<?php echo base_url();?>index.php/fault_parts/views_challan';
				</script>
				<?php
			
		}
		 
	 }
	
	 
	function generate_challan($id)
	{
		 $id = base64_decode($id);
		 $challan_number = "CHL".date('YmdHis');
	  //  $pname = $_POST['name'];
		//$from = $_POST['brand']; die;

	   $this->load->model('fault_parts_model');
		$data = $this->fault_parts_model->get_challan_details($id);
		
		 // $data = $this->fault_parts_model->get_date_challan($from, $to, $pname);
		//echo "<pre>"; print_r($data); die;
		
		//$data['det'] = $this->fault_parts_model->get_customer_det($id);
		//echo "<pre>"; print_r($data); die;
		// $this->load->view('services/generate_challan_view', $data);
		if(!empty($data))
		{
			$date = date("Y-m-d");
			$dates = date("Ymdhis");
			$inv = "INV".$dates;
			$timestamp = strtotime($datesss);			
			$dmy = date("d-m-Y", $timestamp);
			//echo $dmy;
			$ab_i = "http://13.228.144.245/livguard_dms/images/liv.png";
			 $var = '

			 
<table style="width: 100%; font-family: Arial; text-align: center;" border="0" cellspacing="2" cellpadding="2">
<tbody style="text-align: left;">
<tr style="text-align: left;">
<td style="text-align: left;">
<img src="'.$ab_i.'" alt="LIVGUARD LTD" /></p>
</td>
</tr>
<tr style="text-align: left;">
<td style="font-weight: bold; text-align: left;">

</td>
</tr>
<tr style="text-align: left;">
<td style="text-align: left;"> </td>
</tr>
<tr style="text-align: left;">
<td style="text-align: left;">
<h1>CHALLAN</h1>
</td>
</tr>
</tbody>
</table>
<p style="font-family: Arial; text-align: center;"> </p>
<table style="text-align: center; width: 100%; border: 0pt none; border-spacing: 0pt;">
<tbody style="text-align: left;">
<tr style="text-align: left;">
<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Prepared For</td>
<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">CONSIGNEE NAME &amp; ADDRESS</td>
</tr>
<tr style="text-align: left;">
<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
	<div>'.strtoupper($data[0]['name']).'</div>
	<div>'.strtoupper($data[0]['billing_address_city']).', '.strtoupper($data[0]['billing_address_state']).'</div>
	<div>Postal Code -  '.$data[0]['billing_address_postalcode'].'</div>
	<div>'.strtoupper($data[0]['billing_address_country']).'</div>
</td>
<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
<div> <br />LIVGUARD ENERGY TECHNOLOGIES PVT. LTD. - AHM C/O BENCHMARK SUPPLY CHAIN SOLUTIONS PVT. LTD., PLOT NO. 47/48, RK WAREHOUSING SOCITY, BEHIND ALFA HOTEL, TALUKODUSKOI ASLALI, <br />Compans TIN No : 24075501516 <br />Companyies CST No : 24575501516 <br />Companyies PAN No : AACCL6687P</div>
</td>
</tr>
<tr style="text-align: left;">
<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Challan Date</td>
<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;">Challan Number</td>
</tr>
<tr style="text-align: left;">
<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
<div>'. date("d-m-Y",  strtotime($data[0]['invoice_date'])).'</div>
</td>
<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;">
<div> '.$data[0]['number'].'</div>
</td>
</tr>
<tr style="text-align: left;">
<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;"> </td>
<td style="font-weight: bold; background-color: #b0c4de; padding: 2px 6px; border-style: solid; border-width: .5px; vertical-align: top; text-align: left; width: 50%;"> </td>
</tr>
<tr style="text-align: left;">
<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;"> </td>
<td style="padding: 2px 6px; border-style: solid; border-width: .5px; width: 50%; vertical-align: top; text-align: left;"> </td>
</tr>
</tbody>
</table>
<p> </p>
<table style="width: 100%; border: 0pt none; border-spacing: 0pt;">
<tbody>
<tr>
<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Brand</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Product</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Model</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Battery Serial No</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Complaint No.</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Phone Office</td>
				<td style="border-style: solid; background-color: #b0c4de; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: center;">Approximate Scrap Value</td>
</tr>
<tr>
<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$data[0]['brand_c'].'</td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$data[0]['product_c'].'</td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$data[0]['model_c'].'</td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$data[0]['battery_serial_num_c'].'</td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$data[0]['complaint_no_c'].'</td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.$data[0]['phone_office'].'</td>
				<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: normal; text-align: center;">'.'Rs. '.$data[0]['total_amt'].'</td>
</tr>
<tr>
<td colspan="5"> </td>
<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align: right;">Total</td>
<td style="border-style: solid; border-width: .5px; padding: 2px 6px; font-weight: bold; text-align:center;">'.'Rs. '.$data[0]['total_amt'].'</td>
</tr>
</tbody>
</table>



				 
				 ';
			//	echo  $var; die;
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml($var);
           // $pdf->Image('images/pdf-header.jpg',0,0);
			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'landscape');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Can't Generate Invoice this time.')
					window.location.href='http://13.228.144.245/livguard_dms/index.php/fault_parts/view_challan';
					</SCRIPT>");
		}
	}
	
}


?>
