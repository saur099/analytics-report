<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('application/libraries/vendor/autoload.php'); 
use Dompdf\Dompdf;
class Confirm extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$this->load->model('orders');
		$distributor_name = $this->session->userdata['logged_in']['username'];
		$distributor_code = $this->session->userdata['logged_in']['user_uuid']; 
		//echo $distributor_code; die;
		$data['confirm_order'] = $this->orders->get_confirm_orders($distributor_code); 
		$this->load->view('confirm_order/confirm_order_view', $data);

	}
	
	public function view_confirm_order($id, $type)
	{
		$distributor_name = $this->session->userdata['logged_in']['username'];
		$id = base64_decode($id);
		$this->load->model('orders');
		$distributor_reference_no = $this->session->userdata['logged_in']['username'];
		$distributor_code = $this->session->userdata['logged_in']['user_uuid']; 
		$data['order_details'] = $this->orders->getorderbyid($id,$distributor_code,$distributor_reference_no);
		$data['dispatch_details'] = $this->orders->get_all_product_dispatch_details($id,$distributor_code);
		$data['rejected_details'] = $this->orders->get_all_product_rejected_details($id,$distributor_code);
		
		if(!empty($type) && $type == 'invoice_pdf'){
			$data['distributor_invoice'] = $this->orders->getdistributor_details($distributor_code);
			$data['dealer_invoice'] = $this->orders->getdealer_details($id, $distributor_code);
			$data['product_details'] = $this->orders->getproduct_details($id, $distributor_code);
			// $data['pricing'] = $this->orders->getpricing_details($id, $distributor_code);
			// echo $data['distributor_invoice'][0]->name;		die;	
			//echo "<pre>"; print_r($data['product_details']); die;
			$dated = date('d-m-Y');
			$newDate = date("d-m-Y", strtotime($data['product_details'][0]->order_date_entered));
			$rd = 
			$var = '<!DOCTYPE html>
			<html>
			   <head>
				  <title> Delivery Challan</title>
			   </head>
			   <body>
				  <div>
					 <!-- start table -->
					 <table width="100%" border="1" align="center" >
						<!-- start table Heading -->
						<tr>
						   <th colspan="2" align="center">Delivery Challan</th>
						</tr>
						<!-- end table Heading -->
						<tr>
							<td>Challan No:- '.$data['product_details'][0]->order_id.'</td>
							<td>Challan Date :- '.$dated.'</td>
						</tr>
						<tr>
						   <td align="center" width="50%"> 
							  BY
						   </td>
						   <td align="center" width="50%">
							  Customer Nmae & Delivery Address
						   </td>
						</tr>
						<tr>
						   <td>'.$data['distributor_invoice'][0]->name.'('.$distributor_name.')<br>
							  '.$data['distributor_invoice'][0]->billing_address_address1_c.'<br>
												
							  Email :- '.$data['product_details'][0]->email_address.'<br>
							  Mobile :- '.$data['distributor_invoice'][0]->phone_office.'<br>
							  GSTIN NO :- <br>
							  CIN NO :- <br>
						   </td>
						   <td>'.$data['dealer_invoice'][0]->name.'('.$data['product_details'][0]->dealer_code.')<br>
							  '.$data['dealer_invoice'][0]->billing_address_address1_c.'<br>
							  Mobile :- '.$data['dealer_invoice'][0]->phone_office.'<br>
							  PAN No :- <BR>
							  GSTIN No :- <br>
							  Order No :- '.$data['product_details'][0]->order_id.'<br>
							  Order Date :- '.$newDate.' <br>
						   </td>
						</tr>
					 </table>
					 <!-- end table  -->
					 <!-- start table  -->
					 <table width="100%" border="1" align="center">
						<!-- start table row-->
						<tr>
						   <td align="center">	S No</td>
						   <td align="center">SKU Code / Description</td>
						   <td align="center">Serial No</td>
						   <td align="center">HSN Code </td>
						   <td align="center">Qty (NO.s) </td>
						   <td align="center">Unit Price (INR) </td>
						   <td align="center">Cash Discount(%) </td>
						   <td align="center">Total Basic Amount </td>
						   <td align="center">CGST </td>
						   <td align="center">SGST </td>
						   <td align="center">Total Amount (Incl. Tax) </td>
						</tr>
						<!-- start table row-->
						<!-- end table row -->
						';
						$i = 1;
						$s = 0;
						$d = 0;
						$f = 0;
						$e = 0;
						$g = 0;
						$h = 0;
						$price = 0;
						foreach($data['product_details'] as $v)
						{
							$dispatch_serial_no1 = explode(',',$v->dispatch_serial_no);
							for($k=0; $k<count($dispatch_serial_no1); $k++)
							{
								$tb = ($v->actual_price)*($v->discount) / 100;
								$total_basic = ($v->actual_price) - $tb;
								$priceCGST = ($total_basic)*($v->CGST)/100;
								$priceSGST = ($total_basic)*($v->SGST)/100;
								$finalPricing = $total_basic + $priceCGST + $priceSGST;
								$s = $s+($total_basic);
								$d = $d + $finalPricing;
								$f = $f + ($v->actual_price);
								$e = $e + $priceCGST;
								$g = $g+$priceSGST;
								$h = $h+$tb;
								$price = $price+1;
								/*
								$v->actual_price
								$v->discount
								$total_basic
								number_format((float)$priceCGST, 2, '.', '')
								number_format((float)$priceSGST, 2, '.', '')
								$finalPricing
								*/
								$var .='<tr>
									   <td align="center">'.$i.'</td>
									   <td align="center">'.$v->model_code.'</td>
									   <td align="center">'.$dispatch_serial_no1[$k].'</td>
									   <td align="center">'.$v->HSN_CODE.'</td>
									   <td align="center">1</td>
									   <td align="center"></td>
									   <td align="center"></td>
									   <td align="center"></td>
									   <td align="center"></td>
									   <td align="center"></td>
									   <td align="center"></td>
									</tr>';
								$i++;
							}	
						}			
						/*
						$price
						$f
						$h
						$s
						$e
						$g
						$d
						*/	
						/*
						Amount In Rupees ='.round($d).'.00
						*/						
						$var .='<tr>
						   <td></td>
						   <td colspan="2" align="center"><b>Total</b></td>
						   <td align="center"></td>
						   <td align="center"></td>
						   <td align="center"></td>
						   <td align="center"></td>
						   <td align="center"></td>
						   <td align="center"></td>
						   <td align="center"></td>
						   <td align="center"><b></b></td>
						</tr>
					 </table>
					 <table width="100%" border="1" align="center">
						<tr>
						   <td> <u>Amount In Rupees</u> :- <b>  </b>
							  Total Amount (Inclusive Tax)
												<br>
												<br>
							  Remarks :-
						   </td>
						   <td> 
						   FOR <b> '.$data['distributor_invoice'][0]->name.'</b>
						   <br/>
						   <br/>
							    Authorized Signatory :-
						   </td>
						</tr>
						<tr>
						   <td colspan="2">
								Declaration: We declare that is this invoice shows the actual price of goods described and the all particulars true and correct
						   </td>
						</tr>
						<tr>
						   <td colspan="2" align="center">
							 <b>Address</b> : '.$data['distributor_invoice'][0]->billing_address_address1_c.'
							<br/>
							  Phone Number: '.$data['distributor_invoice'][0]->phone_office.'
						   </td>
						</tr>
					 </table>
					 <!--end Table -->
				  </div>
			   </body>
			</html>
			';
					
					$dompdf = new Dompdf();
					$dompdf->loadHtml($var);
					// $pdf->Image('images/pdf-header.jpg',0,0);
					// (Optional) Setup the paper size and orientation
					$dompdf->setPaper('A4', 'landscape');

					// Render the HTML as PDF
					$dompdf->render();

					// Output the generated PDF to Browser
					$dompdf->stream();
					}
					
		$this->load->view('confirm_order/confirm_orderdetails_view', array('data' => $data,'id' => $id));

	}
	
	public function confirm_dispatch($ids)
	{ 
		$data = $ids;
		$this->load->view('order_dispatch', array('data' => $data));
	}
	public function confirm_order_dispatch()
	{ 
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$dispatch = $data['dispatch'];
		$this->load->model('orders');
		$this->orders->update_order($order_id, $dispatch);
		
	}
	
	public function confirm_order($ids)
	{
		$this->load->model('orders');
		$num0 = (rand(10,100));
        $num1 = date("ymd");
        //$num2 = (rand(100,1000));
       // $num3 = time();
    $data = "OR".'#'.$num0 . $num1;
	$this->orders->update_order_id($ids, $data);
		$this->load->view('confirm_order', array('data' => $data));
	}
	
	public function reject_order($ids)
	{
		$data = $ids;
		$this->load->view('reject_order', array('data' => $data));
	}
	
	public function reject_order_details()
	{ 
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$reject = $data['reject'];
		$this->load->model('orders');
		$this->orders->update_order_rejection_details($order_id, $reject);
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */