<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tertiary extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }

	
	public function index()
	{
		
		$this->load->model('tertiarys');
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$data = $this->tertiarys->get_tertiary_data($distributor_code);
		
		
		 $this->load->view('tertiary_view', array('data' => $data));
	}
	
	public function tertiary_upload(){
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$username  = $get_session_data['username'];
		$this->load->model('tertiarys');
		$data['res'] = $this->tertiarys->get_cust_details($user_uuid); 
		//echo "<pre>"; print_r($data); die;
		$this->load->view('tertiary_stock_view', $data);
	}
	
	function upload_excel(){
		$this->load->model('tertiarys');
		$data = $this->tertiarys->get_secondary_stock1($distributor_code);
		//echo "<pre>"; print_r($data);
		$this->load->view('tertiary_upload_excel_view',array('data' => $data));
	}
	
	public function tertiary_upload1(){
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$username  = $get_session_data['username'];
		//if(isset($_POST['submit'])){
			// validate to check uploaded file is a valid csv file
			$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
			//echo "<pre>"; print_r($_FILES);
			if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$file_mimes)){
				if(is_uploaded_file($_FILES['file']['tmp_name'])){
					$csv_file = fopen($_FILES['file']['tmp_name'], 'r');
					$array_key = array();
					while($head = fgetcsv($csv_file)){
						foreach($head as $v) $array_key[] = $v;
						break;
					}
					$data = array();
					$i = 0;
					while($emp_record = fgetcsv($csv_file)){
						foreach($emp_record as $k=>$v) $data[$i][$array_key[$k]] = $v;
						$i++;
					}
					fclose($csv_file);
					if(!empty($data)){
						$this->load->model('tertiarys');
						$data = $this->tertiarys->insert($data, $user_uuid);
					}
					//$import_status = '?import_status=success';
					redirect('tertiary/tertiary_upload');
				} else {
					$import_status = '?import_status=error';
				}
			} else {
				$import_status = '?import_status=invalid_file';
			}
			//}
	}
	
	public function download_excel(){
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$username  = $get_session_data['username'];
		
		$data = array(array('shiv shaankar','sharma','9818540181','shivshankar@gmail.com','110086','nangloi','LIVFAST 4W','A1CABEMA01343A','12-03-2018','123456789','LFTB1AANA090L1','BAT013836','12-03-2018','Flooded','FOC','12-03-2018','17-03-2018','17-03-2018','17-03-2018','','','',''));
		
		//echo "<pre>";
		//print_r($data); die;
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=\"data-list".".csv\"");
		header("Pragma: no-cache");
		//header("Expires: 0");

		$handle = fopen('php://output', 'w');
		$data = array_merge(
			array(
				array(
					'consumer last name',
					'consumer last name',
					'consumer mobile',
					'consumer email',
					'postal code',
					'address',
					'product name',
					'product serial number',
					'purchase date',
					'customer invoice number',
					'sap code',
					'dealer code',
					'Primary billing date',
					'Battery Type',
					'warranty status',
					'warranty start date',
					'warranty end date',
					'pro rata warranty start date',
					'pro rata warranty end date',
					'application fitment',
					'manufacturer',
					'vehicle model',
					'vehicle registration number',
					
				)
			),
			$data
		);
		foreach ($data as $data) {
			fputcsv($handle, $data);
		}
			fclose($handle);
		exit;

	}
	
	
}
