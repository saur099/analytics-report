<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting('E_ALL');
class Service_cases extends CI_Controller 
{
    public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
	     $this->load->model('cc_customer_model');
	     $this->load->model('call_center_model');
             $this->load->model('service_case_model');
             $this->load->model('cc_product_model');
    }
    
     
			
	public function index()
	{
		//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		//echo "hii"; die;
		$this->load->model('service_case_model');
		$data['res'] = $this->service_case_model->get_call_details($user_uuid);
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/service_mycases_view', $data);
	}
        
        public function battery_complaints()
	{
		//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		//echo "hii"; die;
		$this->load->model('service_case_model');
		$data['res'] = $this->service_case_model->get_call_details_battery($user_uuid);
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/service_mycases_view', $data);
	}
        
	function check_cust_purchase()
	{
		//echo "jsdvbg\dsbf";
		
		$purchase_date  =  $_POST['pur_date'];
		$customer_id 	=  $_POST['cust_id'];
		$product_serial =  $_POST['prod_sr'];
		$model_code     =  $_POST['mdl_code'];
		$warranty_end_months =  $_POST['warranty_end'];
		$pro_rata_months     =  $_POST['pro_rata']; 
		//echo "----";
		
		//
		//$effectiveDate = '2018-01-01';
		
		$war_end_date = date('Y-m-d', strtotime("+ $warranty_end_months months", strtotime($purchase_date)));
		$war_end_date11 = date('Y-m-d', strtotime($war_end_date. ' - 1 days'));	
		$war_end_date1 = strtotime($war_end_date11);		
	
		//Convert it to DD-MM-YYYY
		$war_end_date_res = date("d-m-Y", $war_end_date1);
		

		//$pro_rta_start_date = date('d-m-Y', strtotime($war_end_date_res. ' + 1 days'));	
		
		//$pro_rate_warr_enddate = date('d-m-Y', strtotime($war_end_date_res. ' + 1 days'));
		if($pro_rata_months == '0')
		{
			$pro_rta_start_date = '';
			$pro_rate_warr_enddate = '';
		}
		else
		{ 
			$pro_rta_start_date = date('d-m-Y', strtotime($war_end_date_res. ' + 1 days'));	
			
			$pro_rate_warr_enddate = date('d-m-Y', strtotime("+ $pro_rata_months months", strtotime($pro_rta_start_date)));
			$pro_rate_warr_enddate = date('d-m-Y', strtotime($pro_rate_warr_enddate. ' - 1 days'));
		}
			
		

		//echo $pro_rta_start_date = $war_end_date_res + 1;
		//Echo it
		//echo $dmy;
		//
		
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('service_case_model');
		$data['res'] = $this->service_case_model->check_primary_date($purchase_date, $customer_id, $product_serial);
		$pri_sale_date = $data['res'][0]->primary_sale_date;
		$primary_sale_date = date("d-m-Y", strtotime($pri_sale_date));  //change date format
		//////////////
		//print_r($data['res']); die("11");
		$data['res_dt'] = $this->service_case_model->get_warranty($purchase_date,$model_code);
		$warr_inMonth = $data['res_dt'][0]->WARRANTY_IN_MONTHS; 
		$pro_ratawarr = $data['res_dt'][0]->PRO_RATA_WARRANTY;
		$currentDate = date('d-m-Y');
		
		$date11=date_create($purchase_date);
		$date22=date_create($currentDate);
		$diffDt=date_diff($date11,$date22);
		$warrantyInDays=($warr_inMonth*30);
		$prorataInDays=($pro_ratawarr*30);
		$proInDays=$prorataInDays+$warrantyInDays;
		$differenceOf=$diffDt->format("%a");

		/* 
		$date = str_replace('/', '-', $purchase_date);
		$pr_date = date('Y-m-d', strtotime($date));
		
		$date1 = new DateTime($date1);
		$date2 = $date1->diff(new DateTime($letpl_date));
		$ageingCase = $date2->days;

		echo "Warr : ".$warr_inMonth."<br/>";
		echo "ProWarr : ".$pro_ratawarr."<br/>";
		echo "curr : ".$currentDate."<br/>";
		echo "purchase_date : ".$purchase_date."<br/>";
		echo "pr_date : ".$pr_date."<br/>";
		echo "date11 : ".$date11."<br/>";
		echo "date1 : ".$date1."<br/>";
		echo "date2 : ".$date2."<br/>";
		echo "date22 : ".$date22."<br/>";
		echo "diffDt : ".$diffDt."<br/>";
		echo "ageingCase : ".$ageingCase."<br/>";
		echo "warrantyInDays : ".$warrantyInDays."<br/>";
		echo "prorataInDays : ".$prorataInDays."<br/>";
		echo "proInDays : ".$proInDays."<br/>";
		echo "differenceOf : ".$differenceOf."<br/>";
		die("Saurav Test");   
		 */
 
		/*if($differenceOf<=$warrantyInDays){
			$msg = "FOC";
		}elseif($differenceOf>=$warrantyInDays && $differenceOf<=$proInDays){
			$msg = "Pro-Rata Warranty";
		}else{
			$msg = "Out Of Warranty";
		}
		*/
		//get warranty start date
			$wrnty_start_date = $pur_date;
			$currentDate_now = date('d-m-Y');
			
			//get warranty end date
			$war_end_date = date('Y-m-d', strtotime("+ $warranty_end_months months", strtotime($pur_date)));
			$war_end_date11 = date('Y-m-d', strtotime($war_end_date. ' - 1 days'));	
			$war_end_date1 = strtotime($war_end_date11);		 
			//Convert it to DD-MM-YYYY
			$war_end_date_res_ash = date("m/d/Y", $war_end_date1);

			//get pro rata start date
			$pro_rta_start_date_ash = date('m/d/Y', strtotime($war_end_date_res_ash. ' + 1 days'));
		
			//get pro rata end date
			//$pro_rate_warr_enddate = date('d-m-Y', strtotime($war_end_date_res. ' + 1 days'));
			$pro_rate_warr_enddate_ash = date('m/d/Y', strtotime("+ $pro_rata_months months", strtotime($pro_rta_start_date_ash)));
			$pro_rate_warr_enddate_ash = date('m/d/Y', strtotime($pro_rate_warr_enddate_ash. ' - 1 days'));
			
			// calculate the warranty status
			$cust_purchase_date = strtotime($currentDate_now);
            $warranty_end_date = strtotime($war_end_date_res); 
            $pro_rata_warranty_end_date = strtotime($pro_rate_warr_enddate); 
			
		if($cust_purchase_date <= $warranty_end_date)
			{
				 $msg = "FOC";
			}
			else if($cust_purchase_date > $warranty_end_date && $cust_purchase_date <= $pro_rata_warranty_end_date)
			{
				$msg = "Pro-Rata Warranty";
			}
			else 
			{
				$msg = "Out Of Warranty";
			}
		//print_r($msg);
		///shiv
		
		///////////////
		$date1=date_create($purchase_date);
		$date2=date_create($primary_sale_date);		
		$diff=date_diff($date1,$date2);	//get diff btw purchase_date and primary_date		
		$curdate = date_create(date("d-m-Y"));
		$diff1=date_diff($date1,$curdate);	// get diff btw purchase_date and primary_date
		//echo $diff->format("%R%a");
		$arr = array(
			'msg'=>$msg,
		    'diff'=>$diff->format("%R%a"),
			'diff1'=>$diff1->format("%R%a"),	 
			'purchase_date'=>$purchase_date,	 
			'war_end_date_res'=>$war_end_date_res,
			'pro_rta_start_date'=>$pro_rta_start_date,
			'pro_rate_warr_enddate'=>$pro_rate_warr_enddate
		);
		echo json_encode($arr);
		//print_r($arr);
		//echo $primary_sale_date;
		//print_r($data['res'][0]->primary_sale_date);
	}
	public function get_vehicle()
	{
		 $prod_val = $this->input->post('prod_val'); 
		 $this->load->model('service_case_model');
		 $vehicle_list = $this->service_case_model->get_fitment_vehicle($prod_val);
		echo json_encode($vehicle_list);
	}
	public function get_vehicle_model()
	{
		 $veh_val = $this->input->post('veh_val');  //die;
		//echo $veh_val;
		 $this->load->model('service_case_model');
		 $vehicle_model = $this->service_case_model->get_vehicle_no($veh_val);
		echo json_encode($vehicle_model);
	}
        function update_product_batteries()
        {
                $consid                 = trim(stripslashes($_POST['consid']));
                $js_no                  = trim(stripslashes($_POST['js_no']));
                $complaint_type 	= trim(stripslashes($_POST['complaint_type']));
                $callProductSerialNo 	= trim(stripslashes($_POST['callProductSerialNo']));
                $service_call_id 	= trim(stripslashes($_POST['service_call_id']));
                $complain_type          = trim(stripslashes($_POST['complain_type']));
                $warrantyVoids  	= trim(stripslashes($_POST['warrantyVoids']));
                $rescheduled_date 	= trim(stripslashes($_POST['rescheduled_date']));
                
                if($warrantyVoids == 'Yes'){ $wV = "FOC";  }
                else { $wV = "Out of Warranty"; }
                    
                
                $upd_arr = array(
                                    'callType' => $complain_type,
                                    'warranty' => $complain_type,
                                    'se_estd_visit_date' => $rescheduled_date
                                );
                $updResult   =   $this->cc_customer_model->update_serial_data($upd_arr, $service_call_id);
                
                if($updResult > 0)
                {
                    $enc = base64_encode($service_call_id);
                   echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Success ! Please submit the test report to go ahead.')
				window.location.href='".base_url()."index.php/fault_parts/index/$enc';
				</SCRIPT>");
                }
                else
                {
                    echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some problem occurred. Please try again.')
				window.location.href='add_product/$consid/$js_no';
				</SCRIPT>");
                }
        }
        function get_product_battery()
        {
            	$serial 	= trim(stripslashes($_POST['productSerialNo']));
                $verRes 	= $this->cc_product_model->verify_serial_no($serial);
                //print_r($verRes); die;

                if($verRes == 'x') 
                {
                        $this->session->set_flashdata('error', '<div class="alert alert-danger"><strong>Sorry! </strong>The product serial does not exist. Please try another.</div>');
                        redirect($_SERVER['HTTP_REFERER']);
                }

                else if($verRes == 'y') 
                {
                    $this->session->set_flashdata('error', '<div class="alert alert-danger"><strong>Sorry! </strong>The product is already registered with other customer. Please try another.</div>');
                                redirect($_SERVER['HTTP_REFERER']);
                }

                else if($verRes =='z')
                {
                    $this->session->set_flashdata('error', '<div class="alert alert-danger"><strong>Sorry! </strong>The Serial Number is blocked in our system.</div>');
                                redirect($_SERVER['HTTP_REFERER']);
                }

                $js_no = trim(stripslashes($_POST['js_no']));
                if(!empty($js_no)) 
                {
                    $data['js'] = array('js_no' => base64_decode($js_no )); 
                }

                    $user_uuid 			= $get_session_data['user_uuid'];
                    $user 			= $get_session_data['username'];
                    $consid 			= $_POST['consid'];
                    $data['res']  		= $this->service_case_model->get_prod_ser_data($serial);
                    $data['dist'] 		= $this->service_case_model->get_dist_dealer($user_uuid);
                    $data['js_info']  =      $this->cc_customer_model->getJS_Prod_Battery(base64_decode($js_no ));
                    $data['cust']    =      $this->cc_customer_model->get_consumer_detail($consid); 
                    $data['secDate']            = $this->service_case_model->get_secondary_date($serial);
                    $data['pro'] 		= $this->service_case_model->get_fitment_prod();
                    $data['serial'] 	= $serial;
                    // echo '<pre>';print_r($data['consumer']);exit;
                    $this->load->view('services/get_product_view', $data);
                
        }
	
	function get_product()
	{
		
	
		//print_r($_POST);exit;
	    $get_session_data 	= $this->session->userdata('logged_in');
	    $user_uuid 			= $get_session_data['user_uuid'];
	    $user 				= $get_session_data['username'];
		$consid 			= $_POST['consid'];

		if(empty($consid))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! The consumer Id is null.. Please try again.')
				window.location.href='add_product';
				</SCRIPT>");
		}

        $js_no = trim(stripslashes($_POST['js_no']));
        if(!empty($js_no))
        {
            $data['js'] = array('js_no' => base64_decode($js_no )); 
        }
		$serial = $_POST['productSerialNo'];
		$this->load->model('service_case_model');
		$data['cons'] = array('consid' => $consid ); 
		
		$data['res'] = $this->service_case_model->get_product_serial($serial, $user);
		 //print_r($data['res']); die("Hello");
		$data['re']  = $this->service_case_model->get_prod_ser($serial);
		$status  = $this->service_case_model->get_product_status($serial);
		$reject  = $this->service_case_model->get_reject_status($serial);
		$data['secDate'] = $this->service_case_model->get_secondary_date($serial);
		$data['pro'] = $this->service_case_model->get_fitment_prod();
		if($status != "0")
		{
			 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('This Battery was replaced against some other Battery, please contact with Service Engineer for details.')
				window.location.href='add_product/$consid';
				</SCRIPT>");

		}

		if(empty($data['res']))
		{
			 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Search Not Successful.. !! Please Contact Administrator')
				window.location.href='add_product/$consid';
				</SCRIPT>");
		}
		if($data['re'] != "0")
		{
			 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sale of this Battery is already registered in system.')
				window.location.href='add_product/$consid';
				</SCRIPT>");

		}
		
		if($reject == "0")
		{
			 echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('This Battery is already rejected & cannot be processed again.')
				window.location.href='add_product/$consid';
				</SCRIPT>");

		}
		
		$data['dist'] = $this->service_case_model->get_dist_dealer($user_uuid);
		$this->load->view('services/get_product_view', $data); 
	}
	
						
	function get_more_product()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		/* echo "Hii"; 
		echo $user_uuid; 
		die; */
	 	$consid = $_POST['consid'];
	  if(empty($consid))
	  {
		 echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! The consumer Id is null.. Please try again.')
			window.location.href='add_product';
			</SCRIPT>");
	  }
	  else
	  {
			$serial = $_POST['productSerialNo'];
			$this->load->model('service_case_model');
			$data['cons'] = array('consid' => $consid ); 
			
			$data['res'] = $this->service_case_model->get_product_serial($serial, $user_uuid);
			//echo "<pre>"; print_r($data['res']); die;
			$data['re']  = $this->service_case_model->get_prod_ser($serial);
			$status      = $this->service_case_model->get_product_status($serial);
			$reject      = $this->service_case_model->get_reject_status($serial);
			$data['secDate'] = $this->service_case_model->get_secondary_date($serial);
			$data['pro'] = $this->service_case_model->get_fitment_prod();
			
			//echo "<pre>"; print_r($reject); die;
			if($reject == "0")
			{
				 echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Search not successful, Please contact administrator..')
					window.location.href='add_product/$consid';
					</SCRIPT>");

			}
			else
			{	
					if($status != "0")
					{
						 echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('This Battery was replaced against some other Battery, please contact with Service Engineer for details.')
							window.location.href='add_product/$consid';
							</SCRIPT>");

					}
					else
					{
						if($data['re'] == 0)
						{
							//echo "<pre>"; print_r($data['res']); 
							//echo "<pre>"; print_r($data['re']); 
							//echo "<pre>"; print_r($status); 
							//echo "<pre>"; print_r($reject); 
						//	echo "<pre>"; print_r($data['pro']); die;
							//die("jn");
							if(empty($data['res']))
							{
								 echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Search Not Successful.. !! Please Contact Admin.')
									window.location.href='add_product/$consid';
									</SCRIPT>");
							}
							if($data['re'] != "0")
							{
								 echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Sale of this Battery is already registered in system.')
									window.location.href='add_product/$consid';
									</SCRIPT>");

							}
							
							// if($reject != "0")
							// {
								 // echo ("<SCRIPT LANGUAGE='JavaScript'>
									// window.alert('This Battery is already rejected & cannot be processed again.')
									// window.location.href='add_product/$consid';
									// </SCRIPT>");

							// }
							
								/* 
									echo "<pre>"; print_r($data['res']); die;
									echo " Hii ".$consid."     NBSVV:   ".$serial;  die;  
								*/
								$data['dist'] = $this->service_case_model->get_dist_dealer($user_uuid);
								// echo "<pre>"; print_r($data['dist']); die;                    
								$this->load->view('services/get_more_prod_view', $data);
						}
						else
						{
							 echo ("<SCRIPT LANGUAGE='JavaScript'>
									window.alert('Sale of this Battery is already registered in system.')
									window.location.href='add_product/$consid';
									</SCRIPT>");
						}
					}
			}
	  }	
	}
	
	
	function create_get_product()
	{	 
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		//$this->input->post('custPurchaseDate'); die;
		$badh = $this->input->post('custPurchaseDate');// die;
		$test_arr  = explode('-', $badh);
		$consid = $this->input->post('consid');
	
					$date = date("Y-m-d H:i:s");  
					
					function getGUID()
						{ 
							if (function_exists('com_create_guid'))
							{
								return com_create_guid();
							}
							else
							{
								mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
								$charid = strtoupper(md5(uniqid(rand(), true)));
								$hyphen = chr(45);// "-"
								$uuid = substr($charid, 0, 8).$hyphen
									.substr($charid, 8, 4).$hyphen
									.substr($charid,12, 4).$hyphen
									.substr($charid,16, 4).$hyphen
									.substr($charid,20,12);
								return $uuid;
							}
						}
							
							
					$guid = getGUID();
				/////////////////tbl_service_products
					$query = $this->db->query("SELECT productId FROM tbl_service_products WHERE consumerId='".$this->input->post('consid')."' 
					AND productSerialNo='".$this->input->post('productSerialNo')."'");
					$queryCust = $this->db->query("SELECT * FROM tbl_service_customers WHERE cust_code='".trim($this->input->post('consid'))."'");
						$rowCust = $queryCust->result();
								
					/************************************ Product cat ID & Product Id Started here *************************************/	

						$queries = $this->db->query("SELECT a.id AS product_id, d.id AS category_id
													FROM aos_products a
													LEFT JOIN aos_product_categories d ON a.aos_product_category_id = d.id
													WHERE a.deleted =0
													AND a.part_number = '".$this->input->post('model')."'
													LIMIT 1");

					//echo $this->db->last_query();
					$row_pr = $queries->result();
					//print_r($row_pr); 
					$pr_product_id = $row_pr[0]->product_id;
					$pr_category_id = $row_pr[0]->category_id;
					//die(" - Query");
			
				/************************************ Product cat ID & Product Id Ended here *************************************/					 
							
					$row = $query->num_rows();
								
					if ($row>0)
					{
						$arr = 'err';	
					}
					else
					{
						$dataCust =  array(
		                      'createdBy'=>  $user_uuid,
		                      'fname'=>  $rowCust[0]->fname,
		                      'lname'=>  $rowCust[0]->lname,
		                      'cust_code' => $this->input->post('consid'),
		                      'mobile'=>  $rowCust[0]->mobile,
		                      'alt_mobile'=>  $rowCust[0]->alt_mobile,
		                      'address'=>  $rowCust[0]->address,
		                      'landmark'=>  $rowCust[0]->landmark,
		                      'state'=>  $rowCust[0]->state,
		                      'city'=>  $rowCust[0]->city,
		                      'area'=>  $rowCust[0]->area,
		                      'pincode'=>  $rowCust[0]->pincode,
		                      'productCreatedOn'=>  $date,
		                      'isActive'=>  $rowCust[0]->isActive,
		                      'custAssetGuid'=> $guid,
							  'datetime' => date('d-m-Y h:i:s'),
							   'custAssetGuid' => $guid,
							  'cust_product_id' => $this->input->post('productSerialNo'),
							  'custPurchaseDate' => $this->input->post('custPurchaseDate'),
							  'warrantyStatus' => $this->input->post('waranyStatus'),
							  'productType' => $this->input->post('protype'),
							  'model' => $this->input->post('model')
    					   );		
						   
						$dataServiceProducts = array(
												'productId' => $this->input->post('productId'),
												'productGuid' => $guid,
												'consumerId' => $this->input->post('consid'),
												'userId' => $user_uuid,
												'productSerialNo' => $this->input->post('productSerialNo'),
												'batteryType' => $this->input->post('batteryType'),  
												'productSegment' => $this->input->post('productSegment'),  
												'Brand' => $this->input->post('brand'),
												'productType' => $this->input->post('protype'),
												'model' => $this->input->post('model'),
												'warrantyStartDate' => $this->input->post('warrantyStartDate'),
												'warrantyEndDate' => $this->input->post('warrantyEndDate'),
												'proRateWarrStDate' => $this->input->post('proRateWarrStDate'),
												'proRateWarrEndDate' => $this->input->post('proRateWarrEndDate'),
												'custPurchaseDate' => $this->input->post('custPurchaseDate'),  
												'batterymanufacturingCode' => $this->input->post('batterymanufacturingCode'),  
												'activationDate' => $this->input->post('activationDate'), 
												'valid_from' => $this->input->post('valid_from'), 
												'custPurchaseInvoiceNum' => $this->input->post('custPurchaseInvoiceNum'), 
												'letpl_invoice_no' => $this->input->post('letpl_invoice_no'), 
												'letpl_invoice_date' => $this->input->post('letpl_invoice_date'), 
												'proRateDisc' => $this->input->post('pro_rata_disc'), 
												'warrantyStatus' => $this->input->post('waranyStatus'), 
												'dealerName' => $this->input->post('dealer'),									  
												'productAppFitment' => $this->input->post('app'),									  
												'productVehicleMake' => $this->input->post('vehicleMake'),									  
												'productVehicleMfg' => $this->input->post('vehicle_model'),									  
												'vehicle_reg' => $this->input->post('vehicle_reg'),	
												'cust_primary_key'=>  $row[0]->cust_id,													
												'datetime' => date('d-m-Y h:i:s')									  
											);	
						//echo "<pre>>"; print_r($dataServiceProducts); 
						
						//////////////////////sar_asset
						 $dataAsset = array(
										  'id' => $guid,//need to generate guid
										  'name' => $this->input->post('protype'),//name of asset 
										  'assigned_user_id' => "1",
										  'date_entered' => $date
										 ); 
						//////////////////////sar_asset_cstm
						
						
						
						 $dataAssetCstm = array(
											  'id_c' => $guid,//guid that will be same as guid of sar_asset
											  'product_serial_no_c' => $this->input->post('productSerialNo'),
											  'battery_type_c' => $this->input->post('batteryType'),  
											  'product_segment_c' => $this->input->post('productSegment'),
											  'brand_c' => $this->input->post('brand'), 
											  'sap_code_c' => $this->input->post('model'),
											  'warranty_start_date_c' => date('Y-m-d',strtotime($this->input->post('warrantyStartDate'))),     
											  'warranty_end_date_c' => date('Y-m-d',strtotime($this->input->post('warrantyEndDate'))), 
											  'proratewarr_strtdate_c' => date('Y-m-d',strtotime($this->input->post('proRateWarrStDate'))), 
											  'proratewarr_enddate_c' => date('Y-m-d',strtotime($this->input->post('proRateWarrEndDate'))), 
											  'purchase_date_c' => date('Y-m-d',strtotime($this->input->post('custPurchaseDate'))), 
											  'battery_manufacturing_code_c' => $this->input->post('batterymanufacturingCode'), 
											  
											  'purchase_invoice_no_c' => $this->input->post('custPurchaseInvoiceNum'), 
											  'letpl_invoice_no_c' => $this->input->post('letpl_invoice_no'), 
											  'letpl_invoice_date_c' =>  date('Y-m-d',strtotime($this->input->post('letpl_invoice_date'))), 
											  'pro_rate_disc_c' => $this->input->post('proRateDisc'), 
											  'warranty_status_c' => $this->input->post('waranyStatus'), 
											  'application_c' => $this->input->post('app'), 
											  'manufacturer_c' => $this->input->post('vehicle_model'), 
											  'vehicle_model_c' => $this->input->post('vehicleMake'), 
											  'vehicle_registration_no_c' => $this->input->post('vehicle_reg'), 
											  'aos_products_id_c' => $pr_product_id, 
											  'aos_product_categories_id_c' => $pr_category_id, 
											  'product_type_c' => $this->input->post('protype'), 
											  'parent_id' => $this->input->post('dealer'), 
											  'parent_type' => "SAR_dealer", 
											  'account_id_c' => $user_uuid 
											 );						
											 //////////////////////contacts_sar_asset_1_c
						 $contactAsset = array(
											  'id' =>$guid,//need to generate guid
											  'contacts_sar_asset_1contacts_ida' => $this->input->post('consid'),//ifofconsumer 
											  'contacts_sar_asset_1sar_asset_idb ' => $guid //idofasset
											 );
						
							
							
								/*  echo "<pre>"; print_r($cp); 
									echo "<pre>"; print_r($dataAsset); 
									echo "<pre>"; print_r($dataAssetCstm); die; 
										echo "Hello";  */
							
						$this->load->model('service_case_model');
						//$custPrim = $this->service_case_model->get_cust_primary($consid);
						$data = $this->service_case_model->add_cust_prod($dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset,$consid, $dataCust);
						/* if($data > 0) 
						{ */
						 if($data > 0)
						 {
							$arr=1;
						 }
						 else
						 {
							 $arr = 2;
						 }						 
							
					}
				echo $arr;
	}
	
	
	
	function create_more_product()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$date = date("Y-m-d H:i:s"); 
		$badh = $this->input->post('custPurchaseDate');
		// die;
		//echo $badh; die;
		/* if(empty($_POST['warrantyStartDate']) && empty($_POST['warrantyEndDate']))
		*/
		$test_arr  = explode('-', $badh);
		/* 
		if(checkdate($test_arr[0], $test_arr[1], $test_arr[2])) 
		{
		  */
			function getGUID()
			{
						if (function_exists('com_create_guid')){
							return com_create_guid();
						}else{
							mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$uuid = substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12);
							return $uuid;
						}
			}
			$guid = getGUID();
	    /////////////////tbl_service_products
						$query = $this->db->query("SELECT * FROM tbl_service_customers WHERE cust_code='".$this->input->post('consid')."'");
						$row = $query->result();
					//	 echo "<pre>"; print_r($row); die;
		//    $date = date("Y-m-d");
      
			/************************************ Product cat ID & Product Id Started here *************************************/	

			 	$queries = $this->db->query("SELECT a.id AS product_id, d.id AS category_id
											FROM aos_products a
											LEFT JOIN aos_product_categories d ON a.aos_product_category_id = d.id
											WHERE a.deleted =0
											AND a.part_number = '".$this->input->post('model')."'
											LIMIT 1");

		    //echo $this->db->last_query();
			$row_pr = $queries->result();
			//print_r($row_pr); 
			$pr_product_id = $row_pr[0]->product_id;
			$pr_category_id = $row_pr[0]->category_id;
			//die(" - Query");
	
		/************************************ Product cat ID & Product Id Ended here *************************************/					 
			
		$dataCust =  array(
		                      'createdBy'=>  $row[0]->createdBy,
		                      'fname'=>  $row[0]->fname,
		                      'lname'=>  $row[0]->lname,
		                      'cust_code' =>  $row[0]->cust_code,
		                      'mobile'=>  $row[0]->mobile,
		                      'alt_mobile'=>  $row[0]->alt_mobile,
		                      'address'=>  $row[0]->address,
		                      'landmark'=>  $row[0]->landmark,
		                      'state'=>  $row[0]->state,
		                      'city'=>  $row[0]->city,
		                      'area'=>  $row[0]->area,
		                      'pincode'=>  $row[0]->pincode,
		                      'productCreatedOn'=>  $date,
		                      'isActive'=>  $row[0]->isActive,
		                      'custAssetGuid'=> $guid,
							  'datetime' => date('d-m-Y h:i:s')
    					   );				
     //  echo "<pre>"; print_r($dataCust); die;	
	   
		
		$dataServiceProducts = array(
									 //	'productId' => $this->input->post('productId'),
										'productGuid' => $guid,
										'consumerId' => $this->input->post('consid'),
										'userId' => $user_uuid,
										'productSerialNo' => ucfirst($this->input->post('productSerialNo')),
										'batteryType' => $this->input->post('batteryType'),  
										'productSegment' => $this->input->post('productSegment'),  
										'Brand' => $this->input->post('brand'),
										'productType' => $this->input->post('protype'),
										'model' => $this->input->post('model'),
										'warrantyStartDate' => $this->input->post('warrantyStartDate'),
										'warrantyEndDate' => $this->input->post('warrantyEndDate'),
										'proRateWarrStDate' => $this->input->post('proRateWarrStDate'),
										'proRateWarrEndDate' => $this->input->post('proRateWarrEndDate'),
										'custPurchaseDate' => $this->input->post('custPurchaseDate'),  
										'batterymanufacturingCode' => $this->input->post('batterymanufacturingCode'),  
										'activationDate' => $this->input->post('activationDate'), 
										'valid_from' => $this->input->post('valid_from'), 
										'custPurchaseInvoiceNum' => $this->input->post('custPurchaseInvoiceNum'), 
										'letpl_invoice_no' => $this->input->post('letpl_invoice_no'), 
										'letpl_invoice_date' => $this->input->post('letpl_invoice_date'), 
										'proRateDisc' => $this->input->post('pro_rata_disc'), 
										'warrantyStatus' => $this->input->post('waranyStatus'), 
										'dealerName' => $this->input->post('dealer'),									  
										'productAppFitment' => $this->input->post('app'),									  
										'productVehicleMake' => $this->input->post('vehicleMake'),
										'productVehicleMfg' => $this->input->post('model'),							  
										'vehicle_reg' => $this->input->post('vehicle_reg'),	
										'cust_primary_key'=>  $row[0]->cust_id						  
																			  
									);	
									/* 
			 echo "<pre>"; print_r($dataCust); 
			 echo "<pre>"; print_r($dataServiceProducts); exit;
			die;  */
			//////////////////////sar_asset
				 $dataAsset = array(
								  'id' => $guid,//need to generate guid
								  'name' => $this->input->post('protype'),//name of asset 
								  'assigned_user_id' => "1",
								   'date_entered' => $date
								 ); 
				//////////////////////sar_asset_cstm
				$dataAssetCstm = array(
									  'id_c' => $guid,//guid that will be same as guid of sar_asset
									  'product_serial_no_c' => $this->input->post('productSerialNo'),
									  'battery_type_c' => $this->input->post('batteryType'),  
									  'product_segment_c' => $this->input->post('productSegment'),
									  'brand_c' => $this->input->post('brand'), 
									  'sap_code_c' => $this->input->post('model'),
									  'warranty_start_date_c' => date('Y-m-d',strtotime($this->input->post('warrantyStartDate'))),     
									  'warranty_end_date_c' => date('Y-m-d',strtotime($this->input->post('warrantyEndDate'))), 
									  'proratewarr_strtdate_c' => date('Y-m-d',strtotime($this->input->post('proRateWarrStDate'))), 
									  'proratewarr_enddate_c' => date('Y-m-d',strtotime($this->input->post('proRateWarrEndDate'))), 
									  'purchase_date_c' => date('Y-m-d',strtotime($this->input->post('custPurchaseDate'))), 
									  'battery_manufacturing_code_c' => $this->input->post('batterymanufacturingCode'), 
									 
									  'purchase_invoice_no_c' => $this->input->post('custPurchaseInvoiceNum'), 
									  'letpl_invoice_no_c' => $this->input->post('letpl_invoice_no'), 
									  'letpl_invoice_date_c' => $this->input->post('letpl_invoice_date'), 
									  'pro_rate_disc_c' => $this->input->post('proRateDisc'), 
									  'warranty_status_c' => $this->input->post('waranyStatus'), 
									  'application_c' => $this->input->post('app'), 
									  'vehicle_model_c' => $this->input->post('vehicle_model'), 
									  'manufacturer_c' => $this->input->post('vehicleMake'), 
									  'vehicle_registration_no_c' => $this->input->post('vehicle_reg'), 
									  'aos_products_id_c' => $pr_product_id, 
									  'aos_product_categories_id_c' => $pr_category_id, 
									   'product_type_c' => $this->input->post('protype'), 
									  'parent_id' => $this->input->post('dealer'), 
									  'parent_type' => "SAR_dealer", 
									  'account_id_c' => $user_uuid 
									 );						
									 //////////////////////contacts_sar_asset_1_c
				 $contactAsset = array(
									  'id' =>$guid,//need to generate guid
									  'contacts_sar_asset_1contacts_ida' => $this->input->post('consid'),//ifofconsumer 
									  'contacts_sar_asset_1sar_asset_idb ' => $guid //idofasset
									 );
				$cp = array(
							  'custAssetGuid' => $guid,
							  'cust_product_id' => $_POST['productSerialNo'],
							  'custPurchaseDate' => $_POST['custPurchaseDate'],
							  'warrantyStatus' => $_POST['waranyStatus'],
							  'productType' => $_POST['protype'],
							  'model' => $_POST['model']
							);
					
					$consid = $this->input->post('consid');
				 // echo "<pre>"; print_r($cp); 
				 // echo "<pre>"; print_r($cp); 
				 // echo "<pre>"; print_r($dataAsset); 
				 // echo "<pre>"; print_r($dataAssetCstm); 
				 //	echo "Hello"; 
				// die;	 
					
			$this->load->model('service_case_model');
			$sr = $_POST['productSerialNo'];
			$asset_status = $this->service_case_model->update_serial_st($sr);
			//echo "Hello";
			//echo $asset_status; die("kdnfkdznfknzkfnkjzxnzkjfn");
			if($asset_status > 0)
			{ 
                                $arr = 2; 
			} 
			else
			{
			  $data = $this->service_case_model->add_more_cust_prod($dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset, $cp, $consid, $dataCust);
				$arr =  $data;
			}
			echo $arr;
			die;
				
									
	}
	
	function create_sf_product()
	{
		$get_session_data               = $this->session->userdata('logged_in');
		$user_uuid 			= $get_session_data['user_uuid'];
                $user_type 			= $get_session_data['user_type'];
		$date 				= date("Y-m-d H:i:s"); 
		$badh 				= $this->input->post('custPurchaseDate');
                $js_no 				= trim(stripslashes($_POST['js_no']));
                $cc 				= $this->cc_customer_model->get_cc_prod($js_no);
                $asset_status                   = $this->service_case_model->update_serial_st($sr);
		$test_arr  			= explode('-', $badh);
                $prod_id 			= $cc[0]->cc_product_id;
		function getGUID()
                {
                        if (function_exists('com_create_guid')){
                                return com_create_guid();
                        }else{
                                mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
                                $charid = strtoupper(md5(uniqid(rand(), true)));
                                $hyphen = chr(45);// "-"
                                $uuid = substr($charid, 0, 8).$hyphen
                                        .substr($charid, 8, 4).$hyphen
                                        .substr($charid,12, 4).$hyphen
                                        .substr($charid,16, 4).$hyphen
                                        .substr($charid,20,12);
                                return $uuid;
                        }
                }
		$guid = getGUID();
	    
		/************************************ Product cat ID & Product Id Started here *************************************/	

		$queries = $this->db->query("SELECT a.id AS product_id,a.aos_product_category_id, d.id AS category_id FROM aos_products a LEFT JOIN aos_product_categories d ON a.aos_product_category_id = d.id WHERE a.deleted =0 AND a.part_number = '".$this->input->post('model')."' LIMIT 1");
		$row_pr = $queries->result();
		$pr_product_id = $row_pr[0]->product_id;
		$pr_category_id = $row_pr[0]->category_id;
	
		/************************************ Product cat ID & Product Id Ended here *************************************/					 
		
        $dataCCProduct =  array(
            'product_type_id'		=>  $pr_category_id,
            'product_sub_type_id'	=>  $pr_product_id,
            'purchase_date'			=>  $this->input->post('custPurchaseDate'),
            'warranty_start_date' 	=>  $this->input->post('warrantyStartDate'),
            'warranty_end_date'		=>  $this->input->post('warrantyEndDate'),
            'warranty_status'		=>  $this->input->post('waranyStatus'),
            'product_sap_code'		=>  $this->input->post('model'),
            'isCallCentreCreation'	=>  1,
            'asset_serial_no'		=>  ucfirst($this->input->post('productSerialNo')),
            'dealer_id'				=>  $this->input->post('dealer'),
            'js_modified_date'		=>  date('Y-m-d H:i:s'),
            'js_modified_by'		=>  $user_uuid
		);	
		$dataServiceProducts = array(
           	'productGuid' 				=> $guid,
           	'consumerId' 				=> $cc[0]->cc_prod,
           	'userId' 					=> $user_uuid,
           	'productSerialNo' 			=> ucfirst($this->input->post('productSerialNo')),
           	'batteryType' 				=> $this->input->post('batteryType'),  
           	'productSegment' 			=> $this->input->post('productSegment'),  
           	'Brand' 					=> $this->input->post('brand'),
           	'productType' 				=> $this->input->post('protype'),
           	'model' 					=> $this->input->post('model'),
           	'warrantyStartDate' 		=> $this->input->post('warrantyStartDate'),
           	'warrantyEndDate' 			=> $this->input->post('warrantyEndDate'),
           	'proRateWarrStDate' 		=> $this->input->post('proRateWarrStDate'),
           	'proRateWarrEndDate' 		=> $this->input->post('proRateWarrEndDate'),
           	'custPurchaseDate' 			=> $this->input->post('custPurchaseDate'),  
           	'batterymanufacturingCode' 	=> $this->input->post('batterymanufacturingCode'),  
           	'activationDate' 			=> $this->input->post('activationDate'), 
           	'valid_from' 				=> $this->input->post('valid_from'), 
           	'custPurchaseInvoiceNum' 	=> $this->input->post('custPurchaseInvoiceNum'), 
           	'letpl_invoice_no' 			=> $this->input->post('letpl_invoice_no'), 
           	'letpl_invoice_date' 		=> $this->input->post('letpl_invoice_date'), 
           	'proRateDisc' 				=> $this->input->post('pro_rata_disc'), 
           	'warrantyStatus' 			=> $this->input->post('waranyStatus'), 
           	'dealerName' 				=> $this->input->post('dealer'),									  
           	'datetime' 				=> date('Y-m-d H:i:s'),									  
           	'productAppFitment' 		=> $this->input->post('app'),									  
           	'productVehicleMake' 		=> $this->input->post('vehicleMake'),
           	'productVehicleMfg' 		=> $this->input->post('model'),							  
           	'vehicle_reg' 				=> $this->input->post('vehicle_reg'),	
           	'cust_primary_key'			=>  $cc[0]->cust_id
        );	
									
        $dataAsset = array(
           	'id' 				=> $guid,//need to generate guid
           	'name' 				=> $this->input->post('protype'),//name of asset 
           	'assigned_user_id' 	=> "1",
            'date_entered' 		=> $date
          ); 

        $dataAssetCstm = array(
            'id_c'                  		=> $guid,//guid that will be same as guid of sar_asset
            'product_serial_no_c'   		=> $this->input->post('productSerialNo'),
            'battery_type_c'        		=> $this->input->post('batteryType'),  
            'product_segment_c'     		=> $this->input->post('productSegment'),
            'brand_c'               		=> $this->input->post('brand'), 
            'sap_code_c'            		=> $this->input->post('model'),
            'warranty_start_date_c' 		=> date('Y-m-d',strtotime($this->input->post('warrantyStartDate'))),     
            'warranty_end_date_c'   		=> date('Y-m-d',strtotime($this->input->post('warrantyEndDate'))), 
            'proratewarr_strtdate_c'		=> date('Y-m-d',strtotime($this->input->post('proRateWarrStDate'))), 
            'proratewarr_enddate_c' 		=> date('Y-m-d',strtotime($this->input->post('proRateWarrEndDate'))), 
            'purchase_date_c'       		=> date('Y-m-d',strtotime($this->input->post('custPurchaseDate'))), 
            'battery_manufacturing_code_c' 	=> $this->input->post('batterymanufacturingCode'), 
            'purchase_invoice_no_c' 		=> $this->input->post('custPurchaseInvoiceNum'), 
            'letpl_invoice_no_c'    		=> $this->input->post('letpl_invoice_no'), 
            'letpl_invoice_date_c'  		=> $this->input->post('letpl_invoice_date'), 
            'pro_rate_disc_c'       		=> $this->input->post('proRateDisc'), 
            'warranty_status_c'     		=> $this->input->post('waranyStatus'), 
            'application_c'         		=> $this->input->post('app'), 
            'vehicle_model_c'       		=> $this->input->post('vehicle_model'), 
            'manufacturer_c'        		=> $this->input->post('vehicleMake'), 
            'vehicle_registration_no_c' 	=> $this->input->post('vehicle_reg'), 
            'aos_products_id_c'     		=> $pr_product_id, 
            'aos_product_categories_id_c' 	=> $pr_category_id, 
             'product_type_c'       		=> $this->input->post('protype'), 
            'parent_id'             		=> $this->input->post('dealer'), 
            'parent_type'           		=> "SAR_dealer", 
            'account_id_c'          		=> $user_uuid 
        );						
     //////////////////////contacts_sar_asset_1_c ///////////////////////////////////////
        $contactAsset = array(
	        'id' 									=> $guid,//need to generate guid
	        'contacts_sar_asset_1contacts_ida' 		=> $this->input->post('consid'),//ifofconsumer 
	        'contacts_sar_asset_1sar_asset_idb ' 	=> $guid //idofasset
        );		
		$consid 		= $this->input->post('consid');
        $d1  			=  date_create(trim(stripslashes($this->input->post('custPurchaseDate'))));
		$d2  			=  date_create(trim(stripslashes($this->input->post('letpl_invoice_date'))));
        $interval  		=   $d2->diff($d1);
        $Aging_days     =   $interval->format('%R%a');
        if(trim($this->input->post('waranyStatus')) == 'IW')
        {
            $warrStatus = 'FOC';
        }
        else if(trim($this->input->post('waranyStatus')) == 'OW')
        {
            $warrStatus = 'Out of Warranty';
        }
        else
        {
            $warrStatus = 'Out of Warranty';
        }
        $caLL_arr = array(
                            'claimAgeing' 			=>  $Aging_days,
                            'warranty'                          =>  trim(stripslashes($this->input->post('waranyStatus'))),
                            'callProductSerialNo'               =>  ucfirst($this->input->post('productSerialNo'))
                        );
        $js_case_id = $cc[0]->js_case_id;
        $sr = trim(stripslashes($_POST['productSerialNo']));
        if($user_type == 'SF')
        {
            $asset_status = $this->service_case_model->update_serial_st($sr);
            if($asset_status > 0)
            { 
                 $arr = 2; 
            } 
            else
            {
                $data = $this->cc_customer_model->add_more_prod($dataCCProduct, $dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset, $prod_id, $caLL_arr, $js_no);
                $arr =   trim(base64_encode($data[0]->service_call_id));
            }
            echo $arr;
            die;
        }
        else
        {
            $asset_status = $this->service_case_model->update_serial_st($sr);
            if($asset_status > 0)
            { 
                $arr = 2; 
            } 
            else
            {
              	$data 	= $this->service_case_model->add_more_cust_prod($dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset, $cp, $consid, $dataCust);
                $arr 	=  trim(base64_encode($data[0]->service_call_id));
            }
            echo $arr;
            die;
        }						
	}
	
        
	function closed_cases()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/service_close_view', $data);
	}
	
	function check_cases()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/update_case_wrrnty_view', $data);
	}
	
	public function view_cases($caseId)
	{
		$caseId = base64_decode($caseId);
		$get_session_data = $this->session->userdata('logged_in');
		$us = $get_session_data['user_uuid'];
		$this->load->model('service_case_model');
		$data['sas'] = $this->service_case_model->update_wrrnty_details($caseId);
		//echo "<pre>"; print_r($data['sas']); die;
		if(!empty($data['sas']))
		{
                    $pseialno=$data['sas'][0]->callProductSerialNo;
                    $mobile = $data['sas'][0]->callerMobile;
                    $callCustId  = $data['sas'][0]->callCustId;
                    $callAssetId = $data['sas'][0]->callAssetId;
                    /* 
                        echo $caseId;
                        echo "<br/>";
                       echo "prod : ".$pseialno;
                       echo "<br/>";
                       echo "mobile : ".$mobile;
                       echo "<br/>";
                       echo "callCustId : ".$callCustId;
                       echo "<br/>";
                       echo "callAssetId : ".$callAssetId;
                       die; 
                    */
                    $data['cust']       =   $this->service_case_model->get_cust_data($callCustId);
                    //$data['asset'] = $this->service_case_model->get_cust_prod($callAssetId);
                    $data['asset']      =   $this->service_case_model->get_cust_prod($pseialno);
                    $dealer_code        =   $data['asset'][0]->dealerName;
                    //	print_r($dealer_code); die("hsevdjfb");
                    $data['dealName']   =   $this->service_case_model->get_dealName($dealer_code);
                    $data['bak']        =   $this->service_case_model->get_bak_code($dealer_code);

                    $data['gdate']      =   $this->service_case_model->get_date($mobile);
                    $data['pdate']      =   $this->service_case_model->pserial_date($callAssetId);
                    $data['test']       =   $this->service_case_model->getTestData($us, $caseId);
                    $data['replace']    =   $this->service_case_model->getReplaceData($us, $caseId);
                    //echo "<pre>"; print_r($data); die;
			/*	   
				$createDate=$data['gdate'][0]->createdOn;
				$date = str_replace('/', '-',$createDate);
				echo date('Y-m-d', strtotime($date));
			  
				$primaryDate=$data['pdate'][0]->primary_sale_date;
			*/
		 
                    $this->load->view('services/update_case_wrrnty_view', $data);
		}
		else
		{
			$testReport = $this->service_case_model->check_case($caseId);
			// print_r($testReport); die("Hii");
			$rep = $testReport[0]->service_call_id;
			$encRep = base64_encode($rep);
			// echo $rep;
			/* echo $encRep;
			die;
			 */
			if(!empty($encRep))
			{
				//echo "Hello"; die;
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Please submit your test report first to view complaint details.')
					window.location.href='".base_url()."index.php/fault_parts/index/$encRep';
					</SCRIPT>");
			}
			else
			{
				//echo "bye"; die;
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Some problem occured. Please contact admin.')
					window.location.href='".base_url()."index.php/service_cases';
					</SCRIPT>");
			}		
		}
	}
	public function view_cases_battery($caseId)
	{
		$caseId = base64_decode($caseId);
		$get_session_data = $this->session->userdata('logged_in');
		$us = $get_session_data['user_uuid'];
		
		$data['sas'] = $this->service_case_model->update_wrrnty_details($caseId);
		//echo "<pre>"; print_r($data['sas']); die;
		
                    $pseialno=$data['sas'][0]->callProductSerialNo;
                    $mobile = $data['sas'][0]->callerMobile;
                    $callCustId  = $data['sas'][0]->callCustId;
                    $callAssetId = $data['sas'][0]->callAssetId;
                    /* 
                        echo $caseId;
                        echo "<br/>";
                       echo "prod : ".$pseialno;
                       echo "<br/>";
                       echo "mobile : ".$mobile;
                       echo "<br/>";
                       echo "callCustId : ".$callCustId;
                       echo "<br/>";
                       echo "callAssetId : ".$callAssetId;
                       die; 
                    */
                    $data['cust']       =   $this->cc_customer_model->get_consum_detail($mobile);
                   // echo "<pre>"; print_r($data['cust'] ); die;
                    //$data['asset'] = $this->service_case_model->get_cust_prod($callAssetId);
                    $data['asset']      =   $this->cc_customer_model->get_product_data($pseialno);
                    $dealer_code        =   $data['asset'][0]->dealerName;
                   // echo "<pre>"; print_r($data['asset'] ); die;
                    //	print_r($dealer_code); die("hsevdjfb");
                    $data['dealName']   =   $this->service_case_model->get_dealName($dealer_code);
                    $data['bak']        =   $this->service_case_model->get_bak_code($dealer_code);

                    $data['gdate']      =   $this->service_case_model->get_date($mobile);
                    $data['pdate']      =   $this->service_case_model->pserial_date($callAssetId);
                    $data['test']       =   $this->service_case_model->getTestData($us, $caseId);
                    $data['replace']    =   $this->service_case_model->getReplaceData($us, $caseId);
                    //echo "<pre>"; print_r($data); die;
			/*	   
				$createDate=$data['gdate'][0]->createdOn;
				$date = str_replace('/', '-',$createDate);
				echo date('Y-m-d', strtotime($date));
			  
				$primaryDate=$data['pdate'][0]->primary_sale_date;
			*/
		 
                    $this->load->view('service_franchise/battery_call_details_view', $data);
		
		
	}
	function close_outwarranty()
	{
		 
		//echo $user; die;
		$case = $_POST['caseid'];
		$dist = $_POST['dist_remark'];
		if(empty($case))
		{	
			redirect('service_cases');
		}
		$this->load->model('service_case_model');
		$data = $this->service_case_model->close_case($case, $dist, $user);
		$this->load->view('services/update_case_wrrnty_view', $data);
		
	}
	
	function new_consumer()
	{
		$this->load->view('services/add_consumer_view');
	}
	
	function add_new_consumer()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['user_uuid'];
		$dates = date("Y-m-d H:i:s");
		
		//$phoneNumber = $_POST['mobileno'];
		/* $phoneNumber = $_POST['mobile'];

			if(!preg_match('/^\d{10}$/',$_POST['mobile'])) // phone number is valid
			{ */
				function getGUID()
						{
							if (function_exists('com_create_guid')){
								return com_create_guid();
							}else{
								mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
								$charid = strtoupper(md5(uniqid(rand(), true)));
								$hyphen = chr(45);// "-"
								$uuid = substr($charid, 0, 8).$hyphen
									.substr($charid, 8, 4).$hyphen
									.substr($charid,12, 4).$hyphen
									.substr($charid,16, 4).$hyphen
									.substr($charid,20,12);
								return $uuid;
							}
						}
		
				function getsecGUID()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
				function getsecGUIDth()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
				function getsecGUIDfr()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
		
		$tid = getGUID();
		$secid = getsecGUID();
		$thrid = getsecGUIDth();
		$forid = getsecGUIDfr();
		 	
		

		$query = $this->db->query("SELECT * FROM  `tbl_service_customers` where mobile='".$_POST['mobile']."'");
		$rows  = $query->num_rows();
       
		if($rows > 0)
		{ 
			//echo "Hii";  die("Hel");
			$m  =  $_POST['mobile'];
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Mobile No. already exists. You can create product for this consumer.')
				</SCRIPT>");
			$this->load->model('service_case_model');
			$data['res'] = $this->service_case_model->get_existing_consumer($m);
			$this->load->view('services/add_exist_cust_view', $data);
		}
		else
		{
			/* $query = $this->db->query("SELECT * FROM  `contacts` where phone_mobile='".$_POST['mobile']."'");
			$rows  = $query->num_rows();
			//echo $rows; die("Hello");
			if($rows > 0)
			{ 
				$mob  =  $_POST['mobile'];
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Mobile No. already exists. Please Create product for this consumer.')
					</SCRIPT>"); */
			/* 	$this->load->model('service_case_model');
				$data['res'] = $this->service_case_model->get_existing_customer($mob);
				$this->load->view('services/add_exist_consm_view', $data);
			}
			else
			{ */
				
			
			  $data = array(
		                 'fname' => $_POST['fname'],
		                 'lname' => $_POST['lname'],
		                 'mobile' => $_POST['mobile'],
		                 'alt_mobile' => $_POST['altmobile'],
		                 'email' => $_POST['email'],
		                 'address' => $_POST['address'],
		                 'landmark' => $_POST['landmark'],
		                 'state' => $_POST['state'],
		                 'city' => $_POST['city'],
		                 'cust_code' => $tid,
		                 'area' => $_POST['area'],
		                 'pincode' => $_POST['pin'],
		                 'dob' => $_POST['dob'],
		                 'createdBy' => $user
						);
			$dat = array(
		                 'id' => $tid,
		                 'first_name' => $_POST['fname'],
		                 'last_name' => $_POST['lname'],
		                 'date_entered' => $dates,
		                 'date_modified' => $dates,
		                 'phone_mobile' => $_POST['mobile'],
		                 'phone_work' => $_POST['altmobile'],
		                // 'phone_work' => $_POST['email'],
		                // 'primary_address_country' => $_POST['address'],
		                // 'primary_address_street' => $_POST['landmark'],
		                 'primary_address_state' => $_POST['state'],
		                 'primary_address_city' => $_POST['city'],
		                 'primary_address_postalcode' => $_POST['pin'],
		                 'birthdate' => date('Y-m-d',strtotime($_POST['dob']))
		                // 'created_by' => $user
						);
						
			
			
			$d_email = array(
							    'id' => $thrid,
							    'email_address' => $this->input->post('email'),
							    'date_created' => $dates,
							    'date_modified' => $dates
							); 	
            /*  $eml_bean = array(
								'id'
							  ); */
							
			$data_cstm = array(
			                    'id_c' => $tid,
								'primary_address_area_c' => $_POST['area'],
								'primary_address_1_c' => $_POST['address'],
								'primary_address_district_c' => $_POST['landmark']
							   );
			/* $d_email = array(
								'id' => $tid,
								'email_address' => $_POST['email']
								);		 */
            $em_rel = array(
								'id' => $secid,
								'bean_id' => $tid,
								'bean_module' => 'Contacts',
								'email_address_id' => $thrid
								);
								
						
					
				
           /*  echo "<pre>"; print_r($data); 
            echo "<pre>"; print_r($dat); 
            echo "<pre>"; print_r($data_cstm); 
            echo "<pre>"; print_r($data_email); 
			
            // $this->db->insert('tbl_service_customers', $data);
			// $this->db2->insert('contacts', $dat);
			 
			//die;	

			die; */
		


			//echo "<pre>"; print_r($data); die;
		
			$this->load->model('service_case_model');
			$data = $this->service_case_model->add_new_consumer($data, $dat, $data_cstm, $d_email, $em_rel);
			//$this->load->view('services/update_case_wrrnty_view', $data); 
			//echo $data; die("Hello");
			if($data>0)
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Success ! Go ahead to create the product..')
						window.location.href='add_product/$tid';
						</SCRIPT>");
				
			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry! Network issue raised. Please try again.')
						window.location.href='add_new_consumer';
						</SCRIPT>");	
			}		
			//}
		}	
			/* else // phone number is not valid
			{
			  echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry! Please enter 10 digit mobile number..')
						window.location.href='add_new_consumer';
						</SCRIPT>");
			} */
		
		
		
	} 
	
	function add_product($tid, $asd)
	{
            //echo base64_decode($asd); die;
           // echo base64_decode($asd)."</br>"; die;
           // echo "<h1>".base64_decode($asd)."</br>";
           // die;
            $get_session_data = $this->session->userdata('logged_in');
            $user_type = $get_session_data['user_type'];
		  //echo $consid; die("hi   i");
		//echo "KJDANHFK".$tid; die;
            $data['cust']    =      $this->cc_customer_model->get_consumer_detail($tid); 
           $data['js_info']  =      $this->cc_customer_model->getJS_Prod_Battery(base64_decode($asd)); 
           $engg_info = $data['js_info'][0]->assigned_to;
           //echo $engg_info; die("jsdbjks");
          $data['engg_det'] 		= $this->cc_product_model->get_engg_detail($engg_info);
            //  echo "<pre>"; print_r($data['engg_det']); die("Hello");
        
            if(!empty($asd) && $user_type=='SF')
            {
                $this->load->model('service_case_model');
                $viewCall = $this->cc_customer_model->get_complain_data(base64_decode($asd));
                $viewCases = $this->cc_customer_model->get_complain_types(base64_decode($asd));
                $viewCase =  $viewCall[0]->call_id;
                $callProductSerialNo  =  trim($viewCases[0]->callProductSerialNo);
                $complaint_type       =  trim($viewCases[0]->complaint_type);
                $service_call_id       =  trim($viewCases[0]->service_call_id);
                 //print_r($viewCases); die("Hello");
                if(!empty($viewCase))
                {
                    $viewCase = base64_encode($viewCase);
                    echo ("<SCRIPT LANGUAGE='JavaScript'>
                        window.location.href='".base_url()."index.php/service_cases/view_cases/.$viewCase';
                        </SCRIPT>");
                }
                else
                {
                    $dat = $this->service_case_model->count_product_created($tid);
                    $data['rs'] = array('consid' => $tid, 'js_no' => $asd, 'complaint_type'=>$complaint_type, 'callProductSerialNo'=>$callProductSerialNo, 'service_call_id' => $service_call_id);
                    $this->load->view('services/add_product_view', $data);
                }
            }
            else if(empty($asd) && $user_type=='SF')
            {
                    echo ("<SCRIPT LANGUAGE='JavaScript'>
                            window.alert('Sorry! YOu dont have access to view this page.')
                            window.location.href='".base_url()."index.php/cc_complain/battery_complain_module';
                            </SCRIPT>");
            }
            else
            {
                    $this->load->model('service_case_model');
                    $dat = $this->service_case_model->count_product_created($tid);
                    //echo $dat; die;
                     if($dat > 0)
                     {
                            $data['rs'] = array('consid' => $tid);
                            $this->load->view('services/add__more_prod_view', $data);
                     }
                     else
                     { 
                            //echo "hello"; die;
                             $data['rs'] = array('consid' => $tid);
                             $this->load->view('services/add_product_view', $data);
                     }      
            }
	}
	function add_more_product($tid)
	{
            $get_session_data = $this->session->userdata('logged_in');
		//echo $consid; die("hii");
            $data['rs'] = array('consid' => $tid);
            $this->load->view('services/add_product_view', $data);
	}
	
	function get_product_serial()
    {
		$get_session_data = $this->session->userdata('logged_in');
		$serial = $_REQUEST['pinval'];
		//echo "Response get".$id; 
		
		$this->load->model('service_case_model');
	     $dat = $this->service_case_model->get_product_serial($serial);
		 print_r($dat);  
		//echo "Product :  ".$dat[0]->model;
		//$pin = $_POST[''];
	}	
	
	function add_product_details()
	{
		//echo "hii"; die;
	    $get_session_data = $this->session->userdata('logged_in');
	    $user = $get_session_data['username'];
		
		$data  = array(
		         'consumerId' => $_POST['consId'],
		         'userId' => $user,
		         'productSegment' => $_POST['productSegment'],
		         'brand' => $_POST['brand'],
		         'productType' => $_POST['protype'],
		         'model' => $_POST['model'],
		         'custPurchaseInvoiceNum' => $_POST['custPurchaseInvoiceNum'],
		         'custPurchaseDate' => $_POST['custPurchaseDate'],
		         'productSerialNo' => $_POST['productSerialNo'],
		         'batterymanufacturingCode' => $_POST['batterymanufacturingCode'],
		         'activationDate' => $_POST['activationDate'],
		         'dealerName' => $_POST['dealerName'],
		         'warrantyStartDate' => $_POST['warrantyStartDate'],
		         'warrantyEndDate' => $_POST['warrantyEndDate'],
		         'ext_warrantyStartDate' => $_POST['ext_warrantyStartDate'],
		         'warrantyEndDate' => $_POST['warrantyEndDate'],
		         'letpl_invoice_no' => $_POST['letpl_invoice_no'],
		         'letpl_invoice_date' => $_POST['letpl_invoice_date'],
		         'amc_end_date' => $_POST['amc_end_date'],
		         'proRateDisc' => $_POST['proRateDisc'],
		         'proRateWarrStDate' => $_POST['proRateWarrStDate'],
		         'proRateWarrEndDate' => $_POST['proRateWarrEndDate'],
		         'warrantyStatus' => $_POST['waranyStatus'],
		         'productStatus' => $_POST['pro_status']
		);
		
		// echo "<pre>"; print_r($data); die;
			$this->load->model('service_case_model');
			$dat = $this->service_case_model->add_new_product($data);
			if(empty($dat))
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry some problem occured. Product for customer not created.!!')
					</SCRIPT>");
					redirect('service_cases');
			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Customer with product details Created. Create Case Now !!')
				window.location.href='cust_details';
				</SCRIPT>");
				redirect('cust_details');

			}
	}
	function cust_prod($mob)
	{
		$mob = base64_decode($mob);
		$this->load->model('service_case_model');
	    $dat['mob'] = $this->service_case_model->get_cust_pr($mob);
		// echo "<pre>"; print_r($dat['mob']); die("hwllo");
		$this->load->view('services/cust_show_prod', $dat);
	}
	function verify_serial()
	{
		$this->load->model('service_case_model');
		$usern = $get_session_data['username'];
	    $data['serial'] = $this->service_case_model->verify_serial_detail($usern);
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/verify_serial_view',$data);
	}
	function pagination()
	{
		$get_session_data = $this->session->userdata('logged_in');
		
		$draw= $_REQUEST['draw'];
		$start_index= $_REQUEST['start'];
		$length= $_REQUEST['length'];
		$us = $get_session_data['username'];
		$search= $_REQUEST['search']['value'];
		$this->load->model('service_case_model');
		$data['sas'] = $this->service_case_model->get_pagination_data($us, $draw, $start_index, $length, $search);
		
		
	}
	function cases_admin()
	{
			//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		//echo "hii"; die;
		$this->load->model('service_case_model');
		$data['res'] = $this->service_case_model->get_casompl_details();
		//echo "<pre>"; print_r($data); die("mskdnkjfda");
		$this->load->view('services/service_mycases_view', $data);
	}
	
}
?>

