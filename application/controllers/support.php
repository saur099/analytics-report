<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('application/libraries/vendor/autoload.php'); 
// reference the Dompdf namespace
use Dompdf\Dompdf;
class Support extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh');
			$this->load->model('support_model');		
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$this->load->model('support_model');
		$data['res'] = $this->support_model->get_support_details($useruid);
		//$data['resr'] = $this->support_model->get_prod_serial();
		//print_r($data['res']); exit();
		$this->load->view('support_tickets', $data);
	}
	
	function replies()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('support_model');
		$data['res'] = $this->support_model->get_replies_details($user_uuid);
		$this->load->view('replies_view', $data);
	}	
	function ticketing_sys()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$userT = $get_session_data['user_type'];
		if($userT == "admin")
		{
			$this->load->model('support_model');
			$data['res'] = $this->support_model->get_support_admin();
			//$data['resr'] = $this->support_model->get_prod_serial();
			//print_r($data['res']); exit();
			$this->load->view('admin_tickets', $data);
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! You dont have permission to access this page.')
				window.location.href='".base_url()."';
				</SCRIPT>");
		}
	
	}
	
	function  admin_ticket_reply($id)
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		
		$id = base64_decode($id);
		//echo $id; die;
		$userT = $get_session_data['user_type'];
		if($userT == "admin")
		{
			$this->load->model('support_model');
			$data['rp'] = $this->support_model->get_ticket_det($id);
			//$data['resr'] = $this->support_model->get_prod_serial();
			// echo "<pre>"; print_r($data['rp']); exit();
			$this->load->view('reply_admin_tkt_view', $data);
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! You dont have permission to access this page.')
				window.location.href='".base_url()."';
				</SCRIPT>");
		}
		
	}	
	
	function replying_ticket()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
		$ticketId = $_POST['tktId'];
		$repStatus = $_POST['repStatus'];
		$repSub = $_POST['repSub'];
		$repIss = $_POST['repIss'];
		$repToDist = $_POST['repToDist'];
		$repDesc = $_POST['Description'];
		$date = date("Y-m-d");
		$ip = $_SERVER['REMOTE_ADDR']; 
		$arr1 = array(
   						 'supportStatus' => $repStatus
					);
		$arr2 = array(
		             'repliedIssueIn' => $repIss,
		             'repliedTicketid' => $ticketId,
		             'repliedBy' => $repToDist,
		             'repliedSubject' => $repSub,
		             'repliedStatus' => $repStatus,
		             'repliedOn' => $date,
		             'repliedIP' => $ip,
		             'repliedDescription' => $repDesc
					);
					
		// echo "<pre>"; print_r($arr1); 			
		// echo "<pre>"; print_r($arr2); die("Hello");  			
		 
		$this->load->model('support_model');
		$upd = $this->support_model->insert_data_admin($arr1, $arr2, $ticketId, $useruid);
		if($upd > 0)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Successful ! You have replied to the ticket. ')
					window.location.href='".base_url()."index.php/support/ticketing_sys';
					</SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Some problem occurred.. ')
					window.location.href='".base_url()."index.php/support/ticketing_sys';
					</SCRIPT>");
		}
		 
		
	}
	
	
	
	function add_tickets()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		$useruid = $get_session_data['user_uuid'];
$msg = "First line of text\nSecond line of text";

					// use wordwrap() if lines are longer than 70 characters
					$msg = wordwrap($msg,70);

					// send email
					mail("saurav.verma@livpure.in","My subject",$msg);
					die("hello");
		function getGUID(){
			if (function_exists('com_create_guid')){
				return com_create_guid();
			}else{
				mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
				$charid = strtoupper(md5(uniqid(rand(), true)));
				$hyphen = chr(45);// "-"
				$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12);
				return $uuid;
			}
		}
		 $guid = getGUID();
		 
		if(move_uploaded_file($_FILES['filename']['tmp_name'], './uploads/' . $_FILES['filename']['name'])){}
			
				$data = array(
					 'distUUID' => $useruid,
					 'supportTicket' => $_POST['ticket'],
					 'supportIssueIn' => $_POST['issue'],
					 'supportCaseId' => strtoupper($_POST['case']),
					 'supportSubject' => $_POST['subject'],
					 'customer_fname' => $_POST['fname'],
					 'customer_lname' => $_POST['lname'],
					 'custPurchaseDate' => $_POST['custpurchagedate'],
					 'mobile_no' => $_POST['mobile_no'],
					 'pincode' => $_POST['pincode'],
					 //'supportPriority' => $_POST['priority'],
					 'supportUpload' => $_FILES['filename']['name'],
					 'supportDescription' => $_POST['desc'],
					 'supportGoId' => $guid,
					 'supportUser' => $user
				);
			/* 	$data1 = array(
					 'id' => $guid,
					 'name' => $_POST['subject'],
					 'date_entered' => date('Y-m-d H:i:s'),
					 'date_modified' => date('Y-m-d H:i:s'),
					 'modified_user_id' => $useruid,
					 'created_by' => $useruid,
					 'description' => $_POST['desc']
				); */
				/* 
				$data2 = array(
					 'id_c' => $guid,
					 'ticket_id_c' => $_POST['ticket'],
					 'serial_number_c' => strtoupper($_POST['case']),
					 'customer_first_name_c' => $_POST['fname'],
					 'customer_last_name_c' => $_POST['lname'],
					 'customer_pincode_c' => $_POST['pincode'],
					 'customer_mobile_c' => $_POST['mobile_no'],
					 'image_url_c' => $_FILES['filename']['name'],
					 'ticket_status_c' => 'Processing',
					 'issue_in_c' => $_POST['issue'],
					 'date_of_purchase_c' => $_POST['custpurchagedate'],
					 'account_id_c' => $useruid
				); */
				//echo "<pre>"; print_r($data); exit;
				$this->load->model('support_model');
				$data['res'] = $this->support_model->insert_tickets($data,$data1,$data2);
				if(empty($data))
				{
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry! Some Problem occured.. Please Try Again Later.')
					window.location.href='support';
					</SCRIPT>");

				}
				else
				{
					
					echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('The Support Ticket has successfully raised.. Check Status in Support List..')
					window.location.href='support';
					</SCRIPT>");
				}
	}
	
	function ledger_acc()
	{
		//$data['res'] = $this->support_model->get_support_details($useruid);
		$this->load->view('ledger_acc_view');
		
	}
	
	function ledger_acc_details()
	{ //echo "hello kkkk"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$userName = $get_session_data['username'];
		
		
		$user = 'PO_POP_RFC';
		$password = 'ltd@1234';
		$sap_cred = $user . ":" . $password;
		$sapUrl = "http://219.90.65.84:50000/RESTAdapter/CustomerLedger/View";
		//$sapUrl = "http://219.90.65.84:50000/RESTAdapter/CreditNoteSalesReturn/View";
		$curl = curl_init ( $sapUrl );
		curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt ( $curl, CURLOPT_HEADER, false );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $curl, CURLOPT_USERPWD, $sap_cred ); // "poconnect:sap123"
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, array ("Content-type: application/json" ) );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt ( $curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt ( $curl, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
		curl_setopt ( $curl, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
		
		$currentYr = date ( 'Y' );
		$date1 = trim($this->input->post('fromDate'));
		$date2 = trim($this->input->post('toDate'));
		if(empty($date1)) 
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Please select from which date you want to see.')
					window.location.href='".base_url()."index.php/support/ledger_acc';
					</SCRIPT>");
		}
		if(empty($date2)) 
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Please select to which date you want to see.
					window.location.href='".base_url()."index.php/support/ledger_acc';
					</SCRIPT>");
		}
			$records=array (
					"Record" =>array(
									"DistributorCode" => $userName,
									"CompanyCode" => "1200",
									"FiscalYear" => $currentYr,
									"FromDate" => $date1,
									"ToDate" => $date2
									) 
							);
			
			$content = json_encode ( $records );
			 //print_r($content);
			//exit;
			curl_setopt ( $curl, CURLOPT_POSTFIELDS, $content );
			$json_response = curl_exec ( $curl );
			//echo '<pre>'; print_r($json_response);
			$data['sap'] = json_decode ( $json_response );
			//echo '<pre>'; print_r($data['sap']); exit;
			
			$this->load->view('ledger_acc_details', $data);
			
	}
	
	function ledger_acc_details_pdf($type,$from,$to)
	{ //echo $type .' '. $from .' '.$to; die;
		$get_session_data = $this->session->userdata('logged_in');
		$userName = $get_session_data['username'];
		
		
		$user = 'PO_POP_RFC';
		$password = 'ltd@1234';
		$sap_cred = $user . ":" . $password;
		$sapUrl = "http://219.90.65.84:50000/RESTAdapter/CustomerLedger/View";
		//$sapUrl = "http://219.90.65.84:50000/RESTAdapter/CreditNoteSalesReturn/View";
		$curl = curl_init ( $sapUrl );
		curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt ( $curl, CURLOPT_HEADER, false );
		curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $curl, CURLOPT_USERPWD, $sap_cred ); // "poconnect:sap123"
		curl_setopt ( $curl, CURLOPT_HTTPHEADER, array ("Content-type: application/json" ) );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt ( $curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt ( $curl, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
		curl_setopt ( $curl, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
		
		$currentYr = date ( 'Y' );
		$date1 = $from;
		$date2 = $to;
		//$$this->uri->segment(3)
			$records=array (
					"Record" =>array(
									"DistributorCode" => $userName,
									"CompanyCode" => "1200",
									"FiscalYear" => $currentYr,
									"FromDate" => $date1,
									"ToDate" => $date2
									) 
							);
			
			$content = json_encode ( $records );
			 //print_r($content);
			//exit;
			curl_setopt ( $curl, CURLOPT_POSTFIELDS, $content );
			$json_response = curl_exec ( $curl );
			//echo '<pre>'; print_r($json_response);
			$data['sap'] = json_decode ( $json_response );
			//echo '<pre>'; print_r($data['sap']); exit;
			//echo $data['sap']->Records->DistributorCode .'hello sir'; die
			$date = date("Ymdhis");
			$rand = rand(0,99);
			$challan_number = "pdf".$date.$rand;
			foreach($data['sap'] as $v){//echo "<pre>"; print_r($v); die;
				$var = '<center><b><u>Livguard Energy Technologies Pvt Ltd</u></b>
      <br/><u>Plot No.221, Phase-I, Udyog Vihar, Gurgaon 122016 India. T: +91-124-4987 400</u></center>
<br/>
	  <table class="abc1" BORDER="1" width="50%" cellspacing="0">
                <thead>
						<tr width="70%">
							<td  width="40%"><b>G/L Account/BP Code</b></th>
							<td>'. $v->DistributorCode .'</th>
						</tr>
                </thead>
				<tbody>	
				   <tr width="70%">
							<td  width="40%"><b>G/L Account/BP Name</b></td>
							<td>'. $v->DistributorName .'</td>
					</tr>
                </tbody>
           </table>
		   <br/>
		    <table class="abc2" BORDER="1" width="40%" cellspacing="0" style="float: left;">
                <thead>
						<tr width="50%">
							<td  width="40%"><b>Op. Balance (INR)</b></th>
							<td>'. $v->OpeningBalance .'</th>
						</tr>
                </thead>
				<tbody>	
				   <tr width="50%">
							<td  width="40%"><b>Closing Balance (INR)</b></td>
							<td>'. $v->ClosingBalance .'</td>
					</tr>
                </tbody>
           </table>
		   <table class="abc3" width="40%" border="0" cellspacing="0" style="float: right;">
			</table>
		    <table class="bandhuwa" BORDER="1" width="20%" cellspacing="0" style="float: left;">
                <thead>
						<tr width="50%">
							<td  width="40%"><b>From Date</b></th>
							<td>'. $v->FromDate .'</th>
						</tr>
                </thead>
				<tbody>	
				   <tr width="50%">
							<td  width="40%"><b>To Date</b></td>
							<td>'. $v->ToDate .'</td>
					</tr>
                </tbody>
           </table>
	  <br/>
	  <br/>
	  <br/>
	  <br/>
      <!-- Example DataTables Card-->
          <center><h4 style="font-size:15px;"><b>Ledger/Account Balance</b></h4></div></center>
		  <br/>
            <table class="abc4" BORDER="1" width="100%" cellspacing="0">
                <thead>
						<tr>
							<th>DocumentNumber</th>
							<th>DocumentType</th>
							<th>PostingDate</th>
							<th>DebitAmount</th>
							<th>CreditAmount</th>
							<th>RefrenceNumber</th>
							<th>Currency</th>
							<th>Balance</th>
							<th>Description</th>
						</tr>
                </thead>
				<tbody>';
				 foreach($v->TransactionsDetail as $r){
					 $var.= '<tr>
							<td>'. $r->DocumentNumber .'</td>
							<td>'. $r->DocumentType .'</td>
							<td>'. $r->PostingDate .'</td>
							<td>'. $r->DebitAmount .'</td>
							<td>'. $r->CreditAmount .'</td>
							<td>'. $r->RefrenceNumber .'</td>
							<td>'. $r->Currency .'</td>
							<td>'. $r->Balance .'</td>
							<td>'. $r->Description .'</td>
					</tr>';
				 }
                $var.= '</tbody>
           </table>';
			}
			
			$dompdf = new Dompdf();
			$dompdf->loadHtml($var);
           // $pdf->Image('images/pdf-header.jpg',0,0);
			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A3', 'landscape');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream($challan_number);
			$this->load->view('ledger_acc_details', $data);
			
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */