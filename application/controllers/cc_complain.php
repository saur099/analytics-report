<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cc_complain extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
	    $this->load->model('cc_customer_model');
	    $this->load->model('call_center_model');
    }
    
    public function battery_complain_module($id)
    {
        $get_session_data = $this->session->userdata('logged_in');
         $user_type = $get_session_data['user_type'];
          $user_uuid = $get_session_data['user_uuid'];
          $id = base64_decode($id);
          //echo $id; die;
          $SF_id 				= 	'df109cbf-9c3b-fa40-4472-5bb44c737b3e';
          $data['eng'] 	= $this->call_center_model->get_engg_details($SF_id);
          	if($id == 'zero')
          	{
              	$id = 0;
              	if($user_type == 'SF')
                { 
                   $data['res'] = $this->cc_customer_model->get_battery_complains_zero($user_uuid, $id); 
                   
                    $this->load->view('service_franchise/battery_call_list_view', $data);
                }
                else
                { 
                    $this->load->view('includes/top.php');  
                    $this->load->view('includes/call_center_asset.php');  
                    $this->load->view('service_franchise/sorry_error_view');
                    $this->load->view('includes/sidebar.php'); 
                }
          	}
          	else if($id == 'one')
          	{
              	$id =1;
                if($user_type == 'SF')
                { 
                   $data['res'] = $this->cc_customer_model->get_battery_complains_one($user_uuid, $id); 
                    $this->load->view('service_franchise/battery_call_list_view', $data);
                }
                else
                {
                    $this->load->view('includes/top.php');  
                    $this->load->view('includes/call_center_asset.php');  
                    $this->load->view('service_franchise/sorry_error_view');
                    $this->load->view('includes/sidebar.php'); 
                }
         	}
          	else
          	{
                if($user_type == 'SF')
                {
                   $data['res'] = $this->cc_customer_model->get_battery_complains_zero($user_uuid, $id); 
                    $this->load->view('service_franchise/battery_call_list_view', $data);
                }
                else
                {
                    $this->load->view('includes/top.php');  
                    $this->load->view('includes/call_center_asset.php');  
                    $this->load->view('service_franchise/sorry_error_view');
                    $this->load->view('includes/sidebar.php'); 
                }
          	}
        
    }
	
    public function index()
    {
        $this->load->view('includes/call_center_asset.php');  
        $this->load->view('service_franchise/sorry_error_view');
    }
    
    function  update_assigned_engg()
    {
        $get_session_data = $this->session->userdata('logged_in');
        $user_uuid = $get_session_data['user_uuid'];
        $id = trim(stripslashes($_POST['ck_string']));
        $ids = array_filter(explode('|', $id));
        $enggId = trim(stripslashes($_POST['enggId']));

        $this->db->where_in('cc_product_id', $ids);

        $arrUpd = array(
                'js_start_date'  	=> date('Y-m-d H:i:s'),
                'js_modified_by' 	=> $user_uuid,
                'engg_reassigned_to'    => $enggId,
                're_allocated_by' 	=> $user_uuid,
                'accept_call' 		=> 2
        ); 
       $this->db->update('tbl_cc_products',$arrUpd);
        
        
        $this->db->select('job_sheet');
        $this->db->from('tbl_cc_products');
        $this->db->where_in('cc_product_id', $ids);
        $query  = $this->db->get();
        $enngg = $query->result();
        //print_r($enngg); //die;
        
        foreach ($enngg as $r) 
        {
         $arrCom = array(
                        'allocatedCalls'  	=> 1,
                        'SfAssignedUserId' 	=> $enggId
                        ); 

            $this->db->where('call_id', $r->job_sheet);
           $this->db->update('tbl_service_calls',$arrCom); 
        }
        echo 1;
         //echo $this->db-affected_rows(); die;
        
       // $this->db->query("Update tbl_service_calls set allocatedCalls = 1, SfAssignedUserId = '.$enggId.' where call_id IN (select job_sheet where cc_product_id IN ('.$ids.'))");
       
    }


    public function update_asignee()
    {
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$id = trim(stripslashes($_POST['ck_string']));
		$ids = array_filter(explode('|', $id));
		$enggId = trim(stripslashes($_POST['enggId']));
		
		$this->db->where_in('cc_product_id', $ids);
		
		$arrUpd = array(
			'js_start_date'  	=> date('Y-m-d H:i:s'),
			'js_modified_by' 	=> $user_uuid,
			'engg_reassigned_to'=> $enggId,
			're_allocated_by' 	=> $user_uuid,
			'js_status' 		=> 'Assign',
			'accept_call' 		=> 2
		); 
		echo $this->db->update('tbl_cc_products',$arrUpd);exit;
    }
	
	function add_new_consumer()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$user_name = $get_session_data['user_name'];
		$user_type = $get_session_data['user_type'];
		//echo "Custmer submit"; die;
		//print_r($get_session_data['user_id']); die("Hello");
		$fname 		= stripslashes(trim($_REQUEST['fname']));
		$lname 		= stripslashes(trim($_REQUEST['lname']));
		$category 	= stripslashes(trim($_REQUEST['category']));
		$priority 	= stripslashes(trim($_REQUEST['priority']));
		$mobile_no 	= stripslashes(trim($_REQUEST['mobile_no']));
		$alt_mobile = stripslashes(trim($_REQUEST['alt_mobile']));
		$email_id 	= stripslashes(trim($_REQUEST['email_id']));
		$address 	= stripslashes(trim($_REQUEST['address']));
		$landmark 	= stripslashes(trim($_REQUEST['landmark']));
		$pincode 	= stripslashes(trim($_REQUEST['pincode']));
		$area 		= stripslashes(trim($_REQUEST['area']));
		$city 		= stripslashes(trim($_REQUEST['city']));
		$district 	= stripslashes(trim($_REQUEST['district']));
		$state 		= stripslashes(trim($_REQUEST['state']));
		$consumer_code = stripslashes(trim($_REQUEST['consumer_code']));
		
				function getGUID()
				{
					if (function_exists('com_create_guid')){
						return com_create_guid();
					}else{
						mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
		
				function getsecGUID()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
				function getsecGUIDth()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
				function getsecGUIDfr()
				{
					if (function_exists('com_create_guid'))
					{
						return com_create_guid();
					}
					else
					{
						mt_srand((double)microtime()*10000);
						$charid = strtoupper(md5(uniqid(rand(), true)));
						$hyphen = chr(45);// "-"
						$uuid = substr($charid, 0, 8).$hyphen
							.substr($charid, 8, 4).$hyphen
							.substr($charid,12, 4).$hyphen
							.substr($charid,16, 4).$hyphen
							.substr($charid,20,12);
						return $uuid;
					}
				}
				
		
		$tid   = getGUID();
		$secid = getsecGUID();
		$thrid = getsecGUIDth();
		$forid = getsecGUIDfr();
		 	
		//echo "Custmer submit2"; 
		
		$ser_cust= array(
							'fname' 			=> 		$fname,	
							'lname' 			=> 		$lname,	
							'mobile' 			=> 		$mobile_no,	
							'cust_type' 		=> 		$category,	
							'cust_priority_type'=> 		$priority,	
							'alt_mobile'		=> 		$alt_mobile,	
							'email' 			=> 		$email_id,	
							'address' 			=> 		$address,	
							'landmark' 			=> 		$landmark,	
							'pincode' 			=> 		$pincode,	
							'area' 				=> 		$area,	
							'city' 				=> 		$city,	
							'district' 			=> 		$district,	
							'createdBy' 		=> 		$get_session_data['user_uuid'],	
							'consumer_code' 	=> 		$consumer_code,	
							'cust_status' 		=> 		1
						);
		
		//echo "Custmer submit22"; 				
		$cc_cust= array(
							'fname' 				=> 		$fname,	
							'lname' 				=> 		$lname,	
							'mobile' 				=> 		$mobile_no,	
							'cust_type' 			=> 		$category,	
							'cust_priority_type'	=> 		$priority,	
							'alt_mobile'			=> 		$alt_mobile,	
							'email' 				=> 		$email_id,	
							'address' 				=> 		$address,	
							'landmark' 				=> 		$landmark,	
							'pincode' 				=> 		$pincode,	
							'area' 					=> 		$area,	
							'city' 					=> 		$city,	
							'state' 				=> 		$state,	
							'district' 				=> 		$district,	
							'consumer_code' 		=> 		$consumer_code,
							'createdBy' 	    	=> 		$get_session_data['user_uuid'],	
							'created_type' 	    	=> 		$get_session_data['user_type'],	
							'cust_status' 			=> 		1,	
							'customer_activated_on' => 		date('d-m-Y'),	
							'date_modified' 		=> 		date('Y-m-d H:i:s')	
						);		
          // echo "Custmer submit222";                                                                         
		$dat = 	  array(
		                 'id' => $tid,
		                 'first_name' => $fname,
		                 'last_name' => $lname,
		                 'date_entered' => date('Y-m-d H:i:s'),
		                 'date_modified' => date('Y-m-d H:i:s'),
		                 'phone_mobile' => $mobile_no,
		                 'phone_work' => $alt_mobile,
		                 'primary_address_state' => $state,
		                 'primary_address_city' => $city,
		                 'primary_address_postalcode' => $pincode
		    			);
						
		$d_email   = array(
							'id' => $thrid,
							'email_address' => $email_id,
							'date_created' => date('Y-m-d H:i:s'),
							'date_modified' => date('Y-m-d H:i:s')
						); 	
          				
		$data_cstm = array(
							'id_c' => $tid,
							'primary_address_area_c' => $area,
							'primary_address_1_c' => $address,
							'primary_address_district_c' => $landmark
						   );
							   
		$em_rel =   array(
							'id' => $secid,
							'bean_id' => $tid,
							'bean_module' => 'Contacts',
							'email_address_id' => $thrid
						);
		
	 /* 
		echo"<pre>"; print_r($ser_cust); 
		echo"<pre>"; print_r($cc_cust); 
		echo"<pre>"; print_r($dat); 
		echo"<pre>"; print_r($d_email); 
		echo"<pre>"; print_r($data_cstm); 
		echo"<pre>"; print_r($em_rel); 
		die;
		echo "Custmer submit3"; 
		  */
		$data = $this->cc_customer_model->add_new_consumer($ser_cust, $cc_cust, $dat, $data_cstm, $d_email, $em_rel);
		echo base64_encode($data);
		//echo "Custmer submit33"; die;
		//echo "<pre>";   print_r($fname); die("another"); 
	}
	 public function acceptSF()
	 {
		$user_uuid 	= $get_session_data['user_uuid'];
		$id 		= trim(stripslashes($_POST['ck_string']));
		$type 		= $this->input->post('type');
		$commnet 	= $this->input->post('commnet');
		$ids 		= array_filter(explode('|', $id));
		
		$this->db->where_in('cc_product_id', $ids);
		if($type=='accept')
		{
			$data = array (
				'js_modified_date' => date('Y-m-d H:i:s'),
				'js_modified_by'=>$user_uuid,
				'unaccept_remark' => NULL,
				'accept_call' => 1
			);
                        
			echo $this->db->update('tbl_cc_products',$data);  exit;
		}
		if($type=='unaccept')
		{
			$res = $this->db->query("SELECT id FROM `users` WHERE user_name='unassign' and deleted=0")->result();
			$sf_id = $res[0]->id;
			$data = array (
				'assigned_to' => date('Y-m-d H:i:s'),
				'js_modified_by'=>$user_uuid,
				'assigned_to' => $sf_id,
				'unaccept_remark' => $commnet
			);
			echo $this->db->update('tbl_cc_products',$data);exit;
		}
		echo 0;exit;
	 }
         public function acceptSF_zero()
	 {
		$user_uuid = $get_session_data['user_uuid'];
		$id = trim(stripslashes($_POST['ck_string']));
		$type = $this->input->post('type');
		$ids = array_filter(explode('|', $id));
		$this->db->where_in('cc_product_id', $ids);
		if($type=='accept')
		{
			$data = array (
				'js_modified_date' => date('Y-m-d H:i:s'),
				'js_modified_by'=>$user_uuid,
				'accept_call' => 1
			);
                        
			echo $this->db->update('tbl_cc_products',$data);  exit;
                       /*
                        foreach($)
                        
                        if($this->db->affected_rows > 0)
                        {
                            $this->db->select('job_sheet');
                            $this->db->from('tbl_cc_products');
                            $this->db->where('cc_product_id', $ids)
                        }
                        * 
                        */
		}
		if($type=='unaccept')
		{
			
			$data = array (
				'assigned_to' => date('Y-m-d H:i:s'),
				'js_modified_by'=>$user_uuid,
				'assigned_to' => ''
			);
			echo $this->db->update('tbl_cc_products',$data);exit;
		}
		echo 0;exit;
	 }
        public function update_battery_jobsheet()
        {
            $user_uuid = $get_session_data['user_uuid'];
            $callType      = trim($_POST['callType']);
            $js_no         = trim(json_decode($_POST['js_no']));
            $warrantyVoid  = trim($_POST['warrantyVoid']);
            $res_reason    = trim($_POST['res_reason']);
            $res_date      = trim($_POST['res_date']);
            if($warrantyVoid == 'Yes')
            { 
                $wst = 'OW';
            }
            if($warrantyVoid == 'No')
            { 
                $wst = '';
            }
            if($callType == 'productive')
            {
                $callType = 'productive';
                $res_reason = '';
                $res_date = '';
            }
             $this->db->where('job_sheet', $js_no);
            $data = array (
                            'warranty_avoid'    => $warrantyVoid,
                            'js_modified_date'  => date('Y-m-d H:i:s'),
                            'js_modified_by'    => $user_uuid,
                            'warranty_status'   => $wst,
                            'rescheduled_reson' => $res_reason,
                            'rescheduled_date'  => $res_date,
                            'js_status'         => 'Rescheduled'
			);
	    echo $this->db->update('tbl_cc_products',$data);exit;
             
        }
	
	
}
?>

