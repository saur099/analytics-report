<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Return_Stock extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		$this->load->model('stocks');
		//$data = $this->stocks->getallmystock($get_session_data['username']);
		$data['get_my_serial_numbers'] = $this->stocks->get_my_serial_numbers($get_session_data['username']);
		$data['get_my_return_stock_details'] = $this->stocks->get_my_return_stock_details($get_session_data['username']);
		//echo "<pre>"; print_r($data);
		$this->load->view('return_stock',array('data'=>$data));
		  
	}
	
	public function loadmyproducts()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('stocks');
		$data = $this->stocks->get_stock_product_name($get_session_data['username']);
		$options = "";
		$options .= "<option value=''>Select Product</option>";
		foreach($data as $product) {
		     $options .= "<option value='".  $product->my_product_name ."'>".   $product->my_product_name . "</option>";
		}
		echo $options;
	}
	
	public function get_serial_number()
	
	{
		$get_session_data = $this->session->userdata('logged_in');
		$data = $this->input->post();
		$pro_name  = $data['pro_name'];
		$this->load->model('stocks');
		//$data = $this->stocks->get_serial_number_by_proid($pro_name,$get_session_data['username']);
		//echo "<pre>"; print_r($data);
		/*$options = "";
								
								
	
		foreach($data as $product) {
		     $options .= "<option value='".  $product->serial_number ."'>".   $product->serial_number . "</option>";
		}
		
		echo $options;*/
		//$this->load->view('return_stock',array('data'=>$data));
		
	}
	
	public function add_return_product()
	
	{
		$get_session_data = $this->session->userdata('logged_in');
		$data = $this->input->post();
		$product_name  = $data['product_name'];
		echo $assign_branch  = $data['assign_branch'];
		$return_reason  = $data['return_reason'];
		$this->load->model('stocks');
		$data = $this->stocks->insert_return_stock_details($product_name,$assign_branch,$return_reason,$get_session_data['username']);
		
	}
	
	public function delete_return_stock()
	
	{
		$id=$this->input->post('id');
		$this->load->model('stocks');
		$this->stocks->delete_return_stock($id);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */