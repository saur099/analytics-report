<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branch_Order extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	
	public function index()
	{
		$this->load->model('orders');
		$data = $this->orders->get_branch_order();
		
		 $this->load->view('branch_order/getbranch_order_view', array('data' => $data));

	}
		public function view_order($id)
	{  //$this->load->helper('url');
		$this->load->model('orders');
		$data['order_details'] = $this->orders->getorderbyid($id);
		$data['scheme_details'] = $this->orders->check_scheme_appicable($data['order_details'][0]->quan,$data['order_details'][0]->order_date_entered);
		//echo "<pre>"; print_r($data['order_details']);
		 $this->load->view('orderdetails_view', array('data' => $data));

	}
	
	public function confirm_dispatch($ids)
	{ 
		$data = $ids;
		$this->load->view('order_dispatch', array('data' => $data));
	}
	public function confirm_order_dispatch()
	{ 
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$dispatch = $data['dispatch'];
		$this->load->model('orders');
		$this->orders->update_order($order_id, $dispatch);
		
	}
	
	public function confirm_order($ids)
	{
		$this->load->model('orders');
		$num0 = (rand(10,100));
        $num1 = date("ymd");
        //$num2 = (rand(100,1000));
       // $num3 = time();
    $data = "OR".'#'.$num0 . $num1;
	$this->orders->update_order_id($ids, $data);
		$this->load->view('confirm_order', array('data' => $data));
	}
	
	public function reject_order($ids)
	{
		$data = $ids;
		$this->load->view('reject_order', array('data' => $data));
	}
	
	public function reject_order_details()
	{ 
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$reject = $data['reject'];
		$this->load->model('orders');
		$this->orders->update_order_rejection_details($order_id, $reject);
		
	}
	
	public function scheme()
	
	{    $this->load->model('schemes');
		$data['data'] = $this->schemes->getallschemes();
		$data['sch'] = $this->schemes->getallproducts();
		$this->load->view('scheme_view', $data);
	}
	
	public function loadproducts()
	
	{
		$this->load->model('products');
		$products = $this->products->getallproducts();
		
		$options = "";
		foreach($products->result() as $product) {
		     $options .= "<option value='".  $product->product_id ."'>".   $product->product_name . "</option>";
		}
		echo $options;
	}
	
	public function load_scheme_data()
	{
		
		$this->load->model('schemes');
		$data = $this->schemes->getallschemes();
		$this->load->view('scheme_view',array('data' => $data));
		
	}
	
	public function addscheme()
	{
		$this->load->model('schemes');
		date_default_timezone_set('Asia/Kolkata');
		$data = $this->input->post();
		$scheme_name  = $data['scheme_name'];
		$product_name = $data['product_name'];
	    $quantity = $data['quantity'];
	    $duration = $data['duration'];
	    $start_time = date('Y-m-d',strtotime($data['start_time'])); 
	    $end_time = date('Y-m-d',strtotime($data['end_time']));
		$this->schemes->addschemedata($scheme_name, $product_name,$quantity, $duration,$start_time, $end_time);
		
	}
	
	
	 function array_to_csv()
    {
        header('Content-type: application/vnd.ms-excel');

		// It will be called file.xls
		header('Content-Disposition: attachment; filename="file.xls"');

		// Write file to the browser
		$objWriter->save('php://output');
    }
	
	public function delete_scheme()
	
	{
		$id=$this->input->post('id');
		$this->load->model('schemes');
		$this->schemes->delete_scheme($id);
		
	}
	function upload_excel()
	{
		$this->load->model('stocks');
		$data = $this->stocks->get_secondary_stock($distributor_code);
		//echo "<pre>"; print_r($data);
		$this->load->view('upload_excel_view',array('data' => $data));
	}
	
	public function secondary_stock()
	
	{
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$this->load->model('stocks');
		$data = $this->stocks->get_secondary_stock($distributor_code);
		//echo "<pre>"; print_r($data);
		$this->load->view('secondary_stock_view',array('data' => $data));
	}
	
	public function my_stock()
	
	{
		$this->load->model('stocks');
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$data = $this->stocks->my_stock($distributor_code);
		//echo "<pre>"; print_r($data);
		$this->load->view('my_stock_view',array('data' => $data));
	}
	public function loadZoneComboBox()
	{
		$this->load->model('zones');
		$zones = $this->zones->zoneForComboData();
		$options = "";
		foreach($zones->result() as $zone) {
		     $options .= "<option value='".  $zone->zone_id ."'>".   $zone->zone_name . "</option>";
		}
		echo $options;		
	}
	
	
	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */