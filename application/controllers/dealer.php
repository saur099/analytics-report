<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealer extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	
	public function index()
	{
		//echo "<pre>";print_r($this->session->userdata['logged_in']); die;
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$this->load->model('dealers');
		$data = $this->dealers->dealerTableData($distributor_code);
		$this->load->view('dealer_view',array('data'=>$data));
		
	}
		
	public function addDistributorAction() {
	   
	    $zone_id = trim($this->input->post('zone_id'));
		$name     = trim($this->input->post('distributor_name'));
		$address = trim($this->input->post('address'));
		$phone    = trim($this->input->post('phone'));
		$creation_date = date('Y-m-d h:i:s');
		
		$this->load->model('distributors');
		echo $this->distributors->addDistributorActionData( $zone_id   , $name , $address , $phone , $creation_date);
		
	}
	
	
	public function loadZoneComboBox()
	{
		$this->load->model('zones');
		$zones = $this->zones->zoneForComboData();
		$options = "";
		foreach($zones->result() as $zone) {
		     $options .= "<option value='".  $zone->zone_id ."'>".   $zone->zone_name . "</option>";
		}
		echo $options;		
	}
	
	
	public function load_table_data() {
	       $this->load->model('dealers');
			$dealers_data = $this->dealers->dealerTableData();
			
			?>
			   
			  					
	<?php 		
	}	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */