<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//error_reporting('E_ALL');
class Mod_call_center extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('call_center_model');
	}
	
	function get_consumer_detail()
	{ 
		$mobile 	= 	trim(stripslashes($_POST['mobile']));
		$cons_code  = 	trim(stripslashes($_POST['cons_code']));
		//$mobile = '9012892128';
		/*  echo $cons_code;
		die; */
		
		if(!empty($mobile))
		{
			$data = $this->call_center_model->get_consum_detail($mobile);
		}
		else if(!empty($cons_code))
		{
			$data = $this->call_center_model->get_consum_code($cons_code);
		}
		
		
	    //print_r($data); die;
		if($data > 0)
		{
			$customer_details 	= 	$this->call_center_model->get_customer_details($mobile);
			$res 	= 	$this->call_center_model->get_consumer_comp($data[0]->cust_id);
			$c_prod = 	$this->call_center_model->get_cons_product($data[0]->cust_id);
			//echo "<pre>"; print_r($c_prod); die;
			?><div class="row">
			<div class="col-md-12">
			 

<script>
				$( "#prod_button" ).click(function(){customer_details
					// alert(1);
					$( "#dataTable" ).toggle();
					//$( "#dataTableComplain" ).toggle();
				});
				
				$( "#customer_details" ).click(function() {
					$( "#dataTableComplain1" ).toggle();
				});
				
				$( "#comp" ).click(function() {
					// alert(1);
				//$( "#dataTable" ).toggle();
				$( "#dataTableComplain" ).toggle();
				});
</script>
<script type="text/javascript">
        $('.get_tody_btn').click(function(){
            var ck_string = "";
            $.each($("input[name='today_check']:checked"), function(){  
                ck_string += "~"+$(this).val();  
            });
			alert(ck_string);
			console.log(ck_string);
            if (ck_string ){
                ck_string = ck_string .substring(1);
				alert("Hefkki");
				$.ajax({
							type : 'POST',
							url : '<?php echo base_url(); ?>index.php/cc_product/add_more_prod',
							data : {ck_string : ck_string},
							success: function (response) 
							{
								alert(response);
								if(response == '')
								{
									alert("False");
									return false;
								}
								else
								{
									alert("True");
									alert(response);  	
									window.location = "<?php echo base_url(); ?>index.php/cc_product/complaint/"+response;
								}
							    
							}
						});
            }else{
                alert('Please choose atleast one product to create complain.');
            }


        });
    </script>
	
			<style>  
			button#createComplain {
				background: 1px 1px 1px 1px crimson;
				color: 1px 1px 1px 1px crimson;
				box-shadow: 0px 1px 1px 1px #000;
				background: crimson;
				border: 1px solid crimson;
				padding: 2px;
				padding-left: 9px;
				padding-right: 13px;
				padding-bottom: 3px;
				color: #fff;
			}
		</style>  
		
	<!-- Accordion start -->
	<div class="accordion" id="accordionExample">
	  <div class="card">
		<div class="card-header" id="headingOne">
		  <h5 class="mb-0"><i class="fa fa-angle-down rotate-icon"></i>
			<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			  Customer Details
			</button>
		  </h5>
		</div>

		<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
			<div class="card-body">
				<table class="table table-bordered dataTable no-footer" id="dataTableComplain1" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
					<thead>
						<tr role="row">
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 148px;">Customer Name</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 157px;">Mobile No</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">Email </th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 63px;">Consumer Category</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 175px;">Address1</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 78px;">Consumer Code</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 51px;">Area</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 76px;">City</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;">District </th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">State</th>
							
						</tr>
					</thead>
					<tbody>
					<?php
					 for($i = 0; $i<count($customer_details); $i++){
						?>
						<tr role="row" class="even">
							<td><?php echo $customer_details[$i]->fname .' '. $customer_details[$i]->lname;?></td>
							<td><?php echo $customer_details[$i]->mobile;?></td>
							<td><?php echo $customer_details[$i]->email;?></td>
							<td><?php echo $customer_details[$i]->cust_type;?></td>
							<td><?php echo $customer_details[$i]->address;?></td>
							<td><?php echo $customer_details[$i]->consumer_code;?></td>
							<td><?php echo $customer_details[$i]->area;?></td>
							<td><?php echo $customer_details[$i]->city;?></td>
							<td><?php echo $customer_details[$i]->district;?></td>
							<td><?php echo $customer_details[$i]->state;?></td>
						</tr>
						<?php
					} 
					?>
					
					</tbody>
			    </table>
		    </div>
		</div>
	  </div>
	  <div class="card">
		<div class="card-header" id="headingTwo">
		  <h5 class="mb-0"><i class="fa fa-angle-down rotate-icon"></i>
			<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			   Product Details
			</button>
			<span class="pull-right">
				<!--button id="createComplain" onclick="createCase()" class="btn_complain"-->
					<button id="createComplain" class="btn_complain">
						<a class="get_tody_btn">Create Complain</a>
					</button>
			</span>
			
			<span class="pull-right">
				<button id="createComplain" class="btn btn-info" >
					<a href="<?php echo base_url("index.php/cc_product/product/".base64_encode($customer_details[0]->cust_id).""); ?>"  style="color:#FFFFFF">Add Product</a>
				</button>
			</span>
		  </h5>
		  
		</div>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
			<div class="card-body">
				<table class="table table-bordered" id="dataTable"  width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Select Product</th>
							<th>Product Name</th>
							<th>Product Type</th>
							<th>Purchase Date</th>
							<th>Warranty Start Date</th>
							<th>Warranty End Date</th>
							<th>Warranty Status</th>
							<th>Asset Serial No</th>
							<th>Product SAP Code</th>
						</tr>
					</thead>
					<tbody>
					
						<?php foreach($c_prod as $d) {
							//echo"<pre>"; print_r($c_prod);
						?>
						<tr class="odd gradeX">
							<td>
							<?php //if($d->prodId >0) {?>
								<div class="form-check form-check-success">
									<label class="form-check-label">
										<input type="checkbox" id="today_check" name="today_check" class="form-check-input" value="<?= $d->cc_product_id;?>" >
										<i class="input-helper"></i>
									</label>
								</div>
							<?php // } else {?>
							<!--button class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button-->
							<?php // } ?>
						</td>
							<td><?= $d->prod_name;?></td>
							<td><?= $d->prod_type;?></td>
							<td><?= $d->purchase_date;?> </td>
							<td><?= $d->warranty_start_date;?> </td>
							<td><?= $d->warranty_end_date;?> </td>
							<td><?php echo $d->warranty_status; ?> </td>
							<td><?= $d->asset_serial_no;?> </td>
							<td><?= $d->product_sap_code;?> </td>
						</tr>
					<?php  } ?>
					</tbody>
			   </table>
		  </div>
		</div>
	  </div>
	  <div class="card">
		<div class="card-header" id="headingThree">
		  <h5 class="mb-0"><i class="fa fa-angle-down rotate-icon"></i>
			<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			  Complain Details
			</button>
		  </h5>
		</div>
		<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
			<div class="card-body">
				<table class="table table-bordered dataTable no-footer" id="dataTableComplain" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
					<thead>
						<tr role="row">
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 148px;">Complain No</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 157px;">JobSheet</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 63px;">Complaint Reg Date</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 175px;">Product Name</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 78px;">Product Category</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 83px;">SAP Code</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 51px;">Purchase Date</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 76px;">Appointment Date</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 46px;">Job Sheet Status</th>
							<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 66px;">View/Close Jobsheet</th>
						</tr>
					</thead>
					<tbody>
					<?php if($res) { ?>
					<?php for($i=0;$i<count($res);$i++) { ?>
						<tr role="row" class="even">
							<td><button class="btn btn-danger btn-sm"><?php echo $res[$i]->cc_complain_no;?></button></td>
							<td><button class="btn btn-danger btn-sm"><?php echo $res[$i]->job_sheet;?></button></td>
							 <td><?php echo $res[$i]->complain_created_on;?></td>
							 <td><?php echo $res[$i]->prod_name;?></td>
							 <td><?php echo $res[$i]->catg_name;?></td>
							 <td><?php echo $res[$i]->product_sap_code;?></td>
							 <td><?php echo $res[$i]->purchase_date;?></td>
							 <td><?php echo $res[$i]->appointment_date;?></td>
							 <td><?php if($res[$i]->js_status) { ?><span style="color:#fff; background:green; padding:8px;"><?php echo $res[$i]->js_status;?></span><?php } ?></td>
							 <td>	
								 <a href="<?php echo base_url();?>index.php/cc_product/jobSheetView/<?php echo base64_encode($res[$i]->cc_case_id);?>/<?php echo base64_encode($res[$i]->cc_product_id);?>">
								 </a><center><a href="<?php echo base_url();?>index.php/cc_product/jobSheetView/<?php echo base64_encode($res[$i]->cc_case_id);?>/<?php echo base64_encode($res[$i]->cc_product_id);?>"><button class="btn btn-info btn-sm">View <i class="fa fa-eye"></i></button></a></center>
							 </td>
						</tr>
					<?php } ?>
					<?php } ?>
					</tbody>
			    </table>
		  </div>
		</div>
	  </div>
	</div>
	<!-- End Accordion -->
	
		<?php
		}
		else
		{
			return false;
		}
		
	}
	function add_new_consumer()
	{
		 print_r($_REQUEST); die("Hello");
	}
	
	public function index()
	{
		
		$data['res'] = $this->call_center_model->get_states_names();
		// echo "<pre>"; print_r($data); die;
		$this->load->view('call_center/create_complain_view',$data);

		//$this->load->helper('url');
		// $this->load->view('call_center/create_complain_view');

	}
	
	function create_consumer()
	{
		$this->load->view('call_center/create_consumer_view');
	} 
	function create_product()
	{
		$this->load->view('call_center/create_product_view');
	}
	function create_case()
	{
		$this->load->view('call_center/create_case_view');
	}
	function create_case2()
	{
		$this->load->view('call_center/create_case_view2');
	}
	function success()
	{
		$this->load->view('call_center/success_view');
	}
	function view_jobsheet()
	{
		$this->load->view('call_center/job_sheet_view');
	}
	function js_feedback()
	{
		$this->load->view('call_center/js_feedback_view');
	}
	function complain_module($parameter)
	{
		$parameter = base64_decode($parameter);
		//echo $parameter; die;
		$user_uuid 	= 	$this->session->userdata['logged_in']['user_uuid'];
		$user_name 	= 	$this->session->userdata['logged_in']['user_name'];
		$user_id 	= 	$this->session->userdata['logged_in']['user_id'];
		$user_type 	= 	$this->session->userdata['logged_in']['user_type'];
		$SF_id 		= 	'df109cbf-9c3b-fa40-4472-5bb44c737b3e';
		$data['res'] = 	$this->call_center_model->get_complain_details($user_uuid, $complain_no, $user_type, $parameter);
		$data['eng'] = 	$this->call_center_model->get_engg_details($SF_id);
	   // print_r($data); die;
		$this->load->view('call_center/complaint/case_js_view', $data);
	}
	function job_sheet_details()
	{
		$this->load->view('call_center/job_sheet_view');
	}
	function feedback_module()
	{
		$user_id = $this->session->userdata['logged_in']['user_id'];
		$data['res'] = $this->call_center_model->get_closed_js($user_id);
		$this->load->view('call_center/feedback/js_feedback_view', $data);
	}
	
	public function get_city()
	{
		 $state = $this->input->post('state');
		$city = $this->call_center_model->get_city_names($state);
		echo json_encode($city);
	}
	public function get_districts()
	{
		 $city = $this->input->post('city');
		$district = $this->call_center_model->get_district_names($city);
		echo json_encode($district);
	}
	
	public function get_area()
	{
		 $district = $this->input->post('district');
		$area = $this->call_center_model->get_area_names($district);
		echo json_encode($area);
	}
	public function get_pincode()
	{
		 $area = $this->input->post('area');
		$pincode = $this->call_center_model->get_pincode($area);
		echo json_encode($pincode);
	// echo "<pre>"; print_r($data['district']); die;
	}
	
}
?>

