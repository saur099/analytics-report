<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cust_details extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$username  = $get_session_data['username'];
		$this->load->model('cust_details_model');
		$data['res'] = $this->cust_details_model->get_cust_details($user_uuid); 
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/cust_details_view', $data);
	}
	
	function view_cuspro($mob)
	{
		$id = base64_decode($mob); 
		//echo $id; die;
		if(empty($id))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Sorry ! Some problem occurred. Please try again..')
			window.location.href='".base_url()."index.php/cust_details';
			</SCRIPT>");
		}
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$username = $get_session_data['username'];
		$this->load->model('cust_details_model');
		$data['res'] = $this->cust_details_model->get_cuspro_ash($id, $user_uuid);
					
		//$data['res'] = $this->cust_details_model->get_cuspro($id);
		// echo "<pre>"; print_r($data); die;
		
		$this->load->view('services/view_cuspro_details', $data);
	}
	
	function view_customer($id)
	{
		$id = base64_decode($id); 
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('cust_details_model');
		$data['res'] = $this->cust_details_model->get_cust_detail($id);
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/cust_detailed_view', $data);
	}
		
	public function create_case($ids)
    {
		$ids = base64_decode($ids);
		// echo "cond : ".$ids; die;	
		$get_session_data = $this->session->userdata('logged_in');
		$user_uuid = $get_session_data['user_uuid'];
		$this->load->model('cust_details_model');
		$blck = $this->cust_details_model->block_serial($ids);
		$s_block = $blck[0]->status;
		if($s_block > 0)
		{

			echo ("<SCRIPT LANGUAGE='JavaScript'>
						window.alert('Sorry ! This Battery was replaced against some other Battery, please contact with Service Engineer for details.')
						window.location.href='".base_url()."index.php/cust_details';
						</SCRIPT>");
		}
		else
		{
				$wp = $this->cust_details_model->get_call_info($ids);
				$prod_serail=$wp[0]->callProductSerialNo;
				$caLL_status=$wp[0]->caLL_case_status;
				$replacementMode = $wp[0]->replacementMode;
				$is_tat2_closed = $wp[0]->is_tat2_closed;
				$se_decision = $wp[0]->se_decision;
				//echo "<pre>"; print_r($wp); die("Jai hinf");
				/*  echo "<pre>"; print_r($wp); die("Jai hinf"); 
				die("lksdm");
				die("lksdm");
				die("lksdm");
				//echo  $replacementMode; die(" - Replacement"); 
				*/
				
				if(!empty($prod_serail))
				{
					if(!empty($caLL_status == 0 ))
					{
						//echo "Hello - 0"; die("Hello");
						echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('This Battery was replaced against some other Battery, please contact with Service Engineer for details.')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
					}
					elseif(!empty($caLL_status == 2 ))
					{
						echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('This Battery is already rejected & cannot be processed again.')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
					}
					elseif(!empty($caLL_status == 1 ))
					{
						if($is_tat2_closed==1 || $se_decision =='Same Back' || $se_decision == 'Approved')
						{
							//echo "Hello - 1"; die("Hello");
							$consId = $this->cust_details_model->get_consumerId($ids);
							$id = $consId[0]->consumerId;
							//$data['asset'] = $this->cust_details_model->get_prod_info($id);
							$data['asset'] = $this->cust_details_model->get_prod_info_ash($id,$ids);
							
							$dealer_code=$data['asset'][0]->dealerName;
							$data['res'] = $this->cust_details_model->get_cust_product($id);
							$data['deal'] = $this->cust_details_model->get_dist_dealer($id);
							$data['bak'] = $this->cust_details_model->get_dealer_BAKCode($dealer_code);
							
							$this->load->view('services/create_cust_view', $data);
						}
						else
						{
							//window.alert('This Battery was replaced against some other Battery, please contact with Service Engineer for details.')
							echo ("<SCRIPT LANGUAGE='JavaScript'>
								window.alert('This Battery has a complaint opened and not approved by SE. Please contact SE for more details.')
								window.location.href='".base_url()."index.php/cust_details';
								</SCRIPT>");
						}		
					}
					elseif(!empty($caLL_status == 3 ))
					{ 
						$consId = $this->cust_details_model->get_consumerId($ids);
						$id = $consId[0]->consumerId;
						//$data['asset'] = $this->cust_details_model->get_prod_info($id);
						$data['asset'] = $this->cust_details_model->get_prod_info_ash($id,$ids);
						
						$dealer_code=$data['asset'][0]->dealerName;
						$data['res'] = $this->cust_details_model->get_cust_product($id);
						$data['deal'] = $this->cust_details_model->get_dist_dealer($id);
						$data['bak'] = $this->cust_details_model->get_dealer_BAKCode($dealer_code);
						
						$this->load->view('services/create_cust_view', $data);
					}
				
					else
					{
						//echo "Hello -1 else"; die("Hello");
						echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! A compalint is already opened or rejected for this consumer and product.')
							window.location.href='".base_url()."index.php/cust_details/index';
							</SCRIPT>");
					}
					
				}
				else
				{
					// echo "Hello - l1"; die("Hello");
					// $prod_serail=$wp[0]->callProductSerialNo;
					// $id  =  $wp[0]->callCustId;
					$consId = $this->cust_details_model->get_consumerId($ids);
					 $id = $consId[0]->consumerId;
					
					//$data['asset'] = $this->cust_details_model->get_prod_info($id);
					$data['asset'] = $this->cust_details_model->get_prod_info_ash($id,$ids);
					$dealer_code=$data['asset'][0]->dealerName;
					
					$data['res'] = $this->cust_details_model->get_cust_product($id);
					$data['deal'] = $this->cust_details_model->get_dist_dealer($id);
					$data['bak'] = $this->cust_details_model->get_dealer_BAKCode($dealer_code);
					/* 
							echo "<pre>"; print_r($data['res']); 
							echo "<pre>"; print_r($data['asset']); 
							die("Hello"); */
					$this->load->view('services/create_cust_view', $data);
				}
			
		}		
				
	}
	
	public function verify_consumer()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$con_id 	= 	trim($_POST['con_id']);
		$pr_serial  = 	trim($_POST['pr_serial']);
		$cust_code  = 	trim($_POST['cust_code']);
		if(empty($con_id)){ echo ("<SCRIPT LANGUAGE='JavaScript'> window.alert('Succesfully Updated')  window.location.href='index'; </SCRIPT>"); }
		if(empty($pr_serial)){ echo ("<SCRIPT LANGUAGE='JavaScript'> window.alert('Succesfully Updated')  window.location.href='index'; </SCRIPT>"); }
		
		$this->load->model('cust_details_model');
		$data = $this->cust_details_model->verify_case_model($con_id, $pr_serial);
		//print_r($data);
		if(empty($data))
		{
			//echo "Ifbajsfn";
			$con_id = base64_encode($con_id);
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Success ! Proeed to create the case..')
					window.location.href='create_case/$cust_code';
					</SCRIPT>");
		}
		
		$var = $data[0]->caLL_case_status; 
		//echo $var;
		
		//print_r($data); echo "<br/>".$data[0]->caLL_case_status; die;
		//echo "Hello".$data; die;
		//$var = $data[0]->caLL_case_status;
		//echo $var; die("Hii");
		if($var == 2 or $var == 3)
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Success ! The previous cases  are closed. Please proceed..')
    window.location.href='create_case/$con_id';
    </SCRIPT>");
		}
		else
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('So Sorry ! You have already a pending case of this product & consumer.')
					window.location.href='index';
					</SCRIPT>");
		}
		
		
		//$this->load->view('services/create_cust_view', $data);
		
		
		
	}


    public function new_case()
    {	
		$this->load->helper('smslivfast_helper');
		// echo "<pre>";  print_r($_POST); 
	    $get_session_data = $this->session->userdata('logged_in');
	    $user_uuid = $get_session_data['user_uuid'];
	    $defective = $_POST['defective'];
		if(empty($defective)) { $defective = "0"; }
		$cust_code = trim(stripslashes($_POST['cust_code']));
		$cust_id = trim(stripslashes($_POST['cust_id']));
		$productId = trim(stripslashes($_POST['productId']));
		$assetId = trim(stripslashes($_POST['assetId']));
		$productserial = trim(stripslashes($_POST['productserial']));
		$warrStatus = trim(stripslashes($_POST['warrStatus']));
		$callType = trim(stripslashes($_POST['callType']));
		$sevisit = trim(stripslashes($_POST['sevisit']));
		$setime = trim(stripslashes($_POST['setime']));
		$custremark = trim(stripslashes($_POST['custremark']));
		$remark = trim(stripslashes($_POST['remark']));
		$caller_type = trim(stripslashes($_POST['caller_type']));
		$mobile = trim(stripslashes($_POST['mobile']));
		$letpl_date = trim(stripslashes($_POST['letpl_date']));
		$callsource = trim(stripslashes($_POST['callsource']));
		$contact = trim(stripslashes($_POST['contact']));
		$dealer = trim(stripslashes($_POST['dealer']));
		$dealer_code = trim(stripslashes($_POST['dealer_code']));
		$custPurchaseDate = trim(stripslashes($_POST['custPurchaseDate']));
		$fd = date("YmdHis");
		$rand = rand(00,99);
		$caseId = "C".$fd.$rand; 
		$date1 = date('Y-m-d');
	/* 
		$date1 = new DateTime($custPurchaseDate);
		$date2 = $date1->diff(new DateTime($letpl_date));
		$ageingCase = $date2->days;
		 */
		
		/* 
		echo "<pre>"; print_r($_POST);
		echo "Date1 :".$date1;
		echo "<br>Date2 :".$date2."<br/>";
		echo "<br>Ageing :".$ageingCase."<br/>";
		die("Hello");
		 */
		$this->load->model('cust_details_model');
		$letpl_date = $this->cust_details_model->get_serial_letpl($productserial);
		$letpl_date_res = $letpl_date[0]->primary_sale_date;
		$newLetplDate = date("d-m-Y", strtotime($letpl_date_res));
		//echo "<pre>"; print_r($newLetplDate); die("<br>Hello");
		$d1 = date_create($custPurchaseDate);
		$d2 = date_create($newLetplDate);
		$diff=date_diff($d2,$d1);
		//echo $diff;
		$ageingCase = $diff->format("%R%a");	
		/* 
			echo "<br>hsbd".$date2."<br/>";
			echo "<br>hsbd".$date1."<br/>";
			echo $ageingCase; die("hello");
		*/
		//echo $ageingCase; die("Hello");
		if(empty($productserial))
		{
			echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('We couldn't find product serial. Please try again.')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
		}
		else
		{
		
		/***************Work to do***********************/
				// echo "<pre>";  print_r($_POST); 
	/*     $get_session_data = $this->session->userdata('logged_in');
	    $user_uuid = $get_session_data['user_uuid'];
	    $defective = $_POST['defective'];
		if(empty($defective)) { $defective = "0"; }
		$cust_code = trim(stripslashes($_POST['cust_code']));
		$assetId = trim(stripslashes($_POST['assetId']));
		$productserial = trim(stripslashes($_POST['productserial']));
		$custPurchaseDate = trim(stripslashes($_POST['custPurchaseDate']));
		$warrStatus = trim(stripslashes($_POST['warrStatus']));
		$callType = trim(stripslashes($_POST['callType']));
		$sevisit = trim(stripslashes($_POST['sevisit']));
		$setime = trim(stripslashes($_POST['setime']));
		$custremark = trim(stripslashes($_POST['custremark']));
		$remark = trim(stripslashes($_POST['remark']));
		$caller_type = trim(stripslashes($_POST['caller_type']));
		$mobile = trim(stripslashes($_POST['mobile']));
		$letpl_date = trim(stripslashes($_POST['letpl_date']));
		$callsource = trim(stripslashes($_POST['callsource']));
		$contact = trim(stripslashes($_POST['contact']));
		$dealer = trim(stripslashes($_POST['dealer']));
		$dealer_code = trim(stripslashes($_POST['dealer_code']));
		$fd = date("Ymdhis");
		$rand = rand(0,99);
		$caseId = "C".$fd.$rand; 
		
		$date1 = date('Y-m-d');
	
		$date1 = new DateTime($custPurchaseDate);
		$date2 = $date1->diff(new DateTime($letpl_date));
		$ageingCase = $date2->days;
		echo $ageingCase;
		$date = str_replace('/', '-', $custPurchaseDate);
		echo date('Y-m-d', strtotime($date));
        die("Hello"); */
		/**************************************/
		
		
		
		
		
		
		
		/* $end = date("Y-m-d");
		$dmy = date("d-m-Y",  strtotime($end));
		echo "Current".$dmy."<br/>";
		echo "Letpl.".$letpl_date."<br>";
		echo $warrStatus;
		 $days_between = ceil(abs($dmy - $letpl_date) / 86400);
		echo $days_between."<>";   
		*/
		/* 
		$now = time(); // or your date as well
		$your_date = strtotime($letpl_date);
		$datediff = $now - $your_date;

		$ageingCase = round($datediff / (60 * 60 * 24));
        // echo $ageingCase;
		// die;
		 */
		
		$callStatus = "1";
		$callStage = "9";
		$dt = date("d-m-Y H:i:s");
		$cure_date = date('Y-m-d');
		$data = array(
		                'callCustId' => $cust_code,
		                'callCreatedBy' => $user_uuid,
		                'isDefective' => $defective,
		                'callAssetId' => $assetId,
		                'callProductSerialNo' => $productserial,
		                'callType' => $callType,
		                'se_estd_visit_date' => $sevisit,
		                'se_estd_visit_time' => $setime,
		                'customerRemarks' => $custremark,
		                'Remarks' => $remark,
		                'warranty' => $warrStatus,
		                'claimAgeing' => $ageingCase,
		                'callerType' => $caller_type,
		                'callerMobile' => $mobile,
		                'callSource' => $callsource,
		                'contactPerson' => $contact,
		                'dealerName' => $dealer,
		                'dealer_code' => $dealer_code,
		                'callStatus' => $callStatus,
		                'callStage' => $callStage,
                                'call_id' => $caseId,
                                'claim_prod_pri_key'=>$productId,
                                'cust_prim_key'=>$cust_id,
                                'cur_date_entered'=>$cure_date,
                                'modified_date_time'=>$dt
			    );
		
		
	   
		$chkBlock = $this->cust_details_model->is_blocked_serial($productserial);
		//die("Heu");
		$chkBlocking = $chkBlock[0]->status;
		//print_r($chkBlock); die("jai");
		if($chkBlocking>0)
		{
			//echo "Fail".$chkBlocking; die;
			echo ("<SCRIPT LANGUAGE='JavaScript'>
							window.alert('Sorry ! This Battery was replaced against some other Battery, please contact with Service Engineer for details.')
							window.location.href='".base_url()."index.php/cust_details';
							</SCRIPT>");
		}
		else
		{
			//echo "Success".$chkBlocking; die;
						$chkCase = $this->cust_details_model->check_case($productserial);
						//print_r($chkCase);  die("Case Checl");
						$cse = $chkCase[0]->caLL_case_status;
						$se_dec = $chkCase[0]->se_decision;
						$cse1 = $chkCase[1]->caLL_case_status;
						$tat2Clos = $chkCase[0]->is_tat2_closed;
						$tat2Clos1 = $chkCase[1]->is_tat2_closed;
						$se_dec1 = $chkCase[1]->se_decision;
						//echo $cse; die("Hii");
						
						if(!empty($cse))	
						{		//echo "Not empty"; die;	  || $cse == 1 || $cse == 2
							if($cse == 2)
							{
									echo ("<SCRIPT LANGUAGE='JavaScript'>
											window.alert('So Sorry ! A complaint has already rejected on this product serial.')
											window.location.href='".base_url()."index.php/service_cases';
											</SCRIPT>");
							
							}
							else if($cse == 0)
							{
									echo ("<SCRIPT LANGUAGE='JavaScript'>
											window.alert('So Sorry ! A complaint has already opened on this product serial.')
											window.location.href='".base_url()."index.php/service_cases';
											</SCRIPT>");
							}
							else if($cse == 1)
							{
										
										if($tat2Clos == 1 && empty($cse1) && empty($tat2Clos1))
										{
											$data = $this->cust_details_model->add_new_case($data);
											$mobs = $this->cust_details_model->get_all_mobiles($data);
											//echo "<pre>"; print_r($mobs); die(" - Hello");
											// SEND SMS START
											$cust_mob = $mobs[0]->cust_mob;
											$call_id = $mobs[0]->call_id;
											$dist_mob = $mobs[0]->dist_mob;
											$se_mob = $mobs[0]->se_mobile;
											$dist = $mobs[0]->dist;
											$deal_mob = $mobs[0]->deal_mob;
											/* 
											 $send_se = sendsms($deal_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with your area distributor . LivServe Team" );
											 $send_se = sendsms($se_mob,  "Dear Engineer, Please coordinate with ".$dist." ".$dist_mob." for the closure of complaint No ".$call_id." . LivServe Team" );
											$send_dist = sendsms($dist_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );
											$send_cust = sendsms($cust_mob, "Dear Customer, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );	
 */
											
											// SEND SMS  END
											
											//print_r($data); die;
											//print_r($data); die("Hello");
										
											if(empty($data))
											{
												redirect('index');                 
											}
											else
											{
												
													$data = base64_encode($data);
													redirect('fault_parts/index/'.$data);
											}
										}
										else if($tat2Clos == 1 && $cse1=="1" && $tat2Clos1 == "1")
										{
											$data = $this->cust_details_model->add_new_case($data);
											$mobs = $this->cust_details_model->get_all_mobiles($data);
											
											//echo "<pre>"; print_r($mobs); die(" - Hello");
											
											// SEND SMS START
											
											$cust_mob = $mobs[0]->cust_mob;
											$call_id = $mobs[0]->call_id;
											$dist_mob = $mobs[0]->dist_mob;
											$se_mob = $mobs[0]->se_mobile;
											$dist = $mobs[0]->dist;
											$deal_mob = $mobs[0]->deal_mob;
											/* 
											 $send_se = sendsms($deal_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with your area distributor . LivServe Team" );
											 $send_se = sendsms($se_mob,  "Dear Engineer, Please coordinate with ".$dist." ".$dist_mob." for the closure of complaint No ".$call_id." . LivServe Team" );
											$send_dist = sendsms($dist_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );
											$send_cust = sendsms($cust_mob, "Dear Customer, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );	
 */
											
											// SEND SMS  END
											
												//print_r($data); die;
												//print_r($data); die("Hello");
											
												if(empty($data))
												{
													redirect('index');                 
												}
												else
												{
													
														$data = base64_encode($data);
														redirect('fault_parts/index/'.$data);
												}
										}
										else if($se_dec=="Approved" || $se_dec1=="Same Back")
										{
											$data = $this->cust_details_model->add_new_case($data);
											$mobs = $this->cust_details_model->get_all_mobiles($data);
											//echo "<pre>"; print_r($mobs); die(" - Hello");
											// SEND SMS START
											
											$cust_mob = $mobs[0]->cust_mob;
											$call_id = $mobs[0]->call_id;
											$dist_mob = $mobs[0]->dist_mob;
											$se_mob = $mobs[0]->se_mobile;
											$dist = $mobs[0]->dist;
											$deal_mob = $mobs[0]->deal_mob;
											/* 
											 $send_se = sendsms($deal_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with your area distributor . LivServe Team" );
											 $send_se = sendsms($se_mob,  "Dear Engineer, Please coordinate with ".$dist." ".$dist_mob." for the closure of complaint No ".$call_id." . LivServe Team" );
											$send_dist = sendsms($dist_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );
											$send_cust = sendsms($cust_mob, "Dear Customer, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );	

											 */
											// SEND SMS  END
											
												//print_r($data); die;
												//print_r($data); die("Hello");
											
												if(empty($data))
												{
													redirect('index');                 
												}
												else
												{
													
														$data = base64_encode($data);
														redirect('fault_parts/index/'.$data);
												}
										}
										else
										{
												 echo ("<SCRIPT LANGUAGE='JavaScript'>
													window.alert('So Sorry ! A complaint is already open on this product serial.')
													window.location.href='".base_url()."index.php/service_cases';
													</SCRIPT>");
										}	
							}
							
							else
							{
								$data = $this->cust_details_model->add_new_case($data);
								$mobs = $this->cust_details_model->get_all_mobiles($data);
								//echo "<pre>"; print_r($mobs); die(" - Hello");
								// SEND SMS START
								$cust_mob = $mobs[0]->cust_mob;
								$call_id = $mobs[0]->call_id;
								$dist_mob = $mobs[0]->dist_mob;
								$se_mob = $mobs[0]->se_mobile;
								$dist = $mobs[0]->dist;
								$deal_mob = $mobs[0]->deal_mob;
								
								 $send_se = sendsms($deal_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with your area distributor . LivServe Team" );
								 $send_se = sendsms($se_mob,  "Dear Engineer, Please coordinate with ".$dist." ".$dist_mob." for the closure of complaint No ".$call_id." . LivServe Team" );
								$send_dist = sendsms($dist_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );
								$send_cust = sendsms($cust_mob, "Dear Customer, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );	

								// SEND SMS  END
											
								//print_r($data); die;
								//print_r($data); die("Hello");
								
								if(empty($data))
								{
									redirect('index');                 
								}
								else
								{
									$data = base64_encode($data);
									redirect('fault_parts/index/'.$data);
								}
							}
						}
						else
						{
							$data = $this->cust_details_model->add_new_case($data);
							$mobs = $this->cust_details_model->get_all_mobiles($data);
							
							//echo "<pre>"; print_r($mobs); die(" - Hello");
							// SEND SMS START
							$cust_mob = $mobs[0]->cust_mob;
							$call_id = $mobs[0]->call_id;
							$dist_mob = $mobs[0]->dist_mob;
							$se_mob = $mobs[0]->se_mobile;
							$dist = $mobs[0]->dist;
							$deal_mob = $mobs[0]->deal_mob;
							/* 
							 $send_se = sendsms($deal_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with your area distributor . LivServe Team" );
							 $send_se = sendsms($se_mob,  "Dear Engineer, Please coordinate with ".$dist." ".$dist_mob." for the closure of complaint No ".$call_id." . LivServe Team" );
							 $send_dist = sendsms($dist_mob,  "Dear Partner, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );
							$send_cust = sendsms($cust_mob, "Dear Customer, your complaint no is ".$call_id.". For any assistance please coordinate with LivServe executive at 9599292091/92 . LivServe Team" );	
 */
							// SEND SMS  END
											
							//echo "hello"; die;
									$data = base64_encode($data);
									redirect('fault_parts/index/'.$data);
						}
		}

	  // print_r($data); die;
	  }
	}
	
	
}
