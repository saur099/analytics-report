<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		$this->load->model('orders');
		$this->load->model('dealers');
		$this->load->model('schemes');
		$this->load->model('users');
		$data['scheme_data'] = $this->schemes->getallschemes();
		$data['account_data'] = $this->dealers->dealerTableData($get_session_data['username']);
		$data['order_data'] = $this->orders->get_order_by_type($login_type,$get_session_data['username']);
		$data['get_session_data_name'] = $this->users->get_current_user_details($get_session_data['username']);
		$this->load->view('dashboard_view', array('data' => $data));
		  
	}
	
	public function change_pass()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		$usern = $get_session_data['username'];
		$this->load->view('change_pass_view');
		
	}
	public function change_pass_process()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		$usern = $get_session_data['username'];
		$this->load->view('change_pass_view');
		
	}
	
	public function user_profile()
	
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->model('users');
		$data = $this->users->get_current_user_details($get_session_data['username']);
		$this->load->view('user_profile', array('data' => $data));
	}
	
	public function user_profile_update()
	{
		 $this->load->model('users');
		 $distributor_name=$this->input->post('distributor_name');
		 $billing_address_city=$this->input->post('billing_address_city');
		 $billing_address_state=$this->input->post('billing_address_state');
		 $billing_address_postalcode=$this->input->post('billing_address_postalcode');
		 $mobile=$this->input->post('mobile');
		 $first_name=$this->input->post('first_name');
		 $last_name=$this->input->post('last_name');
		 $email_address=$this->input->post('email_address');
		 $hub_name=$this->input->post('hub_name');
		 $password=$this->input->post('password');
		 $user_id=$this->input->post('user_id');
		 $data = $this->users->update_current_user_details($distributor_name,$billing_address_city,$billing_address_state,$billing_address_postalcode,$mobile,$first_name,$last_name,$email_address,$hub_name,$password,$user_id);
		 
	}
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */