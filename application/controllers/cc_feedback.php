<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require_once('application/libraries/vendor/autoload.php'); 
// reference the Dompdf namespace
//use Dompdf\Dompdf;
class Cc_feedback extends CI_Controller 
{
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
			$this->load->model('cc_feedback_model');
			
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('sap_api/credit_note_view');
	}
	function view_details($id)	
	{
		$id = base64_decode($id);
		//echo $id."jsdbf";
		$get_session_data 	= 	$this->session->userdata('logged_in');
		$user_uuid 	 		= 	$get_session_data['user_uuid'];
		$user_id	 		= 	$get_session_data['user_id'];
		 
		$data['id'] = array('feedback_id' => $id);
		$data['js'] = $this->cc_feedback_model->get_js($id);
		$data['res'] = $this->cc_feedback_model->view_feedback_details($id);
		//echo "<pre>"; print_r($data['res']); die;
		if(!empty($data['res']))
		{
			//echo "hii";
			//echo base64_encode($data);
			$this->load->view('includes/top.php');  
			//$this->load->view('includes/call_center_asset.php');  
			$this->load->view('includes/sidebar.php');  
			$this->load->view('call_center/feedback/feedback_details_view', $data);	
		}
		else
		{
			echo "<script>
					alert('Sorry ! You can not provide feedback this time. Please contact admin. ');
					window.location.href=".base_url()."/index.php/mod_call_center/feedback_module;
					</script>";
			
		}
		
	}

	function update_feedback()
	{
		$get_session_data 	= 	$this->session->userdata('logged_in');
		$user_uuid 	 		= 	$get_session_data['user_uuid'];
		$user_id	 		= 	$get_session_data['user_id'];
		$feedbackId = 	trim($_POST['feedbackId']);
		//print_r($_POST);
		
		$feedDone = $this->cc_feedback_model->is_feedback_given($feedbackId);
		$final_cl = $feedDone[0]->final_closure;
		//print_r($final_cl); die;
		if($final_cl==0)
		{	
				$js_no		 = 	trim($_POST['js_no']);
				$happy_code = 	trim($_POST['happy_code']);
				$input_1 	= 	trim($_POST['input_1']);
				$input_2 	= 	trim($_POST['input_2']);
				$input_3 	= 	trim($_POST['input_3']);
				$input_4 	= 	trim($_POST['input_4']);
				$input_5 	= 	trim($_POST['input_5']);
				$input_6 	= 	trim($_POST['input_6']);
				$finalClosure =0;
				
				if($input_1 >= 4 && $input_2 >= 4 && $input_3 >= 4)
				{
					$finalClosure = 'Closed';
				}
				else if($input_2 >= 4 && $input_1 >= 4 && $input_5 >= 4 && $input_3 >= 4 && $input_4 >= 4)
				{
					$finalClosure = 'Closed';
				}
				else if($input_4 >= 4 && $input_2 >= 4 && $input_3 >= 4)
				{
					$finalClosure = 'Closed';
				}
				else if($input_5 >= 4 && $input_2 >= 4 && $input_3 >= 4)
				{
					$finalClosure = 'Closed';
				}
				else if($input_2 >= 4 && $input_1 >= 4 && $input_5 >= 4)
				{
					$finalClosure = 'Closed';
				}
				else
				{
					$finalClosure = 'Open';
				}
				
				//echo $finalClosure; die;
				/* 
				$arr = array($input1, $input_2, $input_3, $input_4, $input_5);
				
				if($input_1 >= 4 && $input_2 >= 4 && $input_3 >= 4)
				{
					$finalClosure = 1;
				}
				else if($input_3 >= 4 && $input_4 >= 4 && $input_5 >= 4)
				{
					$finalClosure = 1;
				}
				if($input_1 >= 4 && $input_2 >= 4 && $input_3 >= 4)
				{
					$finalClosure =1;
				}
				if($input_3 >= 4 && $input_3 >= 4 && $input_4 >= 4)
				{
					$finalClosure =1;
				}
				*/
				
						
				$upd_feed = array(
									'happy_code' => trim(stripslashes($happy_code)),
									'js_feedback_status' 	=> 	$finalClosure,
									'ans1' 	=> 	$input_1,
									'ans2' 	=> 	$input_2,
									'ans3' 	=> 	$input_3,
									'ans4' 	=> 	$input_4,
									'ans5' 	=> 	$input_5,
									'final_closure' => $finalClosure,
									'feedbackGivenOn' => date('Y-m-d H:i:s'),
									'feedbackGivenBy' => $user_uuid,
									'feedbackClosedOn' => date('Y-m-d H:i:s'),
									'feedbackIP' => $_SERVER['REMOTE_ADDR']
									
								  );
				$upd_js =  array(
									'js_status' 	   =>  $finalClosure,
									'js_final_closure' =>  $finalClosure,
									'js_closed_by' 	   =>  $user_uuid,
									'js_end_date' 	   =>  date('Y-m-d H:i:s')
								);
				$ins_remark = array(
										'remarks' => $remarks,
										'ans' => $input_6,
										'feedback_id' => $feedbackId,
										'jobsheet' => $js_no,
										'remark_by' => $user_uuid
									);
/* 
				echo "<pre>"; print_r($ins_remark);						
				echo "<pre>"; print_r($upd_js);						
				echo "<pre>"; print_r($upd_feed);	
                die;	 */			
			/* 	$this->db->where('js_feedback_id', $feedbackId);
			$this->db->update('tbl_cc_feedback', $upd_feed);
			echo $this->db->last_query();
			die; */
				$data = $this->cc_feedback_model->verify_feedback($feedbackId, $user_id, $upd_feed, $upd_js, $js_no, $ins_remark);
				//echo $data; die;
				if($data > 0)
				{
					$data = 'success';
				}
				else
				{
					$data ='fail';
				}
		}
		else
		{
			$data = "fail";
		}		
		echo $data;
	}
	
}
?>

