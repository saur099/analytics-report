<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');error_reporting(1);

class Dashboard extends CI_Controller {
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['user_name']) 
            //return redirect('login', 'refresh');  
            return 1;  
    }
	
	public function index()
	{   
	
		//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		$user 		= 	$get_session_data['user_type'];
		$user_id 	= 	$get_session_data['user_id'];		
		$user_name 	= 	$get_session_data['user_name']; 
		$data = array('user_name'=>$user_name,'user_id'=>$user_id, 'user_id');
		//print_r($get_session_data); die("Hii");
		$this->load->view('includes/admin_top'); 			
		$this->load->view('includes/admin_sidebar');			
		$this->load->view('includes/admin_header');  			
		$this->load->view('dashboard_view', $data);				
		$this->load->view('includes/admin_js_holder');
	
	}
	public function check_transactions()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user 		= 	$get_session_data['user_type'];
		$user_id 	= 	$get_session_data['user_id'];		
		$user_name 	= 	$get_session_data['user_name']; 
		$data['user'] = array('user_name'=>$user_name,'user_id'=>$user_id, 'user_id');
		//print_r($get_session_data); die("Hii");
		
		$this->load->model('bussiness_model');
		$data['t'] = $this->bussiness_model->fetch_transactions($user_name, $user, $user_id);
		$this->load->view('includes/admin_top'); 			
		$this->load->view('includes/admin_sidebar'); 			
		$this->load->view('includes/admin_header'); 			
		$this->load->view('transactions_view', $data);			
		$this->load->view('includes/admin_footer');			
		$this->load->view('includes/admin_js_holder');
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>
  