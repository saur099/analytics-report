  <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <style>
   .row.filter {
    padding-left: 17px;
    padding-right: 17px;
}
   </style>
   <script>
function myFunction() {
    var x = document.getElementById("cnf_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction1() {
    var x = document.getElementById("old_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction2() {
    var x = document.getElementById("new_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}







function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('new_pass');
    var pass2 = document.getElementById('cnf_pass');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
   <title>Services | Ticket Replyies</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>

<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">

  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>index.php/service_cases/verify_serial"><button class="btn btn-info btn-sm"><i class="fa fa-angle-left "></i> Go Back</button></a>
        </li>
        <li class="breadcrumb-item active">Ticket Replies</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
       <div class="card-header">
          <i class="fa fa-dashboard"></i> Ticket Replies
		
		  </div>
        
		
		
		
		<br/>
				  <div class="row filter">
				    <div class="col-md-3">
						<label><b>Support Ticket Id : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->supportTicket; ?>"   readonly>
					</div>
					<div class="col-md-3">
						<label><b>Support Case Id : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->supportCaseId; ?>"   readonly>
					</div>
					<div class="col-md-3">
						<label><b>Support Issue In : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->supportIssueIn; ?>"   readonly>
					</div>
					<div class="col-md-3">
						<label><b>Support Ticket Date : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->ticketRaisedOn; ?>"   readonly>
					</div>
				</div>
				
				 <div class="row filter">
				    <div class="col-md-12">
						<label><b>Support Subject : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->supportSubject; ?>"   readonly>
					</div>
				</div>
				
				<div class="row filter">	
					<div class="col-md-12">
						<label><b>Support Description : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->supportDescription; ?>"   readonly>
					</div>
				</div>
				
				 <div class="row filter">
				    <div class="col-md-3">
						<label><b>Customer Name : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->customer_fname." ".$rp[0]->customer_lname; ?>"   readonly>
					</div>
					<div class="col-md-3">
						<label><b>Customer Purchase Date : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->custPurchaseDate; ?>"   readonly>
					</div>
					<div class="col-md-3">
						<label><b>Mobile No : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->mobile_no; ?>"   readonly>
					</div>
					<div class="col-md-3">
						<label><b>Pincode : </b></label>
						<input type="text" class="form-control" value="<?php echo $rp[0]->pincode; ?>"   readonly>
					</div>
				</div>
				<br/>
				
				 <div class="row filter">
				 <div class="col-md-6">
				  <label><b>Support ticket Status :</b></label>
				  <input type="text" class="form-control" value="<?php echo $rp[0]->supportStatus; ?>"   readonly>
				 </div>
				    <div class="col-md-6">
						<label><b>Support Images : </b></label>
						<?php if(empty($d->supportUpload)) { ?>
						<a href="<?php echo base_url();?>uploads/<?php echo $d->supportUpload;?>" height='50' width="50">
							<img src="<?php echo base_url();?>uploads/<?php echo $d->supportUpload;?>"   readonly>
						</a>
						<?php } else {?>
						<div class="alert alert-danger" role="alert">
						  Sorry ! No Images uploaded..
						</div>
						<?php } ?>
					</div>
				</div>
				<hr>
				
				<h4 align="center"><u> Ticket Replies</u></h4>
				<br/>
		<form id="form_manual" name="name" action="<?php echo base_url(); ?>index.php/support/replying_ticket" method="post">
		
		<div class="row filter">
		<div class="col-md-2">
						<label><b>Reply on Support Ticket : </b></label>
						<input type="text" name="tktId" class="form-control" value="<?php echo $rp[0]->supportTicket; ?>"  readonly >
					</div>
				    <div class="col-md-3">
						<label><b>Select Status of Support Ticket: </b></label>
						<select name="repStatus" class="form-control">
						    <option value="Answered">Answered</option>
						    <option value="Declined">Declined</option>
						    <option value="Waiting">Waiting</option>
						    <option value="Resolved by Saurav">Resolved by Saurav</option>
						    <option value="Resolved by Admin">Resolved by Admin</option>
						    <option value="Checking">Checking</option>
						</select>
					</div>
					<div class="col-md-5">
						<label><b>Reply Subject : </b></label>
						<input type="text" class="form-control" name="repSub" value="<?php echo $rp[0]->supportSubject; ?>"   >
					</div>
					<div class="col-md-2">
						<label><b>Reply Issue In : </b></label>
						<input type="text" class="form-control"  name="repIss" value="<?php echo $rp[0]->supportIssueIn; ?>"   >
					</div>
				</div>
				<div class="row filter">
					<div class="col-md-12">
					<label><b>Reply to Distributor : </b></label>
					<input type="text" class="form-control" name="repToDist" value="<?php echo $rp[0]->distUUID; ?>"  readonly >
					</div>
				</div>	
				<div class="row filter">
					<div class="col-md-12">
					<label><b>Reply Description : </b></label>
						<textarea class="form-control" type="text" name="Description" row="8" cols="50">		</textarea>
						</div>
				</div>
				<br/>
				<div class="row filter">
					<div class="col-md-12">
					<center><button type="submit" class="btn btn-primary " value="submit">Submit</button></center>
					</div>
					</div>
		</form>
		</div>
		</div>
	
	

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 
