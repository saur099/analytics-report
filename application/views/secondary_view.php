  <?php 
   //secho "<pre>"; print_r($d); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Orders List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Order List</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Order's Record List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                            <tr>
                                <th>Order Mode</th>
								<th>No Of Order</th>
								<th>Dealer Name</th>                         
                                <th>Dealer Code</th>
                                <th>Hub Name</th>
                                <th>Order Date</th>
                                <!--<th>Order Status</th>-->
                                <th>Details</th>								
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) { //echo "<pre>"; print_r($d); die;
							
							$result = base64_encode(str_replace(array(',', ' '), '_', $d->ids));
							?>
									<tr class="odd gradeX">
										<td><?php echo strtoupper($d->order_type); ?></td>
										<td><?=  $d->quantity;?> </td>
										<td><?php echo strtoupper($d->dealer_name); ?></td>
										<td><?php echo strtoupper($d->dealer_code); ?></td>
										<td><?php echo strtoupper($d->hub_name);?> </td>
										<td><?php
											$date = str_replace('/', '-', $d->order_date_entered);
											echo date('d-m-Y', strtotime($date));
											?> </td>
										
										<!--<td><?=  $d->order_status;?> </td>-->
										<td><?php echo anchor('secondary/view_order/'.$result, ' <button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Info</button>', array('class' => '', 'id' => '')); ?></td>
										
									</tr>
                            <?php } ?>
                            </tbody>
                          </table>
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
	?>
   <script>$('#dataTable').DataTable( { "order": [[ 0,"desc" ]]});</script>
	
  </div>
</body>

</html>
