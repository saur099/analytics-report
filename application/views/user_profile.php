<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>User Profile</title>
	
	<?php    $this->load->view('library');    ?>
	
    <script>
        $(document).ready(function() {
		
		    $("#product_name").load("http://52.66.23.135/cis/index.php/branch_order/loadproducts" , function(){});
		  ///  $("#table_distributor").load("http://52.66.23.135/cis/index.php/branch_order/load_scheme_data" , function(){});

            $('#distributor_btn').click(function(){
			   var  distributor_name  = $('#distributor_name').val();
			   var  billing_address_city = $('#billing_address_city').val();
			   var  billing_address_state = $('#billing_address_state').val();
			   var  billing_address_postalcode = $('#billing_address_postalcode').val();
			   var  mobile = $('#mobile').val();
			   var  first_name = $('#first_name').val();
			   var  last_name = $('#last_name').val();
			   var  email_address = $('#email_address').val();
			   var  hub_name = $('#hub_name').val();
			   var  user_id = $('#user_id').val();
			   var  password = $('#password').val();
			   
			   var  dataStr = "distributor_name="+distributor_name+"&billing_address_city="+billing_address_city+"&billing_address_state="+billing_address_state +"&billing_address_postalcode="+billing_address_postalcode +"&mobile="+mobile +"&first_name="+first_name +"&last_name="+last_name +"&email_address="+email_address +"&hub_name="+hub_name +"&password="+password +"&user_id="+user_id;
					  
				
				if(user_id=="") {
				   alert('Enter Scheme name');
				   $('#user_id').focus();
				   return false;
				}else{
				   $.ajax({
				      type:"post" ,
					  url: "http://52.66.23.135/cis/index.php/dashboard/user_profile_update/" ,
					  data:		dataStr ,
					  async:true ,
						success:function(st){ //alert(st);
						    
							//'data saved successfully'
							//$("#product_name").load("http://52.66.23.135/cis/index.php/dashboard/user_profile" , function(){});
							location.reload();
							//$("#table_distributor").load("http://52.66.23.135/cis/index.php/branch_order/load_scheme_data" , function(){});
						}	
				   });
				}	
            });           
        });
    </script>
	
	<script>
function delete_scheme(ids) { 
    var x=confirm("Are you sure to delete record?");
	
  if (x == true) {
	  
	  $.ajax({
				      type:"post" ,
					  url: "http://52.66.23.135/cis/index.php/branch_order/delete_scheme/" ,
					  data: {"id":ids},
					  async:true ,
						success:function(st){ 
						    $("#product_name").load("http://52.66.23.135/cis/index.php/branch_order/loadproducts" , function(){});
							location.reload();
							
						}	
				   });
				   
    
  } else {
    return false;
  }
}
</script>

 </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation');  ?>
		
        <div id="page-wrapper">

            
<link href="http://52.66.23.135/cis/css/dcalendar.picker.css" rel="stylesheet" type="text/css">
<br/>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                User Profile
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form">
						<div class="form-group">
                                <label>User Name</label>
           <input  type="text" class="form-control" id="user_name" name="user_name" value = "<?php echo $data[0]->user_name;?>"readonly/>
                            </div>
                            <div class="form-group">
                                <label>Distributor Name</label>
          <input  type="text" class="form-control" id="distributor_name" name="distributor_name" value = "<?php echo $data[0]->distributor_name;?>" readonly/>
                            </div>
							 <div class="form-group">
                                <label>City</label>
         <input  type="text" class="form-control" id="billing_address_city" name="billing_address_city" value = "<?php echo $data[0]->billing_address_city;?>">
                            </div>
                            <div class="form-group">
                                <label>State</label>
           <input  type="text" class="form-control" id="billing_address_state" name="billing_address_state" value = "<?php echo $data[0]->billing_address_state;?>">
                            </div>

                     <div class="form-group">
                                <label>Postal Code</label>
           <input  type="text" class="form-control" id="billing_address_postalcode" name="billing_address_postalcode" value = "<?php echo $data[0]->billing_address_postalcode;?>">
                            </div>



<div class="form-group">
                                <label>Mobile</label>
           <input  type="text" class="form-control" id="mobile" name="mobile" value = "<?php echo $data[0]->mobile;?>">
                            </div>

<div class="form-group">
                                <label>First Name</label>
           <input  type="text" class="form-control" id="first_name" name="first_name" value = "<?php echo $data[0]->first_name;?>">
                            </div>

<div class="form-group">
                                <label>Last Name</label>
           <input  type="text" class="form-control" id="last_name" name="last_name" value = "<?php echo $data[0]->last_name;?>">
                            </div>
							
							<div class="form-group">
                                <label>Hub Name</label>
           <input  type="text" class="form-control" id="hub_name" name="hub_name" value = "<?php echo $data[0]->hub_name;?>" readonly/>
                            </div>

<div class="form-group">
                                <label>Email</label>
           <input  type="text" class="form-control" id="email_address" name="email_address" value = "<?php echo $data[0]->email_address;?>">
                            </div>
							
							<div class="form-group">
                                <label>Change Password</label>
           <input  type="password" class="form-control" id="password" name="password" value = "<?php echo $data[0]->pass;?>">
                            </div>

							
                           <input type="hidden" name ="user_id" id = "user_id" value = "<?php echo $data[0]->user_id;?>">
							
                            <button type="button" id="distributor_btn" class="btn btn-success">Submit Button</button>
                            <button type="reset" class="btn btn-info">Reset Button</button>
                        </form>
                    </div>

                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
