<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#ddlPassport").change(function () {
            if ($(this).val() == "Other Serial number punched") {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
    });
</script>
 <style>
  .modal-header.sss {
    background: #16829a;
    color: #fff;
}
label{
	font-weight:700;
}
  </style>
 
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <?php 
 // echo "<pre>"; print_r($res); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Feedback</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Feedback</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Feedback List
		  <span class="pull-right">
		     <button class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Submit a Feedback</button>
		  </span>	
		  
		  
		  
		  <div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header sss">
			  <h4 class="modal-title"><i class="fa fa-life-ring" aria-hidden="true"></i> Feedback Form </h4>
			</div>
			<div class="modal-body">
			<form action="<?php echo base_url();?>index.php/feedback/add_feedback" enctype="multipart/form-data" method="post">
                                     <div class="form-group">
									  <?php
									    $date = date('hisYmd');
										$st = "FD".$date;
										//echo $st;
									 ?>
										 <label>Feedback Id :</label>
                                            <input class="form-control" type="text" name="ticket" value="<?php echo $st; ?>" placeholder="Saurav Sony" readonly>
                                        </div>
										<div class="form-group">
										<label> Feedback In :</label>
                                           <!-- input class="form-control" type="text" placeholder="Please input the Case Id.. E.g. CS71021112110102" required -->
										   <select name="issue" class="form-control" >
										      <option value="Sales">Sales</option>
										      <option value="Services">Services</option>
										   </select>
                                        </div>
										 <div class="row">
											<div class="col-sm-6 form-group">
												<label for="name"> Your Name: <span class="reqd">*</span></label>
												<input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required>
											</div>
											<div class="col-sm-6 form-group">
												<label for="email"> Mobile: <span class="reqd">*</span></label>
												<input type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile" required>
											</div>
										</div>
										
										
                                     <div class="form-group">
										 <label> Subject :</label>
                                            <input class="form-control" type="text" name="subject" value="Feedback on DMS PORTAL" readonly>
                                        </div>
									<div class="form-group">
										 <label>1. What has attracted you more ? </label>
                                            <select class="form-control" type="text" name="attracted" id="attracted">
												<option value="Service Feedback Detail">User Friendly Experience</option>
												<option value="Service Feedback Detail">Easy to Use</option>
												<option value="Service Feedback Detail">Understandable to all</option>
												<option value="Service Feedback Detail">Better than previous Portal</option>
												<option value="Service Feedback Detail">Easy to log Complaints</option>
												<option value="Service Feedback Detail">Easy to understand</option>
												<option value="Service Feedback Detail">Others</option>
											</select>
                                        </div>

                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>2. How would you rate your overall experience with our service?</label>
                                <p>
									<label>
									<input type="radio" name="experience" id="radio_experience" value="Very poor" >
										Very Poor
                                    </label>
									&nbsp;&nbsp;&nbsp;&nbsp;	
								   <label>
                                        <input type="radio" name="experience" id="radio_experience" value="Bad" >
                                        Bad 
                                    </label> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="experience" id="radio_experience" value="Average" >
                                        Average 
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="experience" id="radio_experience" value="Good" >
                                        Good 
                                    </label>&nbsp;&nbsp;
									<label >
                                        <input type="radio" name="experience" id="radio_experience" value="Best" >
                                        Best 
                                    </label>&nbsp;&nbsp;
                                </p>
                            </div>
                        </div>
						  <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>3. Did you find this portal valuable for you?</label>
                                 <p>
									<label>
									<input type="radio" name="portal_value" id="radio_experience" value="Very poor" >
										Very Poor
                                    </label>
									&nbsp;&nbsp;&nbsp;&nbsp;	
								   <label>
                                        <input type="radio" name="portal_value" id="radio_experience" value="Bad" >
                                        Bad 
                                    </label> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="portal_value" id="radio_experience" value="Average" >
                                        Average 
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="portal_value" id="radio_experience" value="Good" >
                                        Good 
                                    </label>&nbsp;&nbsp;
									<label >
                                        <input type="radio" name="portal_value" id="radio_experience" value="Best" >
                                        Best 
                                    </label>&nbsp;&nbsp;
                                </p>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-sm-12 form-group">
                                <label>4. Are our services fullfilling your requirement?</label>
                                 <p>
									<label>
                                        <input type="radio" name="req" id="req" value="No" >
                                        No
                                    </label> &nbsp;&nbsp;&nbsp;&nbsp;
									<label>
									<input type="radio" name="req" id="req" value="Yes" >
										Yes
                                    </label>
									&nbsp;&nbsp;&nbsp;&nbsp;	
                                </p>
                            </div>
                        </div>
						
						<div class="row">
                            <div class="col-sm-12 form-group">
                                <label>5. Based on this experience, would you continue to use this portal for [sales/services]?</label>
                                 <p>
									<label>
                                        <input type="radio" name="reqs" id="reqs" value="No" >
                                        No
                                    </label> &nbsp;&nbsp;&nbsp;&nbsp;
									<label>
									<input type="radio" name="reqs" id="reqs" value="Yes" >
										Yes
                                    </label>
									&nbsp;&nbsp;&nbsp;&nbsp;	
                                </p>
                            </div>
                        </div>
                      
					   <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>6.How easy was to close TAT-1 from our site? How much is it more smoother than previous one portal?</label>
                                 <p>
									<label>
									<input type="radio" name="tat1" id="tat1" value="Very poor" >
										Very Poor
                                    </label>
									&nbsp;&nbsp;&nbsp;&nbsp;	
								   <label>
                                        <input type="radio" name="tat1" id="tat1" value="Bad" >
                                        Bad 
                                    </label> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="tat1" id="tat1" value="Average" >
                                        Average 
                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <label >
                                        <input type="radio" name="tat1" id="tat1" value="Good" >
                                        Good 
                                    </label>&nbsp;&nbsp;
									<label >
                                        <input type="radio" name="tat1" id="tat1" value="Best" >
                                        Best 
                                    </label>&nbsp;&nbsp;
                                </p>
                            </div>
                        </div>
					  
							<div class="form-group">
								<label>7. What did you like about our website or portal for sales or service perspective? </label>
									<select class="form-control" type="text" name="like_best" id="attracted">
										<option value="Service Feedback Detail">User Friendly Experience</option>
										<option value="Service Feedback Detail">Easy to Use</option>
										<option value="Service Feedback Detail">Understandable to all</option>
										<option value="Service Feedback Detail">Better than previous Portal</option>
										<option value="Service Feedback Detail">Easy to log Complaints</option>
										<option value="Service Feedback Detail">Easy to understand</option>
										<option value="Service Feedback Detail">Others</option>
									</select>
								</div>
                  
                                       <div class="form-group">
										<label>8. What can we do to improve your experience with us? :</label>
                                            <textarea class="form-control" rows="3" name="desc" placeholder="Please tell us your suggestions here.."></textarea>
                                        </div>
										<div class="form-group">
										<label>9. Do you have any other comments, questions, or concerns? :</label>
                                            <textarea class="form-control" rows="3" name="desc" placeholder="Please tell us your suggestions here.."></textarea>
                                        </div>
										 <div class="form-group">
										    <button type="submit" value="submit" class="btn btn-info">Submit</button>
										</div>
                                       
                                    </form>
			</div>
			<div class="modal-footer">
			  
			  <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
			</div>
		  </div>
		  
		</div>
	  </div>
	  
	</div>
		  
		  </div>
		<div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Posted your feedback successfully!</h3> </div>
                   
                       
        <div class="card-body">
          <div class="table-responsive">
             <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  			  <thead>
                            <tr>
								<th>Feedback Id</th>
								<th>Feedback In</th>
								<th>Name</th>
								<th>Mobile</th>
								<th>Subject</th>
								<th>Attracted</th>
								<th>Rate Exp</th>
								<th>Portal value</th>
								<!--<th>Ticket Priority</th>-->
								                            
                                							
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($res as $d) {  
							
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?=  $d->feedback_tkt;?> </td>
										<td><?=  $d->feedback_in;?> </td>
										<td><?=  $d->name;?> </td>
										<td><?=  $d->mobile;?> </td>
										<td><?=  $d->subject;?> </td>
										<td><?=  $d->attracted_more;?> </td>
										<td><?=  $d->rate_exp;?> </td>
										<td><?=  $d->portal_val;?> </td>
										<!--td><?=  $d->fulfill_req;?> </td>
										<td><?=  $d->fulfill_req;?> </td>
										<td><?=  $d->tat1;?> </td>
										<td><?=  $d->like_best;?> </td>
										<td><?=  $d->description;?> </td-->
															
										
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  

			$this->load->view('includes/js-holder.php');  
   ?>
   
<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">*</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="image-gallery-title"></h4>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" src="<?php echo base_url(); ?>uploads/<?php echo $d->supportUpload; ?>" height="300">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</body>

</html>
