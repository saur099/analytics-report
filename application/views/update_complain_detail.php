<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<?php 
   $this->load->view('includes/top.php');  
   ?>
<title>Livguard | Raise Issues</title>
<?php 
   $this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
   <!-- Navigation-->
   <div class="content-wrapper">
      <div class="container-fluid">
         <!-- Breadcrumbs-->
         <div class="panel panel-info">
            <div class="panel-heading">
				   Please put your machine serial number ?
		    </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-12">
                     <div class="row">
                        <div class="col-md-8"><input type="text" class="form-control" id="serial" name="serial" placeholder="Enter machine Serial no"> </div>
                        <div class="col-md-4"><button type="button" class="btn btn-default service_complaint">Service Complaint ?</button> </div>
                     </div>
                     <div class="warning_validation"> </div>
                  </div>
               </div>
            </div>
         </div>
		 
		<div class="update_serial_detail">  </div>
		 
		 
		 
      </div>
   </div>
   <!-- /.container-fluid-->
   <!-- /.content-wrapper-->
   <?php 
      $this->load->view('includes/footer.php');  
      
      $this->load->view('includes/js-holder.php');  
      ?>
</body>
</html>
<script>
   $(document).ready(function(){
   
   $(".service_complaint").click(function(){
   		 var serial=$('#serial').val();
   		if(serial=='')
   		{
   			$('.update_serial_detail').hide();
   			$(".warning_validation").show();
   			 $(".warning_validation").html('<span class="text-info">Machine serial number is mendatory.</span>');
   			return false;
   				
   		}else{
   		$.ajax({
   				url: "<?php echo base_url(); ?>index.php/invoice/update_complain_detail",
   					dataType:'html',
   					success: function(result)
   					{
						$('.update_serial_detail').show();
   						$(".warning_validation").hide();
   						$('.update_serial_detail').html(result);
   						return false;
   					}
   		});
   	}
   });
   
   });
   
</script>