  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Tertiary List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tertiary List</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Tertiary's Record List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  			  <thead>
                            <tr>
                                <th>Customer Name</th>
                                <th>Customer City</th>
                                <th>Customer Mobile</th>
								<th>Dealer Name</th>
								<th>Dealer Code</th>
                                <th>Dealer City</th>                               
                                <th>Serial Number</th>                               
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  
							
							$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?= $d->CUSTOMER_NAME;?></td>
										<td><?=  $d->CITY;?> </td>
										<td><?= $d->MOBILE_NUMBER;?></td>
										<td><?=  $d->DEALER_NAME;?> </td>
										<td><?=  $d->DEALER_CODE;?> </td>
										<td><?=  $d->DEALER_CITY;?> </td>
										<td><?=  $d->Serial_No;?> </td>
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
