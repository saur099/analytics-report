  <style>
  label {
    font-weight: 700;
}
input.add-row 
{
    color: #111;
    background-color: #4e6cffd6;
	border : 1px solid #4e6cffd6;
    padding: 8px;
    color: #fff;
	    border-radius: 7px;
    cursor: pointer;
	    box-shadow: 8px 3px 6px #888;
}

  </style>
  <?php 
   //echo "<pre>"; print_r($r); die;
   //echo "<pre>"; print_r($dealer); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Return Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">


  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Return <b>Dealer to Distributor</b> Record</li>
      </ol>
	  
	  <hr>
	  <div class="container">
		<div class="row">
			<div class="col-sm-6">
				<h5>Return Product :</h5> 
			</div>
			<div class="col-sm-6">
				 <span class="pull-right">
					  <button type="button" class="btn btn-danger btn-sm">
					  Defective Stock Quantity <span class="badge badge-light">
					  <?php 
					  if(!empty($r))
					  {   echo $r[0]->defect; }
				       else
					   {
						   echo "0";
					   }
					  ?>
					  </span>
					</button>
				 </span>
		    </div>
		 </div>
		<hr>
	  <form id="form_val" action="" method="post">
	<div class="row">
    <!-- /.col-lg-12 -->

	    <div class="col-md-4" id="dd1">
		  <label>Select Serial No. <span class="reqd">*</span></label>
	        <select  class="form-control" id="serial_no" name="serial_no"  onchange="get_model_data(this.value);" required>
			    <option>Serial Number</option>
				<?php
				      foreach($serial as $s)
					  {
				?>
				  <option value="<?php echo $s->serial_num; ?>"><?php echo $s->serial_num; ?> </option>
				 
				<?php
					  }
				?>
			</select>
			
		</div>
		
	<!--	
		<div class="col-md-4" id="mod_code" style="display:none;">	
		   <label>Model Description</label>
		   <select id="mod_code" name="mod_code">
		   </select>
		</div>
		 <div class="col-md-4" id="prod" style="display:none;">	
		 <label>Product Type</label>
         
    	 <br/>
    	 </div>
		 -->
		 
    	 </div>
	<div class="row">
	   <div class="col-md-4">
	         <label>Return Type</label>
         <select  class="form-control" name="returntype" id="returntype" onchange=""><option value="1">Distributor</option><option value="2">Company</option></select>
	   </div>
	   <div class="col-md-4"> <label>Remarks</label>
	   <select name="remark" id="remark" class="form-control">
		   <option selected disabled>Please Select Remarks</option>
			<?php foreach($remarks as $rm)  { ?>
							<option value="<?php echo $rm->return_sales_id; ?>"> <?php echo $rm->remarks; ?> </option>
				<?php } ?>
		   </select>
         </div>
	   <div class="col-md-2"> 
	            <label>Post to Defect</label>    <br/>
				<span class="reqd">
					Yes <input  type="radio" value="1" name="defect" id="defect" > &nbsp;&nbsp;&nbsp;&nbsp;
					No <input  type="radio" value="0" name="defect" id="defect" >
					</span>
	   </div>
	   
	   <div class="col-md-2">
	        <br/>
	       <input  type="button" class="add-row" id="button" value="Return Now">
	   </div>
	</div>
	
	
	</form>
</div>
<!--div class="table-responsive">
	 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Select</th>
                <th>Serial No</th>
                <th>Model</th>
                <th>Product</th>
                <th>Returned To</th>
                <th>Reason</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><input type="checkbox" name="record"></td>
                <td>Peter Parker</td>
                <td>peterparker@mail.com</td>
                <td>peterparker@mail.com</td>
                <td>peterparker@mail.com</td>
                <td>peterparker@mail.com</td>
            </tr>
        </tbody>
    </table>
	<input  type="button" class="submit-row" value="Submit">
    <button type="button" class="delete-row">Delete Row</button>
	</div-->
</div>
	  
	  
	  
	  <hr>
	  
	  <div class="col-md-12">
	  <div id="result">
	  
	  </div>
	  </div>
	  
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-tag"></i>  Return Dealer to Distributor List <span class="pull-right"><strong><a href="<?php echo base_url(); ?>index.php/return_orders_dealer"><i class="fa fa-refresh" aria-hidden="true"></i> </a>Refresh Page </strong></span></div>
		  
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  	<thead>
                            <tr>
								<th>Model Code</th>
								<th>Model</th>
								<th>Product</th>  
								<th>Returned to</th>								
                                <th>Return Reason</th> 
                                <th>Return date</th>                              
                                <th>Status</th>                               
                                <th>Stock</th>                               
                                <th>Delete</th>                               
							</tr>
					</thead>
					<tbody>
					<?php   
					    foreach($dealer as $d) { ?>
							<tr class="odd gradeX">
							<?php 
							   /*  $query = $this->db->query("YOUR QUERY");
								$row = $query->row();
								if (isset($row))
								{
										echo $row->title;
										echo $row->name;
										echo $row->body;
								} */
							?>
								<td align="center"><?=  $d->return_serial_number;?>  </td>
								<td align="center"><?=  $d->return_serial_number; ?>  </td>
								<td align="center"><?=  $d->return_product_name; ?></td>
								<td align="center"><?php if($d->returnedTo == 1) {echo "Distributor"; } else{ echo "Company";} ?></td>
								<td align="center">
								<?php  
										switch ($d->return_reason) 
										{
											case 1:
												echo "Battery Damage Supply!";
												break;
											case 2:
												echo "Battery Damaged by Dealer!";
												break;
											case 3:
												echo "Discounted Return & Adjustment!";
												break;
											case 4:
												echo "Relationship Return!";
												break;
											case 5:
												echo "Over 110 days Stock / Unable to sell!";
												break;
											case 6:
												echo "Warranty Issue!";
												break;
											case 7:
												echo "Warranty Prorata Issue!";
												break;			
											default:
												echo "Others!";
										}
								?></td>
								<td align="center">
									<?php
												$a = $d->return_date_entered; 
												$timestamp = strtotime($a);			
												$dmy = date("d-m-Y", $timestamp);
												echo $dmy;
												
									 ?>
								 </td>
							    <td><?php  
								if($d->returnedTo == 1)
								{
									echo '<button class="btn btn-success btn-sm">Approved </button>';
								}
							    else
								{
									if($d->return_status == 2 ) { echo '<button class="btn btn-warning btn-sm">Procesing</button>'; } else if($d->return_status == 0 ) { echo '<button class="btn btn-danger btn-sm">Declined </button>'; } else {echo '<button class="btn btn-success btn-sm">Approved </button>';} 
								}
								?> </td>
								<td> 
								<?php if(!empty($d->post_to_defect)) 
										{ 
											echo "Defective"; 
										}
										else 
										{
											echo 'Non-Defective';
											} 
								 ?>
								
								
								
								</td>
								<td>
								   <a href="<?php echo base_url(); ?>index.php/return_orders_dealer/delete_return/<?php echo base64_encode($d->id); ?>">
								     <button type="button" class="delete_return_stock btn btn-danger btn-circle btn-sm" onclick = "return confirm('Are you sure you want to delete this return? Click Ok to proceed...');">
								     <i class="fa fa-times"></i></button>
								</td>
							</tr>
					<?php } ?>
					
					</tbody>
			      </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

<script>
  /*  $(document).ready(function()
   {
        $(".add-row").click(function(){
			//alert("Hello");
            var serial_no = $("#serial_no").val();
            var model = $("#model").val();
            var prod = $("#prod").val();
            var returntype = $("#returntype").val();
            var remark = $("#remark").val();
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + serial_no + "</td><td id='abc'>" + model + "</td><td>" + prod +"</td><td>" + returntype +"</td><td>" + remark +"</td></tr>";
           // alert(markup); 
			$("table tbody").append(markup);
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });
    });     */
</script>

<script>
$(document).ready(function() {
    $('#button').click(function(e) {
		//alert("Hello");
        data = $('#form_val').serialize();
		// alert(data); 
		// return false;
		// exit;
		// alert(data); exit;
        $.ajax({
			
            url: '<?php echo base_url(); ?>index.php/return_orders_dealer/add_dealer_stock',
            type: 'POST',
            data: data,
            success: function(response) 
			{ 
			console.log(response);
				// alert(response);
				/* if(response>0)
				{
					alert("Success ! The Order has successfully returned.. ");
					  window.location.href='return_orders_dealer';
				}
				 else
				 {
					 alert("Sorry ! Some problem occurred.. Please try again.. ");
				 }		 */		 
            }
        });     
        
    });
});

/* 
$('#result').html("<br />$('form').serialize():<br />"+ $('form').serialize()+"<br /><br />$('form').serializeArray():<br />" + JSON.stringify($('form').serializeArray())); */

/* 
function get_model_data(id)
{
	alert("Loading data.. ..");
$(function() {
            $("#serial_no").change(function() {
               
                    $("#serial_no").prop("disabled", true);
  $.ajax({
				url: '<?php echo base_url(); ?>index.php/return_orders_dealer/get_model_data',
				type: 'post',
				data: {'id': id},
						success: function(data, status) 
						{
						   alert("Success ! The model code and product category is loaded.");
							var data = JSON.parse(data);
							console.log(data);
							return false;
								if(data.length>0) 
								{
									//alert(data); 
									var option1 = "<option value=''>Please Select Area</option>";
									var option2 = "";
									var option3 = "";
									$.each(data, function(index, obj) 
									{
										option1 = option1 + '<option value="'+ obj.MODEL_CODE +'">'+ obj.MODEL_CODE+'</option>';
									});	
									$('#area').html(option1);
									$.each(data, function(index, obj) 
									{
									  option2 = '<option value="'+ obj.state +'">'+ obj.state+'</option>';
									});	
									$('#state').html(option2);
									$.each(data, function(index, obj) 
									{
									   option3 =  '<option value="'+ obj.city +'">'+ obj.city+'</option>';
									});	
									$('#city').html(option3);
										
								}
						}		
						
				   });
        });			
			
		}); // end ajax call
} */


</script>
