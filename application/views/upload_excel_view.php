  <style>
  .modal-header.sss {
    background: #16829a;
    color: #fff;
}
label{
	font-weight:700;
}
  </style>
 
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Upload Excel</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
	  <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>index.php/branch_order/secondary_stock"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Secondary Stock</a>
        </li>
        <li class="breadcrumb-item active">Upload CSV for Secondary</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Upload CSV data for Secondary
		  	</div>
		  	</div>
		  <div class="row">
		        <div class="col-md-1"></div>
			    <div class="col-md-3"><label>Download the format of CSV:</label 
				<a href="<?php echo base_url(); ?>index.php/branch_order/array_to_csv">
					&nbsp;&nbsp; &nbsp;  <img src="<?php echo base_url(); ?>images/abc.png" height="50" width="50" /></a>
				</div>
			</div>
			<hr>
			<div class="row">
			    <div class="col-md-1"></div>
			     <div class="col-md-4"><label>Upload CSV file here :</label> 
				  <input type="file" name="upload_data" type="file" class="form-control">
				  </div>
				  <div class="col-md-1"><br/>
				  <button type="submit" class="btn btn-info">Submit CSV </button>
				</div>
			</div>
		  
		  
		 
	  
	
		  
		  </div>
		  
        <div class="card-body">
          <div class="table-responsive">
             </div>
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  

			$this->load->view('includes/js-holder.php');  
   ?>
   
<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">*</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="image-gallery-title"></h4>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" src="<?php echo base_url(); ?>uploads/<?php echo $d->supportUpload; ?>" height="300">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</body>

</html>
