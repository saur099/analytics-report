<?php $this->load->view('library');    ?>
<body>
 <div id="wrapper">
      
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Dispatch Details
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive11">
				
				
				       <div id="table_distributor">
					   
	
	<form id="frmAddUser" method="post" action="">
<label>Dispatch Through</label>
<select class="form-control" name="dispatch" id = "dispatch">
<option value="">Please Select</option>
<option value="By_Hand">By Hand</option>
<option value="Local_Courier">Local Courier</option>
<option value="Local_Logistic">Local Logistic</option>
<option value="Picked_By_Dealer">Picked By Dealer</option>
<!--<option value="Picked_By_Sales_Executive">Picked By Sales Executive</option>-->
</select>
<br>
<input type ="hidden" id ="data_value" value ="<?php echo $data[0]->id;?>">
<input type ="hidden" id ="dis_code" value ="<?php echo $data[0]->distributor_code;?>">
<input type ="hidden" id ="avail_stock" value ="<?php echo $data[0]->avail_stock;?>">
<input type ="hidden" id ="quantity_ordered" value ="<?php echo $data[0]->quantity;?>">
<button type="button" class="btn btn-primary" id ="btnAdd">Confirm</button>
 </form>
 <?php foreach($data as $d)
 {
	 
	 if($d->dispatch_by_id!= '')
	 {    $dispatch_through = str_replace(array('_', ' '), ' ', $d->dispatch_through);
		 echo "Dispatch Details: ".$dispatch_through;
	 }
 }
 ?>
 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
 <script>
  $(document).ready(function() {

    $('#btnAdd').click(function () {

      var order_id = $('#data_value').val();
	  var dis_code = $('#dis_code').val(); 
	  var dispatch = $('#dispatch').val();
	  var quantity_ordered = $('#quantity_ordered').val();
	  var avail_stock = $('#avail_stock').val();

      var data = 'order_id='+ order_id + '&dispatch='+ dispatch + '&dis_code='+dis_code + '&quantity_ordered='+quantity_ordered + '&avail_stock='+avail_stock; // this where i add multiple data using  ' & '

      $.ajax({
        type:"POST",
        cache:false,
        url: "<?php echo base_url(); ?>index.php/secondary/confirm_order_dispatch",
        data:data,    // multiple data sent using ajax
        success: function (html) {

          parent.$.fancybox.close(); 
        }
      });
      return false;
    });
  });
</script>					   </div>					
                    
                </div>

            </div>
           
        </div>
        
    </div>
    
</div>

        </div>
         
 </div>