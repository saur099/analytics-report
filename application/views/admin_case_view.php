<?php
   //echo "<pre>"; print_r($res); die;
   
   			$this->load->view('includes/top.php');  
      ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script></script>
<title>Services | Case Compalain Details</title>
<style>
.complain_no {margin-left: -75%; margin-left: -75%;  margin-top: 7%;}
</style>
   
<?php 
   $this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
   <!-- Navigation-->
   <div class="content-wrapper">
      <div class="container-fluid">
         <!-- Breadcrumbs-->
         <!-- Example DataTables Card-->
         <div class="card mb-3">
		 
            <div class="card-header"> <i class="fa fa-dashboard"></i> Case Compalain Details  </div>
				
				<div class="complain_search">
				<div class="form-group">
                  <div class="row">
                     
                     <div class="col-md-8">
                        <label class="control-label col-sm-4" ><b>Complain Number : </b></label>
						 <div class="col-sm-8">
                           <input type="text" class="form-control" name="compain_no" id="compain_no" placeholder="Complain Number" required>
                        </div>
						 <div class="loader"><img src="<?php echo base_url();?>images/loader.gif" alt="loading_icon" width="100px" height="60px"></div>
                     </div>
					 <div class="col-md-4">
                           <button type="button"  value="submit" class="btn btn-success btn-sm complain_no" >Search</button>
                     </div>
                  </div>
				  
               </div>
			 
              </div>
			     <div class="warning"></div>
			    <div class="complain_data"> </div>   
			  </div>
      </div>
   </div>
   <!-- /.container-fluid-->
   <!-- /.content-wrapper-->
   <?php 
      $this->load->view('includes/footer.php');  
      $this->load->view('includes/js-holder.php');  
      ?>
   </div>
</body>
</html>
<script type="text/javascript">
$('.complain_search').show();
$('.loader').hide();
  	$('.complain_no').on('click', function(){
		
   		var complain_no = $('#compain_no').val();
		$('.loader').show();
		
   		if(complain_no == ""){
			$('.warning').html('<span class="alert alert-danger">Complain no. can not Empty!</span>');
			$('.complain_data').hide();	
			$('.loader').hide();
			// location.reload();
		}else{

		$.ajax({
   			type:"POST",
   			url: "<?php echo base_url(); ?>index.php/admin_case_controller/get_complain_detail",
			data:{complain_no : complain_no},    // multiple data sent using ajax
			dataType:'html',
   			success: function (res) {
				$('.loader').hide();
				$('.complain_data').show();	
				$('.complain_data').html(res);				
   			}
   		  });
		}
   	});
	var timeout = 3000; // in miliseconds (3*1000)
$('.warning').delay(timeout).fadeOut(300);
  </script>