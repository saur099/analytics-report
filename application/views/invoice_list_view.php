<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<?php 
   $this->load->view('includes/top.php');  
   ?>
<title>Livguard | Raise Issues</title>
<?php 
   $this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
   <!-- Navigation-->
   <div class="content-wrapper">
      <div class="container-fluid">
         <!-- Breadcrumbs-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item">
               <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">invoice list</li>
         </ol>
         <div class="card-body">
            <div class="table-responsive">
               <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                     <tr>
                        <th>Complain Number</th>
                        <th>Serial Number</th>
                        <th>HUB Pincode</th>
                        <th>Contact Person</th>
                        <th>Challan Number</th>
                        <th>Challan Date</th>
                        <th>Generate Challan Date</th>
                        <th>Date From</th>
                        <th>Date To</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php   foreach($res as $d) {  
                        ?>
                     <input type="hidden" value="<?php echo $id= $d->submitted_by;?>">
                     <tr class="odd gradeX">
                        <td><?= $d->complaint_no;?></td>
                        <td><?=  $d->serial_number;?> </td>
                        <td><?=  $d->hub_pincode;?> </td>
                        <td><?=  $d->hub_contact_person;?> </td>
                        <td><?=  $d->challan_number;?> </td>
                        <td><?=  $d->challan_date;?> </td>
                        <td><?=  $d->generated_challan_date;?> </td>
                        <td><?=  $d->from_date;?> </td>
                        <td><?=  $d->to_date;?> </td>
                        <td><a href="<?php echo base_url(); ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a> </td>
                     </tr>
                     <?php } ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <!-- /.container-fluid-->
   <!-- /.content-wrapper-->
   <?php 
      $this->load->view('includes/footer.php');  
      
      $this->load->view('includes/js-holder.php');  
      ?>
</body>
</html>