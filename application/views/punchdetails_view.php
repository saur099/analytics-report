<style>
button.btn.btn-new 
{
    background: #817e92;
    color: #fff;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/drona/q.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/drona/dronahq.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/drona/scanner.js"></script>
<script type="text/javascript">
        $(document).ready(function () {
            if (DronaHQ.IsReady) {
                
            }
            else {
                document.addEventListener('deviceready', function () {

                });
            }
        });
    </script>
	
  <?php 
 // echo "<pre>"; print_r($res); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Order Processing Details</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Order Processing Details</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
         <b> <i class="fa fa-table"></i> Order Processing Details</b>
		  
		</div>
		</div>
		
		  
        <div class="card-body">
          <div class="table-responsive">
				       <div id="table_distributor">
							<?php  foreach($res as $d) {  
							// $get_all_ids = str_replace(array(',', ' '), '_', $d->ids);
							// $get_all_stock = str_replace(array(',', ' '), '_', $d->avail_stock);
							?>
							 <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
									<tr class="odd gradeX"><td><b>Order Name</b></td><td><?= $d->order_name;?></td></tr>
										
									<!--<tr class="odd gradeX"><td>Quantity</td>	<td><?=  $d->quantity;?> </td></tr>-->
									<tr class="odd gradeX"><td><b>Dealer Code</b></td>	<td><?= $d->dealerName;?></td></tr>
								
<tr class="odd gradeX"><td><b><?php echo $d->modelCode; ?> </b></td>	<td colspan="4"><div style="float:left;"><?= $product[$i].'  <button class="btn btn-new"><b>Quantity :</b>'.$quantitys[$i].'</button>'. '   <button class="btn btn-new"><b> Stock Avail: </b>'.$avail_stocks[$i].'</button>';?></div>&nbsp;&nbsp;<div style="margin-left:20%;float:left;"><td>
				
				<button title="Reject Order" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="reject_order btn btn-danger btn-circle" onclick="reject_order(<?php echo $ids_explode[$i];?>,<?php echo $data['distributor_code'];?>);"><i class="fa fa-times"></i>
                </button>&nbsp;&nbsp;
				<?php if($rejection_reason[$i] == '' && $avail_stocks[$i] >= $quantitys[$i]) { ?>
				<button title="Confirm Dispatch" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="addnew btn btn-success btn-circle" onclick="addnew(<?php echo $ids_explode[$i];?>,<?php echo $data['distributor_code'];?>);"><i class="fa fa-check"></i>
                </button>
				<?php } else { ?>
				<button title="Confirm Dispatch" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="addnew btn btn-success btn-circle disabled" onclick="addnew(<?php echo $ids_explode[$i];?>);"><i class="fa fa-check"></i>
                </button>
				<?php } ?>&nbsp;&nbsp;
				<button title="Scan" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="reject_order btn btn-info btn-circle btnscan"><i class="fa fa-check"></i>
                </button>&nbsp;&nbsp;
				</td></div></td></tr>
									<?php } ?>
									<tr class="odd gradeX"><td><b>Mode Of Order</b></td>	<td><?php echo "Secondary "; ?> </td></tr>
									<tr class="odd gradeX"><td><b>Hub Name</b></td>	<td><?=  $d->hub_name;?> </td></tr>
									<tr class="odd gradeX"><td><b>Total Order Qty.</b></td>	<td><?=  $d->totalOrderQty;?> </td></tr>
									<!--<tr class="odd gradeX"><td>Rejection Reason</td>	<td><?php if($d->rejection_reason == '') { echo 'No Reason';} else { echo $d->rejection_reason;} ?> </td></tr>-->
									<tr class="odd gradeX"><td><b>Order Date</b></td>	<td><?=  $d->orderPunchedOn;?> </td></tr>
									<tr class="odd gradeX"><td><b>Remarks</b></td>	<td><?=  $d->remarks;?> </td></tr>
									
									<tr class="odd gradeX">
										<?php for($i=0;$i<$d->modelCode;$i++) { 
									$modelCode = explode(",",$d->modelCode);
									?>
									<td><b>Model Code</b></td>	<td><?=  $d->modelCode;?> 
									
									</td></tr>
									<tr class="odd gradeX"><td><b>Scheme Applicable</b></td>	<td><?php if($data['scheme_details'][0]->scheme_name == '') { echo 'No Scheme Applicable';} else { echo $data['scheme_details'][0]->scheme_name;} ?> </td></tr>
		<!--<tr class="odd gradeX"><td>Dispatch Details</td>	<td><?php if($d->dispatch_by == '') { echo 'Pending';} else { echo $d->dispatch_by;} ?> </td></tr>-->
										
										
									</tr>
                            <?php } ?>
							</table>
							<?php if($data['dispatch_details']) { ?>
							
							<div class="alert alert-success">
								  <strong>Dispatch Details  <i class="fa fa-chevron-circle-down fa-x" aria-hidden="true"></i></strong>  .
								</div>
							
							<table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
							<tr class="odd gradeX"><th>Product Name</th><th>Quantity</th><th>Dispatch Details</th></tr>
							<?php foreach($data['dispatch_details'] as $dispatch_data) { //echo "<pre>"; print_r($ds);?>
									
				<tr class="odd gradeX"><td><?=$dispatch_data->product_name;?></td><td>Quantity : <?=$dispatch_data->quantity;?></td><td>Dispatch Through : <?=str_replace("_"," ",$dispatch_data->dispatch_through);?></td></tr>
										<?php } ?>
							</table>
							<?php } ?>
							
							<table>
							<!--<tr>
							<td><button type="button" class="reject_order btn btn-warning" onclick="reject_order(<?php echo $d->id;?>);">Reject Order</button></td><td>&nbsp;</td><td></td>
							<?php if($d->rejection_reason == '' && $d->order_id == '' && $d->dispatch_by == '') { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<td>&nbsp;</td><td></td>
							<td><button type="button" class="addnew btn btn-success" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } else { if($d->order_id!= '') { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary disabled" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<?php } ?>
							<td>&nbsp;</td><td></td>
							<?php if($d->dispatch_by!= '') { ?>
							<td><button type="button" class="addnew btn btn-success disabled" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew btn btn-success" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } ?>
							<?php } ?>
							
							</tr>-->
							<tr>
							<input type="hidden" id ='all_element_ids' value="<?php echo $get_all_ids;?>">
							<input type="hidden" id ='all_avail_stocks' value="<?php echo $get_all_stock;?>">
							<input type="hidden" id ='all_quantity' value="<?php echo $d->quantity;?>">
							<input type="hidden" id ='all_product' value="<?php echo $d->product_name;?>">
							
							<?php 
							foreach($data['order_details'] as $dd)
							{ if($dd->order_id == '') {
							?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm();">Confirm Order</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary disabled" onclick="addnew_confirm();">Confirm Order</button></td>
							<?php } }?>
							</tr>
                           </table>
						  
						  
						  
						  
						  
						  
						  
							
							
<script type="text/javascript">
function addnew(ids,dis_code) { 
    var url = 'http://13.228.144.245/livguard_dms/index.php/secondary/confirm_dispatch/' +ids + '_' + dis_code;
    CreateFancyBox('button.addnew', url, '45%', 390);
}

function addnew_confirm() { 
var ids = $('#all_element_ids').val(); 
var avail_stock = $('#all_avail_stocks').val(); 
var quantity = $('#all_quantity').val(); 
var all_product = $('#all_product').val(); 
    var url = 'http://13.228.144.245/livguard_dms/index.php/secondary/confirm_order/' +ids;
    CreateFancyBox('button.addnew_confirm', url, '45%', 390);
	
	
	// e.preventDefault(); // avoids calling preview.php
	/*var data = 'ids='+ ids + '&avail_stock='+ avail_stock; 
    $.ajax({
      type: "POST",
      cache: false,
      url: 'http://52.66.23.135/cis/index.php/secondary/confirm_order/', // preview.php
      data: data, // all form fields
      success: function (data) {
        // on success, post (preview) returned data in fancybox
        $.fancybox(data, {
          // fancybox API options
          fitToView: false,
          width: 390,
          height: 390,
               'scrolling'         : 'no',
        'titleShow'         : false,
        'titlePosition'     : 'none',
        'openEffect'        : 'elastic',
        'closeEffect'       : 'none',
        'closeClick'        : false,
        'openSpeed'         : 'fast',
        
		'padding'           : 0,
        'preload'           : true,
        'width'             : 390,
        'height'            : 390,
        'fitToView'         : false,
        'autoSize'          : false,
        'helpers'           : { 
            overlay :   {
                'closeClick': false,
            }
        },
        afterClose          : function() { //I search StackOverflow, add this function to reload parent page, it will appear the flash data message notification which I write on controller "add_user"
            parent.location.reload();
        }
        }); // fancybox
      } // success
    }); // ajax
	
	*/
}

function reject_order(ids,dis_code) { 
    var url = 'http://13.228.144.245/livguard_dms/index.php/secondary/reject_order/' +ids + '_' + dis_code;
    CreateFancyBox('button.reject_order', url, '45%', 390);
}

function CreateFancyBox(selector, url, width, height) {
    $(selector).fancybox({
        'href': url,
        'scrolling'         : 'no',
        'titleShow'         : false,
        'titlePosition'     : 'none',
        'openEffect'        : 'elastic',
        'closeEffect'       : 'none',
        'closeClick'        : false,
        'openSpeed'         : 'fast',
        'type'              : 'iframe',
		'padding'           : 0,
        'preload'           : true,
        'width'             : width,
        'height'            : height,
        'fitToView'         : false,
        'autoSize'          : false,
        'helpers'           : { 
            overlay :   {
                'closeClick': false,
            }
        },
        afterClose          : function() { //I search StackOverflow, add this function to reload parent page, it will appear the flash data message notification which I write on controller "add_user"
            parent.location.reload();
        }
    });
}
</script>
<!-- Add jQuery library -->

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

 <?php 
			$this->load->view('includes/footer.php');  
   ?>

	
	<?php 
			//$this->load->view('includes/js-holder.php');  
   ?>
	

			      
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
