  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Return Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Return Stock List</li>
      </ol>
	  
	  
	  
	  <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
           
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form">
						
                            <div class="form-group">
                                <label>Product Name</label>
                                <select  id="product_name" class="form-control" name ="product_name"></select>
                            </div>
							 <div class="form-group">
                                <label>Serial Number</label>
                                < select id="assign_branch" name ="assign_branch[]" multiple>
								<?php 
								foreach($data as $d) { $con = count($d);
								for($i=0;$i<$con;$i++) {
								?>
								<option value="<?php echo $d[$i]->serial_number;?>"><?php echo $d[$i]->serial_number;?></option>
								<?php } }?>
								
								</select >
                            </div>
							<div class="form-group">
                                <label>Reason For Return</label>
                                <select  id="return_reason" class="form-control" name ="return_reason">
								<option value="">Select Reason</option>
								<option value="Defective_Stock">Defective Stock</option>
								<option value="OutBox_Damage">OutBox Damage</option>
								<option value="Dead_On_Arrival">Dead On Arrival</option>
								<option value="Filter_and_Membrane_Issue">Filter & Membrane Issue</option>
								<option value="Piece_Is_Very_Old">Piece Is Very Old</option>
								<option value="Cabinet_Damage">Cabinet Damage</option>
							    </select>
                            </div>
                            
                            <button type="button" id="distributor_btn" class="btn btn-success">Submit Button</button>
                            <button type="reset" class="btn btn-info">Reset Button</button>
                        </form>
                    </div>

                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
	  
	  
	  
	  <hr>
	  
	  
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Return Stock List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  	  <thead>
                            <tr>
                               
								<th>Product Name</th>
								<th>Serial Number</th>
                                <th>Return date</th>                               
                                <th>Return Reason</th>                               
                                <th>Status</th>                               
                                <th>Delete</th>                               
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data['get_my_return_stock_details'] as $d) { ?>
									<tr class="odd gradeX">
										<td><?= $d->return_product_name;?></td>
										<td><?=  $d->return_serial_number;?> </td>
										<td><?= $d->return_date_entered;?></td>
										<td><?= str_replace("_"," ",$d->return_reason);?></td>
										<td><?php  if($d->return_status == 0 ) { echo 'Pending'; } ?> </td>
										
			<td><button type="button" class="delete_return_stock btn btn-warning btn-circle" onclick = "delete_return_stock(<?php echo $d->return_stock_id;?>)"><i class="fa fa-times"></i></button></td>
									</tr>
                            <?php } ?>
							
                            </tbody>
			      </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 