<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#ddlPassport").change(function () {
            if ($(this).val() == "Other Serial number punched" || $(this).val() == "Others") {
                $("#dvPassport").show();
            } else {
                $("#dvPassport").hide();
            }
        });
    });
</script>
 <style>
  .modal-header.sss {
    background: #16829a;
    color: #fff;
}
label{
	font-weight:700;
}
  </style>
 
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Raise Issues</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Raise Issues</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Raise Issues List
		  <span class="pull-right">
		     <button class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Raise an Issue</button>
		  </span>	
		  
		  
		  
		  <div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header sss">
			  <h4 class="modal-title"><i class="fa fa-life-ring" aria-hidden="true"></i> Raise an Issue </h4>
			</div>
			<div class="modal-body">
			<form action="<?php echo base_url();?>index.php/support/add_tickets" enctype="multipart/form-data" method="post">
                                     <div class="form-group">
									  <?php
									    $date = date('hisYmd');
										$st = "SP".$date;
										//echo $st;
									 ?>
										 <label>Support Ticket Id :</label>
                                            <input class="form-control" type="text" name="ticket" value="<?php echo $st; ?>" placeholder="Saurav Sony" readonly>
                                        </div>
										<div class="form-group">
										<label> Ticket Issue In :</label>
                                           <!-- input class="form-control" type="text" placeholder="Please input the Case Id.. E.g. CS71021112110102" required -->
										   <select name="issue" class="form-control" >
										      <option value="Sales">Sales</option>
										      <option value="Services">Services</option>
										   </select>
                                        </div>
										
										
										
                                     <div class="form-group">
										 <label> Subject :</label>
                                            <select class="form-control" type="text" name="subject" id="ddlPassport">
												<option value="Wrong date of sale punched">Wrong date of sale punched</option>
												<option value="Wrong battery complaint raised">Wrong battery complaint raised</option>
												<option value="Wrong battery punched in replacement">Wrong battery punched in replacement</option>
												<option value="Wrong customer detail entered">Wrong customer detail entered</option>
												<option value="Wrong decision Entered">Wrong decision Entered</option>
												<option value="Wrong Test report entered">Wrong Test report entered</option>
												<option value="Other Serial number punched">Other Serial number punched</option>
												<option value="Serial Number Mismatch on Invoice v/s Physical">Serial Number Mismatch on Invoice v/s Physical</option>
												<option value="Others">Others</option>
												
											</select>
                                        </div>
										<div class="form-group" id="dvPassport" style="display: none">
											 
											<div class="row">
												<div class="col-md-4">
												 <label>Customer first Name :</label>
												   <input class="form-control" type="text" name="fname" value="" placeholder="Customer Frst Name" >
												</div>
												<div class="col-md-4">	
													<label>Customer Last Name :</label>
													<input class="form-control" type="text" name="lname" value="" placeholder="Customer Last Name" > 
												</div>
												<div class="col-md-4">	
													<label>Cust Purchage Date  :</label>
													<input type="date" class="form-control" name="custpurchagedate" >
												</div>
												<!--<div class="col-md-4">
													<b for="exampleInputLastName">Cust Purchage Date </b>
													<div class="row">
													  <div class="col-md-7 sss"><input type="date" class="form-control" name="chrgEndDate"></div>
													  </div>
												</div>-->
											</div>
											<div class="row">
												<div class="col-md-4">
													<label>Mobile No:</label>
													<input class="form-control" type="text" name="mobile_no" value="" placeholder="mobile No" >
												</div>
												<div class="col-md-4">
													<label>Product Serial No :</label>
													<input class="form-control" type="text" name="case" value="" placeholder="Product Serial No" >
												</div>
												<div class="col-md-4">
													<label>Customer Pincode :</label>
													<input class="form-control" type="number" name="pincode" value="" placeholder="Customer Pincode" >
												</div>
											</div>
										</div>
										<!--
										<div class="form-group">
										 <label> Priority :<span class="reqd">*</span></label>
                                            <select class="form-control" name="priority" type="text" required>
											      <option value="1">High</option>
											      <option value="2">Medium</option>
											      <option value="3">Low</option>
											</select>
                                        </div>-->
										
										<div class="form-group">
										<label>Upload Image :</label>
										   <input type="file" class="form-control" id="filename" name="filename">
										</div>
										
                                        <div class="form-group">
										<label> Query Description :</label>
                                            <textarea class="form-control" rows="3" name="desc" placeholder="Please input the complete description of the ticket raised.."></textarea>
                                        </div>
										 <div class="form-group">
										    <button type="submit" value="submit" class="btn btn-info">Submit</button>
										</div>
                                       
                                    </form>
			</div>
			<div class="modal-footer">
			  
			  <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
			</div>
		  </div>
		  
		</div>
	  </div>
	  
	</div>
		  
		  </div>
		  
        <div class="card-body">
          <div class="table-responsive">
             <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  			  <thead>
                            <tr>
								<th>Ticket Id</th>
								<th>Prod Serial No</th>
								<th>Issue In</th>
								<th>Raised On</th>
								<th>Subject</th>
								<th>Query Desc</th>
								<th>Ticket Status</th>
								<th>Reply Ticket</th>
								<!--<th>Ticket Priority</th>-->
								                            
                                							
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($res as $d) {  
							
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><span style="color:#17a2b8;    font-weight: 500;"><?= $d->supportTicket;?></span></td>
										<td><?=  $d->supportCaseId;?> </td>
										<td><?=  $d->supportIssueIn;?> </td>
										<td><?=  $d->supportIssueIn;?> </td>
										<td><?=  $d->supportSubject;?> </td>
										<td><?=  $d->supportDescription;?> </td>
										
										<td>
										<?php
												if($d->supportStatus=="Resolved"){echo '<span style="background:crimson; color:#fff; padding:8px;">Declined</span>'; }
												else if($d->supportStatus=="Processing"){echo '<span style="background:orange; color:#fff; padding:8px;">Processing</span>'; }
												else{echo '<span style="background:green; color:#fff; padding:8px;">Answered</span>'; }
										
										?> 
										</td>
										<!--<td><?php   /*
										if($d->supportPriority==1){echo '<span style="background:#008B8B; color:#fff; padding:8px;">High</span>'; }
										else if($d->supportPriority==2){echo '<span style="background:#20B2AA; color:#fff; padding:8px;">Medium</span>'; }
										else{echo '<span style="background:#66CDAA; color:#000; padding:8px;">Low</span>'; } */
										?> </td>-->
										<td>
										<a href="<?php echo base_url(); ?>index.php/support/admin_ticket_reply/<?php echo base64_encode($d->supportId); ?>">
										  <button type="" class="btn btn-info btn-sm">Reply</button>
										</a>
										</td>
										
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  

			$this->load->view('includes/js-holder.php');  
   ?>
   
<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">*</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="image-gallery-title"></h4>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" src="<?php echo base_url(); ?>uploads/<?php echo $d->supportUpload; ?>" height="300">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</body>

</html>
