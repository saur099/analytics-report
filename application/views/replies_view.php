
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <?php 
	//echo "<pre>"; print_r($res); die("hello");
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Ticket Replies</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
 <body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Replied Tickets</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Replied Ticket List
		  
		</div>
		</div>
		  
        <div class="card-body">
          <div class="table-responsive">
             <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  			  <thead>
                            <tr>
								<th>Ticket Id</th>
								<th>Ticket Issue In</th>
								<th>Reply Subject</th>
								<th>Replied On</th>
								<th>Reply Status</th>
								<th>Ticket Reply</th>                        
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($res as $d) {  
							
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><span style="color:#17a2b8;    font-weight: 500;"><?= $d->repliedTicketid;?></span></td>
										<td><?=  $d->repliedIssueIn;?> </td>
										<td><?=  $d->repliedSubject;?> </td>
										<td><?=   $dmy = date("d-m-Y",  strtotime($d->repliedOn));?> </td>
										<td><?php
													echo '<button class="btn btn-success btn-sm">'.$d->repliedStatus.'</button>'; 
												
										?> </td>
										
										<td><?=  $d->repliedDescription;?> </td>
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
