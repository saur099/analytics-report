  <style>
  label {
    font-weight: 700;
}
  </style>
  <?php 
  //print_r($dealer); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Return Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">


  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Return Dealer to Distributor List</li>
      </ol>
	  
	  <hr>
	  <div class="col-md-12">
	<div class="row">
    <!-- /.col-lg-12 -->
	
	    <div class="col-md-4">
	        <input class="form-control" type="text" id="name" placeholder="Name"></div>
		<div class="col-md-4">	
         <input class="form-control" type="text" id="email" placeholder="Email Address"></div>
		<div class="col-md-4"> 
    	 <input  type="button" class="add-row" value="Add Row"></div>
	</div>
</div>
<div class="table-responsive">
	 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Select</th>
                <th>Serial No</th>
                <th>Model</th>
                <th>Product</th>
                <th>Returned To</th>
                <th>Reason</th>
            </tr>
        </thead>
        <tbody>
            <tr><td><input type="checkbox" name="record"></td>
			<div id="serial_no">
                <td><select  class="form-control" name="serial_no"  onchange=""><option value="">Select Serial No</option><option value="1">AFSCD012302123240</option><option value="2">LGSCD012302123239</option></select></td></div>
                <td><select  class="form-control" name="model" id="model" onchange=""><option value="">Select Model</option><option value="1">AFSCD012302123240</option><option value="2">LGSCD012302123239</option></select></td>
                <td><select  class="form-control" name="prod" id="prod" onchange=""><option value="">Select Product</option><option value="1">AFSCD012302123240</option><option value="2">LGSCD012302123239</option></select></td>
                <td><select  class="form-control" name="returntype" id="returntype" onchange=""><option value="">Choose to Return Authority</option><option value="1">AFSCD012302123240</option></select></td>
                <td><input type="text" class="form-control" id="remark" name="remark" placeholder="remarks"></td>
            </tr>
        </tbody>
    </table>
    <button type="button" class="submit-row">Submit</button>
    <button type="button" class="delete-row">Delete Row</button>
</div>
	  
	  
	  
	  <hr>
	  
	  
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-tag"></i>  Return Dealer to Distributor List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  	<thead>
                            <tr>
								<th>Returned to Distributor</th>
								<th>Serial Number</th>
                                <th>Return date</th>                               
                                <th>Return Reason</th>                               
                                <th>Status</th>                               
                                <th>Delete</th>                               
							</tr>
					</thead>
					<tbody>
					<?php   foreach($dealer as $d) { ?>
							<tr class="odd gradeX">
								
								<td align="center"><?=  $d->distName;?> </td>
								<td><button class="btn btn-primary"><?=  $d->return_serial_number;?> <i class="fa fa-eye"></i></button></td>
								<td><?= $d->return_date_entered;?></td>
								<td><?= $d->return_reason;?></td>
								<td><?php  if($d->return_status == 0 ) { echo '<button class="btn btn-warning">Waiting for approval </button>'; } else {echo '<button class="btn btn-success">Approved </button>';} ?> </td>
								<td>
								     <button type="button" class="delete_return_stock btn btn-danger btn-circle" onclick = "delete_return_stock(<?php echo $d->return_stock_id;?>)">
								     <i class="fa fa-times"></i></button>
								</td>
							</tr>
					<?php } ?>
					
					</tbody>
			      </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

<script>
   $(document).ready(function()
   {
        $(".add-row").click(function(){
			//alert("Hello");
            var serial_no = $("#serial_no").val();
			alert(serial_no);
            var model = $("#model").val();
            var prod = $("#prod").val();
            var returntype = $("#returntype").val();
            var remark = $("#remark").val();
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + serial_no + "</td><td>" + model + "</td></tr>";
            $("table tbody").append(markup);
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });
    });    
</script>
