<script type = "text/javascript" src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<style>
.btn-danger btn-sm {
    cursor: pointer;
    box-shadow: 5px 4px 2px 1px #888;
}
label{font-weight:600;}
.form-control:disabled, .form-control[readonly] {
    background-color: #e9ecef38 !important;
}
</style>
		
<?php
// echo "<pre>"; print_r($dealName[0]->name); die();

		$this->load->view('includes/top.php');  
   ?>
   <title>Services | Defective Detail Management</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
     
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
         <a href="#"> <button type="submit" class="btn btn-info btn-sm"> Defective Complaint</button></a>

		  </div>
        <div class="card-body">
          <div class="table-responsive">

	<!----------------------     Customer Product View starts here       ------------------------->
	  <div class="card border-success mb-3" style="max-width: 120rem;">
			  <div class="card-header"><h5>Product Details of Customer</h5></div>
			  <div class="card-body text-primary">
				<div class="row">
				<div class="col-md-3"> <label>Product Serial Number:</label><input class="form-control" value="<?= $data->serial_num; ?>" readonly> </div>	
				<div class="col-md-3"> <label>Brand:</label><input class="form-control" value="<?= $data->brand; ?>" readonly> </div>	
				<div class="col-md-3"> <label>Product Type :</label><input class="form-control" value="<?= $data->product; ?>" readonly> </div>
				<div class="col-md-3"> <label>Battery Type:</label><input class="form-control" value="<?= $data->BATTERY_TYPE; ?>" readonly> </div>
				</div>
			<div class="row">	
				<div class="col-md-4"> <label>Product Segment:</label><input class="form-control" value="<?php echo $data->PRODUCT_GROUP; ?>" readonly> </div>
				<div class="col-md-4"> <label>LETPL Invoice No:</label><input class="form-control" value="<?= $data->invoice_no; ?>" readonly> </div>	  				
				<div class="col-md-4"> <label>Model:</label><input class="form-control" value="<?= $data->model; ?>" readonly> </div>	  				
				
			</div>
			<div class="row">	
				<?php 
		    $b =$data->primary_sale_date;  $timestamp = strtotime($b);	$ltp = date("d-m-Y", $timestamp); 
		 ?>	
				<div class="col-md-3"> <label>Warranty In Months:</label><input class="form-control" value="<?= $data->WARRANTY_IN_MONTHS; ?>" readonly> </div>	
				<div class="col-md-3"> <label>Pro Rata Warranty:</label><input class="form-control" value="<?= $data->PRO_RATA_WARRANTY; ?>" readonly> </div>
				<div class="col-md-3"> <label>AH Value:</label><input class="form-control" value="<?= $data->AH_VALUE; ?>" readonly> </div>
				<div class="col-md-3"> <label>Aging Limit:</label><input class="form-control" value="<?= $data->AGING_LIMIT; ?>" readonly> </div>
				
				
				<div class="col-md-3"> <label>Pro Rata Min Discount:</label><input class="form-control" value="<?= $data->PRO_RATA_MIN_DISCOUNT; ?>" readonly> </div>					
				<div class="col-md-3"> <label>Scrap Price:</label><input class="form-control" value="<?= $data->SCRAP_PRICE; ?>" readonly> </div>
				<div class="col-md-3"> <label>Weight:</label><input class="form-control" value="<?= $data->WEIGHT; ?>" readonly> </div>
				<div class="col-md-3"> <label>Letpl Invoice Date:</label><input class="form-control" value="<?= $ltp; ?>" readonly> </div>					
					
			</div>
			  
			 </div>
			</div>
	  <!-----------------------------     Customer Product View ends here ---------------------------->		

             <?php  
						//foreach($data as $d) 
						//{  
							// $case_id = $d['case_guid'];
							
							/*  Vehicle remarks part */				         
						 $query 	= $this->db->query("SELECT  `VehicleRemarkDesc` FROM `tbl_service_vehicle_remark` WHERE `vehicleRemarkId`='".$data->vehicl_chrg_remark."'");
						 $row 	= $query->result();
						 $VehicleRemarkDesc = $row[0]->VehicleRemarkDesc;
				 ?>
	  <div class="card border-success mb-3" style="max-width: 120rem;">
	  <div class="card-header"> 
	    <div class="row">
			<div class="col-md-9"><h5>COMPLAINT CREATED DETAILS</h5></div>
			<div class="col-md-3"><button type="button" class="btn btn-primary"> Compaint Id  <span class="badge badge-light"><?= $data->call_id; ?></span></button></div>
		</div>
	  </div>
	  <div class="card-body text-primary">
		  
			<div class="row">
				<div class="col-md-3"> <label>Complaint Id:</label><input class="form-control" value="<?= $data->call_id; ?>" readonly> </div>
				<div class="col-md-3"> <label>Caller Type:</label><input class="form-control" value="<?= $data->callerType; ?>" readonly> </div>
				<div class="col-md-3"> <label>Caller Mobile:</label><input class="form-control" value="<?= $data->callerMobile; ?>" readonly> </div>
				<div class="col-md-3"> <label>Complaint Source:</label><input class="form-control" value="<?= $data->callSource; ?>" readonly> </div>
			</div>
			
			<?php // if(empty($sas[0]->caseId)){ ?>
			
<!-------------------Battery sas Report Details --------------------------->			
			</div>
			</div>
				<?php if(($data->physicalCond)!='Ok'){ ?>
			
				 <div class="card border-success mb-3" style="max-width: 120rem;">
				 <div class="card-header">  <h5>Battery Report Details</h5></div>
				<div class="card-body text-primary">
				<div class="row">
					<div class="col-md-3"> <label>Physical Condition:</label><input class="form-control" value="<?= $data->physicalCond; ?>" readonly> </div>
					<div class="col-md-3"> <label>Condition Terminal:</label><input class="form-control" value="<?= $data->condOfTerminal; ?>" readonly> </div>
					<div class="col-md-3"> <label>Electrolyte Color:</label><input class="form-control" value="<?= $data->electrolyteColor; ?>" readonly> </div>
					<div class="col-md-3"> <label>OCV:</label><input class="form-control" value="<?= $data->ocv; ?>" readonly> </div>
					</div>
				</div>
				</div>
				
			<?php } ?>	
		 
		   
		   <?php 
             if($data->caLL_case_status == 0)
             {   				 
		         if($data->warranty == "inwarranty")
				 {
		   ?>
					<center>
						<a href="<?php echo base_url(); ?>index.php/fault_parts/battery_charge_report">
						<button type="submit" class="btn btn-info" value="Close Outwarranty Case">Test Report of Defective Battery Before/After Charging</button>
					</center>
				 <?php
				 }
				 else if($data->warranty == "Outwarranty")
				 {
				 ?>
				 
				 <form action="<?php echo base_url(); ?>index.php/service_cases/close_outwarranty" method="post">
				<div class="form-group">
					<input type="hidden" name="caseid" value="<?= $data->call_id; ?>">
					<label class="col-form-label" for="formGroupExampleInput"><b>Distributer Remark</b></label>
					<input type="text" name="dist_remark"class="form-control"  id="formGroupExampleInput" placeholder="Example input">
				</div>
				 
				  <div class="form-group">
					<button type="submit" class="btn btn-info" value="Close Outwarranty Case" placeholder="E.g. Battery is rejected due to outwaranty case.">Close Outwarranty Case</button>
				  </div>
			</form>
				 
				 <?php 
				 }
			 }		
			else 
			{			
		
		 
				 ?>
				 <a href="<?php echo base_url(); ?>index.php/service_cases">
				 
				 </a>
				 
			<?php } ?> 
  

 <!--------------------------------------------Battery sas Report Ok------------------------------------> 
  
<?php if(!empty($sas)) { ?>
  <!-- /.container-fluid-->
		<div class="card border-danger mb-3" style="max-width: 120rem;">
		 <div class="card-header"> <h5> Battery Test Report<button type="button" class="btn btn-info pull-right">
		 <?php 
		    $b =$data->submitDate;  $timestamp = strtotime($b);	$dmyy = date("d-m-Y", $timestamp); 
		 ?>
			Report Submitted Date & Time : <span class="badge badge-light"><?= $dmyy; ?></span>
			</button></h5>
		</div>
		<div class="card-body text-primary">
		<h5 class="card-title">Test Report Before Charge</h5><hr>
		<div class="row">
				<div class="col-md-2"> <label>COMPLAINT	 ID:</label><input class="form-control" value="<?= $data->caseId; ?>" readonly> </div>
				<div class="col-md-2"> <label>Physical Condition:</label><input class="form-control" value="<?= $data->physicalCond; ?>" readonly> </div>
				<div class="col-md-2"> <label>Cond. of Terminal:</label><input class="form-control" value="<?= $data->condOfTerminal; ?>" readonly> </div>
				<div class="col-md-2"> <label>Electrolyte Color:</label><input class="form-control" value="<?= $data->electrolyteColor; ?>" readonly> </div>
				<div class="col-md-2"> <label>OCV:</label><input class="form-control" value="<?= $data->ocv; ?>" readonly> </div>
				<div class="col-md-2"> <label>Load:</label><input class="form-control" value="<?= $data->loadsas; ?>" readonly> </div>		
		</div>
			
			
			<div class="row">
				<div class="col-md-1"> <label>HRD:</label><input class="form-control" value="<?= $data->hrd; ?>" readonly> </div>
				<div class="col-md-1"> <label>Backup:</label><input class="form-control" value="<?= $data->backup; ?>" readonly> </div>
				<div class="col-md-1"> <label>CCARatd:</label><input class="form-control" value="<?= $data->bCCAr; ?>" readonly> </div>	
				<div class="col-md-2"> <label>CCA Measured:</label><input class="form-control" value="<?= $data->bCCAm; ?>" readonly> </div>
				<div class="col-md-1"> <label>Verdict:</label><input class="form-control" value="<?= $data->bVerdict; ?>" readonly> </div>
				<div class="col-md-1"> <label>C1:</label><input class="form-control" value="<?= $data->bef_c1; ?>" readonly> </div>
				<div class="col-md-1"> <label>C2:</label><input class="form-control" value="<?= $data->bef_c2; ?>" readonly> </div>
				<div class="col-md-1"> <label>C3:</label><input class="form-control" value="<?= $data->bef_c3; ?>" readonly> </div>	
				<div class="col-md-1"> <label>C4:</label><input class="form-control" value="<?= $data->bef_c4; ?>" readonly> </div>
				<div class="col-md-1"> <label>C5:</label><input class="form-control" value="<?= $data->bef_c5; ?>" readonly> </div>
				<div class="col-md-1"> <label>C6:</label><input class="form-control" value="<?= $data->bef_c6; ?>" readonly> </div>
			</div>
				
				<br><hr>		
			<h5 class="card-title">Test Report After Charge</h5><hr>
			<div class="row">
				<div class="col-md-2"> <label>Charg. Start Date:</label><input class="form-control" value="<?= $data->chrgeStDate; ?>" readonly> </div>
				<div class="col-md-2"> <label>Charg. Start Time:</label><input class="form-control" value="<?= $data->chrgStTime; ?>" readonly> </div>
				<div class="col-md-2"> <label>Charg. End Date:</label><input class="form-control" value="<?= $data->ChrgeEndDate; ?>" readonly> </div>	
				<div class="col-md-2"> <label>Charg. End Time:</label><input class="form-control" value="<?= $data->ChrgeEndTime; ?>" readonly> </div>
				<div class="col-md-2"> <label>Charg. Ampere:</label><input class="form-control" value="<?= $data->chrgingAmpere; ?>" readonly> </div>
				<div class="col-md-2"> <label>TOC:</label><input class="form-control" value="<?= $data->TOC; ?>" readonly> </div>
			</div>
			<?php $c1 = $data->aft_c1;
			if(!empty($c1) || $c1 > 0)
			{ ?>
			<div class="row">
				<div class="col-md-1"> <label>C1:</label><input class="form-control" value="<?= $data->aft_c1; ?>" readonly> </div>
				<div class="col-md-1"> <label>C2:</label><input class="form-control" value="<?= $data->aft_c2; ?>" readonly> </div>
				<div class="col-md-1"> <label>C3:</label><input class="form-control" value="<?= $data->aft_c3; ?>" readonly> </div>	
				<div class="col-md-1"> <label>C4:</label><input class="form-control" value="<?= $data->aft_c4; ?>" readonly> </div>
				<div class="col-md-1"> <label>C5:</label><input class="form-control" value="<?= $data->aft_c5; ?>" readonly> </div>
				<div class="col-md-1"> <label>C6:</label><input class="form-control" value="<?= $data->aft_c6; ?>" readonly> </div>
				<div class="col-md-2"> <label>Load :</label><input class="form-control" value="<?= $data->aft_loadsas; ?>" readonly> </div>
				<div class="col-md-2"> <label>HRD:</label><input class="form-control" value="<?= $data->aft_hrd; ?>" readonly> </div>
				<div class="col-md-2"> <label>Backup:</label><input class="form-control" value="<?= $data->aft_backup; ?>" readonly> </div>	
			</div>
			<?php } ?>	
			<div class="row">
				<div class="col-md-4"> <label>CCA Rated:</label><input class="form-control" value="<?= $data->aCCAr; ?>" readonly> </div>
				<div class="col-md-4"> <label>CCA Measured:</label><input class="form-control" value="<?= $data->aCCAm; ?>" readonly> </div>
				<div class="col-md-4"> <label>Verdict:</label><input class="form-control" value="<?= $data->aVerdict; ?>" readonly> </div>
				<!--div class="col-md-6"> <label>Submitted By Distributor:</label><input class="form-control" value="<?php // $sas[0]->submittedByDist; ?>" readonly> </div-->					
			</div>
			
			<hr><h5 class="card-title">Vehicle test Report</h5></hr>
			<?php if($data->vehicle_avail == 1 || !empty($data->vehicle_avail)) { ?>
			<div class="row">
				<div class="col-md-2"> <label>Vehicle Availability:</label><input class="form-control" value="<?php $x =$data->vehicle_avail;  if($x == "1") {echo "Yes"; } else {echo "No"; } ?>" readonly> </div>
				<div class="col-md-2"> <label>Minimum Output:</label><input class="form-control" value="<?= $data->min_out_curr; ?>" readonly> </div>
				<div class="col-md-2"> <label>Maximum Output:</label><input class="form-control" value="<?= $data->max_out_curr; ?>" readonly> </div>	
			
				<div class="col-md-2"> <label>Lower Cut off:</label><input class="form-control" value="<?= $data->lower_cutoff_volt; ?>" readonly> </div>
				<div class="col-md-2"> <label>Upper Cut off:</label><input class="form-control" value="<?= $data->upper_cutoff_volt; ?>" readonly> </div>
				<div class="col-md-2"> <label>System Leakage:</label><input class="form-control" value="<?= $data->sys_leakage; ?>" readonly> </div>	
			</div>
			
			<div class="row">
				<div class="col-md-3"> <label>Volt. drop during cranking:</label><input class="form-control" value="<?= $data->vd_cranking; ?>" readonly> </div>
				<div class="col-md-3"> <label>Current drawn during cranking:</label><input class="form-control" value="<?= $data->cd_cranking; ?>" readonly> </div>
				<div class="col-md-3"> <label>E-Rickshaw chargers charging O/P:</label><input class="form-control" value="<?= $data->erick_chrg_out; ?>" readonly> </div>	
				<div class="col-md-3"> <label>Vehicle Charging Report Remark:</label><input class="form-control" value="<?= $VehicleRemarkDesc; ?>" readonly> </div>
			</div>
			
				
			<?php }  else{ ?>	
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
							  <strong>The Vehicle was not available for test Report.</strong>
							  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							  </button>
							</div>
<?php } ?>
		
			
			<?php } else if($data->caLL_case_status != 1){ ?>
			
			<!--div class="alert alert-danger" role="alert">
			  Sorry ! You have only created the case.. The status is open because  <a href="#" class="alert-link">battery replacement details has not submitted.</a>. See the status below..
			  <br/>
             <button type="button" class="btn btn-success"> <strong>The green status</strong></button>	&nbsp;&nbsp;&nbsp;shows the current completed status.		  
			</div-->
			<!--div class="stepwizard">
				<div class="stepwizard-row">
					<div class="stepwizard-step">
						<button type="button" class="btn btn-success btn-circle">1</button>
						<p>Create Case</p>
					</div>
					<div class="stepwizard-step">
						<button type="button" class="btn btn-default btn-circle">2</button>
						<p>Battery  sas Report</p>
					</div>
					<div class="stepwizard-step">
						<button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
						   <p>Case Report & Remark</p>
					</div> 
				</div>
			</div -->
			<?php }  ?>
		</div>
		
		</div>
		
		<?php
             if($data->caLL_case_status == 0)
			 {
				 if(!empty($asset[0]->productSerialNo)) {
		?>
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<strong>Case is Open!</strong> You haven't given decision yet. Please give decision on the case.
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>	
          <center> 
		         <a href="<?php echo base_url(); ?>index.php/fault_parts/decision/<?php echo base64_encode($data->call_id); ?>">
                    <button class="btn btn-info">Go to Submit Decision</button> </a>
          </center>		  
		<?php		 
			 } }
		?>
		<?php if( !empty($data->symptomCode)) {
			?>
<!---------------------------Distributor Decision starts ---------------------------------------->
 <div class="card border-success mb-3" style="max-width: 120rem;">
	<div class="card-header"><h5>Distributor Decision</h5></div>
			  <div class="card-body text-primary">
				<h5 class="card-title">Decision given by Distributor on behalf of Battery Report</h5><hr>
				  <div class="row">
				   <?php
						 $query 	= $this->db->query("SELECT * FROM `tbl_service_dist_remarks` WHERE `distRemarkId`='".$data->distributorRemark."'");
						 $row 	= $query->result();
						 $remarks = $row[0]->distRemarkName;
						 $sql 	= $this->db->query("SELECT * FROM `tbl_defect_symptoms` WHERE `id`='".$data->symptomCode."'");
						 $rows 	= $sql->result();
						
						 $symptom = $rows[0]->Symptoms;
						 $defect   = $rows[0]->Defect; 
                      
				  ?>
					<div class="col-md-4"> <label>Complaint Id :</label><input class="form-control" value="<?= $data->call_id; ?>" readonly> </div>
					<div class="col-md-4"> <label>Symptom Code :</label><input class="form-control" value="<?= $symptom; ?>" readonly> </div>
					<div class="col-md-4"> <label>Defect Code:</label><input class="form-control" value="<?= $defect; ?>" readonly> </div>
				</div>
				<hr>
     <!--------------------- Uploaded Documents 1 ------------------------------>	   
			   <div class="row">
				 <div class="col-md-2"><br/> <label>Document Type 1 >></label></div>
				 <div class="col-md-5">
				     <?php if(empty($sas[0]->documentType)) { ?>
					    <div class="alert alert-danger" role="alert">
							Sorry ! There is <a href="#" class="alert-link">no documentation type </a> selected by Distributor. 
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
							<input class="form-control" value="<?php echo $data->documentType; ?>" readonly >
						</div>	
					 <?php } ?>
				 </div>
				  
				 <div class="col-md-5">
				     <?php if(empty($data->uploadDocuments)) { ?>
					    <div class="alert alert-danger" role="alert">
							Sorry ! There is <a href="#" class="alert-link">no file </a> uploaded by Distributor.
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
					        <a href="<?php echo base_url(); ?>docUploadsCases/<?php echo $data->uploadDocuments; ?>" target="_blank">
									<button class="btn btn-info btn-sm"> View Document <i class="fa fa-eye"></i></button>
								 </a>
						</div>	
					 <?php } ?>
				  </div> 
				  
				</div>
		<!---------------------Upload Docs 1------------------------>
		 <!--------------------- Uploaded Documents 2------------------------------>	   
			   <div class="row">
				 <div class="col-md-2"><br/> <label>Document Type 2 >></label></div>
				 <div class="col-md-5">
				     <?php if(empty($data->docType2)) { ?>
					    <div class="alert alert-danger" role="alert">
							  Sorry ! There is <a href="#" class="alert-link">no documentation type </a> selected by Distributor. 
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
								<input class="form-control" value="<?php echo $data->docType2; ?>" readonly >
						</div>	
					 <?php } ?>
				 </div>
				  
				 <div class="col-md-5">
				     <?php if(empty($data->uploadDocs2)) { ?>
					    <div class="alert alert-danger" role="alert">
							  Sorry ! There is <a href="#" class="alert-link">no file </a> uploaded by Distributor.
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
							  <a href="<?php echo base_url(); ?>docUploadsCases/<?php echo $data->uploadDocs2; ?>" target="_blank">
									<button class="btn btn-info btn-sm"> View Document <i class="fa fa-eye"></i></button>
								 </a>
						</div>	
					 <?php } ?>
				  </div> 
				  
				</div>
		<!---------------------Upload Docs 2------------------------>
		 <!--------------------- Uploaded Documents 3 ------------------------------>	   
			   <div class="row">
				 <div class="col-md-2"><br/> <label>Document Type 3 >></label></div>
				 <div class="col-md-5">
				     <?php if(empty($data->docType3)) { ?>
					    <div class="alert alert-danger" role="alert">
							Sorry ! There is <a href="#" class="alert-link">no documentation type </a> selected by Distributor. 
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
							<input class="form-control" value="<?php echo $data->docType3; ?>" readonly >
						</div>	
					 <?php } ?>
				 </div>
				  
				 <div class="col-md-5">
				     <?php if(empty($data->uploadDocs3)) { ?>
					    <div class="alert alert-danger" role="alert">
							 Sorry ! There is <a href="#" class="alert-link">no file </a> uploaded by Distributor.
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
							  <a href="<?php echo base_url(); ?>docUploadsCases/<?php echo $data->uploadDocs3; ?>" target="_blank">
									<button class="btn btn-info btn-sm"> View Document <i class="fa fa-eye"></i></button>
								 </a>
						</div>	
					 <?php } ?>
				  </div> 
				  
				</div>
		<!---------------------Upload Docs 3------------------------>
		 <!--------------------- Uploaded Documents 4 ------------------------------>	   
			   <div class="row">
				 <div class="col-md-2"><br/> <label>Document Type 4 >></label></div>
				 <div class="col-md-5">
				     <?php if(empty($data->docType4)) { ?>
					    <div class="alert alert-danger" role="alert">
							Sorry ! There is <a href="#" class="alert-link">no documentation type </a> selected by Distributor. 
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
								<input class="form-control" value="<?php echo $data->docType4; ?>" readonly >
						</div>	
					 <?php } ?>
				 </div>
				  
				 <div class="col-md-5">
				     <?php if(empty($data->uploadDocs4)) { ?>
					    <div class="alert alert-danger" role="alert">
							Sorry ! There is <a href="#" class="alert-link">no file </a> uploaded by Distributor.
							</div>
					 <?php } else {
						 ?>
						<div class="alert alert-default" role="alert"> 
								<a href="<?php echo base_url(); ?>docUploadsCases/<?php echo $data->uploadDocs4; ?>" target="_blank">
									<button class="btn btn-info btn-sm"> View Document <i class="fa fa-eye"></i></button>
								 </a>
						</div>	
					 <?php } ?>
				  </div> 
				  
				</div>
		<!---------------------Upload Docs 4------------------------>
			  </div>
			</div>
		<?php 
		}
	
		?>
		
			
<!---------------------------Distributor Decision ends ---------------------------------------->


<!---------------------------Distributor Replacement Starts Section ---------------------------------------->
		<?php
		   
			if($data->replacementMode == "Settle Through Other Mode") { 
			?>
			
			<div class="card border-success mb-3" style="max-width: 120rem;">
			  <div class="card-header"><h5>Battery Replacement Mode</h5></div>
			  <div class="card-body text-primary">
				<h5 class="card-title">Battery Settled through Other Mode</h5><hr>
				  <div class="row">
					<div class="col-md-6"> <label> Remarks on chosing Other Mode:</label><input class="form-control" value="<?= $sas[0]->otherRemarks; ?>" readonly> </div>
					<div class="col-md-4"> <label>Date of Replacement:</label><input class="form-control" value="<?= $sas[0]->dateOfReplacementRemark; ?>" readonly> </div>
				  </div>
			  </div>
			</div>
            <?php } 
			elseif($data->replacementMode == "1"){ ?>
			  <div class="card border-success mb-3" style="max-width: 125rem;">
			  <div class="card-header"><h5>Battery Replacement Mode</h5></div>
			  <div class="card-body text-primary">
				
			<?php if($replace[0]->productSerialNo != "0" || !empty($replace[0]->productSerialNo)) { ?>
				<h5 class="card-title">Settled through Battery Replacement</h5><hr>
				<div class="row">
				  <div class="col-md-3"> <label>Replaced Product Serial No:</label><input class="form-control" value="<?= $replace[0]->productSerialNo; ?>" readonly> </div>
					<div class="col-md-3"> <label>Customer Name:</label><input class="form-control" value="<?= strtoupper($replace[0]->custName); ?>" readonly> </div>
					<div class="col-md-3"> <label>JOB Id:</label><input class="form-control" value="<?= strtoupper($replace[0]->jobId); ?>" readonly> </div>
					<div class="col-md-3"> <label>Distributor Settlement Type:</label><input class="form-control" value="<?= strtoupper($replace[0]->dist_settle_type); ?>" readonly> </div>
				</div>
				<div class="row">
					<div class="col-md-3"> <label>Brand:</label><input class="form-control" value="<?= strtoupper($replace[0]->brand); ?>" readonly> </div>
					<div class="col-md-3"> <label>Product Segment:</label><input class="form-control" value="<?= strtoupper($replace[0]->productSegment); ?>" readonly> </div>
					<div class="col-md-3"> <label>Product Type:</label><input class="form-control" value="<?= strtoupper($replace[0]->productType); ?>" readonly> </div>
					<div class="col-md-3"> <label>Model:</label><input class="form-control" value="<?= strtoupper($replace[0]->model); ?>" readonly> </div>					
				</div>
				 <div class="row">
				 <?php
				         $a =$replace[0]->primarySaleDate;
				         $b =$replace[0]->date_of_replace;
						 $timestamp = strtotime($a);	$dmy = date("d-m-Y", $timestamp); 
						 $timestamp = strtotime($b);	$emy = date("d-m-Y", $timestamp); 
				 ?>
					<div class="col-md-3"> <label>Primary Sale Invoice:</label><input class="form-control" value="" readonly> </div>
					<div class="col-md-3"> <label>Primary Sale Date:</label><input class="form-control" value="<?= $dmy; ?>" readonly> </div>
					<div class="col-md-3"> <label>Ageing Days:</label><input class="form-control" value="<?= strtoupper($replace[0]->aging_days); ?>" readonly> </div>					
					<div class="col-md-3"> <label>Date of Replacement:</label><input class="form-control" value="<?= strtoupper($emy); ?>" readonly> </div>					
				</div>
				<hr>
				<?php if(!empty($data->$se_decision)) { ?>
				<div class="alert alert-warning" role="alert">
					<center><strong>STATUS & STAGE: </strong>This complaint is a <b>TAT-1 Close</b> with <a href="#" class="alert-link">SE Decision pending</a>. Check Proress till SE Approval.</center>
				</div>
				
				<?php } } else  {?> <!-----------If serial number number couldnt replaced------>
					<div class="card-header"><h5>Sorry ! Battery Replacement was unsuccessful. Please contact customer care <i class="fa fa-phone"></i> .</h5></div>
			<?php } ?>			
			
			
			
			  </div>
			</div>
			<?php 
			} 
			else
			{
				 if($data->caLL_case_status == 1) {   // Check whether is in replacement mode
			?>
			 <div class="card border-success mb-3" style="max-width: 120rem;">
			  <div class="card-header"><h5>Battery Replacement Mode</h5></div>
			  <div class="card-body text-primary">
			  
			  <?php if(empty($replace)) { ?>
				 <center>
				  <a href="<?php echo base_url(); ?>index.php/fault_parts/replace_battery/<?php echo base64_encode($data->call_id); ?>">
						 <button class="btn btn-info btn-sm">Submit Replacement Details</button>
					</a>
				  </center>
			  <?php } else { ?> 
			      <?php if($replace[0]->productSerialNo != "0" || !empty($replace[0]->productSerialNo)) { ?>
				<h5 class="card-title">Settled through Battery Replacement</h5><hr>
				<div class="row">
				  <div class="col-md-3"> <label>Replaced Product Serial No:</label><input class="form-control" value="<?= $replace[0]->productSerialNo; ?>" readonly> </div>
					<div class="col-md-3"> <label>Customer Name:</label><input class="form-control" value="<?= strtoupper($replace[0]->custName); ?>" readonly> </div>
					<div class="col-md-3"> <label>JOB Id:</label><input class="form-control" value="<?= strtoupper($replace[0]->jobId); ?>" readonly> </div>
					<div class="col-md-3"> <label>Distributor Settlement Type:</label><input class="form-control" value="<?= strtoupper($replace[0]->dist_settle_type); ?>" readonly> </div>
				</div>
				<div class="row">
					<div class="col-md-3"> <label>Brand:</label><input class="form-control" value="<?= strtoupper($replace[0]->brand); ?>" readonly> </div>
					<div class="col-md-3"> <label>Product Segment:</label><input class="form-control" value="<?= strtoupper($replace[0]->productSegment); ?>" readonly> </div>
					<div class="col-md-3"> <label>Product Type:</label><input class="form-control" value="<?= strtoupper($replace[0]->productType); ?>" readonly> </div>
					<div class="col-md-3"> <label>Model:</label><input class="form-control" value="<?= strtoupper($replace[0]->model); ?>" readonly> </div>					
				</div>
				 <div class="row">
				 <?php
				         $a =$replace[0]->primarySaleDate;
				         $b =$replace[0]->date_of_replace;
							$timestamp = strtotime($a);	$dmy = date("d-m-Y", $timestamp); 
							$timestamp = strtotime($b);	$emy = date("d-m-Y", $timestamp); 
				 ?>
					<div class="col-md-3"> <label>Primary Sale Invoice:</label><input class="form-control" value="" readonly> </div>
					<div class="col-md-3"> <label>Primary Sale Date:</label><input class="form-control" value="<?= $dmy; ?>" readonly> </div>
					<div class="col-md-3"> <label>Ageing Days:</label><input class="form-control" value="<?= strtoupper($replace[0]->aging_days); ?>" readonly> </div>					
					<div class="col-md-3"> <label>Date of Replacement:</label><input class="form-control" value="<?= strtoupper($emy); ?>" readonly> </div>					
				</div>
				<hr>
				
				<?php if(!empty($data->$se_decision)) { ?>
				<div class="alert alert-warning" role="alert">
					<center><strong>STATUS & STAGE : </strong>This complaint is a <b>TAT-1 Close</b> with <a href="#" class="alert-link">SE Decision pending</a>. Check Proress till SE Approval.</center>
				</div>
				<?php } ?>
			<?php } else  {?> <!-----------If serial number number couldnt replaced------>
					<div class="card-header"><h5>Sorry ! Battery Replacement was unsuccessful. Please contact customer care. <i class="fa fa-call"></i></h5></div>
			<?php } ?>			
			  <?php } ?>	  
			  
			  
			  </div>
			</div>
			<?php } } ?>
			
			<?php // } ?>  <!-- Submit battery sas report -->
 
		
<!--------------------------------------------------SE APRROVAL SECTION--------------------------------------------->		
<?php if(!empty($sas[0]->se_approval)  || $sas[0]->se_approval != "" || $sas[0]->se_approval == "0")   { ?>
<div class="alert alert-success" role="alert">
							Decision has been given <a href="#" class="alert-link">by SE/SM </a> . Please get in touch for more info.
							</div>

<div class="card border-success mb-3" style="max-width: 120rem;">
	<div class="card-header"><h5>SE/SM Decision Details : </h5></div>
			  <div class="card-body text-primary">
				<h5 class="card-title">Decision given by SE </h5><hr>
				  <div class="row">
				  <?php
//				  $a = $sas[0]->se_estd_visit_date; $timestamp = strtotime($sas[0]->se_estd_visit_date);	 $dmy = date("d-m-Y", strtotime($sas[0]->se_estd_visit_date));  
				      $sed = strtoupper($sas[0]->se_decision);
					 // echo $sas[0]->se_approval."jehnfjewnfk";
				  ?>
						<div class="col-md-4"> <label>SE Visit Date :</label><input class="form-control" value="<?php echo date("d-m-Y", strtotime($sas[0]->se_estd_visit_date)); ?>" readonly> </div>
						<div class="col-md-4"> <label>SE Approval :</label><input class="form-control" value="<?php if($sas[0]->se_approval == "1"){ echo "Physical Verification Done"; } elseif($sas[0]->se_approval == "2") { echo "Battery saser Report"; }  elseif($sas[0]->se_approval =="3"){ echo "OnLine Approval"; }?>" readonly> </div>
						<div class="col-md-4"> <label>SE Decision:</label><input type="text" class="form-control" value="<?php echo $sed; ?>" disabled > </div>
					</div>
	</div>
</div>
			<?php } 
		//	echo $sed."kjeasfn";
			?> 

			<?php  if($sas[0]->se_decision == "rejected") {?>
				<div class="card border-success mb-3" style="max-width: 120rem;">
					<div class="card-header" style="color:#fff; background:crimson;"><center><h5>TAT-1  Closed successfully with SE decision <b>REJECTED</b>.</h5></center></div>
				</div>
			<?php } ?>

			<?php  if($sas[0]->se_decision == "ok_same" ) {?>
				<div class="card border-success mb-3" style="max-width: 120rem;">
					<div class="card-header" style="color:#fff; background:crimson;"><center><h5>TAT-1 Closed successfully with SE decision <b>Ok Same</b>.</h5></center></div>
				</div>
			<?php } ?>
			<?php  if($sas[0]->se_decision == "approved") {?>
				<div class="card border-success mb-3" style="max-width: 120rem;">
					<div class="card-header" style="color:#fff; background:green;"><center><h5>TAT-1  Closed successfully with SE decision <b>Approved</b> (Elligible for Challan).</h5></center></div>
				</div>
			<?php } ?>
		</div>
		</div>
		
		
<!--------------------------------------------------SE APRROVAL SECTION--------------------------------------------->		
	
		</div>
	</div>
</div>
	
   <div id="imgModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<script>
// Get the modal
var modal = document.getElementById('imgModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>

 
    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 
