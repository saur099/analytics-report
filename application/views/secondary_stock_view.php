  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Secondary Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Secondary Stock</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> <b>Secondary Stock</b> 
		  <span class="pull-right"><a href="<?php echo base_url(); ?>index.php/branch_order/upload_excel"><button class="btn btn-info">Upload Excel</button></a></span>
		  </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  			  <thead>
                            <tr>
                                <th>Product Name</th>
								<th>Serial Number</th>
								<th>Model Number</th>
								<th>Dealer Code</th>
								<th>Dealer Name</th>
								<th>Quantity</th>
								<th>Aging</th>
                                <th>Last Scan Date</th>                               
                                							
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  
							
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?= $d->product_name;?></td>
										<td><?=  $d->serial_number;?> </td>
										<td><?=  $d->model_number;?> </td>
										<td><?=  $d->dealer_code;?> </td>
										<td><?=  $d->dealer_name;?> </td>
										<td><?= $d->quantity;?></td>
										<td><?= $d->aging;?></td>
										<td><?=  $d->last_scan_date;?> </td>
										<!--<td><?php echo anchor('secondary/view_order/'.$result, 'Info', array('class' => '', 'id' => '')); ?></td>-->
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 