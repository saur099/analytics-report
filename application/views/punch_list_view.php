  <?php 
//echo "<pre>"; print_r($res); die;
 			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Dealers List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
	   <li class="breadcrumb-item">
			<a href="<?php echo base_url(); ?>index.php/order_punch/dash"><i class="fa fa-chevron-circle-left fa-2x" aria-hidden="true"></i></a>
	    </li>
	   <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>index.php/order_punch/dash"> Order Processing Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Primary Order</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>  <b> Primary Order</b> List</div>
        <div class="card-body">
          <div class="table-responsive">
            

<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                            <tr>
								<th>Date Entered</th>
								<th>Total Order Qty</th>
								<th>View Orders</th>
						    </tr>
                </thead>
				<tbody>
					<?php
					foreach($primary_order as $primary_orders){
						$date1 = explode(' ',$primary_orders->date_entered);
						$date = $date1[0];
						?>
						<tr class="odd gradeX">
							<td><?php echo $date; ?></td>
							<td><span style="background:green; color:#fff; padding:5px;"><?php echo $primary_orders->qty;?></span></td>
							<td><a href="<?php echo base_url(); ?>index.php/secondary/primary_order_details/<?php echo base64_encode($primary_orders->fe_ref_num_c);?>"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View Order</button></a> </td>
						</tr>
						<?php
					}
					?>			
                </tbody>
           </table>
        


			</div>
        </div>
      
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
