  <?php 
 // echo "<pre>"; print_r($data);
 // echo "<pre>"; print_r($sch); die; 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Schemes</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Schemes List</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> All Schemes
		  <span class="div pull-right">
		   <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">
		     <i class="fa fa-plus" aria-hidden="true"></i> Add New Scheme
		   </button>
		  </span>
		  </div>
		  
		  
		  
		  
		 

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add New Schemes</h4>
        </div>
        <div class="modal-body">
         
		           <form role="form" action="<?php echo base_url(); ?>index.php/branch_order/addscheme" method="post">
						<div class="form-group">
                                <label>Scheme Name</label>
                                <input  type="text" class="form-control" name="scheme_name"/>
                            </div>
                            <div class="form-group">
                                <label>Product Name</label>
                                <select  id="product_name" class="form-control" name ="product_name">
								<option value="">Please select any Product serial Number</option>
								<?php
								      foreach($sch as $s)
									  {
								?>		  
									 
									 <option value="<?php echo $s->serial_num; ?>"><?php echo $s->serial_num; ?></option>
								<?php	
									  }
								?>
                                </select>
                            </div>
							 <div class="form-group">
                                <label>Quantity</label>
                                <input  type="text" class="form-control" name="quantity"/>
                            </div>
                            <div class="form-group">
                                <label>Duration</label>
                                <select  id="duration" class="form-control" name ="duration">
								<option value="1_Month">1 Month</option>
								<option value="2_Month">2 Month</option>
								<option value="Quater">Quater</option>
								<option value="5_Month">5 Month</option>
								<option value="Half Yearly">Half Yearly</option>
								<option value="Yearly">Yearly</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Start Time</label>
                                <!--<input id="start_time" name ="start_time" class="form-control"  placeholder="Start Time">-->
								<?php //echo $this->calendar->generate();?>
								<input class="form-control"  type="date" name="start_time">

								
															</div>
															
															
															<div class="form-group">
																<label>End Time</label>
																<input class="form-control" id="end_time" type="date" name ="end_time">


								
															</div>

							<button type="button" id="distributor_btn" class="btn btn-success">Submit Button</button>
							<button type="reset" class="btn btn-info">Reset Button</button>
						</form>
										 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
		  
		  
		  
		  
		  
		  
		  
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                            <tr>
                                <th>Scheme Name</th>
								<th>Product Name</th>
								<th>Quantity</th>
                                <th>Duration</th>                               
                                <th>Start Date</th>                               
                                <th>End Date</th>                               
                               <!-- <th>Delete</th>    -->                           
                            </tr>
                            </thead>
							<tbody>
							
							
                            </tbody>
                          </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
