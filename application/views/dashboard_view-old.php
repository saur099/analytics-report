<!DOCTYPE html>
<html lang="en">
<head>
    <?php 
			$this->load->view('includes/top.php'); 
   ?>
   <title>Dashboard | Livguard </title>
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top" >
  <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
	  
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">My Dashboard</li> </ol>
        
	  
   </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
  


   <?php 
			$this->load->view('includes/footer.php');  
   ?>
   
  	
<script type="text/javascript">
$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('.stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('.stars li').on('click', function(){ //var myid = $(this).parent().attr('id'); alert(myid);
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected 
	var star_id = $(this).parent().attr('id');
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
   
	$('#'+star_id+'_qtn').val(onStar);
    responseMessage(msg);
    
  });
  
  
});


function responseMessage(msg) {
  //$('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
</script>
   
   <script>
   $('document').ready(function(){
	  var ctx = document.getElementById("myPieChart");
	   var a = <?php echo $a; ?>;
		var myPieChart = new Chart(ctx, {
		  type: 'pie',
		  data: {
			labels: ["SameBack", "Rejected", "Pending", "Open"],
			datasets: [{
			  data: [1, 2, 3, 4],
			  backgroundColor: ['#007bff', '#dc3545', '#ffc107', '#28a745'],
			}],
		  },
		}); 
   });
   
   </script>
   
   
    
   
     <?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
  </div>
</body>

</html>
