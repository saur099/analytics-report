<?php //echo "<pre>";print_r($data);?>
<?php    $this->load->view('library');    ?>
<body>
 <div id="wrapper">
      
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Order Rejection Details
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive11">
				
				
				       <div id="table_distributor">
					   
	
	<form id="frmAddUser" method="post" action="">
<label>Reason</label>
<select class="form-control" name="reject" id = "reject">
<option value="">Please Select</option>
<option value="Stock_Not_Available">Stock Not Available</option>
<option value="Past_Credit_Issue_With_Dealer">Past Credit Issue With Dealer</option>
<!--<option value="Unable_To_Give_Credit">Unable To Give Credit</option>-->
<option value="Fake_Order">Fake Order</option>
<option value="Order_Is_Too_Big">Order Is Too Big</option>
<option value="Payment_Not_Received_From_Dealer">Payment Not Received From Dealer</option>
<option value="Not_Intereted_To_Give_Credit">Not Intereted To Give Credit</option>
<option value="Return_Issue">Return Issue</option>
<option value="Not_Dealing_With_Dealer_AnyMore">Not Dealing With Dealer AnyMore</option>
</select>
<br>
<input type ="hidden" id ="data_value" value ="<?php echo $data[0]->id;?>">
<input type ="hidden" id ="dis_code" value ="<?php echo $data[0]->distributor_code;?>">
<button type="button" class="btn btn-primary" id ="btnAdd">Confirm</button>
 </form>
 <?php foreach($data as $d)
 {
	// echo $d->rejection_reason;
	 if($d->rejection_reason!= '')
	 {    $rejection_reason = str_replace(array('_', ' '), ' ', $d->rejection_reason);
		 echo "You have already rejected this product on ".$d->date_entered." due to given reason: ".$rejection_reason;
	 }
 }
 ?>
 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
 <script>
  $(document).ready(function() {

    $('#btnAdd').click(function () {

      var order_id = $('#data_value').val(); 
	   var dis_code = $('#dis_code').val(); 
	  var reject = $('#reject').val();

      var data = 'order_id='+ order_id + '&reject='+ reject + '&dis_code='+dis_code; // this where i add multiple data using  ' & '

      $.ajax({
        type:"POST",
        cache:false,
        url: "<?php echo base_url(); ?>index.php/secondary/reject_order_details",
        data:data,    // multiple data sent using ajax
        success: function (html) {

          parent.$.fancybox.close(); 
        }
      });
      return false;
    });
  });
</script>					   </div>					
                    
                </div>

            </div>
           
        </div>
        
    </div>
    
</div>

        </div>
         
 </div>