
 <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Services | Call Management</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">

      <!-- Example DataTables Card-->
       <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <h4 class="breadcrumb">
        
        <li class="breadcrumb-item active">Order Punching Dashboard</li>
      </h4>
      <!-- Icon Cards-->
     <div class="container dash">
		 <div class="row dash1">
			 <div class="col-md-5 dash">
			      <div class="row dash11">
				         <div class="col-sm-6"><i class="fa fa-pencil-square-o fa-5x" aria-hidden="true"></i></div>
						 <div class="col-sm-6"><br><h4>Create </h4></div>
				  </div>
				<a class="adash" href="<?php echo base_url(); ?>index.php/order_punch/punching">  
				  <div class="row dash22">
				    <h3> Secondary Order  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h3>
				  </div>
				</a>  
			 </div>
			 <div class="col-md-5 dash">
			         <div class="row dash11">
				         <div class="col-sm-6"><i class="fa fa-pencil-square-o fa-5x" aria-hidden="true"></i></div>
						  <div class="col-sm-6"><br><h4>Create</h4></div>
				  </div>
				<a class="adash" href="<?php echo base_url(); ?>index.php/order_punch/punching_comp"> 
				  <div class="row dash22">
				    <h3> Primary Order &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h3>
				  </div>
				</a>  
			 </div>
		 </div>
	<!--	  <div class="row dash1">
			 <div class="col-md-5 dash">
			      <div class="row dash11">
				         <div class="col-sm-6"><i class="fa fa-eye fa-5x" aria-hidden="true"></i></div>
						 <div class="col-sm-6"><br><h4>View</h4></div>
				  </div>
				<a class="adash" href="<?php //echo base_url(); ?>index.php/order_punch/punch_list">   
				  <div class="row dash22">
				    <h3>  Secondary Order  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h3>
				  </div>
				</a>  
			 </div>
			 <div class="col-md-5 dash">
			         <div class="row dash11">
				         <div class="col-sm-6"><i class="fa fa-eye fa-5x" aria-hidden="true"></i></div>
						  <div class="col-sm-6"><br><h4>View</h4></div>
				  </div>
				<a class="adash" href="<?php //echo base_url(); ?>index.php/order_punch/punch_illustrate">  
				  <div class="row dash22">
				    <h3> Primary Order  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h3>
				  </div>
				</a>  
			 </div>
			 -->
		 </div>
	 </div>
	 <!-- Area Chart Example-->
     
			</div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
  
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	<script src="<?php echo base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script>
  </div>
</body>

</html>

					 
