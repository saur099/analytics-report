  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Dealers List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Dealer List</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Dealer's Record List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                            <tr>
                                <th>Dealer Name</th>
								<th>City</th>
								<th>State</th>
								<th>Postal Code</th>
								<th>Customer Number</th>
								<th>Customer Name</th>
								<th>Phone</th>
								<th>Channel Category</th>
								<th>Channel Sub Category</th>
								<th>Hub Name</th>
								<!--<th>Hub Code</th>-->
						    </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  ?>
									<tr class="odd gradeX">
										<td><?= $d->dealer_name;?></td>
										<td><?=  $d->billing_city;?> </td>
										<td><?= $d->billing_state;?></td>
										<td><?=  $d->billing_postalcode;?> </td>
										<td><?=  $d->customer_number;?> </td>
										<td><?=  $d->full_name;?> </td>
										<td><?=  $d->mobile;?> </td>
										<td><?=  $d->channel_category;?> </td>
										<td><?=  $d->channel_sub_category;?> </td>
										<td><?=  $d->hub_name;?> </td>
										<!--<td><?=  $d->hub_code;?> </td>-->
									</tr>
                            <?php } ?>
                            </tbody>
                          </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
