  <style>
  label {
    font-weight: 700;
}
  </style>
  <?php 
  //print_r($dealer); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Return Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Return Dealer to Distributor List</li>
      </ol>
	  
	  <hr>
	  
	<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
           
            <div class="panel-body">
                
           <form role="form" action="<?php echo base_url(); ?>index.php/return_orders_dealer/add_dealer_stock" method="post">
						
               <div class="row">
                    <div class="col-lg-6">
							<div class="form-group">
                                <label>Dealer Name</label>
                                <select id="assign_branch" class="form-control" name ="dealer">
									   <option value="Bajrang">  Bajrang Shankar</option>
									   <option value="Nadeem"> Nadeem Bhatt</option>
									   <option value="Kishore"> Kishore Kumar </option>
								</select>
                            </div>
                            </div>
						 <div class="col-lg-6">	
							<div class="form-group">
                                <label>Product Name</label>
                                <select  id="product_name" class="form-control" name ="product">
								    <option value="FP22E510328975">PEP-STAR </option>
								    <option value="E9GW34D1008375"> ENVY+(RED)</option>
								    <option value="C4HW30D1001885">ENVY+(GREY)</option>
                                </select>
                            </div>
                            </div>
					 <div class="col-lg-12">		
							<div class="form-group">
                                <label>Reason For Return</label>
                                <select  id="return_reason" class="form-control" name ="return_reason">
									<option value="">Select Reason for return</option>
									<option value="Defective_Stock">Defective Stock</option>
									<option value="OutBox_Damage">OutBox Damage</option>
									<option value="Dead_On_Arrival">Dead On Arrival</option>
									<option value="Filter_and_Membrane_Issue">Filter & Membrane Issue</option>
									<option value="Piece_Is_Very_Old">Piece Is Very Old</option>
									<option value="Cabinet_Damage">Cabinet Damage</option>
								</select>
                            </div>
                            </div>
                           <div class="col-lg-12">	 
                            <button type="submit" id="distributor_btn" class="btn btn-success">Submit Button</button>
                            
							</div>
                        </form>
                    </div>

                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
	  
	  
	  
	  <hr>
	  
	  
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-tag"></i>  Return Dealer to Distributor List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  	<thead>
                            <tr>
								<th>Returned to Distributor</th>
								<th>Serial Number</th>
                                <th>Return date</th>                               
                                <th>Return Reason</th>                               
                                <th>Status</th>                               
                                <th>Delete</th>                               
							</tr>
					</thead>
					<tbody>
					<?php   foreach($dealer as $d) { ?>
							<tr class="odd gradeX">
								
								<td align="center"><?=  $d->distName;?> </td>
								<td><button class="btn btn-primary"><?=  $d->return_serial_number;?> <i class="fa fa-eye"></i></button></td>
								<td><?= $d->return_date_entered;?></td>
								<td><?= $d->return_reason;?></td>
								<td><?php  if($d->return_status == 0 ) { echo '<button class="btn btn-warning">Waiting for approval </button>'; } else {echo '<button class="btn btn-success">Approved </button>';} ?> </td>
								<td>
								     <button type="button" class="delete_return_stock btn btn-danger btn-circle" onclick = "delete_return_stock(<?php echo $d->return_stock_id;?>)">
								     <i class="fa fa-times"></i></button>
								</td>
							</tr>
					<?php } ?>
					
					</tbody>
			      </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 