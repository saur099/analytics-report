  <style>
  label {
    font-weight: 700;
}
  </style>
  <?php 
  //print_r($dealer); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Return Company Product List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Return Distributor to Company List</li>
      </ol>
	  
	  <hr>
	  
	<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
         <div class="panel-body">
           <table id="myTable">
				<tr>
					<td><input type="text" name="links"></td>
					<td><input type="text" name="keywords"></td>
					<td><input type="text" name="violationtype"></td>
					<td><input type="button" class="button" value="Add another line" onclick="addField();"></td>
				</tr>
			</table>
			
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
	  
	  
	  
	  <hr>
	  
	  
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-tag"></i>  Return Dealer to Distributor List</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  	<thead>
                            <tr>
								<th>Returned to Distributor</th>
								<th>Serial Number</th>
                                <th>Return date</th>                               
                                <th>Return Reason</th>                               
                                <th>Status</th>                               
                                <th>Delete</th>                               
							</tr>
					</thead>
					<tbody>
					<?php   foreach($deal as $d) { ?>
							<tr class="odd gradeX">
								
								<td align="center"><?=  $d->distName;?> </td>
								<td><button class="btn btn-primary"><?=  $d->return_serial_number;?> <i class="fa fa-eye"></i></button></td>
								<td><?= $d->return_date_entered;?></td>
								<td><?= $d->return_reason;?></td>
								<td><?php  if($d->return_status == 0 ) { echo '<button class="btn btn-warning">Waiting for approval </button>'; } else {echo '<button class="btn btn-success">Approved </button>';} ?> </td>
								<td>
								     <button type="button" class="delete_return_stock btn btn-danger btn-circle" onclick = "delete_return_stock(<?php echo $d->return_stock_id;?>)">
								     <i class="fa fa-times"></i></button>
								</td>
							</tr>
					<?php } ?>
					
					</tbody>
			      </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
<script src="<?php echo base_url(); ?>js/ajaxjquery.min.js"></script>
    <script>
	 $(document).ready(function()
	 { 
		$('.charge').on('keyup', function() 
		{
			var totalQty = 0;
			$(".charge").each(function(){
			 totalQty += parseInt($(this).val());
			});
			$('#letpl_invoice_date').val(totalQty);
		});
					
				//window.alert("" + val1);
	});
	</script>
	<script>
	 function addField (argument) {
        var myTable = document.getElementById("myTable");
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var linksBox = document.createElement("input");
        linksBox.setAttribute("name", "links" + currentIndex);

        var keywordsBox = document.createElement("input");
        keywordsBox.setAttribute("name", "keywords" + currentIndex);

        var violationsBox = document.createElement("input");
        violationsBox.setAttribute("name", "violationtype" + currentIndex);

        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "Add another line");
        addRowBox.setAttribute("onclick", "addField();");
        addRowBox.setAttribute("class", "button");

        var currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(linksBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(keywordsBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(violationsBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
    }
	jQuery(function(){
    var counter = 1;
    jQuery('a.add-author').click(function(event){
        event.preventDefault();

        var newRow = jQuery('<tr><td><input type="text" name="first_name' +
            counter + '"/></td><td><input type="text" name="last_name' +
            counter + '"/></td></tr>');
            counter++;
        jQuery('table.authors-list').append(newRow);

    });
});
	</script>
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 