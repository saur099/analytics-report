  <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
   <title>Services | Change Password</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>

<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">

  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>index.php/service_cases/verify_serial"><button class="btn btn-info btn-sm"><i class="fa fa-angle-left "></i> Go Back</button></a>
        </li>
        <li class="breadcrumb-item active">Change Your AdminPassword</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
       <div class="card-header">
          <i class="fa fa-dashboard"></i> Change Your Admin Password
		
		  </div>
        
		
		
		
		<form id="form_manual" name="name" action="<?php echo base_url(); ?>index.php/dashboard/emailsend" method="post">
		<br/>
		 <div class="row">
		 <div class="col-md-1"> 
		 
		 
		 </div>
			<?php
			$msg =  $this->session->flashdata('email_sent'); 
			if(!empty($msg)){
				echo $msg;
			}else{
				echo $msg;
			}
			?>
				<div class="col-md-4">
					<label><b>Your Name : </b></label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Your Name" required>
					
					<label><b>Address : </b></label>
					<input type="text" class="form-control" name="add" id="add" placeholder="Your Address" required>
					
						<label><b>Email :</b> </label>
					<input type="text" class="form-control" name="email" id="email" onkeyup="checkPass(); return false;" placeholder="Email" required> 
					
					<span id="confirmMessage" class="confirmMessage"></span>
					<br/>
					
					<button type="submit" name="singlebutton" value="submit" class="btn btn-success btn-sm">Send Email</button>
				</div>
				
				 <div class="col-md-4 block">
					    <div class="form-group">
                    	  <div class="col-sm-12 col-md-12 col-lg-12 col-xs-10 mobilePad"  data-toggle="collapse" data-target="#passPolicy" style="font-weight: bold;font-size: 10pt;padding-left: 0px;color: black;cursor: pointer;text-decoration: underline;">Check Password Policy<span class="caret"></span>
                    	  </div>  
                    	 </div>
						 <div class="form-group">
							<a href="<?php echo base_url();?>index.php/dashboard/downloadzip">download Zip</a>
						 </div>
             <div class="form-group" style="margin-bottom: 0px;!important">
                    	  <div id="passPolicy" class="col-sm-12 col-md-12 col-lg-12 col-xs-12 collapse mobilePad" style="padding-right: 17px;">
                       <ul type="disc" style="padding-left: 0px;">
                    	  <li>Your Password must have minimum 6 characters.</li>
                    	   <li>Your Password must contain at least one number, one uppercase, lowercase & special character.</li>
                    	  <li>Your Password must not contain your Username.</li>
                    	  <li>Your Password must not contain Character or Number repetition.</li>
                    
                    	  </ul> 
                    	  </div>
                    	</div>
						<b>Please Click on Checkboxes to SHOW/HIDE password.</b>
				</div>
		 </div>
		</form>	
		
		</div>
		</div>
		</div>
	
	

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 
