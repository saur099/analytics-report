<?php 
// echo "<pre>"; print_r($data); die("Hello");
$this->load->view('library');    ?>
<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#inputBox").blur(function(){
		var abc = $('#inputBox').val(); 
		 if (abc < 0) { alert('Discount percentage can not be in negative value.');  $('#inputBox').val(''); return false;}
		 if (abc > 100) { alert('Discount percentage can not be more than 100 percent.');   $('#inputBox').val(''); return false;}
    });
	
	
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/drona/q.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/drona/dronahq.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/drona/scanner.js"></script>
<script type="text/javascript">
        $(document).ready(function () {
            if (DronaHQ.IsReady) {
                
            }
            else {
                document.addEventListener('deviceready', function () {

                });
            }
        });
    </script>
<style>
.divcode-container {
	width:100%;
	padding:6px 2px;
	border:1px solid #ddd;
	max-height:200px;
	overflow-y:auto;
}
span.mainSpan {
	background:#eee;
	padding:8px 4px;
	margin-right:6px;
}
span.textCode {
	font-size:normal;
}
span.removeSpan {
	padding:4px 8px;
	font-size:larger;
	color: #c71006;
}
@media only screen and (min-width: 375px) {
@media only screen and (max-width: 800px) {
.fancybox-wrap.fancybox-mobile.fancybox-type-iframe.fancybox-opened
{
    width: 302px !important;
    height: auto;
    position: absolute;
    top: 420px !important;
    left: 30px !important;
    opacity: 1;
    overflow: visible;
    display: block;
}

.fancybox-inner
{
    overflow: scroll !important;
    width: 340px !important;
    height: 390px !important;
}
}
</style>
<body>

 <div id="wrapper">
   <div id="page-wrapper" style="margin: 0px !important;">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
             <center> <b>  Dispatch Details</b></center>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive11">
				
				
				       <div id="table_distributor">
					   
	
	<form id="frmAddUser" method="post" action="">
	<div class="row">
		<div class="col-md-6 mrp" style="width: 50% !important;float:  left;">
		<label>Dispatch Through : <span style="color:red; font-weight:800;">*</span></label>
		<select class="form-control" name="dispatch" id = "dispatch" required>
			<option value="">Please Select</option>
			<option value="By_Hand">By Hand</option>
			<option value="Local_Courier">Local Courier</option>
			<option value="Local_Logistic">Local Logistic</option>
			<option value="Picked_By_Dealer">Picked By Dealer</option>
			<!--<option value="Picked_By_Sales_Executive">Picked By Sales Executive</option>-->
		</select>
		<?php 
		//echo "<pre>"; print_r($data); //die;
		//$avail_stock = $data[0]->con;
		$quantity = $data[0]->quantity;
		
		?>
		</div>
		<div class="col-md-6 mrp" style="width: 50% !important;float:  left;">
		<label>Quantities : <span style="color:red; font-weight:800;">*</span></label>
		<select class="form-control qnties" name="Quantities" id = "Quantities" required>
			<option value="">Please Select</option>
			<?php
			if($quantity >=0){
				for($i=1; $i<=$quantity; $i++){
					?>
					<option value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php
				}
			}
			?>
		</select>
		</div>
		</div>
	 	<div class="row">
		<div class="col-md-6 mrp" style="width: 50% !important;float:  left;">
				<label>Distributor Price :</label>
				<input class="form-control" type="text" name="dbprice" id="dbprice" value="<?php if(!empty($mrp_price[0]->SERVICE_CHARGE)){echo $mrp_price[0]->SERVICE_CHARGE;}?>"  disabled>
			</div>
		
		<div class="col-md-6 mrp" style="width: 50% !important;float:  right;">
			<label>MRP Price: </label>
				<input class="form-control " type="text" name="mrpprice" id="mrpprice" value="<?php if(!empty($mrp_price[0]->SCRAP_PRICE)){echo $mrp_price[0]->SCRAP_PRICE;}?>" disabled>
		</div>
		</div>
		<br>
		<?php //echo "<pre>"; echo $mrp_price[0]->SERVICE_CHARGE; die;?>
		<div class="row">
			
			
			<div class="col-md-6 mrp" style="width: 50% !important;float:  left;" >
				<label>Unit Price :<span style="color:red; font-weight:800;">*</span></label>
				<input class="form-control" type="number" name="actualprice" id="actualprice" min="1" max="1000000" value="" autocomplete="off" placeholder="00.00" required>
			</div>
			<div class="col-md-6 mrp" style="width: 50% !important;float:  right;" >
				<label>Discount (%) :</label>
				<input class="form-control" type='number' id='inputBox' placeholder='00' autocomplete="off" required>
			</div>
		</div>
		<br>	
		<?php //echo "<pre>"; print_r($serial_num); die;?>
		<label>Product Serial Number : </label>
		<!--select class="form-control choosen-select" name="dispatch_serial_no" id = "dispatch_serial_no" required multiple>
			<!--<option value="">Please Select</option>-->
			<?php
/* 			if(count($serial_num) > 0){
				foreach($serial_num as $serial_nums){
					?>
					<option value="<?php echo $serial_nums->serial_num;?>"><?php echo $serial_nums->serial_num;?></option>
					<?php
				}
			}
 */			?>
		<!--/select-->
<button title="Scan" type="button" style="width:31px !important;height:31px !important;padding:3px 0 !important;" class="reject_order btn btn-default btn-circle btnscanDispatch"><i class="fa fa-barcode fa-2x" aria-hidden="true"></i>
									</button>
									<!--<br />
									<input type="text" id="scanningCode" placeholder="Scanning code" />-->
		<!--<input type="text" class="form-control repositories" name="dispatch_serial_no" id = "dispatch_serial_no" required>-->
		<div class="divcode-container" name="dispatch_serial_no" id="dispatch_serial_no"></div>

		<br>
		<!--<textarea class="form-control" name="dispatch_serial_no" id="dispatch_serial_no" cols="30" rows="3" placeholder="Dispatch Serial No"></textarea>-->
		<input type ="hidden" id ="data_value" value ="<?php echo $data[0]->id;?>">
		<input type ="hidden" id ="dis_code" value ="<?php echo $data[0]->distributor_code;?>">
		<input type ="hidden" id ="avail_stock" value ="<?php echo $data[0]->avail_stock;?>">
		<input type ="hidden" id ="quantity_ordered" value ="<?php echo $data[0]->quantity;?>">	
		<input type ="text" readonly id ="product_category" class="form-control" value ="<?php echo $data[0]->product_category;?>">		
		<input type ="text" readonly class="form-control" id ="model_code" value ="<?php echo $data[0]->model_code;?>">			
		<br/>
		<center>
			<button type="button" class="btn btn-success" id ="btnAdd">Confirm</button>
		</center>
 </form>
 <?php foreach($data as $d)
 {
	 
	 if($d->dispatch_by_id!= '')
	 {    $dispatch_through = str_replace(array('_', ' '), ' ', $d->dispatch_through);
		 echo "Dispatch Details: ".$dispatch_through ."<br>Dispatch Quentity: " .$d->dispatch_quentity;
	 }
 }
 ?>
 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
 <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chosen.jquery.js"></script>
 <script>
	$(document).ready(function() {
		
	});
</script>					   </div>					
                    
                </div>

            </div>
           
        </div>
        
    </div>
    
</div>

        </div>
         
 </div>
 <script>$('.choosen-select').chosen();</script>