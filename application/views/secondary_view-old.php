  <?php 
//print_r($data['data']); exit;  
			$this->load->view('includes/top.php');  
			
   ?>
   <title>Livguard | Orders List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Order Processing</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Order Processing</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                            <tr>
                                <th>Order Mode</th>
								<th>Quantity</th>
								<th>Dealer Name</th>                              
                                <th>Hub Name</th>
                                <th>Order Date</th>
                                <!--<th>Order Status</th>-->
                                <th>Details</th>								
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  
							
							$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?= $d->order_name;?></td>
										<td><?=  $d->quantity;?> </td>
										<td><?= $d->dealer_name;?></td>
										<td><?=  $d->city;?> </td>
										<td><?=  $d->state;?> </td>
										<td><?=  $d->hub_name;?> </td>
										<td><?=  $d->order_date_entered;?> </td>
										<!--<td><?=  $d->order_status;?> </td>-->
										<td><?php echo anchor('secondary/view_order/'.$result, ' <button class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i> Info</button>', array('class' => '', 'id' => '')); ?></td>
										
									</tr>
                            <?php } ?>
                            </tbody>
                          </table>
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
