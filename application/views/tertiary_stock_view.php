  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Tertiary Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tertiary Stock</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> <b>Tertiary Stock</b> 
		  <span class="pull-right"><a href="<?php echo base_url(); ?>format/tertiary_data_format.xlsx"><button class="btn btn-info btn-sm">Download Excel <i class="fa fa-download"></i> </button></a></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		 
     <span class="pull-right"><a href="<?php echo base_url(); ?>index.php/tertiary/upload_excel"><button class="btn btn-info btn-sm">Upload Excel <i class="fa fa-cloud-upload"></i></button></a></span>
		  </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  			<thead>
                            <tr>
                                <!--th>Select</th-->
								
								<th>Customer Name</th>
								<th>Mobile</th>
								<th>Address</th>								
								<th>Pin Code</th>								
								<th>Product Name</th>      								
								<th>Product Serial No</th>								
								<th>Date of Sale</th>								
								<th>Warranty Start Date</th>
								<th>Warranty End Date</th>								
								<!--<th>Add Product</th>								
								<th>Create Complain</th>  -->                    
                            </tr>
                            </thead>
							<tbody>
							<?php
							  foreach($res as $r)
							  {
							?>
											<tr>
									      <!--td class="center"><input id="clearRadios" name="check_cust" value="" type="checkbox"></td -->
										  <!-- td align="center"><?php // echo $r->cust_id; ?></td -->
										  
										  <td ><?php   echo strtoupper($r->first_name)." ".strtoupper(last_name); ?></td>
										   <td><?php echo strtoupper($r->mobile); ?></a></td>
										   <td><?php echo strtoupper($r->address); ?></td>
										   <td><?php echo strtoupper($r->pin_code); ?></td>
										   <td><?php echo strtoupper($r->model_description); ?></td>
										   <td><?php echo strtoupper($r->productSerialNo); ?></td>
										   <td><?php echo strtoupper($r->date_of_sale); ?></td>
										   <td><?php echo strtoupper($r->warranty_start_date); ?></td>
										   <td><?php echo strtoupper($r->warranty_end_date); ?></td>
										 
										 
										 
										
										
									</tr>
						<?php
							    }
								
						     ?>
                           
                            </tbody>
			      </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 