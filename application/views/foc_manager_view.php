  <style>
  .modal-header.sss {
    background: #16829a;
    color: #fff;
}
  </style>
  <?php 
  // echo "<pre>"; print_r($state); die; 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Support Tickets</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Support Tickets</li>
      </ol>
      <!-- Example DataTables Card-->
	  <!-- update message-->
	  <?php 
	  if($this->session->userdata('feedback')){
		  ?>
		  <div class="alert alert-dismissible alert-success">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong><?php echo $this->session->userdata('feedback'); ?></strong>
		</div>
		  <?php
	  }
	  ?>
	  <!-- end update message-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> FOC Manager List
		  <span class="pull-right">
		     <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Create a FOS</button>
		  </span>	
		  
		  
		  
		  <div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
		
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header sss">
			  <h4 class="modal-title"><i class="fa fa-life-user" aria-hidden="true"></i> FOC Manager</h4>
			</div>
			<div class="modal-body">
			 <form class="well" action="<?php echo base_url(); ?>index.php/order_punch/add_fos" method="post">
			                        <div class="form-group">
									<div class="row">
									     <div class="col-md-12">
											 <label>Select Department of FOS :</label>
												<select class="form-control"  name="dept" value="<?php echo "FOS"; ?>" >
												   <option value="sales"> Sales </option>
												   <!--<option value="service"> Services </option>--> 
												</select>
                                          </div>
										 
									</div>	
									
			<?php   
			      $date = date(ymdhis);
				 $login_code =  "FOS".$date;
			?>						
									<input type="hidden" name="user" value="<?php echo $user; ?>">
                                     <div class="form-group">
									  <div class="row">
									     <div class="col-md-6">
											 <label>FOS Code :</label>
												<input class="form-control" type="text" name="fosc" readonly value="<?php echo $login_code; ?>"  >
                                          </div>
										  <div class="col-md-6">
												<label>Login Id :</label>
												<input class="form-control" type="text" name="loginid" readonly value="<?php echo $login_code; ?>"  >	
   										  </div>
										 </div>
										</div>	
										 <div class="form-group">
									  
										</div>	
										 <div class="form-group">
									  <div class="row">
									     <div class="col-md-6">
											 <label>FOS Name :</label>
												<input class="form-control" name="fosn" type="text" placeholder="FOS Name" required>
                                          </div>
										  <div class="col-md-6">
												<label>Manager:</label>
												<!--
												<select class="form-control" name="manager">
													   <option value="">Select Manager</option>
													   <option value="Saurya Enterprise">Saurya Enterprise</option>
													   <option value="Shukla Enterprise">Shukla Enterprise</option>
												  </select>-->
												  <input class="form-control" name="manager" type="text" readonly value="<?php echo $distributor_name;?>" />
   										  </div>
										 </div>
										</div>	
									<div class="form-group">
									  <div class="row">
									     <div class="col-md-6">
											 <label>Designation :</label>
											 <!--
												<select class="form-control" name="designation">
												   <option value="">Select Designation</option>
												   <option value="Sales Person">Sales Person</option>
												   <option value="Dealer">Dealer</option>
												   </select>-->
												    <input type="text" class="form-control" name="designation" required id="designation"  placeholder="Designation"/>
                                          </div>
										  <div class="col-md-6">
											 <label>Email :</label>
												<input class="form-control" name="email" type="email" placeholder="Email" required>
                                          </div>
										 </div>
										</div>	
									<div class="form-group">
									  <div class="row">
									     <div class="col-md-6">
											 <label>Country :</label>
											 <!--
												<select class="form-control" name="country">
												   <option value="">Select Country</option>
												   <option value="India">India</option>
												   </select>-->
												   <input type="text" class="form-control" name="country" id="country"  readonly value="India"/>
                                          </div>
										  <div class="col-md-6">
												<label>State:</label>
												
												<select class="form-control" required name="state">
												<option value="">Select State</option>
												<?php 
												   foreach($state as $s)
												   {
												?>
												   <option value="<?php echo $s->state_c; ?>"><?php echo $s->state_c; ?></option>
												 
												<?php
												   }
												?>
												   
												   </select>
												  
   										  </div>
										 </div>
										</div>	
									<div class="form-group">
									  <div class="row">
									     <div class="col-md-6">
											 <label>City :</label>
											<select class="form-control" required name="city">
												<option value="">Select City</option>
												<?php 
												   foreach($city as $s)
												   {
												?>
												   <option value="<?php echo $s->city_c; ?>"><?php echo $s->city_c; ?></option>
												 
												<?php
												   }
												?>
												  <option value="Other">Other</option> 
											</select>											

											<!--
												<select class="form-control" name="city">
												   <option value="">Select City</option>
												   <option value="Delhi">Delhi</option>
												   <option value="Jaipur">Jaipur</option>
												   <option value="Lucknow">Lucknow</option>
												   </select>-->
									  </div>
										  <div class="col-md-6">
												<label>Mobile No:</label>
												<input class="form-control" name="mobile" type="number" placeholder="Mobile No" required>	
   										  </div>
										 </div>
										</div>	
									<div class="form-group">
									  <div class="row">
									     <div class="col-md-12">
											 <label>Address :</label>
												<textarea row=2 class="form-control" type="text" name="address" placeholder="Address" ></textarea>
                                          </div>
										  
										 </div>
										</div>
									<div class="form-group">
									<div class="row">
										<div class="col-md-12">
													<label>Remarks:</label>
													<textarea class="form-control" row=2 name="remarks" type="text" placeholder="Put your remarks here" >	</textarea>
										</div>
										</div>
									</div>
									
									<div class="form-group">
									 <label>Status :</label>
										<select class="form-control" required name="status">
												   <option value="">Select Status </option>
												   <option value="0">InActive</option>
												   <option value="1">Active</option>
										</select>		   
									</div>	
                                       
										<div align="center" class="form-group pull-center">
										    <button type="submit" class="btn btn-info" >Submit</button>
										</div>
                                       
                                    </form>
			</div>
			<div class="modal-footer">
			  
			  <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
			</div>
		  </div>
		  
		</div>
	  </div>
	  
	</div>
		  
		  
		  
		  
		  </div>
		  
        <div class="card-body">
          <div class="table-responsive">
             <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  			  <thead>
                            <tr>
                                <th>Dept.</th>
								<th>FOS Name</th>
								<!--<th>FOS LoginId</th>-->
								<th>Manager</th>
								<th>Designation</th>
								<!-- th>Zone</th -->
								<th>City</th>
								<th>Mobile</th>
								<th>Email</th>
								<th>Actions</th>
								                            
                                							
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($fos as $d) {  
							if($d->status == 0){
								$status_img = '<button class="btn btn-danger btn-sm">InActive</button>';
							}else{
								$status_img = '<button class="btn btn-success btn-sm">Active</button>';
							}
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<?php // echo $d->fosId;?>
										<td><?php if($d->dept == "sales"){ echo '<button class="btn btn-warning btn-sm">Sales</button>'; } else{ echo '<button class="btn btn-primary btn-sm">Service</button>'; }  ?> </td>
										<td><?=  $d->fosName;?> </td>
										<!--<td><?//=  $d->loginId	;?> </td>-->
										<td><?=  $d->manager;?> </td>
										<td><?=  $d->designation;?> </td>
										<!-- td><?php // echo $d->zone;?> </td -->
										<td><?=  $d->city;?> </td>
										<td><?=  $d->mobile;?> </td>
										<td><?=  $d->email;?> </td>
										<!--
										<td><?php // if($d->status == 0){ echo '<button class="btn btn-danger btn-sm">InActive</button>'; } else{ echo '<button class="btn btn-success btn-sm">Active</button>'; }?> </td>-->
										<td><a  href="#" onclick="ChangeStatus(<?php echo $d->fosId; ?>,<?php echo $d->status; ?>)" ><?php echo $status_img; ?></a>
										
										&nbsp;<a href="<?php echo base_url()?>index.php/order_punch/index/<?php echo base64_encode($d->fosId);?>" ><button class="btn btn-info btn-sm">Edit</button></a>
										</td>
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
		<?php
		if(count($editFos) > 0){
			//foreach(){
			?>
			<div class="card mb-3">
				<div class="card-header">
				    <i class="fa fa-table"></i> Edit FOC Manager List <br /><br />
					<form class="well" action="<?php echo base_url(); ?>index.php/order_punch/updateFoc/<?php echo base64_encode($editFos[0]->fosId); ?>" method="post">
						<div class="form-group">
							<div class="row">
								<div class="col-md-3">
									<label>FOS Code :</label>
									<input class="form-control" type="text" name="fosc" readonly value="<?php echo $editFos[0]->fosCode; ?>"  >
								</div>
								<div class="col-md-3">
									<label>Login Id :</label>
									<input class="form-control" type="text" name="loginid" readonly value="<?php echo $editFos[0]->loginId; ?>"  >	
								</div>
								<div class="col-md-3">
									<label>FOS Name :</label>
									<input class="form-control" name="fosn" type="text" placeholder="FOS Name" required value="<?php echo $editFos[0]->fosName; ?>">
                                </div>
								<div class="col-md-3">
									<label>Manager :</label>
									<input class="form-control" name="manager" type="text" readonly value="<?php echo $editFos[0]->manager;?>" />
                                </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3">
									<label>Designation :</label>
									<input type="text" class="form-control" name="designation" required id="designation"  placeholder="Designation" value="<?php echo $editFos[0]->designation;?>"/>
								</div>
								<div class="col-md-3">
									<label>Email :</label>
									<input class="form-control" name="email" type="email" placeholder="Email" required value="<?php echo $editFos[0]->email;?>">
								</div>
								<div class="col-md-3">
									<label>Country :</label>
									<input type="text" class="form-control" name="country" id="country"  readonly value="India"/>
                                </div>
								<div class="col-md-3">
									<label>State :</label>
									<select class="form-control" required name="state">
										<option value="">Select State</option>
										<?php 
											foreach($state as $s){
												?>
												<option value="<?php echo $s->state_c; ?>"><?php echo $s->state_c; ?></option> 
												<?php
											}
										?>
									</select>
                                </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3">
									<label>City :</label>
									<select class="form-control" required name="city">
										<option value="">Select City</option>
											<?php 
												foreach($city as $s){
													?>
												   <option value="<?php echo $s->city_c; ?>"><?php echo $s->city_c; ?></option>
													<?php
												}
											?>
												  <option value="Other">Other</option> 
									</select>
								</div>
								<div class="col-md-3">
									<label>Mobile No :</label>
									<input class="form-control" name="mobile" type="number" placeholder="Mobile No" required value="<?php echo $editFos[0]->mobile;?>">	
								</div>
								<div class="col-md-6">
									<label>Address :</label>
									<textarea row=1 class="form-control" type="text" name="address" placeholder="Address" ><?php echo $editFos[0]->address;?></textarea>
                                </div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<label>Remarks :</label>
									<textarea class="form-control" row=2 name="remarks" type="text" placeholder="Put your remarks here" ><?php echo $editFos[0]->remarks;?></textarea>
                                </div>
							</div>
						</div>
						<div align="center" class="form-group pull-center">
							<button type="submit" class="btn btn-info" >Submit</button>
						</div>
					</form>
				</div>
			</div>
			<?php
			//}
		}
		?>
       
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
   
    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
<script type="text/javascript">
	function ChangeStatus(fosid,fossts){
		$.ajax({
			type:"POST",
			cache:false,
			url: "<?php echo base_url(); ?>index.php/order_punch/foschangestatus",
			data:{fosid : fosid, fossts : fossts}, 
			success: function (html) {
			location.reload();
			}
		});
	}
	
</script>