  <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <script>
function myFunction() {
    var x = document.getElementById("cnf_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction1() {
    var x = document.getElementById("old_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction2() {
    var x = document.getElementById("new_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}







function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('new_pass');
    var pass2 = document.getElementById('cnf_pass');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
   <title>Services | Change Password</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>

<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">

  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>index.php/service_cases/verify_serial"><button class="btn btn-info btn-sm"><i class="fa fa-angle-left "></i> Go Back</button></a>
        </li>
        <li class="breadcrumb-item active">Change Your Password</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
       <div class="card-header">
          <i class="fa fa-dashboard"></i> Change Your Password
		
		  </div>
        
		
		
		
		<form id="form_manual" name="name" action="<?php echo base_url(); ?>index.php/dashboard/change_pass_process" method="post">
		<br/>
		 <div class="row">
		 <div class="col-md-1"> 
		 
		 
		 </div>
				<div class="col-md-4">
					<label><b>Old Password : </b></label>
					<input type="password" class="form-control" name="old_pass" id="old_pass" placeholder="Search Serial Number" required>
					<input type="checkbox" onclick="myFunction1()"> &nbsp;&nbsp;Show Old Password<br/><br/>
					<label><b>New Password : </b></label>
					<input type="password" class="form-control" name="new_pass" id="new_pass" placeholder="Search Serial Number" required>
					<input type="checkbox" onclick="myFunction2()">&nbsp;&nbsp;Show New Password<br/><br/>
						<label><b>Confirm Password :</b> </label>
					<input type="password" class="form-control" name="cnf_pass" id="cnf_pass" onkeyup="checkPass(); return false;" placeholder="Search Serial Number" required> 
					<input type="checkbox" onclick="myFunction()">&nbsp;&nbsp;Show Confirm Password<br/>
					<span id="confirmMessage" class="confirmMessage"></span>
					<br/>
					
					<button type="submit" name="singlebutton" value="submit" class="btn btn-success btn-sm">Change Passwword</button>
				</div>
				
				 <div class="col-md-4 block">
					    <div class="form-group">
                    	  <div class="col-sm-12 col-md-12 col-lg-12 col-xs-10 mobilePad"  data-toggle="collapse" data-target="#passPolicy" style="font-weight: bold;font-size: 10pt;padding-left: 0px;color: black;cursor: pointer;text-decoration: underline;">Check Password Policy<span class="caret"></span>
                    	  </div>  
                    	 </div>
             <div class="form-group" style="margin-bottom: 0px;!important">
                    	  <div id="passPolicy" class="col-sm-12 col-md-12 col-lg-12 col-xs-12 collapse mobilePad" style="padding-right: 17px;">
                       <ul type="disc" style="padding-left: 0px;">
                    	  <li>Your Password must have minimum 6 characters.</li>
                    	   <li>Your Password must contain at least one number, one uppercase, lowercase & special character.</li>
                    	  <li>Your Password must not contain your Username.</li>
                    	  <li>Your Password must not contain Character or Number repetition.</li>
                    
                    	  </ul> 
                    	  </div>
                    	</div>
						<b>Please Click on Checkboxes to SHOW/HIDE password.</b>
				</div>
		 </div>
		</form>	
		
		</div>
		</div>
		</div>
	
	

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 
