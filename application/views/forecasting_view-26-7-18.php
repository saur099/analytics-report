  <style>
  label {
    font-weight: 700;
}
input.add-row {
    color: #111;
    background-color: #4e6cffd6;
	border : 1px solid #4e6cffd6;
    padding: 8px;
    color: #fff;
	    border-radius: 7px;
    cursor: pointer;
	    box-shadow: 8px 3px 6px #888;
}
span.green{
	background:green;
	padding:7px;
	color:#fff;
}

  </style>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/daterangepicker.css" />
      <script type="text/javascript" src="<?php echo base_url(); ?>js/datepickerjquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/daterangepicker.js"></script>
  <?php 
  //print_r($dealer); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Forecast Reports</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">


  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
       	<h5>Forecasting Management :</h5>
		</ol>
	  <div class="col-md-12">
	  
	  <form id="form_val">
	<div class="row">
    <!-- /.col-lg-12 -->
        <div class="col-md-3">	
			<label>Product Type</label>
			 <select  class="form-control" name="product_type" id="product_type" onchange="">
			 <option value="">Select Product Type</option>
			 <?php 
				 foreach($product_type as $v){
					 ?>
					<option value="<?php echo $v->id; ?>"><?php echo $v->name; ?></option>
					<?php
				 }
			 ?>
			</select>
		 </div>
	    <div class="col-md-2">
		<label>SKU Code</label>
	        <select  class="form-control" id="sku_code" name="sku_code"  onchange="">
				<!--<option value="LWTDLR27366">LWTDLR27366		</option><option value="LWTDLR39141">LWTDLR39141</option>-->
			</select>
		</div>
		<div class="col-md-3">	
		<label>SKU Name</label>
         <select  class="form-control" name="sku_name" id="sku_name" onchange=""><option value="COOL SYSTEMS">COOL SYSTEMS</option><option value="SINGHAL SANITARY">SINGHAL SANITARY</option></select></div>
		 <div class="col-md-2">	
		 <label>Target Qty.</label>
         <input type="text" name="target" id="target" class="form-control" readonly value="<?php echo "50";?>" id="target"/></div>
		 <div class="col-md-2">	
		 <label>Forecast Qty.</label>
         <input type="number" name="forecast" id="forecast" class="form-control" value="<?php echo "00";?>" id="target"/></div>
		 <div class="col-md-2">	
		 <label>Actual Qty.</label>
         <input class="form-control" type="text" id="actual" name="actual" value="<?php echo "100";?>" readonly id="remark" placeholder="Remarks"	></div>
		<div class="col-md-1"> 
    	 <br/>
    	 <input  type="button" class="add-row" id="button" value="Submit"></div>
	</div>
	</form>
</div>

 </div>
	  
	  
	  <hr>
	  
	  <div class="col-md-12">
	  <div id="result">
	 

	  </div>
	  </div>
	  
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
		 <!--b> Date from :</b> <input type="date" class="daterange" />
		  <b>Date to :</b><input type="date" class="daterange" / -->
		  <div class="row">
		  <div class="col-md-6">
		  <!--div class="row">
		  <div class="col-sm-4">
		  <b> Select Date Range :</b></div> 
		   <div class="col-sm-8">
		   <input type="text" class="form-control daterange" /></div>
		   </div>
          </div>
			<script type="text/javascript">
				$('.daterange').daterangepicker();
			</script>
			<div class="col-md-1">
				<button class="btn btn-primary">Search</button>
		   </div-->
		   </div>
<?php 
		   $date = date('m');
		   $en = base64_encode($date);
?>
		   <div class="col-md-2"></div>
		   	<div class="col-md-3 pull-right">
			<a href="<?php echo base_url(); ?>index.php/forecasting/forecast_report">
			   <button class="btn btn-primary">Last 3 Months Report <i class="fa fa-eye"></i></button > </a>
			</div>
		 </div>
		 </div>
		  <hr>
		  <div id="contact_form">
		  <div id=""></div>
		  </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  	<thead>
                            <tr>
								
								<th>SKU Code</th>
								<th>SKU Name</th>
								<th>Forecast Month</th>  
								<th>Target Qty.</th>  
								<th>Forecast Qty.</th>								
                                <th>Actual Qty.</th>                                
                                <th>Avrage Forcast</th>                                
							</tr>
					</thead>
					<tbody>
					<?php  
                   // echo "<pre>"; print_r($for); die;
				   
					foreach($for as $d) { ?>
							<tr class="odd gradeX">
								
								
								<td><?=  $d->f_sku_code; ?>  </td>
								<td><?=  $d->f_sku_name; ?></td>
								<td><?php 
								$var = $d->forecast_month;
									$arr = explode("-",$var);
									$mon = $arr[0];
									$year  = $arr[1]; 
								    switch($mon){
										case 1: printf("January"); break;
										case 2: printf("February"); break;
										case 3: printf("March"); break;
										case 4: printf("April"); break;
										case 5: printf("May"); break;
										case 6: printf("June"); break;
										case 7: printf("July"); break;
										case 8: printf("August"); break;
										case 9: printf("September"); break;
										case 10: printf("October"); break;
										case 11: printf("November"); break;
										case 12: printf("December"); break;
										default: printf("Month"); break;
									} 
									echo ", ".$year;
								?></td>
								<td align="center"><?=  $d->f_target_qty; ?></td>
								<td align="center"><span class="green"><?=  $d->forecast_qty; ?></span></td>
								<td align="center"><?=  $d->actual_qty; ?></td>
								<td align="center">100</td>
								
								
							   
							</tr>
					<?php }  ?>
					
					</tbody>
			      </table>
          </div>
        </div>
      </div>
   
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
	?>
   
	
  </div>
</body>

</html>

<script>
  /*  $(document).ready(function()
   {
        $(".add-row").click(function(){
			//alert("Hello");
            var serial_no = $("#serial_no").val();
            var model = $("#model").val();
            var prod = $("#prod").val();
            var returntype = $("#returntype").val();
            var remark = $("#remark").val();
            var markup = "<tr><td><input type='checkbox' name='record'></td><td>" + serial_no + "</td><td id='abc'>" + model + "</td><td>" + prod +"</td><td>" + returntype +"</td><td>" + remark +"</td></tr>";
           // alert(markup); 
			$("table tbody").append(markup);
        });
        
        // Find and remove selected table rows
        $(".delete-row").click(function(){
            $("table tbody").find('input[name="record"]').each(function(){
            	if($(this).is(":checked")){
                    $(this).parents("tr").remove();
                }
            });
        });
    });     */
</script>

<script>
$(document).ready(function() {
	
	
	
    $('#button').click(function(e) {
        data = $('#form_val').serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>index.php/forecasting/add_forecast_report',
            type: 'POST',
            data: data,
            success: function(response) { 
                if(response=='Error') { 
                    $('#error_message').text('Error ! Not found here anymore..');
                }else if(response=='Duplicate') { 
                    alert('Sorry!!! Forecast for next month has already done.');
                }else {
					window.alert('Forecast Report has successfully submitted. ')
					window.location.href='forecasting';
					//alert("Successfully added Return Stock.. Refresh the Page and check Status.. ");
                    $('.message').html(response);
                    $('#contact_form').fadeOut('400');
                    $('#info_line').fadeIn('400').text('Takk for din henvendelse');
                }; 
            }
        });     
        
    });
});

/* 
$('#result').html("<br />$('form').serialize():<br />"+ $('form').serialize()+"<br /><br />$('form').serializeArray():<br />" + JSON.stringify($('form').serializeArray())); */
</script>
