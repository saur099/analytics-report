<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('application/libraries/vendor/autoload.php'); 
use Dompdf\Dompdf;
class Secondary extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }

	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$distributor_code = $get_session_data['user_uuid'];
		$this->load->model('orders');
		//$distributor_code = $this->session->userdata['logged_in']['username'];
		$data['data'] = $this->orders->orderTableData($distributor_code);
		//array('data' => $data);
		 $this->load->view('secondary_view', $data);

	}
	public function view_order($id)
	{  //echo $id; exit;
	/* //$this->load->helper('url');
		$this->load->model('orders');
		$this->load->model('stocks');
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$data['distributor_code'] = $this->session->userdata['logged_in']['username'];
		$data['order_details'] = $this->orders->getorderbyid($id,$distributor_code);
		$data['dispatch_details'] = $this->orders->get_all_product_dispatch_details($id,$distributor_code);
		$products = $data['order_details'][0]->product_name;
		
		//$data['stock_details'] = $this->stocks->get_my_stock_details($distributor_code,$products);
		$data['scheme_details'] = $this->orders->check_scheme_appicable($data['order_details'][0]->quan,$data['order_details'][0]->order_date_entered);
		//echo "<pre>"; print_r($data);
		 $this->load->view('orderdetails_view', array('data' => $data));
	*/
		 $this->load->model('orders');
		$this->load->model('stocks');
		$distributor_code = $this->session->userdata['logged_in']['user_uuid'];
		$data['distributor_code'] = $this->session->userdata['logged_in']['user_uuid'];
		$distributor_reference_no = $this->session->userdata['logged_in']['username'];
		$data['order_details'] = $this->orders->getorderbyid($id,$distributor_code,$distributor_reference_no);
		 //echo "<pre>"; print_r($data['order_details']); exit;
		
		$data['dispatch_details'] = $this->orders->get_all_product_dispatch_details($id,$distributor_code);
		$data['rejected_details'] = $this->orders->get_all_product_rejected_details($id,$distributor_code);
		$products = $data['order_details'][0]->product_name;
		
		
		//echo "<pre>"; print_r($data['rejected_details']); exit; 
		
		//$data['stock_details'] = $this->stocks->get_my_stock_details($distributor_code,$products);
		$data['scheme_details'] = $this->orders->check_scheme_appicable($data['order_details'][0]->quan,$data['order_details'][0]->order_date_entered);
		//echo "<pre>"; print_r($data); 
		
		 $this->load->view('orderdetails_view', array('data' => $data));
	}
	
	public function reject_order_can(){ //print_r(); exit;
		$this->load->model('orders');
		$distributor_code = $this->input->post('distributor_code');
		$order_id = $this->input->post('order_id');
		$this->orders->get_rejected_details_cancel($order_id,$distributor_code);
	}
	
	public function dispatch_order_can(){
		$this->load->model('orders');
		$distributor_code = $this->input->post('distributor_code');
		$order_id = $this->input->post('order_id');
		$this->orders->get_dispatch_details_cancel($order_id,$distributor_code);
	}
	
	public function confirm_dispatch($ids){ 
		//$data = $ids;
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$this->load->model('orders');
		$explode_ids = explode("_",$ids);
		//print_r($explode_ids);
		$id = $explode_ids[0];
		$dis_code = $explode_ids[1];
		$avail_stocks1 = $explode_ids[2];
		//print_r($data['avail_stocks1']); die;
		$data = $this->orders->get_dispatch_details($id,$dis_code,$avail_stocks1);
		$serial_num = $this->orders->get_dispatch_serial_num($distributor_code,$id);
		//echo "<pre>"; print_r($data); exit;
		$this->load->view('order_dispatch',array('data' => $data, 'serial_num' => $serial_num)); 
	}
	
	public function confirm_order_dispatch(){ 
	   //echo "asdfajsdfasf"; exit;
	   $distributor_code = $this->session->userdata['logged_in']['username'];
		$data = $this->input->post();
		//print_r($data); exit;
		$order_id = $data['order_id'];
		$dispatch_serial_no = $data['dispatch_serial_no'];
		$dis_code = $data['dis_code'];
		$dispatch = $data['dispatch'];
		$avail_stock = $data['avail_stock'];
		$Quantities = $data['Quantities'];
		$quantity_ordered = $data['quantity_ordered'];
		$update_avail_stock = $avail_stock - $quantity_ordered;
		$this->load->model('orders');
		$this->orders->insert_order_dispatch_details($order_id,$dispatch,$dis_code,$update_avail_stock,$Quantities,$dispatch_serial_no,$distributor_code);
	}
	
	
	public function re_sedule_dispatch(){
		$get_session_data = $this->session->userdata('logged_in');
		$distributor_code = $get_session_data['user_uuid'];
		$distributor_code = $this->session->userdata['logged_in']['username'];
		
		$this->load->model('orders'); 
		$data['res'] = $this->orders->get_dispatch_consumables();
		//echo "<pre>"; print_r($data); die;
		$this->load->view('resedule_dispatch', $data);
	}
	
	
	public function confirm_order($ids)
	{  
	$this->load->helper('sendsms_helper');
	/*$data = $this->input->post();
    $ids = $data['ids'];echo "<br>";
    $avail_stock = $data['avail_stock'];*/
	
		$this->load->model('orders');
		$this->load->model('stocks');
		$num0 = (rand(10,100));
        $num1 = date("ymd");
        //$num2 = (rand(100,1000));
       // $num3 = time();
    $data['order_number'] = "OR".'#'.$num0 . $num1;
	//$this->stocks->update_my_stock($ids);
	//$distributor_code = $this->session->userdata['logged_in']['username'];
	$distributor_code = $this->session->userdata['logged_in']['user_uuid'];
	$data['get_code'] = $this->orders->update_order_id($ids,$data,$distributor_code);
	/* for sms send for order confirmation*/
	//print_r($data['get_code']); die;
	if($data['get_code'] == 2) { 
	$get_mobile_number = $this->orders->get_mobile_number($ids);
	$con =  count($get_mobile_number);
	for($i=0;$i < $con;$i++) { 
	$dealer_mobile = $get_mobile_number[$i]->dealer_mobile;
	$emp_mobile = $get_mobile_number[$i]->emp_mobile;
	$dealer_name = $get_mobile_number[$i]->dealer_name;
	$dealer_code = $get_mobile_number[$i]->dealer_code;
	$order_date_entered = $get_mobile_number[$i]->order_date_entered;
	$distributor_name = $get_mobile_number[$i]->distributor_name;
	$distributor_codes = $get_mobile_number[$i]->distributor_code;
	$order_id = $get_mobile_number[$i]->order_id;
	$product_name = $get_mobile_number[$i]->product_name;
	$quantity_approved = $get_mobile_number[$i]->quantity;
	$emp_name = $get_mobile_number[$i]->emp_name;
	
	// for dealer sms
	
	$send_sms_to_dealer = sendsms('9818540681', "Dear ".$dealer_name." - ".$dealer_code.", Your Secondary Order given on ".$order_date_entered." has been confirmed by ".$distributor_name." - ".$distributor_codes." against Order id ".$order_id.". Total Approved Qty: ".$product_name." : ".$quantity_approved." . Thanks/ Livpure Team" );
	// for employee sms
	$send_sms_to_emp = sendsms('7503343051', "Dear ".$emp_name.",Secondary Order taken for ".$dealer_name." - ".$dealer_code." dated ".$order_date_entered." has been confirmed by ".$distributor_name." - ".$distributor_codes." against Order id is ".$order_id.". Total Approved Qty: ".$product_name." : ".$quantity_approved." . Thanks/ Livpure Team" );
	}
	}
	/* end for sms send for order confirmation*/
	
	/* code to send sms to user and delaer upon product cancellation */
		 
		$get_rejection_sms_details = $this->orders->get_rejection_sms_details($ids,$distributor_code);
		 $con1 = count($get_rejection_sms_details);
		if($con1 > 0) {
	for($i=0;$i < $con1;$i++) { 
	$dealer_mobile = $get_rejection_sms_details[$i]->dealer_mobile;
	$emp_mobile = $get_rejection_sms_details[$i]->emp_mobile;
	$dealer_name = $get_rejection_sms_details[$i]->dealer_name;
	$dealer_code = $get_rejection_sms_details[$i]->dealer_code;
	$order_date_entered = $get_rejection_sms_details[$i]->order_date_entered;
	$distributor_name = $get_rejection_sms_details[$i]->distributor_name;
	$distributor_codes = $get_rejection_sms_details[$i]->distributor_code;
	$order_id = $get_rejection_sms_details[$i]->order_id;
	$product_name = $get_rejection_sms_details[$i]->product_name;
	$quantity_approved = $get_rejection_sms_details[$i]->quantity;
	$emp_name = $get_rejection_sms_details[$i]->emp_name;
	//$rejection_reason = $get_rejection_sms_details[$i]->rejection_reason;
	$rejection_reason = str_replace(array('_', ' '), ' ', $get_rejection_sms_details[$i]->rejection_reason);
	
	// sms to dealer
	$send_sms_to_dealer = sendsms('9654000745', "Dear ".$dealer_name." - ".$dealer_code.", Your Secondary Order given on ".$order_date_entered." has been Rejected by ".$distributor_name." - ".$distributor_codes." against Order id ".$order_id.". ".$rejection_reason." . Thanks/ Livpure Team" );
	// for employee sms
	$send_sms_to_emp = sendsms('7503343051', "Dear ".$emp_name.",Secondary Order taken for ".$dealer_name." - ".$dealer_code." dated ".$order_date_entered." has been rejected by ".$distributor_name." - ".$distributor_codes." against Order id  ".$order_id.". Rejection Reason: ".$product_name." : ".$quantity_approved." : ".$rejection_reason.". Thanks/ Livpure Team" );
	}
	
	}
		
	$this->load->view('confirm_order', array('data' => $data));
	}
	
	public function reject_order($ids)
	{ //echo $ids;
		$this->load->model('orders');
		$explode_ids = explode("_",$ids);
		//print_r($explode_ids);
		$id = $explode_ids[0];
		$dis_code = $explode_ids[1];
		$data = $this->orders->get_reject_details($id,$dis_code);
		$this->load->view('reject_order', array('data' => $data));
	}
	 public function reject_confirm_order($ids)
	 {
		 $this->load->model('orders');
		$data = $this->orders->get_confirm_reject_details($ids);
		
		$this->load->view('reject_confirm_order', array('data' => $data));
	 }
	public function reject_order_details()
	{ 
	    $this->load->helper('sendsms_helper');
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$dis_code = $data['dis_code'];
		$reject = $data['reject'];
		$this->load->model('orders');
		$get_return = $this->orders->insert_order_rejection_details($order_id, $reject,$dis_code);
		
		
		
	}
	
	public function reject_confirm_order_details($ids)
	
	{
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$reject = $data['reject'];
		$this->load->model('orders');
		$this->orders->update_confirm_order_rejection_details($order_id, $reject);
		
	}
	public function loadZoneComboBox()
	{
		$this->load->model('zones');
		$zones = $this->zones->zoneForComboData();
		$options = "";
		foreach($zones->result() as $zone) {
		     $options .= "<option value='".  $zone->zone_id ."'>".   $zone->zone_name . "</option>";
		}
		echo $options;		
	}
	
	
	public function get_all_view()
	{
		$this->load->view('dashboard_view_23aug');
	}
	
	public function punch_order_list(){
		$distributor_namee = $this->session->userdata['logged_in']['username'];
		$distributor_code = $this->session->userdata['logged_in']['user_uuid'];
		$this->load->model('orders');
		
		$data['primary_order'] = $this->orders->get_primary_order_punch();
		//echo "<pre>"; print_r($data); die;
		$this->load->view('punch_list_view', $data);
	}
	
	public function primary_order_details($remark_id){
		$distributor_namee = $this->session->userdata['logged_in']['username'];
		$distributor_code = $this->session->userdata['logged_in']['user_uuid'];
		$this->load->model('orders');
		
		$data['primary_order_detail'] = $this->orders->get_primary_order_details($remark_id);
		//echo "<pre>"; print_r($data['primary_order_detail']); die;
		$this->load->view('confirm_order/primary_orderdetails_view', $data);
	}
	
	public function generates_invoice(){
		$this->load->model('orders');
		$distributor_username = $this->session->userdata['logged_in']['username'];
		$distributor_code = $this->session->userdata['logged_in']['user_uuid']; 
		//$data['orderinvoice_details'] = $this->orders->getinvoice_details($id,$distributor_code,$distributor_username);
		//$data['dispatch_details'] = $this->orders->get_all_product_dispatch_details($id,$distributor_code);
		
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */