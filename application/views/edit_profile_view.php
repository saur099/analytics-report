  <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <script>
function myFunction() {
    var x = document.getElementById("cnf_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction1() {
    var x = document.getElementById("old_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function myFunction2() {
    var x = document.getElementById("new_pass");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}







function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('new_pass');
    var pass2 = document.getElementById('cnf_pass');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
   <title>Services | Change Password</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>

<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">

  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>index.php/service_cases/verify_serial"><button class="btn btn-info btn-sm"><i class="fa fa-angle-left "></i> Go Back</button></a>
        </li>
        <li class="breadcrumb-item active">Change Your Password</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
       <div class="card-header">
          <i class="fa fa-dashboard"></i> Change Your Password
		
		  </div>
        
		
		
		
		<form id="form_manual" name="name" action="<?php echo base_url(); ?>index.php/dashboard/change_pass_process" method="post">
		<br/>
<div class="card bg-light mb-3" style="max-width: 115rem;">	
<div class="card-header">	 
		<div class="container">
		<div class="row">
  <div class="col-md-4">
  
  <div class="card" style="width:300px">
   
    <img class="img-responsive" src="https://pbs.twimg.com/profile_images/954965844550963200/5CjSRRMm_400x400.jpg" style="width:100%;"  alt="Card image" >
	 <div class="card-body">
      <h4 class="card-title">Jane Doe</h4>
      <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer</p>
      <a href="#" class="btn btn-primary">See Profile</a>
    </div>
  </div>
</div>
<div class="col-md-8">
  <div class="card" style="width:600px">
    
    <div class="card-body">
      <h4 class="card-title">Edit Your Profile</h4>
      		<div class="main-login main-center">
				<h5>Sign up once and watch any of our free demos.</h5>
					<form class="" method="post" action="#">
						
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Your Name</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  placeholder="Enter your Username"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>

						
					</form>
				</div>
      <a href="#" class="btn btn-primary">See Profile</a>
    </div>
  </div>
  <br>
</div>
		</div>
		
		</div>
		</div>
		</div>
		</div>
	
	

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 
