  <?php 
		//echo "<pre>"; print_r($res); die;
			$this->load->view('includes/top.php');  
   ?>
   
<style>
span.green{
	background:green;
	padding:7px;
	color:#fff;
}

  </style>   
   <title>Livguard | Forecast Reports</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>index.php/forecasting">Forecasting Management</a>
        </li>
        <li class="breadcrumb-item active">Forecast Reports</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Forecast Report List</div>
        <div class="card-body">
		
				<div class="alert alert-success alert-dismissible fade show" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				  </button>
				  <strong>Success Result!</strong> Here is the list of your last <b>3 Months</b> forecast report.
				</div>
				
          <div class="table-responsive">
             <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  			  <thead>
                            <tr>
                                <th>SKU Code</th>
								<th>SKU Name</th>
								<th>Forecast Month</th>
								<th>Target Qty.</th>
								<th>Forecast Qty. </th>
								<th>Actual Qty.(Secondary) </th>                 
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($res as $d) {  
							
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?=  $d->f_sku_code; ?>  </td>
								<td><?=  $d->f_sku_name; ?></td>
								<td><?php 
								$var = $d->forecast_month;
									$arr = explode("-",$var);
									$mon = $arr[0];
									$year  = $arr[1]; 
								    switch($mon){
										case 1: printf("January"); break;
										case 2: printf("February"); break;
										case 3: printf("March"); break;
										case 4: printf("April"); break;
										case 5: printf("May"); break;
										case 6: printf("June"); break;
										case 7: printf("July"); break;
										case 8: printf("August"); break;
										case 9: printf("September"); break;
										case 10: printf("October"); break;
										case 11: printf("November"); break;
										case 12: printf("December"); break;
										default: printf("Month"); break;
									} 
									echo ", ".$year;
								?></td>
								<td><?=  $d->f_target_qty; ?></td>
								<td align="center"><span class="green"><?=  $d->forecast_qty; ?></span></td>
								<td><?=  $d->actual_qty; ?></td>
										
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
          </div>
        </div>
			
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>
   
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
