<?php
//echo 'ashish'; exit;
  //print_r($data); die("hello");
  //echo $get_session_data['username'];
  //echo "<pre>"; print_r($dist); die;
//$cid = $rs['consid'];
//echo "Hii"; die;
$this->load->view('includes/top.php');  
   ?>
   <script src="<?php echo base_url(); ?>js/ajaxjquery.min.js"></script>
   <title>Services | Call Management</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>

   
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>index.php/order_punch/dash"> <button class="btn btn-infos"><< Go Back to Order Punch Dashboard</button></a>
        </li>
        <li class="breadcrumb-item active">Order Punching</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-plus"></i> Create Order Punching for <b>Dealer to Distributor </b></div>
        <div class="card-body text-info">
		   <span class="reqd">NOTE : </span> <i>After punching models quantity, don't forget to submit the form to complete punching orders..</i><hr>
			
			<form action="<?php echo base_url(); ?>index.php/order_punch/add_order_punch" id="preview_form" method="post">
					<div class="row">
					    <div class="col-md-6">
							<b>Distributor Name : <span class="reqd">*</span>	  </b>
							<?php
							     $get_session_data = $this->session->userdata('logged_in');
								$user_uuid = $get_session_data['user_uuid'];
								$user = $get_session_data['username'];
							?>
							<input type="hidden" readonly class="form-control" name="dist_uuid"  value="<?php echo $user_uuid; ?>">
							<input type="text" readonly class="form-control" name="dist_name"  value="<?php echo $user; ?>">
						</div>
					    <div class="col-md-6">		<b>	Dealer Name : <span class="reqd">*</span>	 </b>
                               <select name="dealer" class="form-control" id="dealer">
							     <option value="">Select the Dealer </option>
								 <?php
								 foreach($dist as $d)
								 {
								 //echo "<pre>"; print_r($d);
								 ?>
								 <option value="<?php echo $d->accounts_sar_dealer_1sar_dealer_idb;?>"><?php echo $d->name;?></option>
								 <?php
								 }
								 ?>
							   </select>
					</div>
						
					</div>
					<br/>
					<div class="row">
					    <div class="col-md-6">
							<b>Call Type : <span class="reqd">*</span>	  </b>
							<select class="form-control" name="callType" required>
								 <option value="">Select vistor call type</option>				
								 <option value="1">Productive</option>				
								 <option value="2">Non- Productive</option>
							</select>					 
						</div>
						<div class="col-md-6">
							<b>Mode of Order:	</b>
							<select class="form-control" name="mode" id="brand" onchange="getBrandChangeProdAttribs()" required>
								<option value="">Select Mode of Order</option>
								<option value="6"> On Call </option>
								<option value="1"> On Visit </option>
								<option value="2"> Others </option>
							</select>
						 </div>	
					</div>
			<br/>

			<div class="row">	
					<div class="col-md-4">			<b>Payment Collection : <span class="reqd">*</span>	</b> <input class="form-control" type="text" name="paycollect" ></div>	
					<div class="col-md-4">			<b>Pay Amount(CASH/CHEQUE/RTGS) : <span class="reqd">*</span>	</b>
						
					
					
					<select id='purpose' name="payoption" class="form-control">
						<option value="">Select Payment Option</option>
						<option value="1">Cash</option>
						<option value="2">Cheque</option>
						<option value="3">RTGS(Internet Transaction)</option>
					</select>
				</div>	
					<div class="col-md-4" id="business" style="display:none;">	
						<b>CASH (in INR) : <span class="reqd">*</span>	  </b>
							<input type='text' class="form-control" class='text' name='cash' placeholder="Please enter your amount in Rupees" value size='10' />
					</div>
					<div class="col-md-4" id="cheque" style="display:none;">		
						<b> Cheque (Cheque No.) <span class="reqd">*</span>	  </b>
							<input type='text' class="form-control" class='text' name='cash' placeholder="Please enter your cheque number" value size='16' />
					</div>
					<div class="col-md-4" id="rtgs" style="display:none;">	
							<b>RTGS (Txn No.)<span class="reqd">*</span>	  </b>
							<input type='text' class="form-control" class='text' placeholder="Enter your transaction number" name='cash' value size='10' />
					</div>
			</div>
	
			<br/>
			
			<div class="row">		
				<div class="col-md-12">	<b>Product Category: </b>
					<select id='productType' name="productCategory" onchange="get_model(this.value);" class="form-control">
						<option value="">Select Product Category</option>
						<?php  
						  foreach($res as $r)
						  {
						?>
						<option value="<?php echo $r->PRODUCT; ?>"><?php echo $r->PRODUCT; ?></option>
						<?php
						  }
						?>
					</select>
				</div>
			</div>	
			<br/>
				<div class="col-md-12" id="detailspro">		
				   
			    </div>
			<br/>

			<div class="row">
				<div class="col-md-6">		<b>Total Order Quantity :	</b> 
						<input class="form-control" type="text" readonly id="ordrQty"  name="ordrQty" >
				</div>
				<div class="col-md-6">		<b>Put your Remarks :	</b> 
						<textarea class="form-control" type="text" id="remark" placeholder="Put some remarks here" row="2" name="remark" >	</textarea>
				</div>
					</div>
			
			<br/>

			<div class="row">		
				
			</div>
			
			<br/>		

			
			   
			   <br/>		
               <br/>
			   
			<center>
				<!--a target="_blank_" href="<?php // echo base_url(); ?>index.php/cust_details"-->
				<button class="btn btn-info" id="submit" type="submit"> Create Product </button>
				
			</center>
			
			</tr>
			   
			</form>
				
</div>





		   </table>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
 
	
  </div>
</body>
<div >
						

</html>
<script>
		function get_model(id)
		{
			
		 $.ajax({
						url: '<?php echo base_url(); ?>index.php/order_punch/get_model_master',
						type: 'post',
						data: {'id': id},
								success: function(data, status) {
									alert("Success ! Please punch your model quantity..");
									//alert(data);
									$('#mytable').show();
								if(data) {
								  $('#detailspro').html(data);
								}
						  },
					error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\nError:" + err);
				  }
				}); // end ajax call
		}
		</script>
		<script type="text/javascript">
			 $(document).ready(function(){ 
			 	
				$('#purpose').on('change', function() {
					
				  if ( this.value == '1')
				  {
					$("#business").show();
				  }
				  else
				  {
					$("#business").hide();
				  }
				  if ( this.value == '2')
				  {
					$("#cheque").show();
				  }
				  else
				  {
					$("#cheque").hide();
				  }
				  if ( this.value == '3')
				  {
					$("#rtgs").show();
				  }
				  else
				  {
					$("#rtgs").hide();
				  }
				});
				
				$('#productType').on('change', function() {
					//$("#mytable").hide();
				  if ( this.value == '1')
				  {
					$("#mytable").show();
				  }
				  else
				  {
					$("#mytable").hide();
				  }
				   if ( this.value == '2')
				  {
					$("#mytable1").show();
				  }
				  else
				  {
					$("#mytable1").hide();
				  }
				});
				

				$('.charge').on('keyup', function() {
					var totalQty = 0;
					$(".charge").each(function(){
					 totalQty += parseInt($(this).val());
					});
					$('#letpl_invoice_date').val(totalQty);
				});
				
				//window.alert("" + val1);
				
				

			});
			</script>
					 