  <style>
  .modal-header.sss {
    background: #16829a;
    color: #fff;
}
label{
	font-weight:700;
}
  </style>
 
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Upload Excel</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
	  <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>index.php/tertiary/tertiary_upload"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
        </li>
        <li class="breadcrumb-item">
          <a href="#">Tertiary Stock</a>
        </li>
        <li class="breadcrumb-item active">Upload CSV for Tertiary</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Upload CSV data for Tertiary
		  	</div>
		  	</div>
		  
			<hr>
			<form action="<?php echo base_url(); ?>index.php/tertiary/tertiary_upload_excel" method="post" enctype="multipart/form-data">
			<div class="row">
			
			    <div class="col-md-1"></div>
			     <div class="col-md-4"><label>Upload CSV file here :</label> 
				  <input type="file" name="file" type="file" class="form-control">
				  </div>
				  <div class="col-md-1"><br/>
				  <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit CSV">
				  <!--<button type="submit" class="btn btn-info">Submit CSV </button>-->
				</div>
			</div>
			</form>
		  
		  
		 
	  
	
		  
		  </div>
		  
        <div class="card-body">
          <div class="table-responsive">
             </div>
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  

			$this->load->view('includes/js-holder.php');  
   ?>
   
<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">*</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="image-gallery-title"></h4>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" src="<?php echo base_url(); ?>uploads/<?php echo $d->supportUpload; ?>" height="300">
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
</body>

</html>
