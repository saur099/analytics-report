  <?php 
//echo "<pre>"; print_r($res); die;
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Dealers List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
	   <li class="breadcrumb-item">
			<a href="<?php echo base_url(); ?>index.php/order_punch/dash"><i class="fa fa-chevron-circle-left fa-2x" aria-hidden="true"></i></a>
	    </li>
	   <li class="breadcrumb-item">
          <a href="<?php echo base_url(); ?>index.php/order_punch/dash"> Order Punching Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Distributor to Company Punching Record</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Order Punch Record <b>Distributor to Company</b></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                            <tr>
                                <th>Distributor UUID</th>
								<th>Distributor Name</th>
                                <th>Dealer Name</th>
								<th>Product Category</th>
								<th>Total Order Qty</th>
								<th>Remarks</th>
								<th>Model</th>
								<th>Qty</th>
						    </tr>
                </thead>
				<tbody>
				
							<?php   foreach($res as $d) {  ?>
									<tr class="odd gradeX">
										<td><?= $d->distUUID;?></td>
										<td><?= $d->distName;?></td>
										<td><?php 
											$query = $this->db->query("SELECT a.name FROM sar_dealer AS a LEFT JOIN accounts_sar_dealer_1_c AS b 
											ON b.accounts_sar_dealer_1sar_dealer_idb = a.id	WHERE b.accounts_sar_dealer_1sar_dealer_idb ='".$d->dealerName."' GROUP BY name" );
											$row = $query->result();
											foreach($row as $r)
											{
												echo $deal = $r->name;
											 } 
										
										?> </td>
										<td><?= $d->productCategory;?> </td>
										<td><span style="background:green; color:#fff; padding:5px;"><?= $d->totalOrderQty;?></span> </td>
										<td><?= $d->remarks;?> </td>
										<td><?= $d->modelCode;?> </td>
										<td><?= $d->OrderQty;?> </td>
									</tr>
												<?php  } ?>
                </tbody>
           </table>
          </div>
        </div>
      
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>
