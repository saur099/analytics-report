<?php
//echo 'ashish'; exit;
  //print_r($data); die("hello");
  //echo $get_session_data['username'];
//echo "<pre>"; print_r($rs['consid']); die;
//$cid = $rs['consid'];
//echo "Hii"; die;
			$this->load->view('includes/top.php');  
   ?>
   <script src="<?php echo base_url(); ?>js/ajaxjquery.min.js"></script>
   <title>Services | Call Management</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>

   
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>index.php/order_punch/add_order_punch"> Service Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Order Punching</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-plus"></i> Add Order Details</div>
        <div class="card-body text-info">
		  
			
			<form action="<?php echo base_url(); ?>index.php/order_punch/add_order_punch" id="preview_form" method="post">
					<div class="row">
					    <div class="col-md-6">
							<b>Distributor Name : <span class="reqd">*</span>	  </b>
							<input type="text" readonly class="form-control" name="dist_name"  value="<?php echo $user; ?>">
						</div>
					    <div class="col-md-6">
						   <b>Dealer Name : <span class="reqd">*</span>	  </b>
							<select class="form-control" name="dealer" required>
								 <option value="">Select any dealer</option>				
								 <option value="1">Ashish Pathak</option>				
								 <option value="2">Saurav Sony</option>
							</select>				
						</div>
						
					</div>
					<br/>
					<div class="row">
					    <div class="col-md-6">
							<b>Call Type : <span class="reqd">*</span>	  </b>
							<select class="form-control" name="callType" required>
								 <option value="">Select vistor call type</option>				
								 <option value="1">Productive</option>				
								 <option value="2">Non- Productive</option>
							</select>					 
						</div>
						<div class="col-md-6">
							<b>Mode of Order:	</b>
							<select class="form-control" name="mode" id="brand" onchange="getBrandChangeProdAttribs()" required>
								<option value="">Select Mode of Order</option>
								<option value="6"> On Call </option>
								<option value="1"> On Visit </option>
								<option value="2"> Others </option>
							</select>
						 </div>	
					</div>
			<br/>

			<div class="row">	
					<div class="col-md-4">			<b>Payment Collection : <span class="reqd">*</span>	</b> <input class="form-control" type="text" name="paycollect" ></div>	
					<div class="col-md-4">			<b>Pay Amount(CASH/CHEQUE/RTGS) : <span class="reqd">*</span>	</b>
						
					
					
					<select id='purpose' name="payoption" class="form-control">
						<option value="">Select Payment Option</option>
						<option value="1">Cash</option>
						<option value="2">Cheque</option>
						<option value="3">RTGS(Internet Transaction)</option>
					</select>
				</div>	
				<div class="col-md-4" id="business" style="display:none;">	
						<b>CASH (in INR) : <span class="reqd">*</span>	  </b>
							<input type='text' class="form-control" class='text' name='cash' placeholder="Please enter your amount in Rupees" value size='10' />
						</div>
					<div class="col-md-4" id="cheque" style="display:none;">		
						<b> Cheque (Cheque No.) <span class="reqd">*</span>	  </b>
							<input type='text' class="form-control" class='text' name='cash' placeholder="Please enter your cheque number" value size='10' />
							</div>
							<div class="col-md-4" id="rtgs" style="display:none;">	
							<b>RTGS (Txn No.)<span class="reqd">*</span>	  </b>
							<input type='text' class="form-control" class='text' placeholder="Enter your transaction number" name='cash' value size='10' />
						</div>
						
					
				
			</div>
	
			
			<br/>
			
			<div class="row">		
				<div class="col-md-12">	<b>Product Category: </b>
					<select id='productType' name="productCategory" class="form-control">
						<option value="">Select Product Category</option>
						<option value="1">Inverter Battery</option>
						<option value="2">Livguard battery</option>
					</select>
				</div>
					
			</div>	
			<br/>
				<div class="col-md-12">		
				    <table id="mytable" class="table table-bordered" style="display:none;">
						 <tr>
                          <th> Product Sub Category Name</th>
                          <th> Product Code</th>
                          <th> Serial No.</th>
                          <th> Quantity</th>
                          </tr>
                        <tr>
                            <td width="80%"><input type="text" class="form-control" name="product" readonly value="LIVGUARD X78901"></td>
                            <td><input type="number" name="batteryIdss" class="charge" value="0"></td>
                           
                         </tr>
                    </table>
			    </div>	
				<div class="col-md-12">		
				    <table id="mytable1" class="table table-bordered" style="display:none;">
						<thead>
						 <tr>
                          <th> Product Sub Category Name</th>
                          <th> Quantity</th>
                          </tr>
						  </thead>
						  <tbody>
                        <tr>
                            <td width="80%"><input type="text" class="form-control" name="product" readonly value="LIVGUARD X78901"></td>
                            <td><input type="number" name="batteryId" class="charge" value="0"></td>

                         </tr>
						 <tr>
                            <td width="80%"><input type="text" class="form-control" name="product" readonly value="LGXBDMAX9382482"></td>
                            <td><input type="number"  value="0" name="batteryIds" class="charge"></td>
                           
                         </tr>
						 </tbody>
                    </table>
			    </div>	
			<br/>

			<div class="row">
				<div class="col-md-6">		<b>Total Order Quantity :	</b> 
						<input class="form-control" type="text" id="letpl_invoice_date" readonly name="totalOrder" placeholder="0" >	
				</div>
				<div class="col-md-6">		<b>Remarks :	</b> 
						<textarea class="form-control" type="text" id="remark" placeholder="Put some remarks here" row=2 name="remark" >	</textarea>
				</div>
					</div>
			
			<br/>

			<div class="row">		
				
			</div>
			
			<br/>		

			
			   
			   <br/>		
               <br/>
			   
			<center>
				<!--a target="_blank_" href="<?php // echo base_url(); ?>index.php/cust_details"-->
				<button class="btn btn-info" id="submit" type="submit"> Create Product </button>
				
			</center>
			
			</tr>
			   
			</form>
				
</div>





		   </table>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
 
	
  </div>
</body>
<div >
						

</html>
		<script type="text/javascript">
			 $(document).ready(function(){ 
			 	
				$('#purpose').on('change', function() {
					
				  if ( this.value == '1')
				  {
					$("#business").show();
				  }
				  else
				  {
					$("#business").hide();
				  }
				  if ( this.value == '2')
				  {
					$("#cheque").show();
				  }
				  else
				  {
					$("#cheque").hide();
				  }
				  if ( this.value == '3')
				  {
					$("#rtgs").show();
				  }
				  else
				  {
					$("#rtgs").hide();
				  }
				});
				
				$('#productType').on('change', function() {
					//$("#mytable").hide();
				  if ( this.value == '1') 
				  {
					$("#mytable").show();
				  }
				  else
				  {
					$("#mytable").hide();
				  }
				   if ( this.value == '2')
				  {
					$("#mytable1").show();
				  }
				  else
				  {
					$("#mytable1").hide();
				  }
				});
				

				$('.charge').on('keyup', function() {
					var totalQty = 0;
					$(".charge").each(function(){
					 totalQty += parseInt($(this).val());
					});
					$('#letpl_invoice_date').val(totalQty);
				});
				
				//window.alert("" + val1);
				
				

			});
			</script>
					 