<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  
  <link href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/progress-bar.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/2.8.94/css/materialdesignicons.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet"> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
  <style>
  button.btn.btn-info:hover {
    background: crimson;
    transition: 1s;
    cursor: pointer;
}
span.yellow {
    padding: 8px;
    background: yellow;
    font-weight: 700;
}
div#dataTable_processing {
    background: crimson;
    color: #fff;
    box-shadow: 3px 1px 2px 1px #888;
}
span.green {
    background: green;
    color: #fff;
    padding: 7px;
    box-shadow: 2px 1px 1px 1px #eee;
}
span.red {
    background: #ec2a0b;
    color: #fff;
    padding: 7px;
    box-shadow: 2px 1px 1px 1px #eee;
}
span.progres {
    background: #22a4d6;
    color: #fff;
    padding: 5px;
}
span.crimson {
    background: crimson;
    color: #fff;
    padding: 5px;
}

span.orange {
    background: #bfc111;
    color: #fff;
    padding: 5px;
}

table.dataTable {
    font-size: 12px;
}
.container-fluid {
    font-size: 13px;
}
.text-primary {
   
    font-size: 12px;
}
.form-control:disabled, .form-control[readonly] {
    
    font-size: 12px;
}




span.white_back {
    box-shadow: 2px 1px 3px 4px #888;
    background: inherit;
    font-weight: 500;
}
button.btn.btn-danger:hover {
    background: crimson;
    transition: 1s;
    cursor: pointer;
}
button.btn.btn-warning:hover {
    background: crimson;
    transition: 1s;
    cursor: pointer;
	color:#fff;
}
button.btn.btn-success:hover {
    background: crimson;
    transition: 1s;
    cursor: pointer;
}
button.btn.btn-primary:hover {
    background: crimson;
    transition: 1s;
    cursor: pointer;
}
.btn-info, .btn-warning,.btn-danger, .btn-success, .btn-primary, .btn-default
{
	box-shadow:8px 3px 6px #888;
}

span.reqd {
    color: red;
    font-weight: bolder;
}

span.block_text {
    box-shadow: -3px 4px 2px -1px #89B;
    padding: 3px;
    background: #eee !important;
    color: #000 !important;
}
 
.form-control {
    font-size: 12px !important;
}
 </style>
