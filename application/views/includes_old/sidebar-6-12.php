<style>
#mainNav.navbar-dark .navbar-collapse .navbar-sidenav {
    background: #343a40;
    padding-top: 30px;
}
li.dropdown.sale {
    padding-bottom: 12px;
}
</style>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/dashboard"><img src="<?php echo base_url(); ?>images/cmpny_logos.png" width="166"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/dashboard">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
		

		
<!----------------------------------------------------------------------------------------------------------------->


<!----------------------------------------------------------------------------------------------------------------->		

<!----------------------------------Sales part starts here-------------------------------------------------------------------------->	
 <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sales">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-bar-chart" aria-hidden="true"></i> 
            <span class="nav-link-text">Sales</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li><a href="<?php echo base_url(); ?>index.php/order_punch/punching"> <i class="fa fa-fw fa-edit"></i> Create Order Punching</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/order_punch/punch_list"> <i class="fa fa-fw fa-list"></i> Order Punching List</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/return_orders_dealer"> <i class="fa fa-fw fa-tag"></i> Return Orders</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/forecasting"> <i class="fa fa-fw fa-tag"></i> Forecasting</a></li>
                 <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/secondary">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Order List</span>
          </a>
        </li>
		<li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/branch_order/scheme">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Schemes</span>
          </a>
		  
        </li>
        <li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/confirm">
			 <i class="fa fa-fw fa-check"></i>
            <span class="nav-link-text">Confirm Orders</span>
          </a>
        </li>
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/branch_order/secondary_stock">
			 <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Stock List</span>
          </a>
        </li>
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/branch_order/secondary_stock">
			 <i class="fa fa-fw fa-cog"></i>
            <span class="nav-link-text">Secondary Stock</span>
          </a>
        </li>
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/return_stock">
			 <i class="fa fa-list-alt" aria-hidden="true"></i>
            <span class="nav-link-text">Return Stock</span>
          </a>
        </li>
		
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/my_contact">
			 <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Livguard Contact</span>
          </a>
        </li>
        <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/tertiary">
            <i class="fa fa-th-large" aria-hidden="true"></i>
            <span class="nav-link-text">Tertiary Data</span>
          </a>
        </li>
          </ul>
        </li>
	
		
	
<!----------------------------------Sales part ends here-------------------------------------------------------------------------->		
		
		
		
		
		
		
<!----------------------Services part starts here-------------------------------------------------------------------------->			
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Services">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Services</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
             <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/all_services">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">All Services</span>
          </a>
        </li>
		 <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/service_cases">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">My Cases</span>
          </a>
        </li>
		 <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/service_cases/closed_cases">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Closed Cases</span>
          </a>
        </li>
        <li >
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/battery_billing">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">LETP (Billing Date)</span>
          </a>
        </li>
		<li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/branch_order/scheme">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Spares Consumption</span>
          </a>
		  
        </li>
        
          
		
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/cust_details">
			 <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Customer Details</span>
          </a>
        </li>
        <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/tertiary">
            <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Tertiary Data</span>
          </a>
        </li>
          </ul>
        </li>
		
		
		
		
		
<!----------------------Services part ends here---------------------->		
		
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="FOS Management">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/order_punch">
            <i class="fa fa-check-square" aria-hidden="true"></i>
            <span class="nav-link-text">FOS Management</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Support Ticket Management">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/support">
            <i class="fa fa-support" aria-hidden="true"></i>
            <span class="nav-link-text">Support Tickets</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
       
        <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/Login/logout">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  
  
  