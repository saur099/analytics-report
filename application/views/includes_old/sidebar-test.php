<style>
#mainNav.navbar-dark .navbar-collapse .navbar-sidenav {
    background: #343a40;
    padding-top: 30px;
}
li.dropdown.sale {
    padding-bottom: 12px;
}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>/css/punching-sss.css">
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/dashboard"><img src="<?php echo base_url(); ?>images/cmpny_logos.png" width="166"></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/dashboard">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
		

		
<!----------------------------------------------------------------------------------------------------------------->


<!----------------------------------------------------------------------------------------------------------------->		

<!----------------------------------Sales part starts here-------------------------------------------------------------------------->	
 <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Sales">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-bar-chart" aria-hidden="true"></i> 
            <span class="nav-link-text">Sales</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li><a href="<?php echo base_url(); ?>index.php/order_punch/dash"> <i class="fa fa-fw fa-list"></i> Order Punching</a></li>
          
                 <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/secondary">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Order Processing</span>
          </a>
        </li>
        <li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/confirm">
			 <i class="fa fa-fw fa-check"></i>
            <span class="nav-link-text">Confirm Orders</span>
          </a>
        </li>
		<li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/branch_order/scheme">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Schemes</span>
          </a>
		  
        </li>
          <li><a href="<?php echo base_url(); ?>index.php/return_orders_dealer"> <i class="fa fa-fw fa-tag"></i> Return Orders</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/forecasting"> <i class="fa fa-fw fa-tag"></i> Forecasting</a></li>
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/branch_order/secondary_stock">
			 <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Stock List</span>
          </a>
        </li>
		
		<!--li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/return_stock">
			 <i class="fa fa-list-alt" aria-hidden="true"></i>
            <span class="nav-link-text">Return Stock</span>
          </a>
        </li -->
		
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/my_contact">
			 <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Livguard Contact</span>
          </a>
        </li>
       
          </ul>
        </li>
	
		
	
<!----------------------------------Sales part ends here-------------------------------------------------------------------------->		
		
		
		
		
		
		
<!----------------------Services part starts here-------------------------------------------------------------------------->			
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Services">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Services</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
             <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/all_services">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Service Dashboard</span>
          </a>
        </li>
		 <li class="nav-item" data-toggle="tooltip" data-placement="right" title="My Complaint">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse"  data-parent="#exampleAccordion"  href="<?php echo base_url(); ?>index.php/service_cases">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">My Complaint</span>
          </a>
		  <ul class="sidenav-second-level collapse" id="collapseExamplePages">
				 <li>
				  <a class="nav-link" href="<?php echo base_url(); ?>index.php/all_services">
					<i class="fa fa-fw fa-area-chart"></i>
					<span class="nav-link-text">TAT-1</span>
				  </a>
				</li>
				 <li>
				  <a class="nav-link" href="<?php echo base_url(); ?>index.php/all_services">
					<i class="fa fa-fw fa-area-chart"></i>
					<span class="nav-link-text">TAT-2</span>
				  </a>
				</li>
				 <li>
				  <a class="nav-link" href="<?php echo base_url(); ?>index.php/all_services">
					<i class="fa fa-fw fa-area-chart"></i>
					<span class="nav-link-text">Service Dashboard</span>
				  </a>
				</li>
			 </ul>	
        </li>
		<li>
			<a class="nav-link" href="<?php echo base_url(); ?>index.php/cust_details">
			 <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Customer Details</span>
          </a>
        </li>
		 <li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/service_cases/verify_serial">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Verify Product</span>
          </a>
        </li>
        <!--li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/battery_billing">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">LETP (Billing Date)</span>
          </a>
        </li-->
		<li>
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/fault_parts/view_challan">
             <i class="fa fa-check-circle" aria-hidden="true"></i>
            <span class="nav-link-text">View Challan</span>
          </a>
		  
        </li>
        
          
		
		
       
          </ul>
        </li>
		
		
		
		
		
<!----------------------Services part ends here---------------------->		
		
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="FOS Management">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/order_punch">
            <i class="fa fa-check-square" aria-hidden="true"></i>
            <span class="nav-link-text">FOS Management</span>
          </a>
        </li>
		<!--li class="nav-item" data-toggle="tooltip" data-placement="right" title="Challan Generation">
          <a class="nav-link" href="<?php // echo base_url(); ?>index.php/challan_generate">
            <i class="fa fa-check-circle" aria-hidden="true"></i>
            <span class="nav-link-text">Challan Generation</span>
          </a>
        </li-->
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Support Ticket Management">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/support">
            <i class="fa fa-support" aria-hidden="true"></i>
            <span class="nav-link-text">Raise Issues</span>
          </a>
        </li>
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Ticket Replies">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/support/replies">
            <i class="fa fa-recycle" aria-hidden="true"></i>
            <span class="nav-link-text">Ticket Replies</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
       
        <li class="nav-item">
        <a class="nav-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i>
			 Welcome  <b><?php 
			  $get_session_data = $this->session->userdata('logged_in');
			  $user = $get_session_data['user_uuid'];
			  $users = $get_session_data['username'];
			  $sql = "SELECT `first_name`, `last_name` FROM `tbl_user` WHERE `user_uuid`='".$user."'";	
             //echo $sql; die;								
			$res = 	$this->db->query($sql)->result();
			//print_r($res); die;			
			if(empty($res))
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
											window.alert('Sorry ! Session expired. Please Login Again..')
											window.location.href='http://13.228.144.245/livguard_dms';
											</SCRIPT>");
			}
			else
			{
			   echo $res[0]->first_name." ".$res[0]->last_name;
			   echo " (". $users .")";
			}
			  ?></b>, You are logged in !! </a> 
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url(); ?>index.php/Login/logout">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  
  
  
	