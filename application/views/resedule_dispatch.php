  <?php 
			$this->load->view('includes/top.php');  
   ?>
   <title>Livguard | Tertiary Stock List</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Consumables</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> <b>Consumables</b> 
		  
		  </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  
                  			<thead>
                            <tr>
                                <!--th>Select</th-->
								
								<th>Serial Num.</th>                           
								<th>Brand</th>                           
								<th>Product</th>								
								<th>Model Code</th>								
								<th>Primary Sale Date</th>							
								<th>Secondary Sale Date</th>							
								<th>Distributor Name/Code</th>								
								<th>Dealer Name/Code</th>								
								<!--<th>Add Product</th>								
								<th>Create Complain</th>  -->                    
                            </tr>
                            </thead>
							<tbody>
							<?php
							foreach($res as $val_dis){
								$secondary_sale_date = explode(' ',$val_dis->secondary_sale_date);
								?>
								<tr>
									<td ><?php echo  $val_dis->serial_num; ?></td>
									<td ><?php echo  $val_dis->brand; ?></td>
									<td ><?php echo  $val_dis->product; ?></td>
									<td ><?php echo  $val_dis->model; ?></td>
									<td ><?php echo  $val_dis->primary_sale_date; ?></td>
									<td ><?php echo  $secondary_sale_date[0]; ?></td>
									<td ><?php echo  $val_dis->Distributor_Name; ?> (<?php echo  $val_dis->Distributor_Code; ?>)</td>
									<td ><?php echo  $val_dis->dealer_name; ?> (<?php echo  $val_dis->dealer_code; ?>)</td>
								</tr>
								<?php
							}
							?>
										
                           
                            </tbody>
			      </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
   ?>

    
	
	<?php 
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 