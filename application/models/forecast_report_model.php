<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forecast_report_model extends CI_Model 
{

	
	function get_forecast_details($user)
	{
		
		$this->db->select('*');	
	    $this->db->from('tbl_forecast');	
	    $this->db->where('f_user', $user);
	    $this->db->order_by("forecastId", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); // die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}
	}	
	
	function get_product_type(){
		$this->db->select('name,id');
		$this->db->from('aos_product_categories');
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		return $query->result();
		/*$sql = "SELECT product FROM `tblProductSerialNum1` GROUP BY `product`";
         $query = $this->db->query($sql);
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}*/
	}
	

	
	function get_forecast_report($m1, $m2, $m3, $user)
	{
		$sql = "SELECT * FROM `tbl_forecast` WHERE  `forecast_month` ='".$m1."' OR `forecast_month`='".$m2."'  Or `forecast_month`= '".$m3."' AND `f_user`='".$user."'";
         $query = $this->db->query($sql);
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}
	}
	
	
	public function insert_forecast_report($data) {	
		$sql = "SELECT forecast_month FROM `tbl_forecast` WHERE  `forecast_month` ='".$data['forecast_month']."' ";
        $query = $this->db->query($sql);		
		$cnt=$query->num_rows();
		if($cnt>0){		
			return "Duplicate"; 
		}else { //echo "<pre>"; print_r($data); die;
			$this->db->insert('tbl_forecast',$data);
			echo $this->db->last_query(); die;
			if($this->db->affected_rows() >0){
				return $this->db->affected_rows();
			}
			else{
				return 'Error';
			}
		}				
	}
	
	public function aos_prodcuts($product_type,$segment_id){
		$this->db->select('a.id,a.name,a.part_number');
		$this->db->from('aos_products as a');
		$this->db->join('aos_products_cstm as b','a.id=b.id_c','left');
		$this->db->join('sar_segment_aos_products_1_c as c','a.id=c.sar_segment_aos_products_1aos_products_idb','left');
		$this->db->join('sar_segment as d','c.sar_segment_aos_products_1sar_segment_ida=d.id','left');
		$this->db->where('a.aos_product_category_id', $product_type);
		$this->db->where('d.id', $segment_id);
		$this->db->where('a.deleted', '0');
		$this->db->order_by('a.id','asc');
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query();
	}
	
	public function available_stock($part_number, $user){
		$this->db->select('serial_num');
		$this->db->from('tbl_order_dispatch_consumables');
		$this->db->where('model',$part_number);
		$this->db->where('Distributor_Code',$user);
		$query1 = $this->db->get();
		$result1 = $this->db->last_query();
		
		$this->db->select('count(*) as con,product');
		$this->db->from('tblProductSerialNum1');
		$this->db->where('model',$part_number);
		$this->db->where('Distributor_Code',$user);
		$this->db->where_not_in('serial_num',$result1);
		$query = $this->db->get();
		//return $query->result();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0){
			$result = $query->result();
		
			$currentDate = date("Y:m:d");
			$beforeThreeMonth = date("Y:m:d",strtotime("-3 Months"));
			$currentDate1 = explode(":",$currentDate);
			$beforeThreeMonth1 = explode(":",$beforeThreeMonth);
			$currentDate1_forment = $currentDate1[0].'-'.$currentDate1[1].'-'.$currentDate1[2];
			$beforeThreeMonth1_forment = $beforeThreeMonth1[0].'-'.$beforeThreeMonth1[1].'-'.$beforeThreeMonth1[2];
			//echo $beforeThreeMonth1_forment .'='. $currentDate1_forment ; die;
			$this->db->select('count(*) as average');
			$this->db->from('tblProductSerialNum1');
			$this->db->where("primary_sale_date BETWEEN '".$beforeThreeMonth1_forment."' AND '".$currentDate1_forment."'");
			$this->db->where('model',$part_number);
			$this->db->where('Distributor_Code',$user);
			$this->db->where_not_in('serial_num',$result1);
			$query1 = $this->db->get(); 
			$return2 = $query1->result(); 
			$average = $return2[0]->average;
			//echo $average; die;
			$result[0]->average = $average;
			//echo $result[0]->average; die;
			return $result;
		}
	}
}

