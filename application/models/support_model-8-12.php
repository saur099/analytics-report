<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Support_model extends CI_Model {

	public function get_support_details($user)	
	{
	    $this->db->select('*');	
	    $this->db->from('tbl_support');	
	    $this->db->where('supportUser', $user);
	    $this->db->order_by("supportId", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); // die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}					
	}
		
	function insert_tickets($data)
	{
        $this->db->insert('tbl_support', $data);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
    }   
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */