<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class invoice_model extends CI_Model 
{
	public function get_invoices_list()
	{
	    	$sql ="SELECT`complaint_no`,`serial_number`,`hub_pincode`,`hub_contact_person`,`challan_number`,
				  `challan_date`,`generated_challan_date`,`submitted_by`,distributor_gstin_no, distributor_person_name,product, brand, model, `from_date`,`to_date`,
					distributor_person_name, distributor_phone, distributor_state, distributor_city, distributor_pincode, hub_contact_mobile, hub_contact_person, scrap_value,total_amt					  
				   FROM `tbl_service_challan_reprint` ";
			return  $this->db->query($sql)->result();	
	}
	function get_dist_invoices($user_uuid)
	{
		$sql = "SELECT CASE WHEN d.isDefective = '1' THEN 'defective' ELSE 'No' END AS defective, d.callType, b.brand_c, b.product_c, b.model_c, b.battery_serial_num_c, b.complaint_no_c, c.challan_number, c.challan_date FROM `aos_invoices` a left join `aos_invoices_cstm` b on a.id= b.id_c left join `tbl_service_challan_reprint` c on a.id = c.invoice_id left join tbl_service_calls d on d.call_id = b.complaint_no_c WHERE a.billing_account_id ='".$user_uuid."' limit 0, 100";
		//echo $sql; die;
		return  $this->db->query($sql)->result();
	}
}