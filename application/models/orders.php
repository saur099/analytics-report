<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders extends CI_Model     {

	public function addDistributorActionData( $zone_id   , $name , $address , $phone , $creation_date)	{	
	    $sql = "INSERT INTO `tbl_distributor`(`user_id`, `zone_id`,
								`name`, `address`, `phone`, `creation_date`, `created_by`) 
							VALUES (1,$zone_id,'$name','$address','$phone','$creation_date',1)";							
		return $this->db->query($sql);							
	}
		
	public function orderTableData($distributor_code)	{	
	    	 
   /* $data = array();
    $query = $this->db->get('tbl_order');
    $res   = $query->result();        
    return $res;*/	
	/* 
	 $sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids,IF(order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),2),'Pending',
IF(order_id = '','New', 
IF(order_id!= '','Processed','zzz'))
) as order_status,order_date_entered FROM `tbl_order` where distributor_code = '".$distributor_code."' GROUP by dealer_code order by order_status asc";								
		return $this->db->query($sql)->result();	
	 */
		$sql = "SELECT order_type,dealer_name,dealer_code,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids,IF(order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),2),'Pending',
		IF(order_id = '','New', 
		IF(order_id!= '','Processed','zzz'))
		) as order_status,order_date_entered FROM `tbl_order` where distributor_code = '".$distributor_code."' and order_id is null group by order_random order by order_status asc";								
	//echo $sql; die;
				return $this->db->query($sql)->result();	
	}
	
	public function getorderbyid($id, $distributor_code, $distributor_reference_no){
	/*
		$ids = str_replace(array('_', ' '), ',', $id);
		$sql = "select order_name,dealer_name,group_concat(product_name) as product_name,order_id,group_concat(dispatch_by) as dispatch_by,group_concat(rejection_reason) as rejection_reason,
group_concat(quantity) as quantity,count(quantity) as quan,city,state,hub_name,group_concat(id) as ids,order_date_entered,order_confirmation_date,group_concat(avail_stock) as avail_stock,emp_name,emp_type,employee_code,billing_address
 from tbl_order where id in ($ids) and distributor_code = '".$distributor_code."' group by dealer_code";	
 return $this->db->query($sql)->result(); */
 

		$ids = str_replace(array('_', ' '), ',', $id);
		$sql = "select order_name,dealer_name,dealer_code,group_concat(product_category) as product_category,group_concat(product_name) as product_name,order_id,order_type, group_concat(dispatch_by) as dispatch_by,group_concat(rejection_reason) as rejection_reason,
group_concat(quantity) as quantity,count(quantity) as quan,city,state,hub_name,group_concat(id) as ids,order_date_entered,order_confirmation_date,group_concat(avail_stock) as avail_stock,emp_name,emp_type,employee_code,billing_address, group_concat(model_code) as model_code
 from tbl_order where id in ($ids) and distributor_code = '".$distributor_code."' group by dealer_code";	
 $result = $this->db->query($sql)->result(); 
 $tblProductSerialNum="";
 $disp_quentity="";
 /*while (list($key, $value) = each($result)) { 
	$model_code1 = explode(',', $value->model_code);
	
		$i = 0;
	foreach($model_code1 as $v){
		$tblProductSerialNum1 = "select count(*) as con,product from tblProductSerialNum1 where model = '".$v."' and distributor_code = '".$distributor_reference_no."' and serial_num not in (select serial_num from tbl_order_dispatch_consumables where model = '".$v."' and distributor_code = '".$distributor_reference_no."')";
		$result_row = $this->db->query($tblProductSerialNum1)->result();
		//echo "<pre>"; print_r($result_row); die;
		$tblProductSerialNum .= $result_row[0]->con.",";
		//$product .= $result_row[0]->product.",";
	}
} */
$dispatch_qnt = "select a.id,IFNULL(sum(b.dispatch_quentity), 0) AS dispatch_quantity from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where a.id in ($ids)  group by a.id";
	$dispatch_quantity1 = $this->db->query($dispatch_qnt)->result();
	foreach($dispatch_quantity1 as $qnt){
		$disp_quentity .= $qnt->dispatch_quantity.",";
	}
/*	
$this->db->select('fosId,fosName');
 $this->db->from('tbl_service_fos');
 $this->db->where('fosId',$result[0]->emp_name);
 $query = $this->db->get();
 $order_taken_by1 = $query->result();
 $order_taken_by = $order_taken_by1[0]->fosName;*/
 //echo "<pre>"; print_r($order_taken_by); die;
$result[0]->con = substr($tblProductSerialNum,0,-1);
//$result[0]->order_taken_by = $order_taken_by;
//$result[0]->product_category = substr($product,0,-1);
$result[0]->dispatch_quantity = substr($disp_quentity,0,-1);

return $result;

		
	}
	
	public function check_scheme_appicable($quantity,$date_captured)
	{
		
		$sql = "select scheme_name,quantity from tbl_scheme where quantity >= '".$quantity."' and date(end_time) >= date('".$date_captured."')";
		return $this->db->query($sql)->result();
	}
	
	public function insert_order_dispatch_details($order_id,$dispatch,$dis_code,$update_avail_stock,$Quantities,$dispatch_serial_no,$distributor_code,$mrpprice,$dbprice,$actualprice,$discount)
	{ //echo $dis_code; die;
	  $cur_date = date('Y-m-d H:i:s');
	  $check_sql = "select count(*) as cnt from tbl_order_dispatch_details where order_id = '".$order_id."' and dispatch_by_id = '".$dis_code."'"; 
	  $checks = $this->db->query($check_sql)->result();
	
	if($checks[0]->cnt > 0)
	{ 
		$ups = "update tbl_order_dispatch_details set dispatch_through = '".$dispatch."',dispatch_date = '".$cur_date."',dispatch_quentity = '".$Quantities."',dispatch_serial_no = '".$dispatch_serial_no."',mrp_price = '".$mrpprice."',db_price = '".$dbprice."',actual_price = '".$actualprice."',discount = '".$discount."' where order_id = '".$order_id."' and dispatch_by_id = '".$dis_code."'";
		 $this->db->query($ups);
		 
		 
		// $update_avail_stock = $this->db->query("update tbl_order set avail_stock = '".$update_avail_stock."' where id = '".$order_id."'");
	}
	else { /*
		$this->db->select('b.serial_num');
		$this->db->from('tbl_order as a');
		$this->db->join('tblProductSerialNum1 as b','a.model_code = b.model','left');
		$this->db->where('b.distributor_code',$distributor_code);
		$query = $this->db->get();
		$check = $query->result();
		foreach($check as $serialno){ 
			if($dispatch_serial_no == $serialno->serial_num){
				
			}
		}
		*/
	 $sql = "insert into tbl_order_dispatch_details(order_id,dispatch_by_id,dispatch_through,dispatch_date,dispatch_quentity,dispatch_serial_no,mrp_price,db_price,actual_price,discount) values('".$order_id."','".$dis_code."','".$dispatch."','".$cur_date."','".$Quantities."','".$dispatch_serial_no."','".$mrpprice."','".$dbprice."','".$actualprice."','".$discount."')";
	  $this->db->query($sql);		 //avail_stock = '".$update_avail_stock."',
	  
	  $dispatch_serial_no = explode(',',$dispatch_serial_no);
	  foreach($dispatch_serial_no as $v){
		  $sql1 = "insert into tbl_seirialno_dispatch(order_id,dispatch_by_id,serial_num) values('".$order_id."','".$dis_code."','".$v."')";
		  $this->db->query($sql1);
	  }
	  
	 $update_avail_stock = $this->db->query("update tbl_order set dispatch_by = '1' where id = '".$order_id."'");
	}
	}
	
	
	
	public function get_all_product_dispatch_details($id,$dis_code)
	
	{
		$ids = str_replace(array('_', ' '), ',', $id);
		$sql = "select a.product_name as product_name,a.model_code,a.quantity,b.dispatch_quentity,b.dispatch_through,b.dispatch_date,b.order_id,b.dispatch_by_id from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where b.order_id in ($ids) and b.dispatch_by_id = '".$dis_code."'";
		return $this->db->query($sql)->result(); 
		
	}
	
	
	public function get_all_product_rejected_details($id,$dis_code){ 
		$ids = explode('_',$id);
		$this->db->select('a.product_name,a.model_code,b.rejection_reason,b.rejected_date,b.rejected_by_id,b.order_id');
		$this->db->from('tbl_order as a');
		$this->db->join('tbl_order_rejection_details as b','a.id = b.order_id','left');
		$this->db->where_in('b.order_id',$ids);
		$this->db->where('rejected_by_id',$dis_code);
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); die;
	}
	
	public function get_rejected_details_cancel($order_id, $distributor_code){
		$sql_del = "delete from tbl_order_rejection_details where order_id = '".$order_id."' AND rejected_by_id = '".$distributor_code."'"; 	
		$del = $this->db->query($sql_del);
		$sql_update = "update tbl_order set rejection_reason = '0' where id = '".$order_id."' AND distributor_code = '".$distributor_code."'";
		$update = $this->db->query($sql_update);
		
		/*$this->db->where('order_id', $order_id);
		$this->db->where('rejected_by_id', $distributor_code 'AND ')
		$this->db->delete('tbl_order_rejection_details'); 
		$query = $this->db->get();
		echo $this->db->last_query(); die;
		/*$this->db->where('id', $order_id 'AND distributor_code = "'.$distributor_code.'"');
		$this->db->update('tbl_order', $data);
		$query = $this->db->get();
		return $query->result();*/
		
	}
	
	public function get_dispatch_details_cancel($order_id, $distributor_code){
		$sql_del = "delete from tbl_order_dispatch_details where order_id = '".$order_id."' AND dispatch_by_id = '".$distributor_code."'"; 	
		$del = $this->db->query($sql_del);
		
		$sql_del_scan = "delete from tbl_seirialno_dispatch where order_id = '".$order_id."' AND dispatch_by_id = '".$distributor_code."'"; 	
		$del2 = $this->db->query($sql_del_scan);
		
		$sql_update = "update tbl_order set dispatch_by = '0' where id = '".$order_id."' AND distributor_code = '".$distributor_code."'";
		$update = $this->db->query($sql_update);
	}
	
	public function get_dispatch_consumables($distributor_code){ 
		$this->db->select('tbl_order_dispatch_consumables.*');
		$this->db->from('tbl_order_dispatch_consumables');
		$this->db->where('Distributor_Code', $distributor_code);
		$this->db->order_by('productSerialId','desc');
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); die;
	}
	
	
	public function update_order_id($ids,$data,$distributor_code, $dist_username)
	{ 
		
	$get_ids_count = explode("_",$ids);
	$get_avail_stock = explode("_",$avail_stock);
	$con = count($get_ids_count);
	$ids1 = $ids;
	//$ids = str_replace(array('_', ' '), ',', $ids);
	$ids = str_replace('_',"','",$ids); 
	$avail_stock = str_replace(array('_', ' '), ',', $avail_stock);
	$cur_date = date('Y-m-d H:i:s');
	
	    $check_sql_dispatch = "select count(*) as dispatch_cnt from tbl_order_dispatch_details where order_id in ('".$ids."') and dispatch_by_id = '".$distributor_code."'";
	   $checks1 = $this->db->query($check_sql_dispatch)->result();
	   
	   $check_sql_reject = "select count(*) as reject_cnt from tbl_order_rejection_details where order_id in ('".$ids."') and rejected_by_id = '".$distributor_code."'";
	   $checks2 = $this->db->query($check_sql_reject)->result();
	   
	
	$false_data = "Enter Dispatch Details";
	if($checks1[0]->dispatch_cnt == 0 && $checks2[0]->reject_cnt == 0)
	{
		return '1';
	} else {
	  
	  $sql = "select a.id,a.product_name,a.avail_stock,a.distributor_code,b.dispatch_quentity from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where a.id in ('".$ids."')";
	  $checks = $this->db->query($sql)->result();
	  
	    foreach($checks as $udate){
		    //if($udate->avail_stock >=$udate->dispatch_quentity){
			  //$avail_stock_1 = $udate->avail_stock - $udate->dispatch_quentity;
			  $ups = "update tbl_order set order_id = '".$data['order_number']."',order_confirmation_date = '".$cur_date."' where id ='".$udate->id."'";
			  $this->db->query($ups);
			//}
	    }
	 
	 
	$get_details = "select a.model_code,a.quantity,a.dealer_code,a.dealer_name,a.distributor_reference_no,a.order_name,a.avail_stock,b.dispatch_quentity,b.dispatch_serial_no from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where a.id in('".$ids."')";
	$result_val = $this->db->query($get_details)->result(); //echo "<pre>"; print_r($result_val); die;
	foreach($result_val as $v){ //echo "<pre>"; print_r($v); die; 
		if($v->dispatch_quentity !="" && $v->dispatch_serial_no !=""){ 
				$dispatch_serial_no1 = str_replace(array(',', ' '), "','", $v->dispatch_serial_no);
				$get_productDetails = "select * from tblProductSerialNum1 where serial_num in('".$dispatch_serial_no1."') limit  $v->dispatch_quentity "; 
		
				$result_val1 = $this->db->query($get_productDetails)->result();
			
				foreach($result_val1 as $vins){ //echo "<pre>"; print_r($vins); die;
					$dispatch_cunsum = array(
						'serial_num' => $vins->serial_num,
						'brand' => $vins->brand,
						'product' => $vins->product,
						'model' => $vins->model,
						'primary_sale_date' => $vins->primary_sale_date, //date('Y-m-d H:i:s'),
						'invoice_no' => $vins->invoice_no,
						'Distributor_Code' => $v->distributor_reference_no,
						'Distributor_Name' => $v->order_name,
						'dealer_code' => $v->dealer_code,
						'dealer_name' => $v->dealer_name,
						'blocked' => $vins->blocked
					);
					$this->db->insert('tbl_order_dispatch_consumables',$dispatch_cunsum);
				}
			
		}
	}
	 return '2';
	}
	
	
	
	
	}
	
	public function dspch_dtls($ids){
		$id = explode('_',$ids);
		$this->db->select('a.dispatch_by');
		$this->db->from('tbl_order as a');
		$this->db->join('tbl_order_dispatch_details as b','a.id = b.order_id','left');
		$this->db->where_in('a.id', $id);
		$query = $this->db->get();
		return $query->result_array();
		//echo $this->db->last_query(); die;
	}
	
	public function dspch_reject($ids){
		$id = explode('_',$ids);
		$this->db->select('a.rejection_reason');
		$this->db->from('tbl_order as a');
		$this->db->join('tbl_order_rejection_details as b','a.id = b.order_id','left');
		$this->db->where_in('a.id', $id);
		$query = $this->db->get();
		return $query->result_array();
		//echo $this->db->last_query(); die;
	}
	
	
	public function get_mobile_number($ids)
	
	{ //echo $ids;
		//echo $ids1 = str_replace(array('_', ' '), ',', $ids); echo "<br>";
		$ids = str_replace('_',"','",$ids); 
		 $sql = "select a.id,a.dealer_name,a.dealer_code,a.order_date_entered,a.distributor_reference_no,b.distributor_name,c.dispatch_quentity,a.order_id,
a.product_name,a.quantity,a.dealer_mobile,a.dispatch_by,a.emp_name,
emp_mobile from tbl_order as a left join tbl_user as b on a.distributor_reference_no = b.user_name left join tbl_order_dispatch_details as c on a.id = c.order_id
where  a.id in ('".$ids."') and a.dispatch_by = 1 or a.rejection_reason = 1"; // or rejection_reason = 1
//echo $sql; die("Hello");
		return $this->db->query($sql)->result();
		
	}
	
	public function get_rejection_sms_details($ids,$dis_code)
	{
		//$ids = str_replace(array('_', ' '), ',', $ids);
		$ids = str_replace('_',"','",$ids); 
		$get_details = "select a.dealer_name,a.dealer_code,a.order_date_entered,a.distributor_code,tbl_user.distributor_name,
a.order_id,a.product_name,a.quantity,a.dealer_mobile,a.dispatch_by,a.emp_name,a.emp_mobile,b.rejection_reason from tbl_order a left join tbl_order_rejection_details b on a.id = b.order_id join tbl_user on a.distributor_reference_no = tbl_user.user_name where a.id in ('".$ids."') and a.distributor_code= '".$dis_code."' and b.order_id is not null"; 
		return $this->db->query($get_details)->result();
	}
	public function insert_order_rejection_details($order_id, $reject,$dis_code)
	{
		/*$data = array(
        'rejection_reason' => $reject
    );
    $this->db->where('id', $order_id);
    $this->db->update('tbl_order', $data);*/
	
	$cur_date = date('Y-m-d H:i:s');
	  $check_sql = "select count(*) as cnt from tbl_order_rejection_details where order_id = '".$order_id."' and rejected_by_id = '".$dis_code."'";
	// $checks =  $this->db->query($check_sql); 
	 $checks = $this->db->query($check_sql)->result();
	
	if($checks[0]->cnt > 0)
	{
		$ups = "update tbl_order_rejection_details set rejection_reason = '".$reject."',rejected_date = '".$cur_date."' where order_id = '".$order_id."' and rejected_by_id = '".$dis_code."'";
		 $this->db->query($ups);
	}
	else { 
	 $sql = "insert into tbl_order_rejection_details(order_id,rejected_by_id,rejection_reason,rejected_date) values('".$order_id."','".$dis_code."','".$reject."','".$cur_date."')";
	 
	 $update_avail_stock = $this->db->query("update tbl_order set rejection_reason = '1' where id = '".$order_id."'");
	 return $this->db->query($sql);		 
	}
		
	}
	
	public function update_confirm_order_rejection_details($order_id, $reject)
	
	{
		//$ids = str_replace(array('_', ' '), ',', $order_id);
		$ids = str_replace('_',"','",$ids); 
	$ups = "update tbl_order set rejection_reason = '".$reject."' where id in ('".$ids."')";
	$this->db->query($ups);
		
	}
	
	//** to get details of confirm order in listview
	public function get_confirm_orders($dis_code){ 
		$this->db->select('a.order_name,a.order_id,a.dealer_name,a.city,a.state,a.hub_name,a.order_confirmation_date,count(a.quantity) as quantity,group_concat(a.id)as ids');
		$this->db->from('tbl_order as a');
		$this->db->join('tbl_order_dispatch_details as b','a.id = b.order_id','left');
		$this->db->where('a.order_id != ""', ' AND distributor_code = "'.$dis_code.'"');  
		$this->db->group_by('a.order_random');
		$this->db->order_by('a.id','asc');
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); exit;
		//echo $this->db->last_query(); die;
		// $sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids FROM `tbl_order` where order_id!= '' and distributor_code = '".$dis_code."' GROUP by dealer_code";								
		// return $this->db->query($sql)->result();
	}
	
	// ** to get branch order 
	
	public function get_branch_order()
	{
		/*$data = array();
		$this->db->where("order_id = ''", null, false);
        $query = $this->db->get('tbl_order');
		$retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids,IF(order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),2),'Pending',
IF(order_id = '','New', 
IF(order_id!= '','Processed','zzz'))
) as order_status FROM `tbl_order` where order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),1) GROUP by dealer_code";								
		return $this->db->query($sql)->result();
	}
	
	// ** to get all orders on behalf of user login type
	
	public function get_order_by_type($login_type,$dis_code)
	{ 
		if($login_type == 'Branch')
		{
			/*$data = array();
		$this->db->where("order_id = ''", null, false);
        $query = $this->db->get('tbl_order');
		$retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids FROM `tbl_order` where order_id = '' GROUP by dealer_code";								
		return $this->db->query($sql)->result();
		}
		else if($login_type == 'Distributor')
		{
		/*$data = array();
        $query = $this->db->get('tbl_order');
        $retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids FROM `tbl_order` where distributor_code = '".$dis_code."'
		GROUP by dealer_code";								
		return $this->db->query($sql)->result();
		}
	}
	
	// ** get rejection details
	
	public function get_reject_details($ids,$dis_code)
	
	{
	    $get_details = "select a.*,b.order_id,b.rejected_by_id,b.rejection_reason,b.rejected_date from tbl_order a left join tbl_order_rejection_details b on a.id = b.order_id where a.id = '".$ids."' and a.distributor_code = '".$dis_code."'";
		return $this->db->query($get_details)->result();
	}
	
	public function get_confirm_reject_details($ids)
	{
		$ids = str_replace(array('_', ' '), ',', $ids);
	     $get_details = "select group_concat(id) as ids,rejection_reason from tbl_order where id in ($ids)";
		return $this->db->query($get_details)->result();
	}
	
	public function check_product_serial($serial, $user)
	{
		$this->db->select('product,model');
		$this->db->from('tblProductSerialNum1');
		$this->db->where('serial_num',$serial);
		//$this->db->where('Distributor_Code',$user);
		$query = $this->db->get();
		$tblProductSerialNum1 = $query->result();
		$tblProductSerialNum1_model = $tblProductSerialNum1[0]->model;
		
		$this->db->select('serial_num');
		$this->db->from('tbl_seirialno_dispatch');
		$this->db->where('serial_num',$serial);
		$query1 = $this->db->get();
		$dispatch_dits = $query1->result();
		$dispatch_serial_no = $dispatch_dits[0]->serial_num;
		 //echo $dispatch_serial_no;
		if($dispatch_serial_no){ 
			return "demo"; 
		}
		return $tblProductSerialNum1_model;
		//echo $this->db->last_query(); die;
	}
	
	// ** get dispatch details
	
	public function get_dispatch_details($ids,$dis_code,$avail_stocks1){
		 /*$get_details = "select a.*,b.order_id,b.dispatch_by_id,b.dispatch_through,b.dispatch_date,b.dispatch_quentity from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where a.id = '".$ids."' and a.distributor_code = '".$dis_code."'";
		return $this->db->query($get_details)->result(); */
		
		
		$get_details = "select a.*,b.order_id,b.dispatch_by_id,b.dispatch_through,b.dispatch_date,b.dispatch_quentity,b.dispatch_serial_no,a.product_category from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where a.id = '".$ids."' and a.distributor_code = '".$dis_code."'";
		$result = $this->db->query($get_details)->result(); 
		$result[0]->con = $avail_stocks1;
		return $result;
	}
	
	public function get_dispatch_price($distributor_code,$model_code){
		$this->db->select('SERVICE_CHARGE,SCRAP_PRICE');
		$this->db->from('tbl_model_master1');
		$this->db->where('model_code',$model_code);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_dispatch_serial_num($distributor_code,$order_id){ 
		
		/*$this->db->select('b.serial_num');
		$this->db->from('tbl_order as a');
		$this->db->join('tblProductSerialNum1 as b','a.model_code = b.model','left');
		$this->db->where('b.distributor_code',$distributor_code);
		$query = $this->db->get();
		return $query->result();*/
		
		$sql = "select model,serial_num,a.quantity from tbl_order a left join tblProductSerialNum1 b on a.model_code = b.model where a.id = '".$order_id."' and b.distributor_code = '".$distributor_code."' and b.serial_num not in (select serial_num from tbl_order_dispatch_consumables where distributor_code = '".$distributor_code."')";
		return $this->db->query($sql)->result();
	}
	
	
	public function zoneWiseDistributorData($zone_id)  {
	     $sql = "SELECT d.`distributor_id`,  d.`name`
									FROM 	   `tbl_distributor` `d` 
								where   d.`zone_id`=$zone_id";								
		return $this->db->query($sql);				
	}
	
	public function get_primary_order_punch($distributor_namee){
		$this->db->select('a.date_entered,a.description,sum(b.quantity_c) as qty,b.fe_ref_num_c');
		$this->db->from('sar_secondary_order as a');
		$this->db->join('sar_secondary_order_cstm as b','a.id = b.id_c','left');
		$this->db->where('b.distributor_code_c', $distributor_namee);
		$this->db->group_by('b.fe_ref_num_c');
		$this->db->order_by('date(a.date_entered) desc');	
		//$this->db->where('');
		//echo $this->db->last_query(); die;
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		return $query->result();
		//
	} 
	/*
	public function get_primary_order_details($fe_ref_num_c,$distributor_code){
		$this->db->select('c.PRODUCT,c.MODEL_DESCRIPTION,b.quantity_c as qty');
		$this->db->from('sar_secondary_order as a');
		$this->db->join('sar_secondary_order_cstm as b','a.id = b.id_c','left');
		$this->db->join('tbl_model_master1 as c','b.aos_products_id_c = c.aos_product_id','left');
		//$this->db->group_by('remarks_c');
		$this->db->where('b.distributor_code_c', $distributor_code);
		$this->db->where('b.fe_ref_num_c', $fe_ref_num_c);
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); die;
	} */
	public function get_primary_order_details($fe_ref_num_c,$distributor_namee){
		$this->db->select('c.PRODUCT,c.MODEL_DESCRIPTION,b.quantity_c as qty');
		$this->db->from('sar_secondary_order as a');
		$this->db->join('sar_secondary_order_cstm as b','a.id = b.id_c','left');
		$this->db->join('tbl_model_master1 as c','b.aos_products_id_c = c.aos_product_id','left');
		//$this->db->group_by('remarks_c');
		$this->db->where('b.distributor_code_c', $distributor_namee);
		$this->db->where('b.fe_ref_num_c', $fe_ref_num_c);
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); die;
	}
	
	public function getdistributor_details($distributor_code){
		$this->db->select('a.name,a.phone_office,a.assigned_user_id,b.billing_address_address1_c');
		$this->db->from('accounts a');
		$this->db->join('accounts_cstm as b','a.id = b.id_c','left');
		$this->db->where('a.id', $distributor_code);
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); die;
	}
	
	public function getdealer_details($id,$distributor_code){ //echo $distributor_code; die;
		$id1 = explode('_', $id);
		$this->db->select('dealer_code,id,model_code');
		$this->db->from('tbl_order');
		$this->db->where('distributor_code', $distributor_code);
		$this->db->where_in('dispatch_by', '1');
		$this->db->where_in('id',$id1);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		$order_details = $query->result();
		
		$dealer_code = $order_details[0]->dealer_code;
		
		$this->db->select();
		$this->db->from('sar_dealer a');
		$this->db->join('sar_dealer_cstm as b','a.id = b.id_c','left');
		$this->db->where('b.customer_number_c', $dealer_code);
		$query = $this->db->get();
		$dealer = $query->result();
		//echo "<pre>"; print_r($dealer); die;
		return $query->result();
		//echo $this->db->last_query(); die;
		
	}
	
	public function getproduct_details($id,$distributor_code){
		$id1 = explode('_', $id);
		$this->db->select('a.dealer_code,a.id,a.model_code,a.product_category,b.dispatch_serial_no,b.actual_price, b.mrp_price, b.db_price, b.discount, b.SGST, b.CGST,b.dispatch_quentity, a.order_date_entered, a.order_id, c.email_address, d.HSN_CODE');
		$this->db->from('tbl_order as a');
		$this->db->join('tbl_order_dispatch_details as b','a.id = b.order_id','left');
		$this->db->join('tbl_user as c','a.distributor_reference_no = c.user_name','left');
		$this->db->join('tbl_model_master1 as d','d.MODEL_CODE = a.model_code','left');
		$this->db->where('a.distributor_code', $distributor_code);
		$this->db->where_in('a.dispatch_by', '1');
		$this->db->where_in('a.id',$id1);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		return $query->result();
		
	}	
}

/* End of file distributors.php */
/* Location: ./application/models/distributors.php */