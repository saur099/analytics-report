<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Model {

	public function addUserActionData($user_name ,
													$user_pass , $user_type , $creation_date)	{
	    	$sql = "INSERT INTO `tbl_user`( `user_name`, `pass`, `user_type`, `user_status`, `creation_date`)
			VALUES ('$user_name','$user_pass' ,'$user_type',1, '$creation_date' )";
			return $sql; 
			//$this->db->query($sql)	;					
	}
		
	public function userTableData()	{
	    	$sql = "SELECT `user_id`, `user_name`, `pass`, `user_type`, `user_status`, 
						DATE_FORMAT(`creation_date`,'%d-%m-%Y')  `creation_date` FROM `tbl_user` 
						ORDER BY user_id DESC"; 
			return $this->db->query($sql)	;					
	}
	
	public function get_dist_username($usern, $old_pass)
	{
	    	$sql = "SELECT  `user_name`, `pass`, `changed_times`, email_address, distributor_name FROM `tbl_user` WHERE `user_name`='".$usern."' and `pass` ='".$old_pass."' "; 
			return $this->db->query($sql)->result();					
	}
	function change_old_password($usern, $chng)
	{
		$this->db->where('user_name', $usern);
		$this->db->update('tbl_user',$chng);
		if($this->db->affected_rows() > 0)
		{
			 return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}
	
	public function get_current_user_details($dis_code)
	
	{
		$sql = "select user_id,user_name,pass,user_type,distributor_name,billing_address_city,billing_address_state,billing_address_postalcode,billing_address_country,mobile,first_name,last_name,email_address,hub_name,ROUND(closing_balance) as closing_balance  from tbl_user where user_name = '".$dis_code."'";
		return $this->db->query($sql)->result();
		
	}
	
	public function update_current_user_details($distributor_name,$billing_address_city,$billing_address_state,$billing_address_postalcode,$mobile,$first_name,$last_name,$email_address,$hub_name,$password,$user_id)
	
	{
		$this->db->query("update tbl_user set distributor_name = '".$distributor_name."',billing_address_city='".$billing_address_city."',billing_address_state='".$billing_address_state."',billing_address_postalcode='".$billing_address_postalcode."',mobile='".$mobile."',first_name='".$first_name."',last_name='".$last_name."',email_address='".$email_address."',hub_name='".$hub_name."' ,pass='".$password."' where user_id = '".$user_id."'");
		return 'update_success';
	}
}

/* End of file user.php */
/* Location: ./application/models/user.php */