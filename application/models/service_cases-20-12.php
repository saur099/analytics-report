<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting('E_ALL');
class Service_cases extends CI_Controller 
{
	public function index()
	{
		//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
		//echo "hii"; die;
		$this->load->model('service_case_model');
		$data['res'] = $this->service_case_model->get_call_details();
		//echo "<pre>"; print_r($data); die;
		$this->load->view('services/service_mycases_view', $data);
	}
	function closed_cases()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/service_close_view', $data);
	}
	function check_cases()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$this->load->view('services/update_case_wrrnty_view', $data);
	}
	public function view_cases($caseId)
	{
		$get_session_data = $this->session->userdata('logged_in');
		//echo "Hello "; die;
		$this->load->model('service_case_model');
		$data['sas'] = $this->service_case_model->update_wrrnty_details($caseId);
		$this->load->view('services/update_case_wrrnty_view', $data);
	}
	function close_outwarranty()
	{
		 
		//echo $user; die;
		$case = $_POST['caseid'];
		$dist = $_POST['dist_remark'];
		if(empty($case))
		{	
			redirect('service_cases');
		}
		$this->load->model('service_case_model');
		$data = $this->service_case_model->close_case($case, $dist, $user);
		$this->load->view('services/update_case_wrrnty_view', $data);
		
	}
	function new_consumer()
	{
		$this->load->view('services/add_consumer_view');
	}
	
	function add_new_consumer()
	{
		$get_session_data = $this->session->userdata('logged_in');
		$user = $get_session_data['username'];
		
		function getGUID()
		{
			if (function_exists('com_create_guid')){
				return com_create_guid();
			}else{
				mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
				$charid = strtoupper(md5(uniqid(rand(), true)));
				$hyphen = chr(45);// "-"
				$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12);
				return $uuid;
			}
		}
		
		function getsecGUID()
		{
			if (function_exists('com_create_guid'))
			{
				return com_create_guid();
			}
			else
			{
				mt_srand((double)microtime()*10000);
				$charid = strtoupper(md5(uniqid(rand(), true)));
				$hyphen = chr(45);// "-"
				$uuid = substr($charid, 0, 8).$hyphen
					.substr($charid, 8, 4).$hyphen
					.substr($charid,12, 4).$hyphen
					.substr($charid,16, 4).$hyphen
					.substr($charid,20,12);
				return $uuid;
			}
		}
		
		
		$tid = getGUID();
		$secid = getsecGUID();
		 	
		

		$query = $this->db->query("SELECT *FROM  `tbl_service_customers` where mobile='".$_POST['mobile']."'");
		$rows  = $query->num_rows();
       
		if($rows > 0)
		{ 
			//echo "Hii";  die("Hel");
			$m  =  $_POST['mobile'];
			echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Mobile No. already exists. You can create product for this consumer.')
				</SCRIPT>");
			$this->load->model('service_case_model');
			$data['res'] = $this->service_case_model->get_existing_consumer($m);
			$this->load->view('services/add_exist_cust_view', $data);
		}
		else
		{
			$query = $this->db->query("SELECT *FROM  `contacts` where phone_mobile='".$_POST['mobile']."'");
			$rows  = $query->num_rows();
			//echo $rows; die("Hello");
			if($rows > 0)
			{ 
				$mob  =  $_POST['mobile'];
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry ! Mobile No. already exists. Please Create product for this consumer.')
					</SCRIPT>");
				$this->load->model('service_case_model');
				$data['res'] = $this->service_case_model->get_existing_customer($mob);
				$this->load->view('services/add_exist_consm_view', $data);
			}
			else
			{
				
			
			  $data = array(
		                 'fname' => $_POST['fname'],
		                 'lname' => $_POST['lname'],
		                 'mobile' => $_POST['mobile'],
		                 'alt_mobile' => $_POST['altmobile'],
		                 'email' => $_POST['email'],
		                 'address' => $_POST['address'],
		                 'landmark' => $_POST['landmark'],
		                 'state' => $_POST['state'],
		                 'city' => $_POST['city'],
		                 'area' => $_POST['area'],
		                 'pincode' => $_POST['pin'],
		                 'dob' => $_POST['dob'],
		                 'createdBy' => $user
						);
			$dat = array(
		                 'id' => $tid,
		                 'first_name' => $_POST['fname'],
		                 'last_name' => $_POST['lname'],
		                 'phone_mobile' => $_POST['mobile'],
		                 'phone_other' => $_POST['altmobile'],
		                 'phone_work' => $_POST['email'],
		                 'primary_address_country' => $_POST['address'],
		                 'primary_address_street' => $_POST['landmark'],
		                 'primary_address_state' => $_POST['state'],
		                 'primary_address_city' => $_POST['city'],
		                 'primary_address_postalcode' => $_POST['pin'],
		                 'birthdate' => $_POST['dob']
						);	
			$data_cstm = array(
			                    'id_c' => $tid,
								'primary_address_area_c' => $_POST['area']
							   );
			$d_email = array(
								'id' => $tid,
								'email_address' => $_POST['email']
								);		
            $em_rel	  = array(
								'id' => $secid,
								'bean_id' => $tid,
								'bean_module' => 'Contacts',
								'email_address_id' => $_POST['email']
								);							
						
					
				
           /*  echo "<pre>"; print_r($data); 
            echo "<pre>"; print_r($dat); 
            echo "<pre>"; print_r($data_cstm); 
            echo "<pre>"; print_r($data_email); 
			
            // $this->db->insert('tbl_service_customers', $data);
			// $this->db2->insert('contacts', $dat);
			 
			//die;	

			die; */
		


			//echo "<pre>"; print_r($data); die;
		
			$this->load->model('service_case_model');
			$data = $this->service_case_model->add_new_consumer($data, $dat, $data_cstm, $d_email, $em_rel);
			//$this->load->view('services/update_case_wrrnty_view', $data); 
			echo $data; die;
			echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Success ! Go ahead to create the product..')
					window.location.href='add_product/$data';
					</SCRIPT>");
			}
		}	
		
		
		
	}
	function add_product($consid)
	{

		//echo $consid; die("hii");
		$data['rs'] = array('consid' => $consid);
		$this->load->view('services/add_product_view', $data);
	}
	
	
	
	
	function add_product_details()
	{
		//echo "hii"; die;
		$get_session_data = $this->session->userdata('logged_in');
	    $user = $get_session_data['username'];
		
		$data  = array(
		         'consumerId' => $_POST['consId'],
		         'userId' => $user,
		         'productSegment' => $_POST['productSegment'],
		         'brand' => $_POST['brand'],
		         'productType' => $_POST['protype'],
		         'model' => $_POST['model'],
		         'custPurchaseInvoiceNum' => $_POST['custPurchaseInvoiceNum'],
		         'custPurchaseDate' => $_POST['custPurchaseDate'],
		         'productSerialNo' => $_POST['productSerialNo'],
		         'batterymanufacturingCode' => $_POST['batterymanufacturingCode'],
		         'activationDate' => $_POST['activationDate'],
		         'dealerName' => $_POST['dealerName'],
		         'warrantyStartDate' => $_POST['warrantyStartDate'],
		         'warrantyEndDate' => $_POST['warrantyEndDate'],
		         'ext_warrantyStartDate' => $_POST['ext_warrantyStartDate'],
		         'warrantyEndDate' => $_POST['warrantyEndDate'],
		         'letpl_invoice_no' => $_POST['letpl_invoice_no'],
		         'letpl_invoice_date' => $_POST['letpl_invoice_date'],
		         'amc_end_date' => $_POST['amc_end_date'],
		         'proRateDisc' => $_POST['proRateDisc'],
		         'proRateWarrStDate' => $_POST['proRateWarrStDate'],
		         'proRateWarrEndDate' => $_POST['proRateWarrEndDate'],
		         'warrantyStatus' => $_POST['waranyStatus'],
		         'productStatus' => $_POST['pro_status']
				 
		);
		
		// echo "<pre>"; print_r($data); die;
			$this->load->model('service_case_model');
			$dat = $this->service_case_model->add_new_product($data);
			if(empty($dat))
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
					window.alert('Sorry some problem occured. Product for customer not created.!!')
					</SCRIPT>");
					redirect('service_cases');

			}
			else
			{
				echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Customer with product details Created. Create Case Now !!')
				window.location.href='cust_details';
				</SCRIPT>");
				redirect('cust_details');

			}
	}
	

	
}

