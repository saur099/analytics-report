<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Support_model extends CI_Model {

	public function get_support_details($user){
	    $this->db->select('*');	
	    $this->db->from('tbl_support');	
	    $this->db->where('distUUID', $user);
	    $this->db->order_by("supportId", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}					
	}
	
	function get_prod_serial()
	{
		 $this->db->select('*');	
	    $this->db->from('tblProductSerialNum');
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	}
		
	function insert_tickets($data,$data1,$data2)
	{
        $this->db->insert('tbl_support', $data);
		$this->db->insert('sar_dms_support_ticket', $data1);
		$this->db->insert('sar_dms_support_ticket_cstm', $data2);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
    }   
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */