<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Support_model extends CI_Model {

	public function get_support_details($user)
	{
	    $this->db->select('*');	
	    $this->db->from('tbl_support');	
	    $this->db->where('distUUID', $user);
	    $this->db->order_by("supportId", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}					
	}
	
	/* function get_prod_serial()
	{
		 $this->db->select('*');	
	    $this->db->from('tblProductSerialNum1');
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	} */
		
	function get_support_admin()
    {
		$this->db->select('*');	
	    $this->db->from('tbl_support');	
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}	
	  
	function get_replies_details($user_uuid)
	{
		$this->db->select('*');	
	    $this->db->from('tbl_support_replies');	
	    $this->db->where('repliedBy', $user_uuid);	
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
	function insert_data_admin($arr1, $arr2, $ticketId, $useruid)
	{

		//$array = array('distUUID' => $useruid, 'supportTicket' => $ticketId);
		$this->db->where('supportTicket', $ticketId); 
		//$this->db->where('distUUID', $useruid); 
		$this->db->update('tbl_support', $arr1);
		// echo $this->db->last_query();
		// die;
		// return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			$this->db->insert('tbl_support_replies', $arr2);
			 if($this->db->affected_rows() >0)
			{
				return $this->db->affected_rows();
			}
            else
			{
				return '';
			}		
		}
		else
		{
			return '';
		}
	} 
	
	function get_ticket_det($id)
    {
		$this->db->select('*');	
	    $this->db->from('tbl_support');	
	    $this->db->where('supportId', $id);	
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}	
	
	function insert_tickets($data,$data1,$data2)
	{
        $this->db->insert('tbl_support', $data);
		$this->db->insert('sar_dms_support_ticket', $data1);
		$this->db->insert('sar_dms_support_ticket_cstm', $data2);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
    }   
	function get_feedback_detail($user)
	{
		$this->db->select('*');	
	    $this->db->from('tbl_service_feedback');	
	    $this->db->where('createdBy', $user);
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}	
	}
	function insert_feedback($feed)
	{
		$this->db->insert('tbl_service_feedback', $feed);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */