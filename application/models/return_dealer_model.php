<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Return_dealer_model extends CI_Model 
{

	
	function get_dealer_list($users)
	{
		$this->db->select('*');	
	    $this->db->from('tbl_return_stock');	
	    $this->db->where('distName', $users);
	    $this->db->order_by("id", "desc");
		
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}	
	
	function get_defective_stock($user)
	{
		$sql = "SELECT count(post_to_defect) as defect FROM `tbl_return_stock` WHERE `distName` ='".$user."' and post_to_defect=1";								
		return $this->db->query($sql)->result();
	}
	
	function get_model_data($id)
	{
		$this->db->select('*');	
	    $this->db->from('tbl_model_master');	
	    $this->db->where('MODEL_CODE', $id);
		
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	function get_remarks()
	{
		$this->db->select('*');	
	    $this->db->from('tbl_sales_return_remarks');	
	    
		$query = $this->db->get();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
	function get_serial_list()
	{
		$this->db->select('*');	
	    $this->db->from('tblProductSerialNum1');
		$this->db->limit(10, 5);		
	   /*  $this->db->where('distName', $user);
	    $this->db->order_by("return_stock_id", "desc"); */
		
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}	
	
	
	public function insert_dealer_list($data2)
	{
		//echo "<pre>"; print_r($data); die;
		$this->db->insert('tbl_return_stock', $data2);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
	   
	}	
	
	
}

/* End of file user.php */
/* Location: ./application/models/user.php */
