<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stocks extends CI_Model {

	public function addschemedata($scheme_name, $product_name,$quantity, $duration,$start_time, $end_time)	{
	    	$sql = "INSERT INTO `tbl_scheme`( `product_name`, `scheme_name`, `quantity`, `duration`,start_time,end_time) 
				VALUES ('".$product_name."' ,'".$scheme_name."' ,'".$quantity."', '".$duration."', '".$start_time."','".$end_time."' )";
			return  $this->db->query($sql)	;					
	}
		
	public function getallproducts()	{
	    	$sql = "select `product_id`,`name`  `product_name`,
			`price`  `product_price`  from `tbl_product` order by product_id desc";
			return $this->db->query($sql)	;					
	}
	
	public function get_secondary_stock($dis_code)
	
	{
		$sql = "select * from tbl_secondary_stock where distributor_code = '".$dis_code."'";
			return $this->db->query($sql)->result()	;	
		
	}
	
	public function get_my_stock_details($distributor_code,$products)
	
	{
		$pro = str_replace(array(',', ' '), "','", $products);
		$pro_duc =  "'".$pro."'";
		echo $sql = "select * from tbl_my_stock where my_distributor_code = '".$distributor_code."' and my_product_name in ($pro_duc)";
		
	}
	
	public function my_stock($dis_code)
	
	{
		$sql = "select * from tbl_my_stock where my_distributor_code = '".$dis_code."'";
			return $this->db->query($sql)->result()	;
	}
	
	public function delete_scheme($id)
	
	{
		$sql = "delete from tbl_scheme where id = '".$id."'";
		return $this->db->query($sql)	;
	}
	
	public function getallmystock($dis_code)
	
	{
		$sql = "select * from tbl_my_stock where my_distributor_code = '".$dis_code."'";
		return $this->db->query($sql)->result();
		
		
	}
	
	
	public function get_stock_product_name($dis_code)
	
	{
		$sql = "select mystock_id,my_product_name from tbl_my_stock where my_distributor_code = '".$dis_code."' and my_product_name!= '' group by my_product_name";
		return $this->db->query($sql)->result();
	}
	
	public function get_serial_number_by_proid($pro_name,$dis_code)
	
	{
		   $sql = "select serial_number from tbl_my_stock where my_distributor_code = '".$dis_code."' and my_product_name LIKE '%".$pro_name."%'";
		return $this->db->query($sql)->result();
	}
	
	public function get_my_serial_numbers($dis_code)
	
	{
		
		$sql = "select mystock_id,my_product_name,serial_number from tbl_my_stock where my_distributor_code = '".$dis_code."' and my_product_name!= ''";
		return $this->db->query($sql)->result();
	}
	
	public function insert_return_stock_details($product_name,$assign_branch,$return_reason,$dis_code)
	
	{
		$cur_date = date('Y-m-d H:i:s');
		$ins = "insert into tbl_return_stock(return_product_name,return_serial_number,return_reason,stock_return_by,return_date_entered) values('".$product_name."','".$assign_branch."','".$return_reason."','".$dis_code."','".$cur_date."')";
		$this->db->query($ins);
		
	}
	
	public function get_my_return_stock_details($dis_code)
	
	{
		$sql = "select * from tbl_return_stock where stock_return_by = '".$dis_code."'";
		return $this->db->query($sql)->result();
	}
	
	public function delete_return_stock($ids)
	
	{
		 $del = "delete from tbl_return_stock where return_stock_id = '".$ids."'";
		return $this->db->query($del);
	}
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */