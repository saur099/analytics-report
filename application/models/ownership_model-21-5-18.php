<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ownership_model extends CI_Model {

	function get_ownership_mob($mobile)
	{
	  $query = $this->db->query("SELECT a.*, b.name as dist, c.* FROM `tbl_service_customers` as a left join tbl_service_products as c on a.custAssetGuid = c.productGuid  left join accounts b on a.createdBy = b.id where a.mobile = '".$mobile."'");
		
		//echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else{ 
			return '';
		}		


	}
	
	function get_ownership_serial($serial)
	{
		$sql = "SELECT a.*, b.name as dist, c.* FROM `tbl_service_products` a right join tbl_service_customers c on c.cust_code = a.consumerId left join accounts b on c.createdBy = b.id  where a.productSerialNo = '".$serial."'"; 
		echo $sql; die;
		$query = $this->db->query("SELECT a.*, b.name as dist, c.* FROM `tbl_service_products` a right join tbl_service_customers c on c.cust_code = a.consumerId left join accounts b on c.createdBy = b.id  where a.productSerialNo = '".$serial."'");
		
		echo $this->db->last_query();  die;
		// return $query->result(); 	
		//echo $query->num_rows(); die("hello");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else{ 
			return '';
		}			
	}
	function insert_data_owner($arr, $create, $mobile)
	{
		$this->db->where('mobile', $mobile);
		$this->db->update('tbl_service_customers', $arr);
		if($this->db->affected_rows() > 0)
		{
			 return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}
	function update_mob_product($arr1, $arr2, $create, $productGuid, $mobile)
	{
		$this->db->where('mobile', $mobile);
		$this->db->update('tbl_service_customers', $arr1);
		if($this->db->affected_rows() > 0)
		{
			 $this->db->where('id_c', $productGuid);
			$this->db->update('sar_asset_cstm', $arr2);
			if($this->db->affected_rows() > 0)
			{
				 return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		}
		else
		{
			return '';
		}
	}
	function insert_data_serial($arr1, $arr2, $arr3, $Serial, $productGuid, $mobile)
	{
		$this->db->where('mobile', $mobile);
		$this->db->update('tbl_service_customers', $arr1);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows() > 0)
		{
			 		$this->db->where('productSerialNo', $Serial);
					$this->db->update('tbl_service_products', $arr2);
					if($this->db->affected_rows() > 0)
					{
						 $this->db->where('id_c', $productGuid);
						$this->db->update('sar_asset_cstm', $arr3);
						if($this->db->affected_rows() > 0)
						{
							 return $this->db->affected_rows();
						}
						else
						{
							return 'e1';
						}
					}
					else
					{
						return 'e2';
					}
		}
		else
		{
			return 'e3';
		}
	}
	  
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */