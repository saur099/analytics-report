<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders extends CI_Model     {

	public function addDistributorActionData( $zone_id   , $name , $address , $phone , $creation_date)	{	
	    $sql = "INSERT INTO `tbl_distributor`(`user_id`, `zone_id`,
								`name`, `address`, `phone`, `creation_date`, `created_by`) 
							VALUES (1,$zone_id,'$name','$address','$phone','$creation_date',1)";							
		return $this->db->query($sql);							
	}
		
	public function orderTableData($distributor_code)	{	
	    	 
   /* $data = array();
    $query = $this->db->get('tbl_order');
    $res   = $query->result();        
    return $res;*/	
	/* 
	 $sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids,IF(order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),2),'Pending',
IF(order_id = '','New', 
IF(order_id!= '','Processed','zzz'))
) as order_status,order_date_entered FROM `tbl_order` where distributor_code = '".$distributor_code."' GROUP by dealer_code order by order_status asc";								
		return $this->db->query($sql)->result();	
	 */
		$sql = "SELECT order_type,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids,IF(order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),2),'Pending',
		IF(order_id = '','New', 
		IF(order_id!= '','Processed','zzz'))
		) as order_status,order_date_entered FROM `tbl_order` where distributor_code = '".$distributor_code."' GROUP by dealer_code order by order_status asc";								
	//echo $sql; die;
				return $this->db->query($sql)->result();	
	}
	
	public function getorderbyid($id,$distributor_code)
	{ //echo $id;
		/*$data = array();
		 $query = $this->db->get_where('tbl_order', array('id' => $id));
		$res   = $query->result();        
        return $res;*/
		$ids = str_replace(array('_', ' '), ',', $id);
		  $sql = "select order_name,dealer_name,group_concat(product_name) as product_name,order_id,group_concat(dispatch_by) as dispatch_by,group_concat(rejection_reason) as rejection_reason,
group_concat(quantity) as quantity,count(quantity) as quan,city,state,hub_name,group_concat(id) as ids,order_date_entered,order_confirmation_date,group_concat(avail_stock) as avail_stock,emp_name,emp_type,employee_code,billing_address
 from tbl_order where id in ($ids) and distributor_code = '".$distributor_code."' group by dealer_code";								
 
		return $this->db->query($sql)->result();
		/* 
		echo $sql;
		print_r($this->db->query($sql)->result());
		die("Jbasj"); */
	}
	
	public function check_scheme_appicable($quantity,$date_captured)
	{
		
		$sql = "select scheme_name,quantity from tbl_scheme where quantity >= '".$quantity."' and date(end_time) >= date('".$date_captured."')";
		return $this->db->query($sql)->result();
	}
	
	public function insert_order_dispatch_details($order_id, $dispatch,$dis_code,$update_avail_stock)
	{
		$cur_date = date('Y-m-d H:i:s');
	  $check_sql = "select count(*) as cnt from tbl_order_dispatch_details where order_id = '".$order_id."' and dispatch_by_id = '".$dis_code."'";
	// $checks =  $this->db->query($check_sql); 
	 $checks = $this->db->query($check_sql)->result();
	
	if($checks[0]->cnt > 0)
	{
		$ups = "update tbl_order_dispatch_details set dispatch_through = '".$dispatch."',dispatch_date = '".$cur_date."' where order_id = '".$order_id."' and dispatch_by_id = '".$dis_code."'";
		 $this->db->query($ups);
		 
		// $update_avail_stock = $this->db->query("update tbl_order set avail_stock = '".$update_avail_stock."' where id = '".$order_id."'");
	}
	else {
	 $sql = "insert into tbl_order_dispatch_details(order_id,dispatch_by_id,dispatch_through,dispatch_date) values('".$order_id."','".$dis_code."','".$dispatch."','".$cur_date."')";
	  $this->db->query($sql);		 
	 $update_avail_stock = $this->db->query("update tbl_order set avail_stock = '".$update_avail_stock."',dispatch_by = '1' where id = '".$order_id."'");
	 return true;
	}
	}
	
	public function get_all_product_dispatch_details($id,$dis_code)
	
	{
		$ids = str_replace(array('_', ' '), ',', $id);
		$sql = "select a.product_name as product_name,a.quantity,b.dispatch_through,b.dispatch_date from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where b.order_id in ($ids) and b.dispatch_by_id = '".$dis_code."'";
		return $this->db->query($sql)->result();
	}
	
	
	public function update_order_id($ids,$data,$distributor_code)
	{
		
	$get_ids_count = explode("_",$ids);
	$get_avail_stock = explode("_",$avail_stock);
	$con = count($get_ids_count);
	$ids = str_replace(array('_', ' '), ',', $ids);
	$avail_stock = str_replace(array('_', ' '), ',', $avail_stock);
	$cur_date = date('Y-m-d H:i:s');
	
	    $check_sql_dispatch = "select count(*) as dispatch_cnt from tbl_order_dispatch_details where order_id in ($ids) and dispatch_by_id = '".$distributor_code."'";
	   $checks1 = $this->db->query($check_sql_dispatch)->result();
	   
	   $check_sql_reject = "select count(*) as reject_cnt from tbl_order_rejection_details where order_id in ($ids) and rejected_by_id = '".$distributor_code."'";
	   $checks2 = $this->db->query($check_sql_reject)->result();
	   
	
	$false_data = "Enter Dispatch Details";
	if($checks1[0]->dispatch_cnt == 0 && $checks2[0]->reject_cnt == 0)
	{
		return '1';
	} else {
	   
	  $sql = "select a.id,a.product_name,a.avail_stock,a.distributor_code from tbl_order a where a.id in ($ids)";
	  $checks = $this->db->query($sql)->result();
	  $con = count($checks);
	  
	  for($j=0;$j<=$con;$j++)
	  {
		  $avl_stock = $checks[$j]->avail_stock;
		  $pro_name = $checks[$j]->product_name;
	//$ups = $this->db->query("update tbl_my_stock set my_quantity = '".$avl_stock."' where my_product_name = '".$pro_name."' and my_distributor_code = '".$checks[$j]->distributor_code."'");
	
	
	//$update_all_order_stock = $this->db->query("update tbl_order set avail_stock = '".$avl_stock."' where product_name = '".$pro_name."' and distributor_code = '".$checks[$j]->distributor_code."'");
	  }
	 $ups = "update tbl_order set order_id = '".$data['order_number']."',order_confirmation_date = '".$cur_date."' where id in ($ids) ";
	$this->db->query($ups);
	 
	
	 return '2';
	}
	
	
	
	
	}
	
	public function get_mobile_number($ids)
	
	{
		$ids = str_replace(array('_', ' '), ',', $ids);
		$sql = "select dealer_name,dealer_code,order_date_entered,distributor_code,distributor_name,order_id,
product_name,quantity,dealer_mobile,dispatch_by,emp_name,
emp_mobile from tbl_order join tbl_user on tbl_order.distributor_code = tbl_user.user_name
where  id in ($ids) and dispatch_by = 1";
		return $this->db->query($sql)->result();
		
	}
	
	public function get_rejection_sms_details($ids,$dis_code)
	
	{
		$ids = str_replace(array('_', ' '), ',', $ids);
		 $get_details = "select a.dealer_name,a.dealer_code,a.order_date_entered,a.distributor_code,tbl_user.distributor_name,
a.order_id,a.product_name,a.quantity,a.dealer_mobile,a.dispatch_by,a.emp_name,a.emp_mobile,b.rejection_reason from tbl_order a left join tbl_order_rejection_details b on a.id = b.order_id join tbl_user on a.distributor_code = tbl_user.user_name where a.id in ($ids) and a.distributor_code= '".$dis_code."' and b.order_id is not null";
		return $this->db->query($get_details)->result();
	}
	public function insert_order_rejection_details($order_id, $reject,$dis_code)
	{
		/*$data = array(
        'rejection_reason' => $reject
    );
    $this->db->where('id', $order_id);
    $this->db->update('tbl_order', $data);*/
	
	$cur_date = date('Y-m-d H:i:s');
	  $check_sql = "select count(*) as cnt from tbl_order_rejection_details where order_id = '".$order_id."' and rejected_by_id = '".$dis_code."'";
	// $checks =  $this->db->query($check_sql); 
	 $checks = $this->db->query($check_sql)->result();
	
	if($checks[0]->cnt > 0)
	{
		$ups = "update tbl_order_rejection_details set rejection_reason = '".$reject."',rejected_date = '".$cur_date."' where order_id = '".$order_id."' and rejected_by_id = '".$dis_code."'";
		 $this->db->query($ups);
	}
	else {
	 $sql = "insert into tbl_order_rejection_details(order_id,rejected_by_id,rejection_reason,rejected_date) values('".$order_id."','".$dis_code."','".$reject."','".$cur_date."')";
	 return $this->db->query($sql);		 
	}
		
	}
	
	public function update_confirm_order_rejection_details($order_id, $reject)
	
	{
		$ids = str_replace(array('_', ' '), ',', $order_id);
	$ups = "update tbl_order set rejection_reason = '".$reject."' where id in ($ids)";
	$this->db->query($ups);
		
	}
	
	//** to get details of confirm order in listview
	public function get_confirm_orders($dis_code)
	{
		/*$data = array();
		$this->db->where("order_id!= ''", null, false);
        $query = $this->db->get('tbl_order');
		$retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids FROM `tbl_order` where order_id!= '' and distributor_code = '".$dis_code."' GROUP by dealer_code";								
		return $this->db->query($sql)->result();
	}
	
	// ** to get branch order 
	
	public function get_branch_order()
	{
		/*$data = array();
		$this->db->where("order_id = ''", null, false);
        $query = $this->db->get('tbl_order');
		$retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids,IF(order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),2),'Pending',
IF(order_id = '','New', 
IF(order_id!= '','Processed','zzz'))
) as order_status FROM `tbl_order` where order_id = '' and date(order_date_entered) <= SUBDATE(CURDATE(),1) GROUP by dealer_code";								
		return $this->db->query($sql)->result();
	}
	
	// ** to get all orders on behalf of user login type
	
	public function get_order_by_type($login_type,$dis_code)
	{ 
		if($login_type == 'Branch')
		{
			/*$data = array();
		$this->db->where("order_id = ''", null, false);
        $query = $this->db->get('tbl_order');
		$retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids FROM `tbl_order` where order_id = '' GROUP by dealer_code";								
		return $this->db->query($sql)->result();
		}
		else if($login_type == 'Distributor')
		{
		/*$data = array();
        $query = $this->db->get('tbl_order');
        $retrun_result   = $query->result();        
        return $retrun_result;*/
		$sql = "SELECT order_name,dealer_name,city,state,hub_name,count(quantity) as quantity,group_concat(id) as ids FROM `tbl_order` where distributor_code = '".$dis_code."'
		GROUP by dealer_code";								
		return $this->db->query($sql)->result();
		}
	}
	
	// ** get rejection details
	
	public function get_reject_details($ids,$dis_code)
	
	{
	    $get_details = "select a.*,b.order_id,b.rejected_by_id,b.rejection_reason,b.rejected_date from tbl_order a left join tbl_order_rejection_details b on a.id = b.order_id where a.id = '".$ids."' and a.distributor_code = '".$dis_code."'";
		return $this->db->query($get_details)->result();
	}
	
	public function get_confirm_reject_details($ids)
	
	{
		$ids = str_replace(array('_', ' '), ',', $ids);
	     $get_details = "select group_concat(id) as ids,rejection_reason from tbl_order where id in ($ids)";
		return $this->db->query($get_details)->result();
	}
	
	// ** get dispatch details
	
	public function get_dispatch_details($ids,$dis_code)
	
	{
		 $get_details = "select a.*,b.order_id,b.dispatch_by_id,b.dispatch_through,b.dispatch_date from tbl_order a left join tbl_order_dispatch_details b on a.id = b.order_id where a.id = '".$ids."' and a.distributor_code = '".$dis_code."'";
		return $this->db->query($get_details)->result();
	}
	
	
	public function zoneWiseDistributorData($zone_id)  {
	     $sql = "SELECT d.`distributor_id`,  d.`name`
									FROM 	   `tbl_distributor` `d` 
								where   d.`zone_id`=$zone_id";								
		return $this->db->query($sql);				
	}
		
}

/* End of file distributors.php */
/* Location: ./application/models/distributors.php */