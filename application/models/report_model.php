x<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model 
{
	public function get_complaint_report($id)
	{
		/* //echo $id; exit;
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('service_call_id', $id);
		$this->db->order_by("service_call_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows(); */
		
		/* $query 	= $this->db->query("");
		$row 	= $query->result();
		return $row; */
		$sql = "SELECT a.contactPerson, a.caLL_case_status, a.call_id, a.callerMobile, d.customer_number_c, e.name, a.callProductSerialNo, a.symptomCode, a.symptomCode, a.defectCode, a.batteryType, a.productType, a.warranty, a.customerRemarks, a.callRegDate, b.*, c.* FROM  tbl_service_calls as a left join tbl_service_products as b on a.callAssetId = b.productGuid left join sar_dealer_cstm d on d.id_c = b.dealerName left join sar_dealer e on e.id =  b.dealerName left join tblservicecasetestreport as c on c.caseId = a.call_id where a.call_id ='".$id."'";
		//echo $sql; die("Query");
		return $this->db->query($sql)->result();
		/*  if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		} */
    }
	function get_dist_data($user)
	{
		$sql = "SELECT * FROM `tbl_user` WHERE `user_name` = '".$user."'";
		//echo $sql; die;
		return $r = $this->db->query($sql)->result();
	}
	
	function get_product_exist($prod_serial)
	{
	 	$sql = "SELECT serial_num FROM `tblProductSerialNum1` where serial_num = '".$prod_serial."'";
		//echo $sql; die;
		return $r = $this->db->query($sql)->num_rows();
	}
	function get_dist_serial($prod_serial, $usern)
	{
		$sql = "SELECT serial_num FROM `tblProductSerialNum1` where serial_num = '".$prod_serial."' and Distributor_Code='".$usern."'";
		//echo $sql; die;
		return $this->db->query($sql)->num_rows();
	}
	
	function check_ageing($prod_serial)
	{
		$sql = "SELECT a.`AGING_LIMIT` 
				FROM  `tbl_model_master1` AS a
				LEFT JOIN tblProductSerialNum1 AS b ON a.`MODEL_CODE` = b.model
				WHERE b.serial_num =  '".$prod_serial."'";
		//echo $sql; die;
		return $r = $this->db->query($sql)->result();
	}
	
	function get_cas_status($case)
	{
		$sql = "SELECT caLL_case_status FROM `tbl_service_calls` where call_id='".$case."' and caLL_case_status IN(1,2,3)";
		//echo $sql; die;
		return $r = $this->db->query($sql)->num_rows();
	}
	
	function check_status($caseId)
	{
		$this->db->select('replacementMode');
		$this->db->from('tbl_service_calls');
		$this->db->where("call_id", $caseId);
		$query = $this->db->get();
	   // echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
// Battery Test report
		function get_comp_date($case)
		{
			$sql=$this->db->query("SELECT callRegDate FROM tbl_service_calls WHERE call_id='".$case."' ");
			//echo $sql; exit;
			return $run=$sql->result();
		}
		function check_decision($case)
		{
			$sql=$this->db->query("SELECT caLL_case_status FROM tbl_service_calls WHERE call_id='".$case."' ");
			//echo $sql; exit;
			return $run=$sql->result();
		}		
		
		function status_report($case)
		{
			$sql = "SELECT caseId FROM `tblservicecasetestreport` WHERE caseId = '".$case."'"; 
			//echo $sql; die;
			$r = $this->db->query($sql)->num_rows();
		}
		
		function upd_dec($status, $case)
		{
			if($status == "1" )
			{
				$st = "1";
				$sg = "12";
				$arr1 = array('callStatus'=>$st , 'callStage'=>$sg);
				$this->db->where('call_id', $case);
				$this->db->update('tbl_service_calls',$arr1);
				if($this->db->affected_rows() > 0)
				{
					 return $this->db->affected_rows();
				}
				else
				{
					return '';
				}
			}
			if($status == "2")
			{
				$st = "2";
				$sg = "5";
				$arr1 = array('callStatus'=>$st , 'callStage'=>$sg);
				$this->db->where('call_id', $case);
				$this->db->update('tbl_service_calls',$arr1);
				if($this->db->affected_rows() > 0)
					{
						 return $this->db->affected_rows();
					}
					else
					{
						return '';
					}
			}
			if($status == "3")
			{
				$st = "2";
				$sg = "2";
				$arr1 = array('callStatus'=>$st , 'callStage'=>$sg);
				$this->db->where('call_id', $case);
				$this->db->update('tbl_service_calls',$arr1);
				if($this->db->affected_rows() > 0)
				{
					 return $this->db->affected_rows();
				}
				else
				{
					return '';
				}
			}
		}
		
		function update_status_stage($arr, $caseId)
		{
			$this->db->where('call_id', $caseId);
			$this->db->update('tbl_service_calls',$arr);
			if($this->db->affected_rows() > 0)
			{
				 return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		}
		
	function insert_batt_report($arr)
	{
		$this->db->insert('tblservicecasetestreport', $arr);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
	}
	
	function get_challan_list($user)
	{
		$sql=$this->db->query("SELECT i.id,i.name,ic.brand_c,ic.product_c,ic.model_c,ic.battery_serial_num_c,i.invoice_date 
								FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								where i.billing_account_id ='".$user."' and i.deleted = 0
								order by i.date_entered
								
								");
							//	echo $this->db->last_query(); die("Challan");
							 return $run=$sql->result_array();

	}
	/*function get_date_challan($from, $to)
	{
		$sql=$this->db->query("SELECT a.`id_c`, a.`brand_c`,b.invoice_date, a.`product_c`, a.`model_c`, a.`battery_serial_num_c`, a.`complaint_no_c`,b.name, b.billing_address_street, b.billing_address_city  ,b.billing_address_state, 
		b.billing_address_postalcode, b.billing_address_country FROM `aos_invoices_cstm` as a left join  aos_invoices as b 
		on b.id = a.`id_c`
		WHERE date(b.invoice_date) BETWEEN '".$from."' AND '".$to."'
		");
		//echo $this->db->last_query(); die("HDzbf");
		return $sql->result(); 
	}*/
	function get_date_challan($from, $to, $pname, $user)
	{
		if($pname == 1)
		{
			$pname ="2-W";
		}
		if($pname == 3)
		{
			$pname ="E-RICKSHAW";
		}
		if($pname == 4)
		{
			$pname ="IB";
		}
		if($pname == 5)
		{
			$pname ="INV";
		}
		
		$sql=$this->db->query("SELECT a.`id_c`, a.`brand_c`,b.invoice_date, a.`product_c`, a.`model_c`, a.`battery_serial_num_c`, a.`complaint_no_c`,b.name, 
		b.billing_address_street, b.billing_address_city  ,b.billing_address_state, 
		b.billing_address_postalcode, b.billing_address_country,b.total_amt,a.aprox_scrap_value_c,
        c.name as acc_nam, c.billing_address_city as prb_city, c.billing_address_state as prb_state, c.billing_address_postalcode as prb_pincode, c.billing_address_country as prb_ctry,acc_cstm.first_name_c,acc_cstm.last_name_c,acc_cstm.gst_no_c,a.gstin_number_c,a.hub_contact_person_c,a.hub_contact_email_c,a.hub_contact_phone_c,
		c.phone_office
		FROM `aos_invoices_cstm` as a left join  aos_invoices as b
		on b.id = a.`id_c` 
		LEFT JOIN accounts as c 
		on c.id = b.billing_account_id 
		left join accounts_cstm acc_cstm on c.id = acc_cstm.id_c
		WHERE date(b.invoice_date) BETWEEN '".$from."' AND '".$to."' AND  a.product_c='".$pname."' and b.billing_account_id = '".$user."' and b.deleted = 0");
		//echo $this->db->last_query(); die("    - HDzbf");
		return $sql->result(); 
	}
	
	function get_date_challan2($from, $to, $p1, $p2, $p3, $p4, $user)
	{
		$sql=$this->db->query("
		SELECT a.`id_c`, a.`brand_c`,b.invoice_date, a.`product_c`, a.`model_c`, a.`battery_serial_num_c`, a.`complaint_no_c`,b.name, 
		b.billing_address_street, b.billing_address_city  ,b.billing_address_state, 
		b.billing_address_postalcode, b.billing_address_country,b.total_amt,a.aprox_scrap_value_c,
        c.name as acc_nam, c.billing_address_city as prb_city, c.billing_address_state as prb_state, c.billing_address_postalcode as prb_pincode, c.billing_address_country as prb_ctry,acc_cstm.first_name_c,acc_cstm.last_name_c,acc_cstm.gst_no_c,a.gstin_number_c,a.hub_contact_person_c,a.hub_contact_email_c,a.hub_contact_phone_c,
		c.phone_office
		FROM `aos_invoices_cstm` as a left join  aos_invoices as b
		on b.id = a.`id_c` 
		LEFT JOIN accounts as c 
		on c.id = b.billing_account_id
		left join accounts_cstm acc_cstm on c.id = acc_cstm.id_c
		WHERE date(b.invoice_date) BETWEEN '".$from."' AND '".$to."' 
		AND  a.product_c='".$p1."' or a.product_c='".$p2."' or a.product_c='".$p3."' or a.product_c='".$p4."' 
		AND b.billing_account_id = '".$user."' and b.deleted = 0");
		//echo $this->db->last_query(); die("    - HDzbf");
		return $sql->result(); 
	}
	
	
	
	
	function get_challan_details($id)
	{
		$sql=$this->db->query("
		SELECT i.*,ic.brand_c,ic.product_c,ic.model_c,ic.battery_serial_num_c,ic.complaint_no_c,i.invoice_date,ac.phone_office,acc_cstm.first_name_c,acc_cstm.last_name_c,acc_cstm.gst_no_c,
		ac.name,ac.date_entered,ac.billing_address_city,ac.	billing_address_state,ac.billing_address_postalcode,ac.billing_address_country, i.total_amt,i.billing_address_street as hub_address,ic.gstin_number_c,ic.hub_contact_person_c,ic.hub_contact_email_c,ic.hub_contact_phone_c,i.billing_address_postalcode as hub_postal,
		ac.phone_office, ic.aprox_scrap_value_c  
								FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								left join accounts as ac
								on ac.id = i.billing_account_id
								left join accounts_cstm acc_cstm on ac.id = acc_cstm.id_c
								WHERE i.id='".$id."' and i.deleted = 0
								");
								//echo $this->db->last_query(); die("jhbdf");
								
							 return $run=$sql->result_array();

	}
	
	function update_replace_mod($rep, $case)
	{
		
	}
	
	/*Verify Product serial num*/
	function get_serial_case($case)
	{
		$sql=$this->db->query("SELECT callProductSerialNo FROM tbl_service_calls WHERE call_id='".$case."'");
	  //echo $this->db->last_query(); die("jhbdf");
	  return $run=$sql->result();
	}
	
	function block_serial($sr_no, $user)
	{
		function getGUID()
			 {
						if (function_exists('com_create_guid'))
						{
							return com_create_guid();
						}
						else
						{
							mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$uuid = substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12);
							return $uuid;
						}
			}
					$guid = getGUID();
					
	//	echo $old_sr;
		$date = date("Y-m-d H:i:s");
		$st = "inactive";
		$data = array(
						'id' => $guid,
						'name' => $sr_no,
						'date_entered' => $date,
						'date_modified' => $date,
						'modified_user_id' => $user,
						'created_by' => $user,
						'status' => $st
					);
			
			$this->db->insert('sar_serial_master', $data);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		//}	
	}
	
	function get_serial_data($case, $user)
	{
	  $sql=$this->db->query("SELECT callProductSerialNo, callRegDate, callAssetId, callCreatedBy, callCustId FROM tbl_service_calls WHERE call_id='".$case."' && callCreatedBy='".$user."'");
	  //echo $this->db->last_query(); die("jhbdf");
	  return $run=$sql->result();
	}
	function check_blocking($prod_serial)
	{
		$sql=$this->db->query("SELECT  `name` FROM `sar_serial_master` WHERE `name` ='".$prod_serial."' ");
			//echo $sql; exit;
		return $run=$sql->result();	
	}
	
	/*Verify Product serial num*/
	
	function update_decision($data, $case)
	{
			// $case = $data['call_id']; 
		// echo $case; die;
		//echo "<pre>"; print_r($data); die;
		$this->db->where('call_id', $case);
		$this->db->update('tbl_service_calls',$data);
		if($this->db->affected_rows() > 0)
			{
				 return $data['caLL_case_status'];
			}
			else
			{
				return '';
			}
	}
	
	
     function get_excel_data_range($user, $dat1, $dat2)
	 {/* 
		  $sql = "select a.*,	 b.*,	 d.*,	 e.*,	 h.Symptoms as Symptom_Code,	 h.Defect as Defect_Code  from tbl_service_calls a left join tbl_service_customers b on a.callCustId=b.cust_code  left join tblservicecasetestreport d  on d.caseId=a.call_id LEFT JOIN tbl_service_replacement e on e.caseId = a.call_id  left join tbl_defect_symptoms h on h.id = a.symptomCode where a.callCreatedBy = '".$user."' and DATE(a.callRegDate) between '".$dat1."' and '".$dat2."' group by a.call_id";
		  echo $sql; die("kjdbnfkj");
		 $sql = $this->db->query("select a.*, b.*, c.*, d.*, e.*, f.name as Call_status, g.name as Call_stage, h.Symptoms as Symptom_Code, h.Defect as Defect_Code  from tbl_service_calls a left join tbl_service_customers b on a.callCustId=b.cust_code left join tbl_service_products c on a.callAssetId = c.productGuid left join tblservicecasetestreport d  on d.caseId=a.call_id LEFT JOIN tbl_service_replacement e on e.caseId = a.call_id left join tbl_service_call_stage f on a.callStatus=f.id  left join tbl_service_call_status g on a.callStage = g.id left join tbl_defect_symptoms h on h.id = a.symptomCode where a.callCreatedBy = '".$user."' and DATE(a.callRegDate) between '".$dat1."' and '".$dat2."' group by a.call_id");  
		  */
		  $sql = $this->db->query("select a.*,	 b.*,	 d.*,	 e.*,	 h.Symptoms as Symptom_Code,	 h.Defect as Defect_Code  from tbl_service_calls a left join tbl_service_customers b on a.callCustId=b.cust_code  left join tblservicecasetestreport d  on d.caseId=a.call_id LEFT JOIN tbl_service_replacement e on e.caseId = a.call_id  left join tbl_defect_symptoms h on h.id = a.symptomCode where a.callCreatedBy = '".$user."' and DATE(a.callRegDate) between '".$dat1."' and '".$dat2."' group by a.call_id");
			//$sql = $this->db->query("SELECT  * FROM `tbl_service_calls` WHERE `call_id` = 'C2018041706040651'"); 
		//  echo $this->db->last_query(); die("Query");
			// return $result=$sql->result();
			
		return $result=$sql->result_array();
	 }
	
	function get_case_status($case)
	{
		// $status = "inactive";
		$this->db->select('caLL_case_status');
		$this->db->from('tbl_service_calls');
		// $this->db->order_by("service_call_id", "desc");
		$this->db->where('call_id', $case);
		// $this->db->where('caLL_case_status', $case);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	function get_serial_info($prod_serial)
	{
		$this->db->select('productSerialNo');
		$this->db->from('tbl_service_products');
		// $this->db->order_by("service_call_id", "desc");
		$this->db->where('productSerialNo', $prod_serial);
		// $this->db->where('caLL_case_status', $case);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	
	function update_replace_settle($mode, $others, $date, $case, $user)
	{
		//echo "Hikjdnfkajnfkj"; print_r($data);
		//$case = $data['call_id']; 
		// echo $case; die;
		//print_r($data['caLL_case_status']); die;
		$arr = array(
		             'replacementMode' => $mode,
		             'otherRemarks' => $others,
		             'dateOfReplacementRemark' => $date
		);
		$this->db->where('callCreatedBy', $user);
		$this->db->where('call_id', $case);
		$this->db->update('tbl_service_calls', $arr);
		return $this->db->affected_rows();
	}
	function get_symptom_code()
	{
		$this->db->select('*');
		$this->db->from('tbl_defect_symptoms');
		// $this->db->order_by("service_call_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
	function insert_fault_parts($data, $data_pb)
	{
		/* echo "<pre>"; print_r($data); 
		echo "<pre>"; print_r($data_pb);
        echo $data['caseId']; 		
		die; */
		//echo $data['caseid']; die;
		   
		   
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('call_id', $data['caseId']);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
				$this->db->update('tbl_service_calls', $data_pb);
				$this->db->where('call_id', $data['caseId']); 
				if($this->db->affected_rows() > 0)
				{
					 return $data['distributorActionCode'];
				}
				else
				{
					return '';
				}
		}
		else
		{
			$this->db->insert('tbl_service_faults_parts', $data);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		}
		   
		   
		  /*  
		  // echo $this->db->affected_rows();
		   
			$this->db->insert('tbl_service_faults_parts', $data);
			if($this->db->affected_rows() > 0)
			{
				$data['caseId']; die("Affected Row");
				$this->db->update('tbl_service_calls', $data_pb);
				$this->db->where('call_id', $data['caseId']); 
				//$this->db->get();
				//echo $this->db->last_query();
				if($this->db->affected_rows() > 0)
				{
					 return $data['distributorActionCode'];
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			} */
		  
	}
	
	function insert_replace_details($battery)
	{
		$this->db->insert('tbl_service_replacement', $battery);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}	
		
	}
	
	function get_product_type($old_sr)
	{
		 $sql = $this->db->query("SELECT  `product` FROM `tblProductSerialNum1` WHERE `serial_num` = '".$old_sr."'"); 
		 //echo $this->db->last_query(); die("Query");
			return $result=$sql->result();
	}
	function get_new_product($prod_serial)
	{
		
		 $sql = $this->db->query("SELECT  `product` FROM `tblProductSerialNum1` WHERE `serial_num` = '".$prod_serial."'"); 
		 //echo $this->db->last_query(); die("Query");
			return $result=$sql->result();
	}
	function check_replcaement($case, $user)
	{
		$sql = $this->db->query("SELECT `replacementMode`  FROM `tbl_service_calls` WHERE `call_id` = '".$case."' && callCreatedBy='".$user."' "); 
		// echo $this->db->last_query(); die("Query");
			return $result=$sql->result();
	}
	 
	function check_replcaement_ag($case, $user)
	{
		$sql = $this->db->query("SELECT `productSerialNo` FROM `tbl_service_replacement` WHERE `submittedByDist`='".$user."' && `caseId` = '".$case."'"); 
		// echo $this->db->last_query(); die("Query");
		return $result=$sql->result();
	}
	 
		public function serial_verify_data($prod_serial)
		{
			 $sql = $this->db->query("
				SELECT * FROM tblProductSerialNum1 tbl1 
				LEFT JOIN tbl_model_master1 tbl2 
				ON tbl1.model = tbl2.MODEL_CODE 
				WHERE tbl1.serial_num = '".$prod_serial."'"); 
			return $result=$sql->result();
		}
		
		function get_product_status($prod_serial)
	    {
			$query = $this->db->query("SELECT name,status FROM `sar_serial_master` WHERE `status` = 'inactive' AND `name` ='".$prod_serial."'");
			   // echo $this->db->last_query();  die;
			return $query->num_rows(); 
	    }
		function get_serial_num($case)
	    {
				$query = $this->db->query("SELECT callProductSerialNo FROM `tbl_service_calls` WHERE `call_id`='".$case."' ");
			  	return $query->result(); 
	    }
		function update_sar_serial($master)
	    {
			$this->db->insert('sar_serial_master', $master);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}	
				
	    }
		
		
		public function serial_verify_data_case($caseId){
			$sql = $this->db->query("SELECT cust_code,CONCAT(fname,' ',lname) custnm FROM tbl_service_customers tbl1
								LEFT JOIN tbl_service_calls tbl2
								ON tbl1.cust_code = tbl2.callCustId
								WHERE call_id='".$caseId."'");
			return $result=$sql->result();
			
		}	
function getfault_detail($case,$user)
{
	//echo $case; die("sjdbh");
    $this->db->select('*');
		$this->db->from(' tbl_service_replacement');
		$this->db->where('caseId', $case);
		$this->db->where('submittedByDist', $user);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
}	
	
	
	
	public function case_report($data)
	{
	    $save = $this->db->insert('tblservicecasetestreport', $data);
		if($this->db->affected_rows() > 0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}		
	}
	
     function get_case_details($id)
	 {
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('service_call_id', $id);
		$this->db->order_by("service_call_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}

	 }
	 function get_cust_data($consumerId)
	 {
		 $sql = "select * from  tbl_service_customers where cust_code='".$consumerId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
	 }
	 function get_dealerName($dealerCode)
	 {
		 $sql = "SELECT `customer_number_c` FROM  `sar_dealer_cstm` WHERE  `id_c` = '".$dealerCode."' ";
					return  $this->db->query($sql)->row();	
	 }
	 function get_cust_prod($consumerId)
	 {
		 $sql = "select * from tbl_service_products where consumerId='".$consumerId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
	 }
	 function getTestData($us, $caseId)
	 {
		 $sql = "SELECT * FROM `tblservicecasetestreport` as a left join tbl_service_calls as b on b.call_id = a.caseId where a.submittedByDist= '".$us."' && a.caseId ='".$caseId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();
	 }
	 function update_wrrnty_details($caseId)
		{
			$sql = "select * from tbl_service_calls where call_id='".$caseId."'";
			//echo $sql; die;
			return  $this->db->query($sql)->result();	
		}
		public function products_detail()
		{
				$sql = "SELECT * FROM `tblProductSerialNum1` group by `product` ";
					return  $this->db->query($sql)->result();	
		}
}

/* End of file user.php */
/* Location: ./application/models/user.php */
