<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_punch_model extends CI_Model 
{

	public function insert_fos($data)
	{
		echo "Heelll<br/>"; //die;
		//echo "<pre>"; print_r($data); die;
	   $this->db->insert('tbl_service_fos', $data);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
	}
	function get_product_det($id,$segment_id){ 
		
		$sql = "select a.id,a.aos_product_category_id,a.name as model_code,a.part_number,d.name from aos_products a left join aos_products_cstm b on a.id = b.id_c
left join sar_segment_aos_products_1_c c on a.id = c.sar_segment_aos_products_1aos_products_idb
left join sar_segment d on c.sar_segment_aos_products_1sar_segment_ida = d.id
where a.deleted = 0 and a.aos_product_category_id = '".$id."'
and d.id in ('".$segment_id."')";
		$resArr = $this->db->query($sql)->result_array();
        /*$this->db->select('*');
		$this->db->from('aos_products');
		$this->db->where('aos_product_category_id', $id); 
		$query = $this->db->get();
		$resArr=$query->result_array();*/
		//$query->result(); 
		// print_r($query->result()); //exit;
		//echo $this->db->last_query(); die;
		 if(sizeof($resArr)>0){
			return $resArr;
		}
		else{
			return '';
		}
	}
	
	public function get_distributor_name($distributor_code){
		$this->db->select('distinct(b.name) as dis_name');
		$this->db->from('sar_segment_accounts_1_c as a');
		$this->db->join('sar_segment as b','a.sar_segment_accounts_1sar_segment_ida = b.id','right');
		$this->db->where('a.sar_segment_accounts_1sar_segment_ida', $distributor_code);
		$query = $this->db->get();
		//return $query->result();
		echo $this->db->last_query(); die;
	}
	
	function get_distributor_segmentid($distributor_code){ 
		$sql = "select b.sar_segment_accounts_1sar_segment_ida as segment_id from accounts a left join sar_segment_accounts_1_c b on a.id = b.sar_segment_accounts_1accounts_idb
where a.id = '".$distributor_code."'";
		return $this->db->query($sql)->result();
	}
	
	function get_product_data($prod_type)
	{
        $this->db->select('*');
		$this->db->from('tbl_model_master');
		$this->db->where('PRODUCT', $prod_type); 
		$query = $this->db->get();
		$resArr=$query->result();
		//$query->result(); 
		// sprint_r($query->result()); //exit;
		//echo $this->db->last_query(); die;
		 if(sizeof($resArr)>0){
			return $resArr;
		}
		else{
			return '';
		}
	}
	function insert_punch_comp($quantity, $moc_val, $mod_val, $user_uuid, $prod_type)
	{
		 $this->db->select('*');
		 $this->db->from('tbl_sales_comp_punch');
		 $this->db->where('model_code', $moc_val);
		 $this->db->where('punchedBy', $user_uuid);
		  $this->db->where('orderPunchedFor', '1');
		 $query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		if($query->num_rows()>0)
		{
			$data = array(
							'model_quantity' => $quantity
						 );
			//$this->db->update('tbl_sales_comp_punch', $data);
			$this->db->where('model_code', $moc_val);
			$this->db->where('punchedBy', $user_uuid);
			$this->db->update('tbl_sales_comp_punch', $data);
			$row = $this->db->affected_rows();
			return $prod_type;
		}
		else
		{
			
			$data = array(
			   'model_quantity' => $quantity,
			   'model_code' => $moc_val,
			   'model_desc' => $mod_val,
			   'punchedBy' => $user_uuid,
			   'product_type' => $prod_type,
			   'orderPunchedFor' => '1'
			);
			//print_r($data); die;
			$this->db->insert('tbl_sales_comp_punch', $data);
			$row = $this->db->affected_rows();
			return $prod_type;
		}
	}
	
	function insert_punch_dist($quantity, $moc_val, $mod_val, $user_uuid, $prod_type)
	{
		 $this->db->select('*');
		 $this->db->from('tbl_sales_dist_punch');
		 $this->db->where('model_code', $moc_val);
		 $this->db->where('dist_puched_by', $user_uuid);
		 $this->db->where('orderPunchedFor', '2');
		 $query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		if($query->num_rows()>0)
		{
			$data = array(
							'model_quantity' => $quantity
						 );
			//$this->db->update('tbl_sales_dist_punch', $data);
			$this->db->where('model_code', $moc_val);
			$this->db->where('dist_puched_by', $user_uuid);
			$this->db->update('tbl_sales_dist_punch', $data);
			$row = $this->db->affected_rows();
			return $prod_type;
		}
		else
		{
			
			$data = array(
			   'model_quantity' => $quantity,
			   'model_code' => $moc_val,
			   'model_desc' => $mod_val,
			   'dist_puched_by' => $user_uuid,
			   'prod_type' => $prod_type,
			   'orderPunchedFor' => '2'
			);
			//print_r($data); die;
			$this->db->insert('tbl_sales_dist_punch', $data);
			$row = $this->db->affected_rows();
			return $prod_type;
		}
	}
	
	/* function insert_punch_dist($quantity, $moc_val, $mod_val, $user_uuid, $prod_type)
	{
		 $this->db->select('*');
		 $this->db->from('tbl_sales_comp_punch');
		 $this->db->where('model_code', $moc_val);
		 $this->db->where('punchedBy', $user_uuid);
		 $query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		if($query->num_rows()>0)
		{
			$data = array(
							'model_quantity' => $quantity
						 );
			//$this->db->update('tbl_sales_comp_punch', $data);
			$this->db->where('model_code', $moc_val);
			$this->db->where('punchedBy', $user_uuid);
			$this->db->update('tbl_sales_comp_punch', $data);
			$row = $this->db->affected_rows();
			return $prod_type;
		}
		else
		{
			
			$data = array(
			   'model_quantity' => $quantity,
			   'model_code' => $moc_val,
			   'model_desc' => $mod_val,
			   'punchedBy' => $user_uuid,
			   'product_type' => $prod_type,
			   'orderPunchedFor' => '1'
			);
			//print_r($data); die;
			$this->db->insert('tbl_sales_comp_punch', $data);
			$row = $this->db->affected_rows();
			return $prod_type;
		}
	}
 */	
	
	function get_product_type()
	{
        $this->db->select('id,name');
		$this->db->from('aos_product_categories');
		 $this->db->group_by('name'); 
		 $query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
    function get_prod_model($prod)
	{
        $this->db->select('*');
		$this->db->from('tbl_model_master');
		 $this->db->group_by('PRODUCT'); 
		 $query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	function get_punch_list($user_uuid)
	{
		//echo "Hello"; 
		 $this->db->select('*');	
	    $this->db->from('tbl_service_dealer_sales');	
	    $this->db->where('distUUID', $user_uuid);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		//print_r($query->result());
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
	function view_punch_detail($user_uuid)
	{
		//echo "Hello"; 
		 $this->db->select('*');	
	    $this->db->from('tbl_service_dealer_sales');	
	    $this->db->where('distUUID', $user_uuid);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		//print_r($query->result());
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
	function get_punch_list_dc($user_uuid)
	{
		$this->db->select('*');	
	    $this->db->from('tbl_sales_dist_cmp');	
	    $this->db->where('distUUID', $user_uuid);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}	
	
	public function insert_order_punch($data)
	{
		//echo "<pre>"; print_r($data); die;
		 $sql=$this->db->insert('tbl_order', $data);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows($sql) >0)
		{
			return $this->db->affected_rows($sql);
		}
		else
		{
			return '';
		}
	}	
	public function insert_secorder_punch($dataSec)
	{
		//echo "<pre>"; print_r($dataSec); die;
		 $this->db->insert('tbl_order', $dataSec);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}	
	
	public function insert_order_punch_dc($data)
	{
		//ini_set('display_errors','1');
		$sql=$this->db->insert('order_primary_order_dms_cstm', $data);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows($sql) >0)
		{
			return $this->db->affected_rows($sql);
		}
		else
		{
			return '';
		}
	}
	
	public function insert_order_punch_dms($data)
	{
		//echo "<pre>"; print_r($data); die;
		 $sql=$this->db->insert('order_primary_order_dms', $data);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}
	
	public function insert_priorder_punch_dc($dataPri)
	{
		//echo "<pre>"; print_r($data); die;
		 $this->db->insert('tbl_order', $dataPri);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}	
	function get_fos($user)
	{
	    $this->db->select('*');	
	    $this->db->from('tbl_service_fos');	
	    $this->db->where('createdBy', $user);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
}

/* End of file user.php */
/* Location: ./application/models/user.php */