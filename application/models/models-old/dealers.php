<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dealers extends CI_Model     
{

	public function addDistributorActionData( $zone_id   , $name , $address , $phone , $creation_date)	{	
	    $sql = "INSERT INTO `tbl_distributor`(`user_id`, `zone_id`,
								`name`, `address`, `phone`, `creation_date`, `created_by`) 
							VALUES (1,$zone_id,'$name','$address','$phone','$creation_date',1)";							
		return $this->db->query($sql);							
	}
		
	public function dealerTableData($distributor_code)	{	
	    	  $sql = "select dealer_name ,dealer_type,billing_city,billing_state,billing_postalcode,billing_country,mobile,customer_number,channel_category,channel_sub_category,full_name,hub_name,hub_code,parent_id,date_entered,count(id) as quantity,beat_day,next_visit_date  from tbl_dealer where parent_id = '".$distributor_code."' group by customer_number";
						
			return $this->db->query($sql)->result()	;					
	}
	
	public function zoneWiseDistributorData($zone_id)  {
	     $sql = "SELECT d.`distributor_id`,  d.`name`
									FROM 	   `tbl_distributor` `d` 
								where   d.`zone_id`=$zone_id";								
		return $this->db->query($sql);				
	}
		
}

/* End of file distributors.php */
/* Location: ./application/models/distributors.php */