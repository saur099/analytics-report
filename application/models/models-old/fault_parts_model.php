<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fault_parts_model extends CI_Model 
{
	public function get_parts_details($id)
	{
		//echo $id; exit;
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('service_call_id', $id);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}

    }
	
	function insert_fault_parts($data, $data_pb)
	{
		//print_r(); die;
		//echo $data['caseid']; die;
		   
		  // echo $this->db->affected_rows();
		   
			$this->db->insert('tbl_service_faults_parts', $data);
			if($this->db->affected_rows() > 0)
			{
				//echo $data['caseId']; die("jsdbg");
				$this->db->update('tbl_service_calls', $data_pb);
				$this->db->where('call_id', $data['caseId']); 
				//$this->db->get();
				//echo $this->db->last_query();
				if($this->db->affected_rows() > 0)
				{
					 return $data['distributorActionCode'];
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			}
		  
	}
	
	function insert_battery_details($id)
	{
	      
	}
	
     function get_case_details($id)
	 {
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('service_call_id', $id);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}

	 }
}

/* End of file user.php */
/* Location: ./application/models/user.php */