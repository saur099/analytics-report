<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Return_dealer_model extends CI_Model 
{

	
	function get_dealer_list($user)
	{
		 $this->db->select('*');	
	    $this->db->from('tbl_return_stock');	
	    $this->db->where('distName', $user);
	    $this->db->order_by("return_stock_id", "desc");
		
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}	
	
	
	public function insert_dealer_list($data)
	{
		//echo "<pre>"; print_r($data); die;
		$this->db->insert('tbl_return_stock', $data);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
	   
	}	
	
	
}

/* End of file user.php */
/* Location: ./application/models/user.php */