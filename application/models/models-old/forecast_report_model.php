<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forecast_report_model extends CI_Model 
{

	
	function get_forecast_details($user)
	{
		
		 $this->db->select('*');	
	    $this->db->from('tbl_forecast');	
	    $this->db->where('f_user', $user);
	    $this->db->order_by("forecastId", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); // die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}
	}	
	

	
	function get_forecast_report($m1, $m2, $m3, $user)
	{
		$sql = "SELECT * FROM `tbl_forecast` WHERE  `forecast_month` ='".$m1."' OR `forecast_month`='".$m2."'  Or `forecast_month`= '".$m3."' AND `f_user`='".$user."'";
         $query = $this->db->query($sql);
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}
	}
	
	
	public function insert_forecast_report($data)
	{
		//echo "<pre>"; print_r($data); die;
		$this->db->insert('tbl_forecast', $data);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
	   
	}	
	
	
}

