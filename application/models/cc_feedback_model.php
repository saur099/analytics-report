<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cc_feedback_model extends CI_Model 
{

	public function view_feedback_details($id)
	{ 
	    	 $sql = "SELECT  *  FROM  `tbl_cc_feedback`  
								WHERE `isActiveFeedback`= 1 
								and `josheet_status` = 'Closed'  and js_feedback_id =".$id."  LIMIT 1";	
							//	and `josheet_status` = 1  and feedbackAsseignedTo ='".$user_id."' and js_feedback_id = ".$id." LIMIT 1";	
             //echo $sql; die;								
			$res = 	$this->db->query($sql)->result();				
			return  $res;
		// die("Hello"); die;                                       
		/*															
			$this->db->where(array('user_name' => $user_name));		
			$query = $this->db->get('tbl_user');						
			$retrun_result   = $query->result();        				
			return $retrun_result;											
		*/                                                          
	}
	
	function verify_feedback($feedbackId, $user_id, $upd_feed, $upd_js, $js_no, $ins_remark)
	{
			$this->db->where('js_feedback_id', $feedbackId);
			$this->db->update('tbl_cc_feedback', $upd_feed);
			//echo $this->db->last_query();
			if($this->db->affected_rows() > 0)
			{
				$this->db->where('job_sheet', $js_no);
				$this->db->update('tbl_cc_products', $upd_js);
				//echo $this->db->last_query();
				if($this->db->affected_rows() > 0)
				{
					$this->db->insert('tbl_cc_feedback_remarks', $ins_remark);
					//echo $this->db->last_query();
					//die;
					if($this->db->affected_rows() > 0)
					{
						return $this->db->affected_rows();
					}
					else
					{
						return 0;
					}	
				}
				else
				{
					return 0;
				}					
			}
			else
			{
				return 0;
			}
	}
	
	function is_feedback_given($feedbackId)
	{
			$sql = "SELECT `final_closure` FROM `tbl_cc_feedback` WHERE `js_feedback_id`=".$feedbackId;	
            //echo $sql; die;								
			$res = 	$this->db->query($sql)->result();				
			return  $res;
	}
	function get_js($id)
	{
			$this->db->select('jobsheet_no');
			$this->db->from('tbl_cc_feedback');
			$this->db->where('js_feedback_id', $id);
			$query = $this->db->get();
			 if($query->num_rows()>0)
			{
				return $query->result();
			}
			else
			{
				return '';
			}
	}
	function get_sales_report($id)
	{
			$sql = "SELECT `first_name`, `last_name` FROM `tbl_user` WHERE `user_uuid`='".$id."'";	
             //echo $sql; die;								
			$res = 	$this->db->query($sql)->result();				
			return  $res;
	}
	function get_service_report($id)
	{
			$sql = "select count(call_id) cases from tbl_service_calls group by(caLL_case_status)";	
            //echo $sql; die;								
			$res = 	$this->db->query($sql)->result();				
			return  $res;
	}	
	public function question_dits(){
		$this->db->select('*');
		$this->db->from('tbl_question');
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function reviews_insert($data){
		return $this->db->insert('tbl_reviews', $data);
	}
	
	public function check_distributor_date($user){ //echo $user; exit
		$this->db->select('review_date,username');
		$this->db->from('tbl_reviews');
		$this->db->where('MONTH(`review_date`) = MONTH(CURDATE())and YEAR(`review_date`)=YEAR(CURDATE())');
		$this->db->where('username',$user);
		$query = $this->db->get();
		return $query->result();
		//SELECT * FROM tbl_reviews WHERE MONTH(`review_date`) = MONTH(CURDATE()) and YEAR(`review_date`)=YEAR(CURDATE())
	}
}

/* End of file user.php */
/* Location: ./application/models/user.php */