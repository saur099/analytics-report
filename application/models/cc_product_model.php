<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cc_product_model extends CI_Model 
{	
	public function getCustomerInfo($id)
	{
		return $this->db->select('*')->get_where('tbl_cc_consumer', array('cust_id' => $id))->result();
	}
	
	public function save_jobsheet($data)
	{
		$this->db->insert('tbl_cc_products',$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}
        function get_engg_detail($engg_info)
        {
            $this->db->select('id, first_name, last_name');
            $this->db->from('users');
            $this->db->where('id', $engg_info);
            $this->db->where('deleted', 0);
            $res=$this->db->get();
           // echo $this->db->last_query();
            return $res->result();
        }
        function get_sdr_details_m($prod_id, $engg_det_id)
        {
            $this->db->select('c.symptom_c, c.symptom_code_c, c.id_c');
            $this->db->from('aos_products_sar_sdr_1_c a');
            $this->db->join('sar_sdr b', 'a.aos_products_sar_sdr_1sar_sdr_idb = b.id', 'left');
            $this->db->join('sar_sdr_cstm c', 'b.id = c.id_c', 'left');
            $this->db->where('a.aos_products_sar_sdr_1aos_products_ida', $prod_id);
            $this->db->where('b.deleted', 0);
            $this->db->group_by('c.symptom_c');
            $res=$this->db->get();
            //echo $this->db->last_query();
            if($res->num_rows()>0)
            {
            	
                return $res->result_array();
            }
            else 
            {
                return '';
            }
            
        }
        
        function getSerialDetails($serial)

        {
            $sql = "select a.id,b.sap_code_c,b.aos_products_id_c, b.account_code_c ,c.aos_product_category_id, c.name as prod_name,
                    d.name as prod_category_name,f.name as brand_name,b.primary_sale_date_c,b.account_name_c  from sar_product_serial_number a 
                    left join sar_product_serial_number_cstm b on a.id = b.id_c left join aos_products c on b.aos_products_id_c = c.id 
                    left join aos_product_categories d on c.aos_product_category_id = d.id left join sar_segment_aos_products_1_c e 
                    on e.sar_segment_aos_products_1aos_products_idb = c.id left join sar_segment f 
                    on f.id = e.sar_segment_aos_products_1sar_segment_ida where a.deleted = 0 and a.name = '".$serial."'";
            $res = $this->db->query($sql);
            return $res->result_array();
        }
		function verify_serial_no($asn)
        {
            $this->db->where('name', $asn);
            $this->db->where('deleted', 0);
            $this->db->select('name');
            $this->db->from('sar_serial_master');
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
               return 'z';
            }
            else
            {
                
                $this->db->where('a.product_serial_no_c', $asn);
                $this->db->where('b.deleted', 0);
                $this->db->select('a.id_c');
                $this->db->join('sar_asset b', 'b.id = a.id_c', 'left');
                $this->db->from('sar_asset_cstm a');
                $query = $this->db->get();
               // echo $this->db->last_query();
                if($query->num_rows()>0)
                {
                     return 'y';
                }
                else 
                {
                    $this->db->where('name', $asn);
                    $this->db->where('deleted', 0);
                    $this->db->select('name');
                    $this->db->from('sar_product_serial_number');
                    $query1 = $this->db->get();
                    //echo $this->db->last_query(); //die;
                    //print_r($query1->result());
                    if($query1->num_rows()>0)
                    {
                        //echo "1"; die;
                        return $asn;
                    }
                    else 
                    {
                        //echo "0"; die;
                        return 'x';
                    }
                }
            }
        }
	function find_prod_data($d)
	{
		$this->db->select('*');
		$this->db->from('tbl_cc_products');
		$this->db->where('cc_product_id', $d);
		$this->db->where('isActiveProduct', 1);
		$res =  $this->db->get();
		//echo $this->db->last_query();
		return $res->result();
	}
	
	public function get_category($name)
	{
		$res = $this->db->query("SELECT * FROM aos_product_categories where deleted=0 and name in ($name)");
		//echo $this->db->last_query();
		return $res->result();
	}
	
	public function get_product($cate_id)
	{
		$this->db->select('*');
		$this->db->join('aos_products_cstm','aos_products_cstm.id_c=aos_products.id');
		$this->db->where('aos_product_category_id',$cate_id);
		$res = $this->db->get('aos_products');
		//echo $this->db->last_query();
		return $res->result();
	}
	
	public function getJSInfo($js_id)
	{
		$this->db->select('tbl_cc_products.*,aos_products_cstm.warranty_c, aos_product_categories.name as cate_name,aos_products.name as sub_cate_name');
		$this->db->from('tbl_cc_products');
		$this->db->join('aos_product_categories','aos_product_categories.id = tbl_cc_products.product_type_id');
		$this->db->join('aos_products','aos_products.id = tbl_cc_products.product_sub_type_id');
		$this->db->join('aos_products_cstm','aos_products_cstm.id_c = tbl_cc_products.product_sub_type_id');
		$this->db->where('js_case_id', $js_id);
		$res =  $this->db->get();
		//echo $this->db->last_query();
		return $res->result();
		//return $this->db->select('*')->get_where('tbl_cc_products', array('js_case_id' => $js_id))->result();
	}
	
	public function save_complaint($data)
	{
		$this->db->insert('tbl_cc_complain',$data);
		//echo $this->db->last_query();exit;
		return $this->db->insert_id();
	}
	
	
	public function getComplaintInfo($complaintID)
	{
		$this->db->select('tbl_cc_complain.*,tbl_cc_consumer.*');
		$this->db->from('tbl_cc_complain');
		$this->db->where('cc_complain_no', $complaintID);
		$this->db->join('tbl_cc_consumer','tbl_cc_consumer.cust_id=tbl_cc_complain.cust_id');
		$res =  $this->db->get();
		//echo $this->db->last_query();
		return $res->result();
	}
	
	public function getJS($jsID)
	{
		$this->db->select('tbl_cc_products.*,tbl_cc_consumer.*,aos_product_categories.name as cate_name,aos_products.name as sub_cate_name');
		$this->db->from('tbl_cc_products');
		$this->db->where('cc_product_id', $jsID);
		$this->db->join('tbl_cc_consumer','tbl_cc_consumer.cust_id=tbl_cc_products.cust_id');
		$this->db->join('aos_product_categories','aos_product_categories.id = tbl_cc_products.product_type_id');
		$this->db->join('aos_products','aos_products.id = tbl_cc_products.product_sub_type_id');
		$res =  $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result();
	}
	
	public function comp_info($comp_id)
	{
		$this->db->select('*');
		$this->db->from('tbl_cc_complain');
		$this->db->where('cc_case_id', $comp_id);
		$res =  $this->db->get();
		//echo $this->db->last_query();
		return $res->result();
	}
	
	public function getWarranty($subCateID)
	{
		return $this->db->select('warranty_c')->get_where('aos_products_cstm', array('id_c' => $subCateID))->result();
		//echo $this->db->last_query();
	}
	
	public function updateJobSheet($cc_product_id, $data)
	{
		$this->db->where('cc_product_id',$cc_product_id); 
		return $this->db->update('tbl_cc_products', $data);
	}
	
	public function saveFeedback($data)
	{
		$this->db->insert('tbl_cc_feedback',$data);
		return $this->db->insert_id();
	}
	
	public function saveSDR($data)
	{
		$this->db->insert('tbl_cc_sdr',$data);
		return $this->db->insert_id();
	}
	
	
	public function getSymptemList()
	{
		return $this->db->query("SELECT sar_sdr.id,sar_sdr_cstm.symptom_c FROM sar_sdr RIGHT JOIN sar_sdr_cstm on sar_sdr.id=sar_sdr_cstm.id_c GROUP BY sar_sdr_cstm.symptom_c")->result();
	}
	
	public function getDRList($symtoms_name)
	{
		return $this->db->query("SELECT sar_sdr.id,sar_sdr_cstm.repair_c,sar_sdr_cstm.defect_c,sar_sdr_cstm.defect_code_c FROM sar_sdr LEFT JOIN sar_sdr_cstm on sar_sdr.id=sar_sdr_cstm.id_c WHERE sar_sdr_cstm.symptom_c='".$symtoms_name."' And sar_sdr.deleted=0 group by defect_c")->result();
	}
        
    public function getRepairDetails($defect_code, $symptems)
    {
       
        return $this->db->query("SELECT sar_sdr.id,sar_sdr_cstm.repair_c,sar_sdr_cstm.repair_code_c,sar_sdr_cstm.type_c FROM sar_sdr LEFT JOIN sar_sdr_cstm on sar_sdr.id=sar_sdr_cstm.id_c WHERE sar_sdr_cstm.symptom_c='".$symptems."' and sar_sdr_cstm.defect_c = '".$defect_code."' And sar_sdr.deleted=0")->result();
    }
        
	
	public function prd_list($catid)
	{
		//$catid = '1A914231-01DC-997C-924E-EA0BAC537FA9';
		$res = $this->db->query("SELECT sar_spare.*,spc.* FROM `sar_spare` left JOIN sar_spare_cstm as spc on sar_spare.id=spc.id_c LEFT JOIN aos_products_sar_spare_1_c as ps on sar_spare.id=ps.aos_products_sar_spare_1sar_spare_idb WHERE ps.aos_products_sar_spare_1aos_products_ida='".$catid."' and ps.deleted=0 and sar_spare.deleted=0");
		return $res->result_array();
	}
	
	function insert_new_product($newProd)
	{
		$this->db->insert('tbl_cc_products', $newProd);
		return $this->db->affected_rows();
	}
	
	public function get_sdr($job_sheet_no)
	{
		$this->db->select('tbl_cc_sdr.*,sar_spare.name, sar_sdr_cstm.repair_c as repair_name');
		$this->db->where('jobsheet_no',$job_sheet_no);
		$this->db->join('sar_sdr_cstm', 'sar_sdr_cstm.id_c = tbl_cc_sdr.repaire', 'left');
		$this->db->join('sar_spare_cstm', 'sar_spare_cstm.aos_products_id_c=tbl_cc_sdr.product', 'left');
		$this->db->join('sar_spare', 'sar_spare_cstm.id_c=sar_spare.id', 'left');
		$res = $this->db->get('tbl_cc_sdr');
		return $res->result();
	}
	
	public function assignJobSheet($pincode, $js_case_id, $parentCat = null)
	{
		$type = $parentCat;
		switch ($type) {
			case 1: //STABILIZER
				$product_types = "STABILIZER";
				break;
			case 2: //INVERTER
				$product_types = "INVERTER";
				break;
			case 3: //BATTERY
				$product_types = "BATTERY";
				break;
			case 4: //INVERTER+BATTERY
				$product_types = "INVERTER+BATTERY";
				break;
			case 5: //SOLAR
				$product_types = "SOLAR";
				break;
			case 6: //SOLAR+INVERTER+BATTERY
				$product_types = "SOLAR+INVERTER+BATTERY";
				break;
				
			default:
				$product_types = null;
		}
		$user_type = $this->session->userdata['logged_in']['user_type'];
		
		if($user_type=='SF')
		{
			$uuser_id = $this->session->userdata['logged_in']['user_uuid'];
			$id = $this->db->query("SELECT apu.sar_area_pinmaster_users_1users_idb FROM sar_area_pinmaster_users_1_c as apu LEFT JOIN users on apu.sar_area_pinmaster_users_1users_idb=users.id LEFT JOIN users_cstm as uc on users.id=uc.id_c WHERE apu.sar_area_pinmaster_users_1sar_area_pinmaster_ida IN (SELECT id FROM sar_area_pinmaster WHERE name='".$pincode."' AND deleted=0) AND apu.deleted=0 and users.deleted=0 and users.status='Active' and user_skill_set_c LIKE '%".$product_types."%' AND title='SE' AND  users.reports_to_id = '".$uuser_id."' AND apu.deleted = 0")->result();
			$eng_id = $id[0]->sar_area_pinmaster_users_1users_idb;
			$accept_call = 2;
			$js_status = 'Assign';
			if(empty($eng_id))
			{
				$eng_id = null;
				$accept_call = 1;
				$js_status = 'Open';
			}
			$data = array(
					'assigned_to' 			=> $uuser_id, 
					'engg_reassigned_to' 	=> $eng_id, 
					'accept_call' 			=> $accept_call,	//Accept
					'js_status' 			=> $js_status,	//Assigned
					'js_start_date' 		=> date("d-m-y h:i:s")
				);
				$this->db->where('js_case_id',$js_case_id);
				$this->db->update('tbl_cc_products', $data);
		}
		
		if($user_type=='CALLCENTER')
		{
			$uuser_id = $this->session->userdata['logged_in']['user_uuid'];
			$id = $this->db->query("SELECT apu.sar_area_pinmaster_users_1users_idb FROM sar_area_pinmaster_users_1_c as apu LEFT JOIN users on apu.sar_area_pinmaster_users_1users_idb=users.id LEFT JOIN users_cstm as uc on users.id=uc.id_c WHERE apu.sar_area_pinmaster_users_1sar_area_pinmaster_ida IN (SELECT id FROM sar_area_pinmaster WHERE name='".$pincode."' AND deleted=0) AND apu.deleted=0 and users.deleted=0 and users.status='Active' and user_skill_set_c LIKE '%".$product_types."%' AND title IN ('SF','CSC') AND apu.deleted = 0")->result();
			$sf_id = $id[0]->sar_area_pinmaster_users_1users_idb;
			if(empty($sf_id))
			{
				$res = $this->db->query("SELECT id FROM `users` WHERE user_name='unassign' and deleted=0")->result();
				$sf_id = $res[0]->id;
			}
			$data = array(
					'assigned_to' => $sf_id, 
					'accept_call' => 0,	//Accept
					'js_status' => 'Open',	//Assigned
					'js_start_date' => date("d-m-y h:i:s")
				);
				$this->db->where('js_case_id',$js_case_id);
				$this->db->update('tbl_cc_products', $data);
				//echo $this->db->last_query();
		}
	}

	public function getStock($prod_id, $engg_det_id)
	{
		$res = $this->db->query("SELECT isc.hand_stock_c FROM `sar_inventory_stock` as ins LEFT JOIN sar_inventory_stock_cstm as isc on ins.id=isc.id_c LEFT JOIN aos_products_sar_inventory_stock_1_c as pis on ins.id=pis.aos_products_sar_inventory_stock_1sar_inventory_stock_idb WHERE ins.assigned_user_id='".$engg_det_id."' and pis.aos_products_sar_inventory_stock_1aos_products_ida='".$prod_id."' and ins.deleted=0 and pis.deleted=0 and isc.inventory_status_c='Active' and isc.sap_movement_type_c='631'");
		return $res->result();
	}
	
	public function getComplainDetails($product_random_id)
	{
		return $this->db->get_where('tbl_cc_complain', array('product_random_id'=>$product_random_id))->result();
	}
	
	public function getPhysicalVarification($job_sheet)
	{
		return $this->db->get_where('tbl_cc_physical_verification', array('job_sheet'=>$job_sheet))->result();
	}
	
	public function check_status($type, $keyword)
	{
		$this->db->select('tbl_cc_products.*,tbl_cc_complain.cc_complain_no,aos_product_categories.name as cate_name,aos_products.name as sub_cate_name,u.user_name,tbl_cc_consumer.mobile as mobile_no');
		$this->db->from('tbl_cc_products');
		$this->db->join('tbl_cc_complain','tbl_cc_complain.product_random_id = tbl_cc_products.js_case_id', 'left');
		$this->db->join('tbl_cc_consumer','tbl_cc_consumer.cust_id = tbl_cc_products.cust_id');
		$this->db->join('aos_product_categories','aos_product_categories.id = tbl_cc_products.product_type_id', 'left');
		$this->db->join('aos_products','aos_products.id = tbl_cc_products.product_sub_type_id', 'left');
		$this->db->join('users as u','tbl_cc_products.assigned_to = u.reports_to_id', 'left');
		
		if($type=="searial_no")
		{
			$this->db->like('tbl_cc_products.asset_serial_no', $keyword);
		}
		if($type=="cust_code")
		{
			$this->db->like('tbl_cc_consumer.consumer_code', $keyword);
		}
		if($type=="job_sheet")
		{
			$this->db->like('job_sheet', $keyword);
		}
		if($type=="complaint_number")
		{
			$this->db->like('tbl_cc_complain.cc_complain_no', $keyword);
		}
		if($type=="mobile")
		{
			$this->db->like('tbl_cc_consumer.mobile', $keyword);
		}
		$this->db->group_by('tbl_cc_complain.product_random_id');
		$res =  $this->db->get();
		//echo $this->db->last_query(); die;
		return $res->result();
	}
	
	public function get_spare($prod_id)
	{
		$this->db->select('a.name, a.id');
		$this->db->from('sar_spare a');
		$this->db->join('aos_products_sar_spare_1_c b', 'a.id = b.aos_products_sar_spare_1sar_spare_idb');
		$this->db->where('b.aos_products_sar_spare_1aos_products_ida', $prod_id);
		$this->db->where('a.deleted', 0);
		$res=$this->db->get();
		//echo $this->db->last_query();
		if($res->num_rows()>0)
		{
			return $res->result_array();
		}
		else 
		{
			return '';
		}
	}

	public function get_voc($js_id)
	{
		return $this->db->query("select c.name from aos_products a left join aos_products_sar_sdr_1_c b on a.id = b.aos_products_sar_sdr_1aos_products_ida left join sar_sdr c on b.aos_products_sar_sdr_1sar_sdr_idb = c.id where a.deleted = 0 and b.deleted = 0 and a.id in (select product_sub_type_id from tbl_cc_products where js_case_id = '$js_id') group by a.name")->result();
	}
	
	public function get_alljobsheet()
	{
		$sql = "SELECT b.cc_product_id, b.job_sheet, e.mobile, e.fname, e.lname, a.cc_case_id, b.warranty_status, a.complain_address, a.call_type, a.cc_complain_no, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join tbl_cc_consumer e on e.cust_id = b.cust_id WHERE a.`isActiveComplain` = 1 and js_status != 'Closed' GROUP BY b.job_sheet order by b.created_on ASC";
        return $this->db->query($sql)->result();
	}

	public function getRepairName($repaire)
	{
		$repaire = explode("|", $repaire)[0];
		return $this->db->query("SELECT sar_sdr_cstm.repair_c FROM sar_sdr_cstm where id_c ='".$repaire."'")->result();
	}
}

/* End of file user.php */
/* Location: ./application/models/user.php */
