<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	Class Ownership_model extends CI_Model {

	function get_ownership_mob($mobile)
	{
	  $query = $this->db->query("SELECT a.*, b.name FROM `tbl_service_customers` a left join  accounts b on a.createdBy = b.id where mobile = '".$mobile."' group by cust_product_id");
		
		// echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else{ 
			return '';
		}	
	}
	function get_product_detail($serial)
	{
		$query = $this->db->query("SELECT count(*) as num FROM `tbl_service_products` WHERE productSerialNo = '".$serial."' group by productSerialNo");
		
		// echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else{ 
			return '';
		}		
	}
	
	function get_prod_cust($id)
	{
		$query = $this->db->query("select a.productId, a.productGuid, a.productType, a.model, a.custPurchaseDate, a.productSerialNo, a.warrantyStatus, b.fname, b.lname, b.mobile, b.address, b.state, b.city, b.area, b.pincode, b.cust_code  from tbl_service_products a left join tbl_service_customers b on a.consumerId=b.cust_code where a.productId='".$id."'");
		
		//echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else{ 
			return '';
		}		
	}
	
	function insert_new_owner($new_cons)
	{
		$this->db->insert('tbl_service_customers', $new_cons);
		if($this->db->affected_rows() > 0)
		{
			 return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}
	
	function confirm_new_owner($useruid, $id) 
	{
		$date = date("d-m-Y H:i:s");
		$ip = $_SERVER['REMOTE_ADDR'];
		$arr = array(
						'ownership_taken_on' => $date,
						'createdBy' => $useruid,
						'owned_by_ip' => $ip
						
					);
		    $this->db->where('cust_id', $id);
			$this->db->update('tbl_service_customers', $arr);
			if($this->db->affected_rows() > 0)
			{
				 return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
	}
	
	
	function get_ownership_serial($serial)
	{
		// $sql = "SELECT a.*, b.name as dist, c.* FROM `tbl_service_products` a right join tbl_service_customers c on c.cust_code = a.consumerId left join accounts b on c.createdBy = b.id  where a.productSerialNo = '".$serial."' group by a.productSerialNo"; 
		// echo $sql; die;
		/* $query = $this->db->query("SELECT a.*, b.name as dist, c.* FROM `tbl_service_products` a left join tbl_service_customers c on c.cust_code = a.consumerId left join accounts b on c.createdBy = b.id  where a.productSerialNo = '".$serial."' group by a.productSerialNo");
		 */
		 $query = $this->db->query("select a.*, b.callType, b.call_id, b.callRegDate, b.callerMobile, b.callProductSerialNo,  b.callStatus, b.callStage, c.distributor_name from  tbl_service_customers as a left join tbl_service_calls as b on a.cust_product_id = b.callProductSerialNo left JOIN tbl_user c on c.user_uuid=a.createdBy where a.cust_product_id ='".$serial."' group by cust_product_id  order by b.service_call_id desc ");
		
		//echo $this->db->last_query();  die;
		// return $query->result(); 	
		//echo $query->num_rows(); die("hello");
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else{ 
			return '';
		}			
	}
	function insert_data_owner($arr, $create, $mobile)
	{
		$this->db->where('mobile', $mobile);
		$this->db->update('tbl_service_customers', $arr);
		if($this->db->affected_rows() > 0)
		{
			 return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	}
	function update_mob_product($arr1, $arr2, $create, $productGuid, $mobile)
	{
		$this->db->where('mobile', $mobile);
		$this->db->update('tbl_service_customers', $arr1);
		if($this->db->affected_rows() > 0)
		{
			 $this->db->where('id_c', $productGuid);
			$this->db->update('sar_asset_cstm', $arr2);
			if($this->db->affected_rows() > 0)
			{
				 return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		}
		else
		{
			return '';
		}
	}
	function insert_data_serial($arr1, $arr2, $arr3, $Serial, $productGuid, $mobile)
	{
		$this->db->where('mobile', $mobile);
		$this->db->update('tbl_service_customers', $arr1);
		//echo $this->db->last_query(); die;
		if($this->db->affected_rows() > 0)
		{
			 		$this->db->where('productSerialNo', $Serial);
					$this->db->update('tbl_service_products', $arr2);
					if($this->db->affected_rows() > 0)
					{
						 $this->db->where('id_c', $productGuid);
						$this->db->update('sar_asset_cstm', $arr3);
						if($this->db->affected_rows() > 0)
						{
							 return $this->db->affected_rows();
						}
						else
						{
							return 'e1';
						}
					}
					else
					{
						return 'e2';
					}
		}
		else
		{
			return 'e3';
		}
	}
	  
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */