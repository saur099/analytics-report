<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Defective_stock_model extends CI_Model {

	public function get_serial_data($srl)
	{
	    $query = $this->db->query("SELECT a.`serial_num` , a.`brand` , a.`product` , a.`model` , a.`primary_sale_date` , a.`invoice_no` , a.`Distributor_Code` , a.`Distributor_Name` , b.brand, b.PRODUCT_GROUP, b.PRODUCT, b.MODEL_CODE, b.MODEL_DESCRIPTION, b.WARRANTY_IN_MONTHS, b.PRO_RATA_WARRANTY, b.AH_VALUE, b.BATTERY_TYPE, b.AGING_LIMIT, b.PRO_RATA_MIN_DISCOUNT
									FROM  `tblProductSerialNum1` AS a
									LEFT JOIN tbl_model_master1 AS b ON a.`model` = b.MODEL_CODE
									WHERE a.`serial_num` =  '".$srl."'
									LIMIT 0 , 30");
		
		//echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ 
			return '';
		}				
		
	}
	
	function check_serial_one($srl)
	{
		 $this->db->select('count(productSerialNo) ct');	
	    $this->db->from('tbl_service_products');
		$this->db->where('productSerialNo', $srl);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	}
	function check_case_one($case)
	{
		 $this->db->select('count(isDefective) sr');	
	    $this->db->from('tbl_service_calls');
		$this->db->where('call_id', $case);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	}
		
    function check_serial_two($srl)
	{
		 $this->db->select('count(name) sr');	
	    $this->db->from('sar_serial_master');
		$this->db->where('name', $srl);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	} 	
	function check_case_two($case)
	{
		$this->db->select('count(caseId) sr');	
	    $this->db->from(' tblservicecasetestreport');
		$this->db->where('caseId', $case);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	} 	
	
	function insert_tickets($data)
	{
        $this->db->insert('tbl_support', $data);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
    }   
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */