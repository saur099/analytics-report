<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Defective_stock_model extends CI_Model {
	
	public function get_serial_data($srl){
	    $query = $this->db->query("SELECT a.`serial_num` , a.`brand` , a.`product` , a.`model` , a.`primary_sale_date` , a.`invoice_no` , a.`Distributor_Code` , a.`Distributor_Name` , b.brand, b.PRODUCT_GROUP, b.PRODUCT, b.MODEL_CODE, b.MODEL_DESCRIPTION, b.WARRANTY_IN_MONTHS, b.PRO_RATA_WARRANTY, b.AH_VALUE, b.BATTERY_TYPE, b.AGING_LIMIT, b.PRO_RATA_MIN_DISCOUNT
									FROM  `tblProductSerialNum1` AS a
									LEFT JOIN tbl_model_master1 AS b ON a.`model` = b.MODEL_CODE
									WHERE a.`serial_num` =  '".$srl."'
									LIMIT 0 , 30");
		
		//echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0){
			return $query->result();
		}
		else{ 
			return '';
		}
	}
	public function get_serial_data1($srl)
	{
		
	    $query = $this->db->query("select a.*, b.*, c.* from tbl_service_calls a left join tblProductSerialNum1 b on b.serial_num = a.callProductSerialNo left join tbl_model_master1 c on c.MODEL_CODE = b.model where a.service_call_id = '".$srl."'
		LIMIT 0 , 30");
		
		//echo $this->db->last_query();  die;
		// return $query->result(); 	
		
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ 
			return '';
		}				
		
	}
	public function case_report($data)
	{
		//echo "<pre>"; 
		//print_r($data); die;
	    $save = $this->db->insert('tblservicecasetestreport', $data);
		if($this->db->affected_rows() > 0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}		
	}
	
	function update_status_stage($arr, $caseId)
		{
			//echo "<pre>";
			//echo $caseId;
			//print_r($arr); die;
			$this->db->where('call_id', $caseId);
			$this->db->update('tbl_service_calls',$arr);
			if($this->db->affected_rows() > 0)
			{ 
				 return $this->db->affected_rows();
			}
			else
			{ 
				return '';
			}
		}
	
	function check_serial_one($srl)
	{
		 $this->db->select('count(productSerialNo) ct');	
	    $this->db->from('tbl_service_products');
		$this->db->where('productSerialNo', $srl);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	}
	
	function check_case($productserial)
	{
	    	$sql = "SELECT count(isDefective) ct FROM `tbl_service_calls` WHERE  `callProductSerialNo` ='".$productserial."'";
		//echo $sql; die;
		return  $this->db->query($sql)->result();	
	}
	
	public function add_new_case($data)
	{
		//echo "Hiii<pre>"; print_r($data); die;
		$query  = $this->db->insert('tbl_service_calls', $data);
		//$q = $this->db->affected_rows();
		
		if($this->db->affected_rows() > 0)
       {
          return $this->db->insert_id();           
       }
       else 
	   {
		  echo ("<SCRIPT LANGUAGE='JavaScript'>
				window.alert('Sorry ! Some problem occured. Please Try again')
				
				</SCRIPT>");
				redirect('service_cases');
	   }   
	   
		
		
	} 	
	
	function check_case_one($case)
	{
		 $this->db->select('count(isDefective) sr');	
	    $this->db->from('tbl_service_calls');
		$this->db->where('call_id', $case);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	}
		
    function check_serial_two($srl)
	{
		 $this->db->select('count(name) sr');	
	    $this->db->from('sar_serial_master');
		$this->db->where('name', $srl);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	} 	
	function check_case_two($case)
	{
		$this->db->select('count(caseId) sr');	
	    $this->db->from(' tblservicecasetestreport');
		$this->db->where('caseId', $case);
        //  $this->db->limit(0,30);		
		$query = $this->db->get();
		//echo $this->db->last_query();  die;
		//echo $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{ //echo "iii";
			return '';
		}				
	} 	
	
	function insert_tickets($data)
	{
        $this->db->insert('tbl_support', $data);
		//return $this->db->affected_rows;
		 if($this->db->affected_rows() >0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}
	   
    }   
	
}

/* End of file zone.php */
/* Location: ./application/models/zone.php */