<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * Author : Saurav Sony 
 */
class Cc_customer_model extends CI_Model 
{
	function getConsumerDetailForProd($cust_id)
	{
		$this->db->select('pincode');
		$this->db->from('tbl_cc_consumer');
		$this->db->where("cust_id", $cust_id);
		$query = $this->db->get();
	   // echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
        
        function update_final_dec($sub_dec, $caseId)
        {
            $this->db->where('call_id', $caseId);
            $this->db->update('tbl_service_calls', $sub_dec);
            if($this->db->affected_rows() > 0 )
            {
                return $this->db->affected_rows();
            }
            else 
            {
                 return 0;
            }
        }
        
        function get_consum_detail($mobile)
        {
            //echo $mobile;
            $query = $this->db->query("select a.*, b.* from contacts a left join contacts_cstm b on a.id=b.id_c where a.phone_mobile ='".$mobile."' and a.deleted = 0 group by a.phone_mobile limit 1");
            return $query->result();
        }
         function get_consumer_detail($consid)
        {
            //echo $mobile;
            $query = $this->db->query("select a.*, b.* from contacts a left join contacts_cstm b on a.id=b.id_c where a.id ='".$consid."' and a.deleted = 0  limit 1");
            return $query->result();
        }
          public function getJS_Prod_Battery($jsID)
	{
		$this->db->select('tbl_cc_products.*,tbl_cc_consumer.*,aos_product_categories.name as cate_name,aos_products.name as sub_cate_name');
		$this->db->from('tbl_cc_products');
		$this->db->where('job_sheet', $jsID);
		$this->db->join('tbl_cc_consumer','tbl_cc_consumer.cust_id=tbl_cc_products.cust_id');
		$this->db->join('aos_product_categories','aos_product_categories.id = tbl_cc_products.product_type_id');
		$this->db->join('aos_products','aos_products.id = tbl_cc_products.product_sub_type_id');
		$res =  $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result();
	}
        
        function get_product_wrrnt($case)
	{
                $case = trim(stripslashes($case));
		//$sql = "SELECT * FROM `tbl_service_calls` as a left join tbl_service_products as b on a.`callAssetId` = b.productGuid where a.`call_id` = '".$case."'";
		//echo $sql; die;
                $this->db->select('a.*, d.first_name as fname, d.last_name as lname');
		$this->db->from('tbl_service_calls a');
		$this->db->where('a.call_id', $case);
		$this->db->join('sar_asset_cstm b','b.product_serial_no_c = a.callProductSerialNo', 'left');
		$this->db->join('sar_asset c','c.id=b.id_c', 'left');
		$this->db->join('contacts d','d.phone_mobile = a.callerMobile', 'left');
		$this->db->group_by('d.phone_mobile');
		$res =  $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result();
	}
        
        function get_product_data($pseialno)
        {
           // echo $pseialno;
            $query = $this->db->query("select a.*, b.* from sar_asset a left join sar_asset_cstm b on a.id=b.id_c where b.product_serial_no_c ='".$pseialno."' and a.deleted = 0 group by b.product_serial_no_c limit 1");
            return $query->result();
        }
        function add_more_prod($dataCCProduct, $dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset, $prod_id, $caLL_arr, $js_no)
        {
                //echo "hii"; die;
                //echo "Hello"; 
                /* 
                echo "<pre>"; print_r($dataServiceProducts);
                echo "<pre>"; print_r($dataAsset); 
                echo "<pre>"; print_r($dataAssetCstm); 
                echo "<pre>"; print_r($contactAsset); 
                echo "<pre>"; print_r($cp); 
                echo "<pre>"; print_r($dataCust); 
                echo "<pre>"; print_r($consid); die;
                 */
                 $this->db->where('cc_product_id', $prod_id);
                $this->db->update('tbl_cc_products', $dataCCProduct);
                $this->db->insert('tbl_service_products', $dataServiceProducts);
                $this->db->insert('sar_asset', $dataAsset);
                $this->db->insert('sar_asset_cstm', $dataAssetCstm);
                $this->db->insert('contacts_sar_asset_1_c', $contactAsset);
                if($this->db->affected_rows() > 0 )
                {
                   // $this->db->where('jobsheet_no', $js_no);
                    $this->db->where('call_id', $js_no);
                    $this->db->update('tbl_service_calls', $caLL_arr);
                    if($this->db->affected_rows() > 0 )
                    {
                        //return 1;
                        $this->db->select('a.service_call_id');
                        $this->db->from('tbl_service_calls a');
                        $this->db->join('tbl_cc_products b', 'b.js_case_id=a.cc_call_ref_id', 'left');
                        $this->db->where("b.cc_product_id", $prod_id);
                        $this->db->where("a.isDefective", 0);
                        $this->db->where("a.deleted", 0);
                        $this->db->where("a.isCallCentreCall", 1);
                        $query = $this->db->get();
                        if($query->num_rows()>0)
                       {
                               return $query->result();
                       }
                       else
                       {
                               return 0;
                       }
                    }
                else {
                    return 0;
                }
                }
                else
                {
                    return 0;
                }
                
                //print_r($cp); die;
           

        }
 
        
        function get_cc_prod($js_no)
        {
            $query = $this->db->query("SELECT b.cust_code, b.cust_id,a.cc_product_id, a.js_case_id FROM tbl_cc_products a left join tbl_cc_consumer b on a.cust_id = b.cust_id WHERE a.job_sheet ='".$js_no."' group by b.cust_code");
            return $query->result();
        }    
	
        function get_complain_data($asd)
        {
            $query = $this->db->query("SELECT call_id, complaint_type, callProductSerialNo FROM tbl_service_calls  WHERE jobsheet_no ='".$asd."' and callStatus <> 1 and callStage <> 9");
            return $query->result();
        }
         function get_complain_types($asd)
        {
            $query = $this->db->query("SELECT service_call_id, complaint_type, callProductSerialNo FROM tbl_service_calls  WHERE call_id ='".$asd."' limit 1");
            return $query->result();
        }
        function update_serial_data($upd_arr, $service_call_id)
        {
            $this->db->where('service_call_id', $service_call_id);
            $this->db->update('tbl_service_calls', $upd_arr);
            return $this->db->affected_rows();
        }
        
        function get_battery_complains($user_uuid)
        {
            $this->db->select('c.cc_product_id, c.job_sheet,c.js_status, c.created_on, d.name as prod_name, e.name as cat_name, b.address, b.state, b.city, b.district, b.area, b.pincode');
            $this->db->from('tbl_cc_products c');
            $this->db->join('tbl_cc_consumer b', 'c.cust_id = b.cust_id', 'left');
            $this->db->join('aos_products d', 'd.id = c.product_sub_type_id', 'left');
            $this->db->join('aos_product_categories e', 'c.product_type_id = e.id', 'left');
            $this->db->where("c.assigned_to", $user_uuid);
            $this->db->where("c.accept_call", 1);
            //$this->db->order_by('a.service_call_id desc');
            $query = $this->db->get();
	   // echo $this->db->last_query(); die;
	   // $this->db->num_rows();
            if($query->num_rows()>0)
           {
                   return $query->result();
           }
           else
           {
                   return '';
           }
        }
        
        function get_battery_complains_zero($user_uuid, $id)
        {
           $this->db->select('c.cc_product_id, c.job_sheet,c.js_status, c.created_on, d.name as prod_name, e.name as cat_name, b.address, b.state, b.city, b.district, b.area, b.pincode');
            $this->db->from('tbl_cc_products c');
            $this->db->join('tbl_cc_consumer b', 'c.cust_id = b.cust_id', 'left');
            $this->db->join('aos_products d', 'd.id = c.product_sub_type_id', 'left');
            $this->db->join('aos_product_categories e', 'c.product_type_id = e.id', 'left');
            $this->db->where("c.assigned_to", $user_uuid);
            $this->db->where("c.accept_call", 1);
            //$this->db->order_by('a.service_call_id desc');
            $query = $this->db->get();
	  // echo $this->db->last_query(); die;
	   // $this->db->num_rows();
            if($query->num_rows()>0)
           {
                   return $query->result();
           }
           else
           {
                   return '';
           }
        }
        
        function get_battery_complains_one($user_uuid, $id)
        {
             $this->db->select('c.cc_product_id, c.job_sheet,c.js_status, c.created_on, d.name as prod_name, e.name as cat_name, b.address, b.state, b.city, b.district, b.area, b.pincode');
            $this->db->from('tbl_cc_products c');
            $this->db->join('tbl_cc_consumer b', 'c.cust_id = b.cust_id', 'left');
            $this->db->join('aos_products d', 'd.id = c.product_sub_type_id', 'left');
            $this->db->join('aos_product_categories e', 'c.product_type_id = e.id', 'left');
            $this->db->where("c.assigned_to", $user_uuid);
            $this->db->where("c.accept_call", 1);
            //$this->db->order_by('a.service_call_id desc');
            $query = $this->db->get();
	  //echo $this->db->last_query(); die;
	   // $this->db->num_rows();
            if($query->num_rows()>0)
           {
                   return $query->result();
           }
           else
           {
                   return '';
           }
        }
        
	function update_assigned_engg($user_uuid, $r, $arrUpd)
	{
		$this->db->where('cc_product_id', $r);
		$this->db->update('tbl_cc_products', $arrUpd);
		return $this->db->affected_rows();
	}
	
	public function add_new_consumer($ser_cust, $cc_cust, $dat, $data_cstm, $d_email, $em_rel)
	{
            $this->db->insert('contacts', $dat);
            $this->db->insert('contacts_cstm', $data_cstm);
            $this->db->insert('email_addresses', $d_email);
            $this->db->insert('email_addr_bean_rel', $em_rel);
            $this->db->insert('tbl_service_customers', $ser_cust);
            $this->db->insert('tbl_cc_consumer', $cc_cust);
            $insert_id = $this->db->insert_id();

            if ($this->db->affected_rows() > 0)
            {
                    return $insert_id;

            }
            else
            {
                    return 0;

            }
        }
	
        
	function check_ageing($prod_serial)
	{
		$sql = "SELECT a.`AGING_LIMIT` 
				FROM  `tbl_model_master1` AS a
				LEFT JOIN tblProductSerialNum1 AS b ON a.`MODEL_CODE` = b.model
				WHERE b.serial_num =  '".$prod_serial."'";
		//echo $sql; die;
		return $r = $this->db->query($sql)->result();
	}
	
	function get_cons_product($cust_id)
	{
	     $sql = "SELECT b.name as prod_name, c.name as prod_type, a.purchase_date, a.warranty_start_date, a.warranty_end_date, a.warranty_status, a.asset_serial_no, a.product_sap_code, a.js_final_closure,  a.isActiveProduct FROM `tbl_cc_products` a left join aos_products b on a.`product_sub_type_id` = b.id left JOIN aos_product_categories c on c.id = a.`product_type_id` where a.cust_id = ".$cust_id." and a.isActiveProduct = 1 and a.js_final_closure='Closed'";
		 return $r = $this->db->query($sql)->result();
	}	 
	
	function get_cas_status($case)
	{
		$sql = "SELECT caLL_case_status FROM `tbl_service_calls` where call_id='".$case."' and caLL_case_status IN(1,2,3)";
		//echo $sql; die;
		return $r = $this->db->query($sql)->num_rows();
	}
	
	function check_status($caseId)
	{
		$this->db->select('replacementMode');
		$this->db->from('tbl_service_calls');
		$this->db->where("call_id", $caseId);
		$query = $this->db->get();
	   // echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
		
	public function consumerInfo($mobile)
	{ 
		return $this->db->select('*')->get_where('tbl_cc_consumer',array('mobile' => $mobile))->result();
	}
	
	public function updateAddress($data){ 
		$this->db->where('mobile', $data['mobile']);
		$this->db->update('tbl_cc_consumer',$data);
		if($this->db->affected_rows() > 0){
			return 1;
		}
		else{
			return 0;
		}
	}
        function verify_serial_no($prod_serial)
        {
            $this->db->where('name', $prod_serial);
            $this->db->where('deleted', 0);
            $this->db->select('name');
            $this->db->from('sar_serial_master');
            $query = $this->db->get();
            if($query->num_rows()>0)
            {
               return 'x';
            }
            else
            {
                
                $this->db->where('a.product_serial_no_c', $prod_serial);
                $this->db->where('b.deleted', 0);
                $this->db->select('a.id_c');
                $this->db->join('sar_asset b', 'b.id = a.id_c', 'left');
                $this->db->from('sar_asset_cstm a');
                $query = $this->db->get();
               // echo $this->db->last_query();
                if($query->num_rows()>0)
                {
                     return 'x';
                }
                else 
                {
                    $this->db->where('name', $prod_serial);
                    $this->db->where('deleted', 0);
                    $this->db->select('name');
                    $this->db->from('sar_product_serial_number');
                    $query1 = $this->db->get();
                    //echo $this->db->last_query(); //die;
                    //print_r($query1->result());
                    if($query1->num_rows()>0)
                    {
                        //echo "1"; die;
                        return $asn;
                    }
                    else 
                    {
                        //echo "0"; die;
                        return 'x';
                    }
                }
            }
        }
        
        function get_product_type($old_sr)
	{
		$sql = $this->db->query("select e.name as category
                                        from sar_product_serial_number a 
                                        left join sar_product_serial_number_cstm b on a.id=b.id_c 
                                        left join aos_products c on c.id=b.aos_products_id_c
                                        left join aos_products_cstm d on d.id_c = c.id
                                        left join aos_product_categories e on e.id = c.aos_product_category_id
                                        where a.deleted=0 and a.name = '".$old_sr."'"); 
		return $result=$sql->result();
	}
        function get_new_product($prod_serial)
	{
	    $sql = $this->db->query("select e.name as category
                                        from sar_product_serial_number a 
                                        left join sar_product_serial_number_cstm b on a.id=b.id_c 
                                        left join aos_products c on c.id=b.aos_products_id_c
                                        left join aos_products_cstm d on d.id_c = c.id
                                        left join aos_product_categories e on e.id = c.aos_product_category_id
                                        where a.deleted=0 and a.name = '".$prod_serial."'"); 
	    return $result=$sql->result();
	}
        public function serial_verify_data($prod_serial)
        {
            $sql = $this->db->query("select a.name, b.sap_code_c, b.primary_sale_date_c as primary_sale_date, b.invoice_number_c as invoice_no, b.battery_type_c, c.name, d.warranty_c, 
                                    d.pro_rata_warranty_c, d.aeging_limit_c, e.name as category, g.name as product_segment, g.name as brand
                                    from sar_product_serial_number a 
                                    left join sar_product_serial_number_cstm b on a.id=b.id_c 
                                    left join aos_products c on c.id=b.aos_products_id_c
                                    left join aos_products_cstm d on d.id_c = c.id
                                    left join aos_product_categories e on e.id = c.aos_product_category_id
                                    left join sar_segment_aos_products_1_c f on f.sar_segment_aos_products_1aos_products_idb = c.id
                                    left join sar_segment g on g.id = f.sar_segment_aos_products_1sar_segment_ida
                                    where a.deleted=0 and a.name = '".$prod_serial."'"); 
            return $result=$sql->result();
        }
}

/* End of file user.php */
/* Location: ./application/models/user.php */
