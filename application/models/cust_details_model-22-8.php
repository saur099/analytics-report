<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cust_details_model extends CI_Model 
{
	public function get_cust_details($user_uuid)
	{
		//$sql = "select * from tbl_service_customers";
		//$sql = "select a.fname,a.lname,a.cust_code,a.custAssetGuid,a.mobile,a.state,a.city,b.productType,b.model,b.productSerialNo,b.custPurchaseDate,b.warrantyStatus from tbl_service_customers a left join tbl_service_products b on a.cust_code = b.consumerId where a.createdBy ='".$user_uuid."' group by b.productSerialNo order by b.datetime desc";
		
		$sql = "select a.fname,a.lname,a.cust_code,a.custAssetGuid,a.mobile,a.state,a.city,a.productType,a.model,a.cust_product_id,a.custPurchaseDate,a.warrantyStatus from tbl_service_customers a where a.createdBy ='".$user_uuid."' group by a.cust_product_id order by a.cust_id desc";
		/* 
		$sql = "select a.fname,a.lname,a.cust_code,a.custAssetGuid,a.mobile,a.state,a.city,b.productType,b.model,b.productSerialNo,b.custPurchaseDate,b.warrantyStatus from tbl_service_customers a left join tbl_service_products b on a.cust_code = b.consumerId where a.createdBy ='".$user_uuid."' group by b.productSerialNo order by b.datetime desc";
		 */
		// echo $sql; die;
		return  $this->db->query($sql)->result();	
	}
	
	function get_serial_letpl($productserial)
	{
		$sql = "SELECT primary_sale_date FROM `tblProductSerialNum1` WHERE `serial_num` LIKE '".$productserial."'";
		return  $this->db->query($sql)->result();
	}
	
	function block_serial($ids) 
	{
		$sql = "SELECT count(name) as status FROM sar_serial_master where deleted = 0 and status = 'inactive' and name='".$ids."'";
		return  $this->db->query($sql)->result();
	}
	
	function is_blocked_serial($productserial)
	{
		$sql = "SELECT count(name) as status FROM sar_serial_master where deleted = 0 and status = 'inactive' and name='".$productserial."'";
		//echo $sql; die("heii");
		return  $this->db->query($sql)->result();
	}
	
	
	function get_all_mobiles($data)
	{
		/* 
		$sql = "SELECT a.`callerMobile` as cust_mob, a.call_id , b.`mobile` as dist_mob, d.phone_mobile as se_mobile, b.distributor_name as dist, e.phone_office as deal_mob FROM `tbl_service_calls` a left join `tbl_user` b on a.`callCreatedBy` = b.`user_uuid` left join accounts_users_1_c c on c.`accounts_users_1accounts_ida` = a.`callCreatedBy` left join `users` d on d.`id`= c.`accounts_users_1users_idb` left join sar_dealer e on e.id = a.dealer_code where a.`service_call_id` = '".$data."' and d.title = 'SERVICE ENGINEER'";
		 */	
		$sql = "SELECT a.service_call_id, a.`callerMobile` AS cust_mob, a.call_id, b.`phone_office` AS dist_mob, d.phone_mobile AS se_mobile, 
			b.name 	AS 	dist, e.phone_office AS deal_mob
			FROM  `tbl_service_calls` a
			LEFT JOIN  `accounts` b ON a.`callCreatedBy` = b.`id` 
			LEFT JOIN accounts_users_1_c c ON c.`accounts_users_1accounts_ida` = a.`callCreatedBy` 
			LEFT JOIN  `users` d ON d.`id` = c.`accounts_users_1users_idb` 
			LEFT JOIN sar_dealer AS e ON e.id = a.dealer_code
			WHERE a.`service_call_id` =  '".$data."' and d.title IN('SERVICE ENGINEER','SERVICE MANAGER') and c.deleted = 0
			GROUP BY a.`service_call_id`  ";
			//echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	
	function check_case($productserial)
	{
	    	$sql = "SELECT caLL_case_status, is_tat2_closed, se_decision  FROM `tbl_service_calls` WHERE  `callProductSerialNo`='".$productserial."' order by service_call_id desc limit 1";
		//echo $sql; die;
		return  $this->db->query($sql)->result();	
	}
	public function get_serial_data($productserial)
	{
	/*     $query = $this->db->query("SELECT a.`serial_num` , a.`brand` , a.`product` , a.`model` , a.`primary_sale_date` , a.`invoice_no` , a.`Distributor_Code` , a.`Distributor_Name` , b.brand, b.PRODUCT_GROUP, b.PRODUCT, b.MODEL_CODE, b.MODEL_DESCRIPTION, b.WARRANTY_IN_MONTHS, b.PRO_RATA_WARRANTY, b.AH_VALUE, b.BATTERY_TYPE, b.AGING_LIMIT, b.PRO_RATA_MIN_DISCOUNT
									FROM  `tblProductSerialNum1` AS a
									LEFT JOIN tbl_model_master1 AS b ON a.`model` = b.MODEL_CODE
									WHERE a.`serial_num` =  '".$productserial."'
									LIMIT 0 , 30");
		
		//echo $this->db->last_query();  die;
		print_r($query->result()); 	 */
		
		$sql = "SELECT a.`serial_num` , a.`brand` , a.`product` , a.`model` , a.`primary_sale_date` , a.`invoice_no` , a.`Distributor_Code` , a.`Distributor_Name` , b.brand, b.PRODUCT_GROUP, b.PRODUCT, b.MODEL_CODE, b.MODEL_DESCRIPTION, b.WARRANTY_IN_MONTHS, b.PRO_RATA_WARRANTY, b.AH_VALUE, b.BATTERY_TYPE, b.AGING_LIMIT, b.PRO_RATA_MIN_DISCOUNT
									FROM  `tblProductSerialNum1` AS a
									LEFT JOIN tbl_model_master1 AS b ON a.`model` = b.MODEL_CODE
									WHERE a.`serial_num` =  '".$productserial."'
									LIMIT 0 , 30";
		//echo $sql; die;
		return  $this->db->query($sql)->result();
		
	}
	function get_call_info($ids)
	{
		$sql = "SELECT call_id,callProductSerialNo,callCustId,caLL_case_status, is_tat2_closed, replacementMode, se_decision FROM tbl_service_calls WHERE callProductSerialNo ='".$ids."' order by service_call_id desc";
	   //echo $sql; die;
		return  $this->db->query($sql)->result();

	}
	function get_consumerId($ids)
	{
		$sql = "SELECT `consumerId` FROM `tbl_service_products` WHERE `productSerialNo` ='".$ids."' order by productId desc limit 1";
		// echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	function get_cust_detail($id)
	{
		$sql = "SELECT * FROM `tbl_service_products` WHERE `consumerId`='".$id."' ";
		//echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	function get_dealer_BAKCode($dealer_code)
	{
		$sql = "SELECT customer_number_c FROM `sar_dealer_cstm` WHERE `id_c`='".$dealer_code."' ";
		return  $this->db->query($sql)->row();
	}
	function get_cuspro($id, $user_uuid)
	{
		$sql = "select a.*,b.*,c.*,dc.customer_number_c from tbl_service_customers as a 
				left join tbl_service_products as b on b.consumerId =a.cust_code 
				right join sar_dealer as c 
				on b.dealerName = c.id
				LEFT JOIN sar_dealer_cstm dc
				on dc.id_c=c.id
				where a.mobile='".$id."' && a.createdBy ='".$user_uuid."' group by b.productSerialNo";
	 // echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	function get_cuspro_ash($id, $user_uuid)
	{
		$sql = "select a.`fname`, a.lname, a.mobile, a.alt_mobile, a.email, a.address, a.landmark, a.state, a.city, a.area, a.pincode, a.dob,a.spouse, b.productSegment, b.brand, b.productType, b.batteryType, b.model, b.custPurchaseInvoiceNum, b.custPurchaseDate, b.productSerialNo, b.warrantyStatus,b.warrantyStartDate,b.warrantyEndDate,b.letpl_invoice_no, b.letpl_invoice_date, b.proRateWarrStDate,b.proRateDisc,b.proRateWarrEndDate,b.productAppFitment,c.name, dc.customer_number_c from tbl_service_customers as a 
				left join tbl_service_products as b on b.consumerId =a.cust_code 
				right join sar_dealer as c 
				on b.dealerName = c.id
				LEFT JOIN sar_dealer_cstm dc
				on dc.id_c=c.id
				where b.productId ='".$id."' limit 0,1";
	 // echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	
	public function add_new_case($data)
	{
		//echo "Hiii<pre>"; print_r($data); die;
		$query  = $this->db->insert('tbl_service_calls', $data);
		//$q = $this->db->affected_rows();
		
		if($this->db->affected_rows() > 0)
       {
          return $this->db->insert_id();           
       }
       else 
	   {
		  echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Sorry ! Some problem occured. Please Try again')
    
    </SCRIPT>");
	redirect('service_cases');
	   }   
	   
		
		
	} 	
	public function get_cust_product($id)
	{
		
		$sql = "select * from tbl_service_customers where cust_code='".$id."' order by cust_id desc limit 1";
		//echo $sql; die;
		return  $this->db->query($sql)->result();	
	}
	function get_prod_info($id)
	{
		$sql = "select *from tbl_service_products where consumerId='".$id."'";
		//echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	function get_prod_info_ash($id,$ids)
	{
		$sql = "select * from tbl_service_products where consumerId='".$id."' and productSerialNo = '".$ids."'";
		//echo $sql; die;
		return  $this->db->query($sql)->result();
		
	}
	
	function get_dist_dealer($id)
	{
		//echo " Hii ";  die;
		$sql = "SELECT  `name` 
				FROM  `sar_dealer` AS a
				LEFT JOIN tbl_service_products AS b ON b.dealerName = a.id
				WHERE b.consumerId =  '".$id."'";
	//	echo $sql; die;
		return  $this->db->query($sql)->result();
	}
	
	public function verify_case_model($con_id, $pr_serial)
	{
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('callerMobile', $con_id);
		$this->db->where('callProductSerialNo', $cust_product_id);
		$query = $this->db->get();
	    //echo "<pre>"; print_r($query->result()); die;
		//echo $this->db->last_query(); die;
		//echo $query->num_rows(); die;
		if($query->num_rows() > 0)
       {
           return $query->result();            
       }
       else 
	   {
		  return'';
	   }   
	}

}

/* End of file user.php */
/* Location: ./application/models/user.php */
