<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Service_case_model extends CI_Model 
		{
			
			public function get_call_details($user_uuid)
			{
				$sql = "select * from tbl_service_calls where callCreatedBy='".$user_uuid."' and isDefective =0 group by call_id  order by service_call_id DESC";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			public function get_casompl_details()
			{
				$sql = "SELECT * FROM `tbl_service_calls` ORDER BY `service_call_id` DESC LIMIT 0,300";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			function get_pagination_data($us, $draw, $start_index, $length, $search)
			{
				//$us = "1110001231";
				
				$sql_count=  $this->db->query("SELECT   `serial_num` ,  `brand` ,  `product` ,  `model` ,  `primary_sale_date` ,  `Distributor_Code` , `Distributor_Name`  
							FROM  `tblProductSerialNum1` WHERE  `Distributor_Code` =  '".$us."'");
				$count_all_record=$sql_count->num_rows();	
				
				if($search!="")
				{
					$sql = "SELECT  `serial_num`, `brand`, `product`, `model`, `primary_sale_date`,  `Distributor_Code`, Distributor_Name FROM `tblProductSerialNum1` where `Distributor_Code` =  '".$us."' and ( `serial_num` like  '%".$search."%' OR brand like '%".$search."%' OR product like '%".$search."%' OR model like '%".$search."%' OR primary_sale_date like '%".$search."%' OR Distributor_Code like '%".$search."%') LIMIT ".$start_index.",".$length;		
					//echo $sql;  
					$sql_count_search = $this->db->query("SELECT  `serial_num`, `brand`, `product`, `model`, `primary_sale_date`,  `Distributor_Code`, Distributor_Name FROM `tblProductSerialNum1` where `Distributor_Code` =  '".$us."' and ( `serial_num` like  '%".$search."%' OR brand like '%".$search."%' OR product like '%".$search."%' OR model like '%".$search."%' OR primary_sale_date like '%".$search."%' OR Distributor_Code like '%".$search."%')");		
					$count_filter_record=$sql_count_search->num_rows();
				}
				else
				{
					$sql = "SELECT  `serial_num` ,  `brand` ,  `product` ,  `model` ,  `primary_sale_date` ,    `Distributor_Code` , `Distributor_Name`  
						FROM  `tblProductSerialNum1` 
						WHERE  `Distributor_Code` =  '".$us."'
						LIMIT ".$start_index.",".$length; 
						$count_all_record=$count_filter_record;
				}
				
				$result = $this->db->query($sql)->result();
				$datatable_data['draw']=$draw;
				$datatable_data['recordsTotal']=$count_all_record;
				$datatable_data['recordsFiltered']=$count_filter_record;
				$i=0;	
				foreach($result as $r)
				{
					$colum_value=array(); 
					$serial_no="";
					$increamt=0;
					foreach($r as $c=>$value)
					{
						$colum_value[] = $value;
						if($increamt==0){
							$serial_no=$value;
						}
						$increamt++;
						//print_r($colum_value);
					}
					$colum_value[]='<a href="'.base_url().'index.php/defective_stock/check/'.base64_encode($serial_no).'"><button class="btn btn-danger btn-sm">Defective</button></a>';
					
					$row_value[$i] = $colum_value;
					$i++;
					
				}	
				//print_r($row_value);
				
				$datatable_data['data']=$row_value;
				$arr = array();
				if($count_filter_record == "0")
				{
					$datatable_data['data']=$arr;
				}
				echo json_encode($datatable_data);
				
			}
			function count_product_created($tid)
			{
				
				$query = $this->db->query("SELECT * FROM `tbl_service_customers` WHERE  custAssetGuid != 'NULL' && cust_code = '".$tid."'");
				// echo $this->db->last_query();  die;
				return $query->num_rows(); 
			}
			
			function get_prod_ser($serial)
			{
				$query = $this->db->query("SELECT `productSerialNo` FROM `tbl_service_products` WHERE `productSerialNo`='".$serial."'");
				 //echo $this->db->last_query();  die;
				return $query->num_rows(); 
			}
			
			function get_customer_detailz($consid)
			{
				$query = $this->db->query("SELECT fname, lname FROM `tbl_service_customers` where cust_id = '".$consid."'");
				// echo $this->db->last_query();  die;
				return $query->result(); 
			}
			function get_customer_detailees($consid)
			{
				$query = $this->db->query("SELECT cust_code FROM `tbl_service_customers` where cust_id =  '".$consid."'");
				// echo $this->db->last_query();  die;
				return $query->result(); 
			}
			 function getReplaceData($us, $caseId)
			{
				$query = $this->db->query("SELECT * FROM `tbl_service_replacement` WHERE  `submittedByDist` ='".$us."' && `caseId`='".$caseId."'");
				// echo $this->db->last_query();  die;
				return $query->result(); 
			}
			
			function update_serial_st($sr)
			{
				$query = $this->db->query("SELECT cust_product_id FROM `tbl_service_customers` WHERE `cust_product_id`='".$sr."'");
				// echo $this->db->last_query();  die;
				return $query->num_rows(); 
			}
			
			function get_product_status($serial)
			{
				$query = $this->db->query("SELECT name FROM `sar_serial_master` WHERE `status` = 'inactive' && deleted =0 && `name` ='".$serial."'");
			    // echo $this->db->last_query();  die;
				return $query->num_rows(); 
			}
			
			function get_reject_status($serial)
			{
				$query = $this->db->query("SELECT serial_num FROM `tblProductSerialNum1` WHERE `serial_num`='".$serial."'");
				// echo $this->db->last_query();  die;
				return $query->num_rows(); 
			}
			
			function get_product_serial($serial, $user_uuid)
			{
				$sql = "SELECT *
						FROM tblProductSerialNum1 tbl1
						LEFT JOIN tbl_model_master1 tbl2 ON tbl1.model = tbl2.MODEL_CODE
						WHERE tbl1.serial_num =  '".$serial."'";
				 //echo $sql; die;
				return  $this->db->query($sql)->result();
				//return json_encode($outcome);  
			} 
			function getTestData($us, $caseId)
			{
				
				$sql = "SELECT * FROM `tblservicecasetestreport` as a left join tbl_service_calls as b on b.call_id = a.caseId where a.submittedByDist= '".$us."' && a.caseId ='".$caseId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();
			}
			function get_product_fitment()
			{
				$sql = "select * from tbl_service_app_fitment";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			function get_fitment_prod()
			{
				$sql = "select prod_category from tbl_service_app_fitment group by prod_category";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			function get_fitment_vehicle($prod_val)
			{
				$sql = "SELECT `app_fitment_id`, `prod_category`,`vehicle_make` FROM `tbl_service_app_fitment`  
						WHERE prod_category='".$prod_val."'
						group by `vehicle_make`,`prod_category`
						order by `prod_category` ASC
					";
				//echo $sql; die;
				return  $this->db->query($sql)->result_array();	
			}
			function get_vehicle_no($veh_val)
			{
				$sql = "SELECT `vehicle_make` ,`vehicle_model`
						FROM  `tbl_service_app_fitment` 
						WHERE `vehicle_make`='".$veh_val."'
						GROUP BY  `vehicle_make` ,  `vehicle_model` 
						ORDER BY  `vehicle_model` ASC 
					";
				//echo $sql; die;
				return  $this->db->query($sql)->result_array();	
			}
		
			function get_dist_dealer($user_uuid)
			{
				//echo " Hii ";  die;
				$sql = "SELECT * 
						FROM sar_dealer AS a
						LEFT JOIN accounts_sar_dealer_1_c AS b ON b.accounts_sar_dealer_1sar_dealer_idb = a.id
						LEFT JOIN sar_dealer_cstm AS c ON c.id_c = a.id
						WHERE b.accounts_sar_dealer_1accounts_ida =  '".$user_uuid."' and a.deleted = 0 and b.deleted = 0 and c.account_status_c = 'Active' group by c.customer_number_c
						ORDER BY  a.name ASC ";
			/* 	$sql = "SELECT * 
						FROM sar_dealer AS a
						LEFT JOIN accounts_sar_dealer_1_c AS b ON b.accounts_sar_dealer_1sar_dealer_idb = a.id
						WHERE b.accounts_sar_dealer_1accounts_ida ='".$user_uuid."'"; */
				// echo $sql; die;
				return  $this->db->query($sql)->result();
			}
			function get_cust_data($callCustId)
			{
				$sql = "select * from  tbl_service_customers where cust_code='".$callCustId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			
			function get_cust_prod($pseialno)
			{
				//$sql = "select * from tbl_service_products where productGuid='".$callAssetId."'";
				$sql = "select * from tbl_service_products where productSerialNo='".$pseialno."'";
				return  $this->db->query($sql)->result();	
			}
			
			function get_dealName($dealer_code)
			{
				$sql = "SELECT  `name` FROM sar_dealer WHERE `id`='".$dealer_code."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			
			function get_bak_code($dealer_code)
			{
				$sql = "SELECT customer_number_c FROM  `sar_dealer_cstm` WHERE  `id_c`='".$dealer_code."'";
				return  $this->db->query($sql)->row();	
			}
			
			/*function update_wrrnty_details($caseId)
			{
				$sql = "select * from tbl_service_calls where call_id='".$caseId."'";
				 // echo $sql; die;
				return  $this->db->query($sql)->result();	
			}*/
			function update_wrrnty_details($caseId)
			{
				$sql = "
					SELECT SC.* ,SR.* FROM tbl_service_calls  SC
					join tblservicecasetestreport SR
					on SC.call_id=SR.caseId
					where SC.call_id='".$caseId."'
				";
				 // echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			
			function check_case($caseId)
			{
				$sql = "select service_call_id from tbl_service_calls where call_id='".$caseId."'";
				return  $this->db->query($sql)->result();	
			}
			
			function get_date($mobile)
			{
				$sql = "select createdOn from tbl_service_customers where mobile='".$mobile."'";
				// echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			function pserial_date($pseialno)
			{ 
				$sql = "select primary_sale_date from tblProductSerialNum1 where serial_num='".$pseialno."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
			}
			
			function close_case($case, $dist, $user)
			{
			  $sql = "UPDATE `tbl_service_calls` SET `caLL_case_status`=2,`distributorRemark`='".$dist."' , `closedByOn` ='".$user."' WHERE `call_id`='".$case."'";
			  //echo $sql; die;
			  $this->db->query($sql);
			  echo ("<SCRIPT LANGUAGE='JavaScript'>
			window.alert('Successfully closed the cases.')
			window.location.href='service_cases';
			</SCRIPT>");

			}
			
		   function get_cust_pr($mob)
		   {
			   /*$this->db->select('*');
				$this->db->from('tbl_service_customers');
				$this->db->where('mobile',$mob);
				//echo $this->db->last_query(); die;
				$query = $this->db->get();
				return $query->result();*/
				
				 $sql = "select a.fname,a.lname, a.cust_code,a.cust_id, a.custAssetGuid, a.cust_product_id,a.mobile,a.state, a.city,b.productId, b.cust_primary_key, b.productType,b.model,b.productSerialNo,b.custPurchaseDate,b.warrantyStatus from tbl_service_customers a left join tbl_service_products b on a.cust_code = b.consumerId where a.mobile = '".$mob."' group by b.productSerialNo order by b.datetime desc"; 
				// echo $sql; die;
				return  $this->db->query($sql)->result();
		
		   }
			function add_new_consumer($data, $dat, $data_cstm, $d_email, $em_rel)
			{
				
				
				$this->db->insert('tbl_service_customers_backup_june_twofive', $data);
				$this->db->insert('contacts', $dat);
				$this->db->insert('contacts_cstm', $data_cstm);
				$this->db->insert('email_addresses', $d_email);
				$this->db->insert('email_addr_bean_rel', $em_rel);
				if($this->db->affected_rows() >0)
				{
					$this->db->insert('tbl_service_customers', $data);
					$insert_id = $this->db->insert_id();
					return $insert_id;
				}
				else
				{
					return '';
				}
				
				/* $this->db->insert('tbl_service_customers', $data);
				$insert_id = $this->db->insert_id();
				return  $insert_id;
				 */			}
 
            function add_cust_prod($dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset,$consid, $dataCust)
			{
				$this->db->insert('tbl_service_products', $dataServiceProducts);
				if($this->db->affected_rows() >0)
				{
					$this->db->insert('tbl_service_customers', $dataCust);
					/* echo $this->db->last_query();
					die; */
					if($this->db->affected_rows() >0)
					{
						//$this->db->insert('tbl_service_products_backup_june_25', $dataServiceProducts);
						$this->db->insert('sar_asset', $dataAsset);
						$this->db->insert('sar_asset_cstm', $dataAssetCstm);
						$this->db->insert('contacts_sar_asset_1_c', $contactAsset);
						return $this->db->affected_rows();
					}
					else
					{
						return 0; 
					}		
				}
				else
				{
					return 0; 
				}
				
			}
			
			function add_more_cust_prod($dataServiceProducts, $dataAsset, $dataAssetCstm, $contactAsset, $cp, $consid, $dataCust)
			{
				//echo "hii"; die;
				//echo "Hello"; 
				/* 
				echo "<pre>"; print_r($dataServiceProducts);
				echo "<pre>"; print_r($dataAsset); 
				echo "<pre>"; print_r($dataAssetCstm); 
				echo "<pre>"; print_r($contactAsset); 
				echo "<pre>"; print_r($cp); 
				echo "<pre>"; print_r($dataCust); 
				echo "<pre>"; print_r($consid); die;
				 */
				 $this->db->where('cust_code', $consid);
				//$this->db->update('tbl_service_customers', $cp, "cust_code = '".$consid."'");
				$this->db->update('tbl_service_customers', $cp);
				
				//echo $this->db->last_query(); die;
				
				$this->db->insert('tbl_service_products', $dataServiceProducts);
				//$this->db->insert('tbl_service_customers', $dataCust);
				//$this->db->insert('tbl_service_customers_backup_june_twofive', $dataCust);
				$this->db->insert('sar_asset', $dataAsset);
				$this->db->insert('sar_asset_cstm', $dataAssetCstm);
				$this->db->insert('contacts_sar_asset_1_c', $contactAsset);
				
				//print_r($cp); die;
			   return 1;
				
			}
 
			function add_new_product($data)
			{
			//	echo "<pre>"; print_r($data); die;
				 $this->db->insert('tbl_service_products', $data);
				 if($this->db->affected_rows() >0)
				{
					return $this->db->affected_rows();
				}
				else
				{
					return '';
				}
			}
			function get_existing_consumer($m)
			{
				$this->db->select('*');
				$this->db->from('tbl_service_customers');
				$this->db->where('mobile',$m);
				//echo $this->db->last_query(); die;
				$query = $this->db->get();
				return $query->result();
			}
			
			function get_existing_customer($mob)
			{
				$this->db->select('*');
				$this->db->from('contacts');
				$this->db->where('phone_mobile',$mob);
				$query = $this->db->get();
				return $query->result();
				
			}
			//chetan
			function check_primary_date($purchase_date, $customer_id, $product_serial)
			{		
				$sql = "SELECT primary_sale_date FROM `tblProductSerialNum1` where  serial_num ='".$product_serial."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();
			}
			function get_warranty($purchase_date, $model_code)
			{		
				$sql = "SELECT WARRANTY_IN_MONTHS,PRO_RATA_WARRANTY FROM tbl_model_master1 where  MODEL_CODE ='".$model_code."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();
			}
			function verify_serial_detail($usern)
			{		
				$sql = "SELECT * FROM `tblProductSerialNum1` where Distributor_Code ='".$usern."' LIMIT 500 ";
				//echo $sql; die;
				return  $this->db->query($sql)->result();
			}
		}
?>		
