<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class call_center_model extends CI_Model{
	
	public function get_consum_detail($mobile)
	{
		$this->db->select('mobile,cust_id');
		$this->db->from('tbl_cc_consumer');
		$this->db->where('mobile', $mobile);
		$this->db->or_where('alt_mobile', $mobile);
		$query = $this->db->get();
		//echo $this->db->last_query(); echo "<br>";
		$num_rows = $query->num_rows();
		if($num_rows > 0){
			return $query->result();
		}else{
			return $num_rows;
		}
	}
	
	public function get_customer_details($mobile){
		$this->db->select("*");
		$this->db->from('tbl_cc_consumer');
		$this->db->where("mobile", $mobile);
		$this->db->or_where('alt_mobile', $mobile);
		$query = $this->db->get();
		return $query->result();
		
	}
	
	function get_consum_code($cons_code)
	{
		$sql="SELECT * FROM tbl_cc_consumer WHERE consumer_code ='".$cons_code."' ";
		//echo $sql; die;
		$res = $this->db->query($sql);
		if($res->num_rows() > 0 )
	    {
		   return $res->result();
	    }
	    else
	    {
		 return 0;
	    } 
	}
	function get_complain_details_battery($user_uuid, $complain_no, $user_type, $parameter)
	{

            if($parameter =='unaccept')
            {
                
                $sql = "SELECT b.job_sheet, b.created_on,b.cc_product_id, a.cc_case_id, b.warranty_status, a.complain_address, a.call_type, a.cc_complain_no, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."' and accept_call = 0 and d.name in ('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV') GROUP BY b.job_sheet order by b.created_on ASC";
            }
            if($parameter =='accept')
            {
                
                $sql = "SELECT b.cc_product_id, b.btr_no, b.warranty_status, b.js_status, b.created_on, b.job_sheet, a.cc_complain_no, a.call_type,a.cc_case_id, a.complain_address, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."'  and accept_call = 1 and d.name in ('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV') GROUP BY b.job_sheet order by b.created_on ASC";
            }
            if($parameter =='allocate')
            {
                
                $sql = "SELECT e.first_name, e.last_name, b.warranty_status, b.cc_product_id, b.js_status, a.cc_complain_no, b.created_on, b.job_sheet, b.js_start_date, a.cc_case_id, a.complain_address, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name ,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join users e on e.id=b.engg_reassigned_to left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."' and accept_call = 2 and d.name in ('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV') GROUP BY b.job_sheet order by b.created_on ASC";
            }
        //echo $sql; die;
            return $this->db->query($sql)->result(); 
	}
	
    function get_complain_details($user_uuid, $complain_no, $user_type, $parameter)
	{
        if($parameter =='unaccept')
        {
            
            $sql = "SELECT b.cc_product_id, b.job_sheet, b.product_type, b.created_on, b.warranty_status, a.cc_case_id, a.cc_complain_no, a.complain_address, a.call_type, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name ,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."' and accept_call = 0 GROUP BY b.job_sheet order by b.created_on ASC";
        }
        if($parameter =='accept')
        {
            
            $sql = "SELECT b.cc_product_id, b.job_sheet, b.product_type, b.created_on, b.warranty_status, b.btr_no, b.js_status, b.created_on, b.job_sheet, a.preferd_datetime, a.cc_complain_no, a.call_type,a.cc_case_id, a.complain_address, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name ,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."'  and accept_call = 1 GROUP BY b.job_sheet order by b.created_on ASC";
        }
        if($parameter =='allocate')
        {
            
            $sql = "SELECT e.first_name, e.last_name, b.product_type, b.cc_product_id, b.js_status, b.warranty_status, b.created_on, b.job_sheet, b.js_start_date, a.cc_case_id, a.cc_complain_no, a.complain_address, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name ,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join users e on e.id=b.engg_reassigned_to left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."' and b.js_status != 'Closed' and accept_call = 2 GROUP BY b.job_sheet order by b.created_on ASC";
        }
		if($parameter =='Closed')
        {
            
            $sql = "SELECT e.first_name, e.last_name, b.product_type, b.cc_product_id, b.js_status, b.created_on, b.job_sheet, b.js_start_date, a.cc_case_id, a.complain_address, a.complain_pincode, a.complain_area, a.complain_city, c.name as prod_name, d.name as catg_name ,dd.name as parent_cat_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id left join users e on e.id=b.engg_reassigned_to left join aos_product_categories dd on d.parent_category_id = dd.id WHERE a.`isActiveComplain` = 1 and b.assigned_to ='".$user_uuid."' and b.js_status = 'Closed' and accept_call = 2 GROUP BY b.job_sheet order by b.created_on ASC";
        }
        return $this->db->query($sql)->result();
	}
	
        
	function get_cons_product($cust_id)
	{
	    // $sql = "SELECT count(a.cc_product_id) as prodId, b.name as prod_name, c.name as prod_type, a.purchase_date, a.cc_product_id, a.warranty_start_date, a.warranty_end_date, a.warranty_status, a.asset_serial_no, a.product_sap_code, a.js_final_closure, a.warranty_start_date, a.warranty_end_date, a.warranty_status, a.isActiveProduct FROM `tbl_cc_products` a left join aos_products b on a.`product_sub_type_id` = b.id left JOIN aos_product_categories c on c.id = a.`product_type_id` where  a.isActiveProduct = 1 and a.js_final_closure='Closed' and a.cust_id =".$cust_id;
		//$sql = "SELECT count(a.cc_product_id) as prodId, b.name as prod_name, c.name as prod_type, a.purchase_date, a.cc_product_id, a.warranty_start_date, a.warranty_end_date, a.warranty_status, a.asset_serial_no, a.product_sap_code, a.js_final_closure, a.warranty_start_date, a.warranty_end_date, a.warranty_status, a.isActiveProduct FROM `tbl_cc_products` a left join aos_products b on a.`product_sub_type_id` = b.id left JOIN aos_product_categories c on c.id = a.`product_type_id`  where a.isActiveProduct = 1 and a.js_final_closure='Closed' and a.cust_id =".$cust_id." group by a.cc_product_id ";
		
		$this->db->select('a.cc_product_id as prodId,a.asset_serial_no,b.name as prod_name,c.name as prod_type,a.purchase_date,a.warranty_end_date,a.warranty_start_date,a.isActiveProduct');
		$this->db->from('tbl_cc_products as a');
		$this->db->join('aos_products as b','a.product_sub_type_id=b.id','left');
		$this->db->join('aos_product_categories as c','c.id=a.product_type_id','left');
		$this->db->where('a.isActiveProduct','1');
		$this->db->where('a.cust_id',$cust_id);
		// echo $this->db->last_query(); exit;
		// $this->db->where('a.js_final_closure','Closed');
		$query = $this->db->get();
		
		return $query->result();
		//$shiv = $query->result();
		//echo "<pre>"; print_r($shiv); die;
		//echo $sql; die;
		 //return $r = $this->db->query($sql)->result();
	}	
	
	function get_engg_details($SF_id)
	{
		$this->db->select('user_name, first_name,last_name, id');
		$this->db->from('users');
		$this->db->where('deleted', 0);
		$this->db->where('reports_to_id', $SF_id);
		$res =  $this->db->get();
		return $res->result();
	}
	
	function get_closed_js($user_id, $user_uuid=null)
	{
		$this->db->select('js_feedback_id, js_feedback_status,jobsheet_no, complain_no, josheet_status, customer_name, customer_mobile');
		$this->db->from('tbl_cc_feedback');
		if(MASTER_CALLCENTER!=$user_id)
		{
			$this->db->where('feedbackAsseignedTo',$user_uuid);
		}
		if(MASTER_CALLCENTER==$user_id)
		{
			$this->db->where('feedbackAsseignedTo',NULL);
		}
		//$this->db->where('cc_product_id', $jsID);
		$this->db->where('isActiveFeedback', 1);
		//echo $this->db->last_query();exit;
		$res =  $this->db->get();
		//echo $this->db->last_query();exit;
		return $res->result();
	}
	
	function get_consumer_comp($cust_id)
	{
			$sql = "SELECT a.cc_complain_no, a.appointment_date,a.cc_case_id, b.cc_product_id, a.complain_created_on, a.complain_status, b.job_sheet, b.purchase_date, b.product_sap_code, b.js_status, c.name as prod_name, d.name as catg_name FROM `tbl_cc_complain` a left JOIN tbl_cc_products b on a.product_random_id = b.js_case_id left join aos_products c on c.id=b.product_sub_type_id left join aos_product_categories d on d.id = b.product_type_id WHERE a.`isActiveComplain` = 1 and a.cust_id =".$cust_id;
			return $this->db->query($sql)->result();
	}
	public function get_states_names()
	{
		$sql="SELECT pc.state_c FROM sar_area_pinmaster p left join sar_area_pinmaster_cstm pc on p.id=pc.id_c where p.deleted=0 and pc.state_c is not null and pc.state_c!= 'Null' group by pc.state_c order by pc.state_c ASC ";
		return $this->db->query($sql)->result(); 
	}
	
	public function get_district_names($state)
	{
		$sql="SELECT pc.`district_c`,pc.state_c FROM sar_area_pinmaster p left join sar_area_pinmaster_cstm pc on p.id=pc.id_c where p.deleted=0 and pc.state_c='".$state."' group by pc.district_c order by pc.district_c ASC ";
		return $this->db->query($sql)->result();
	}
	
	public function get_city_names($district)
	{
		 $sql="SELECT pc.city_c,pc.state_c FROM sar_area_pinmaster p left join sar_area_pinmaster_cstm pc on p.id=pc.id_c where p.deleted=0 and pc.district_c='".$district."' group by pc.city_c order by pc.city_c ASC "; 
		return $this->db->query($sql)->result();
	}
	

	public function get_area_names($city)
	{
	   $sql="SELECT pc.id_c,pc.area_c FROM sar_area_pinmaster p left join sar_area_pinmaster_cstm pc on p.id=pc.id_c where p.deleted=0 and pc.city_c='".$city."' group by pc.area_c order by pc.area_c ASC ";
		return $this->db->query($sql)->result();
	}
	public function get_pincode($area)
	{
	    $sql="SELECT p.id,p.name FROM sar_area_pinmaster p left join sar_area_pinmaster_cstm pc on p.id=pc.id_c where p.deleted=0 and pc.area_c='".$area."' ";
		return $this->db->query($sql)->row();
	}

	public function get_count($user_uuid)
	{
		$new = $this->db->query("select count(cc_product_id) as new from tbl_cc_products a left join aos_product_categories d on d.id = a.product_type_id WHERE a.assigned_to ='".$user_uuid."' and a.js_status != 'Closed' and a.accept_call = 0")->result();

		$accept = $this->db->query("select count(cc_product_id) as accept from tbl_cc_products a left join aos_product_categories d on d.id = a.product_type_id WHERE a.assigned_to ='".$user_uuid."' and a.js_status != 'Closed' and accept_call = 1")->result();
		
		$allocate = $this->db->query("select count(cc_product_id) as allocate from tbl_cc_products a left join aos_product_categories d on d.id = a.product_type_id WHERE a.assigned_to ='".$user_uuid."' and a.js_status != 'Closed' and accept_call = 2")->result();

		$newb = $this->db->query("select count(cc_product_id) as newb from tbl_cc_products a left join aos_product_categories d on d.id = a.product_type_id WHERE a.assigned_to ='".$user_uuid."' and a.js_status != 'Closed' and a.accept_call = 0 and d.name in ('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV')")->result();

		$acceptb = $this->db->query("select count(cc_product_id) as acceptb from tbl_cc_products a left join aos_product_categories d on d.id = a.product_type_id WHERE a.assigned_to ='".$user_uuid."' and a.js_status != 'Closed' and accept_call = 1 and d.name in ('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV')")->result();
		
		$allocateb = $this->db->query("select count(cc_product_id) as allocateb from tbl_cc_products a left join aos_product_categories d on d.id = a.product_type_id WHERE a.assigned_to ='".$user_uuid."' and a.js_status != 'Closed' and accept_call = 2 and d.name in ('CV','2-W','AUTO BATTERIES','TRACTOR','3-W','CAR & UV')")->result();
		
		return $new[0]->new.'|'.$accept[0]->accept.'|'.$allocate[0]->allocate.'|'.$newb[0]->newb.'|'.$acceptb[0]->acceptb.'|'.$allocateb[0]->allocateb;
	}

	public function get_cc_emp($user_id)
	{
		return $this->db->select('id, first_name, last_name')->get_where('users', array('reports_to_id'=>$user_id))->result();
	}
	
}
?>
