<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fault_parts_model extends CI_Model 
{
	public function get_parts_details($id)
	{
		/* //echo $id; exit;
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('service_call_id', $id);
		$this->db->order_by("service_call_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows(); */
		
		/* $query 	= $this->db->query("");
		$row 	= $query->result();
		return $row; */
		$sql = "SELECT * FROM  tbl_service_calls as a right join tbl_service_products as b on a.callAssetId = b.productGuid where a.service_call_id ='".$id."'";
		//echo $sql; die("Query");
		return $this->db->query($sql)->result();
		/*  if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		} */
    }
	function get_product_wrrnt($case)
	{
		$sql = "SELECT * FROM `tbl_service_calls` as a left join tbl_service_products as b on a.`callAssetId` = b.productGuid where a.`call_id` = '".$case."'";
		//echo $sql; die;
		return $r = $this->db->query($sql)->result();
	}
	function check_ageing($prod_serial)
	{
		$sql = "SELECT a.`AGING_LIMIT` 
				FROM  `tbl_model_master1` AS a
				LEFT JOIN tblProductSerialNum1 AS b ON a.`MODEL_CODE` = b.model
				WHERE b.serial_num =  '".$prod_serial."'";
		//echo $sql; die;
		return $r = $this->db->query($sql)->result();
	}
	
	function get_cas_status($case)
	{
		$sql = "SELECT caLL_case_status FROM `tbl_service_calls` where call_id='".$case."' and caLL_case_status IN(1,2,3)";
		//echo $sql; die;
		return $r = $this->db->query($sql)->num_rows();
	}
	
	function check_status($caseId)
	{
		$this->db->select('replacementMode');
		$this->db->from('tbl_service_calls');
		$this->db->where("call_id", $caseId);
		$query = $this->db->get();
	   // echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
// Battery Test report
		function get_comp_date($case)
		{
			$sql=$this->db->query("SELECT callRegDate FROM tbl_service_calls WHERE call_id='".$case."' ");
			//echo $sql; exit;
			return $run=$sql->result();
		}
		function check_decision($case)
		{
			$sql=$this->db->query("SELECT caLL_case_status FROM tbl_service_calls WHERE call_id='".$case."' ");
			//echo $sql; exit;
			return $run=$sql->result();
		}		
		
		function status_report($case)
		{
			$sql = "SELECT caseId FROM `tblservicecasetestreport` WHERE caseId = '".$case."'"; 
			//echo $sql; die;
			$r = $this->db->query($sql)->num_rows();
		}
		
		function upd_dec($status, $case)
		{
			if($status == "1" )
			{
				$st = "1";
				$sg = "12";
				$arr1 = array('callStatus'=>$st , 'callStage'=>$sg);
				$this->db->where('call_id', $case);
				$this->db->update('tbl_service_calls',$arr1);
				if($this->db->affected_rows() > 0)
				{
					 return $this->db->affected_rows();
				}
				else
				{
					return '';
				}
			}
			if($status == "2")
			{
				$st = "2";
				$sg = "5";
				$arr1 = array('callStatus'=>$st , 'callStage'=>$sg);
				$this->db->where('call_id', $case);
				$this->db->update('tbl_service_calls',$arr1);
				if($this->db->affected_rows() > 0)
					{
						 return $this->db->affected_rows();
					}
					else
					{
						return '';
					}
			}
			if($status == "3")
			{
				$st = "2";
				$sg = "2";
				$arr1 = array('callStatus'=>$st , 'callStage'=>$sg);
				$this->db->where('call_id', $case);
				$this->db->update('tbl_service_calls',$arr1);
				if($this->db->affected_rows() > 0)
				{
					 return $this->db->affected_rows();
				}
				else
				{
					return '';
				}
			}
		}
		
		function update_status_stage($arr, $caseId)
		{
			$this->db->where('call_id', $caseId);
			$this->db->update('tbl_service_calls',$arr);
			if($this->db->affected_rows() > 0)
			{
				 return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		}
		
	function insert_batt_report($arr)
	{
		$this->db->insert('tblservicecasetestreport', $arr);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
	}
	
	public function gets_challan_list_month($challan_month, $product_type){ //echo $challan_month .'/'. $product_type; die;
		$challan_month1 = explode('-', $challan_month);
		$challan_month_yy = $challan_month1[0];
		$challan_month_mm = $challan_month1[1];
		$challan_month_start = $challan_month_yy .'-'. $challan_month_mm .'-'. '01';
		$challan_month_lastm = $challan_month_yy .'-'. $challan_month_mm .'-'. '31';
		//echo $challan_month_yy; die;
		$sql=$this->db->query("SELECT i.*,ic.* FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								where MONTH(i.date_entered) = '".$challan_month_mm."' AND YEAR(i.date_entered) = YEAR(CURRENT_DATE()) and DATE_FORMAT(i.date_entered,'%d') between '01' and '31' AND ic.product_c = '".$product_type."'");
								//echo $this->db->last_query(); die();
							 return $run=$sql->result_array();
	}
	
	public function gets_challan_list($user){
		$sql=$this->db->query("SELECT i.id,i.name,i.date_entered,ic.brand_c,ic.product_c,ic.model_c,ic.battery_serial_num_c,i.invoice_date 
								FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								where i.billing_account_id ='".$user."' AND MONTH(i.date_entered) = MONTH(CURRENT_DATE()) AND YEAR(i.date_entered) = YEAR(CURRENT_DATE()) and DATE_FORMAT(i.date_entered,'%d') between '01' and '31'
								");
								//echo $this->db->last_query(); die("Challan");
							 return $run=$sql->result_array();
	}
	
	function get_challan_list($user)
	{
		$sql=$this->db->query("SELECT i.id,i.name,ic.brand_c,ic.product_c,ic.model_c,ic.battery_serial_num_c,i.invoice_date 
								FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								where i.billing_account_id ='".$user."'
								order by i.date_entered
								
								");
							//	echo $this->db->last_query(); die("Challan");
							 return $run=$sql->result_array();

	}
	/*function get_date_challan($from, $to)
	{
		$sql=$this->db->query("SELECT a.`id_c`, a.`brand_c`,b.invoice_date, a.`product_c`, a.`model_c`, a.`battery_serial_num_c`, a.`complaint_no_c`,b.name, b.billing_address_street, b.billing_address_city  ,b.billing_address_state, 
		b.billing_address_postalcode, b.billing_address_country FROM `aos_invoices_cstm` as a left join  aos_invoices as b 
		on b.id = a.`id_c`
		WHERE date(b.invoice_date) BETWEEN '".$from."' AND '".$to."'
		");
		//echo $this->db->last_query(); die("HDzbf");
		return $sql->result(); 
	}*/
	function get_date_challan($from, $to, $pname, $user)
	{
		if($pname == 1)
		{
			$pname ="2-W";
		}
		if($pname == 3)
		{
			$pname ="E-RICKSHAW";
		}
		if($pname == 4)
		{
			$pname ="IB";
		}
		if($pname == 5)
		{
			$pname ="INV";
		}
		
		$sql=$this->db->query("SELECT a.`id_c`, a.`brand_c`,b.invoice_date, a.`product_c`, a.`model_c`, a.`battery_serial_num_c`, a.`complaint_no_c`,b.name, 
		b.billing_address_street, b.billing_address_city  ,b.billing_address_state, 
		b.billing_address_postalcode, b.billing_address_country,b.total_amt,a.aprox_scrap_value_c,
        c.name as acc_nam, c.billing_address_city as prb_city, c.billing_address_state as prb_state, c.billing_address_postalcode as prb_pincode, c.billing_address_country as prb_ctry
		FROM `aos_invoices_cstm` as a left join  aos_invoices as b
		on b.id = a.`id_c` 
		LEFT JOIN accounts as c 
		on c.id = b.billing_account_id WHERE date(b.invoice_date) BETWEEN '".$from."' AND '".$to."' AND  a.product_c='".$pname."' and b.billing_account_id = '".$user."' AND a.sync_to_dms_c !=1");
		//echo $this->db->last_query(); die("    - HDzbf");
		return $sql->result(); 
	}
	
	function get_date_challan2($from, $to, $p1, $p2, $p3, $p4, $user)
	{
		$sql=$this->db->query("
		SELECT a.`id_c`, a.`brand_c`,b.invoice_date, a.`product_c`, a.`model_c`, a.`battery_serial_num_c`, a.`complaint_no_c`,b.name, 
		b.billing_address_street, b.billing_address_city  ,b.billing_address_state, 
		b.billing_address_postalcode, b.billing_address_country,b.total_amt,a.aprox_scrap_value_c,
        c.name as acc_nam, c.billing_address_city as prb_city, c.billing_address_state as prb_state, c.billing_address_postalcode as prb_pincode, c.billing_address_country as prb_ctry
		FROM `aos_invoices_cstm` as a left join  aos_invoices as b
		on b.id = a.`id_c` 
		LEFT JOIN accounts as c 
		on c.id = b.billing_account_id
		WHERE date(b.invoice_date) BETWEEN '".$from."' AND '".$to."' 
		AND  a.product_c='".$p1."' or a.product_c='".$p2."' or a.product_c='".$p3."' or a.product_c='".$p4."' 
		AND b.billing_account_id = '".$user."' AND a.sync_to_dms_c !=1 ");
		//echo $this->db->last_query(); die("    - HDzbf");
		return $sql->result(); 
	}
	
	public function insert_generate_challan_view_print($data){
			$query = $this->db->query("select battery_serial_num_c from aos_invoices_cstm");
			$generate_challanviewprint = $query->result(); 
			$update_data = array('sync_to_dms_c'=>'1');
			
			foreach($generate_challanviewprint as $challan){ 
				if($data['serial_number'] == $challan->battery_serial_num_c){
					//$query = $this->db->query("update aos_invoices_cstm set sync_to_dms_c='1' where battery_serial_num_c= '". $challan->battery_serial_num_c."'"); 
					
					$query = $this->db->where('battery_serial_num_c', $challan->battery_serial_num_c); 
							 $this->db->update('aos_invoices_cstm',$update_data);
				}
			}
		$this->db->insert('tbl_service_challan_reprint', $data);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		
	}
	
	
	public function re_gets_challan_details($from_date, $to_date, $challan_date_mm, $product_type){
		$challan_date_start_mm = '2018-' .$challan_date_mm . '-01';
		$challan_date_last_mm = '2018-' .$challan_date_mm . '-31';
		
		$product_types =$product_type; 
		if($product_types == 'TRACTOR')
		{
		$product_types_CAR = "3-W"."','"."CAR & UV"."','"."CV"."','"."TRACTOR";
		}
		else 
		{
			$product_types_CAR = $product_types;
		}
		if(!empty($challan_date_mm)){ //echo "hello"; die;
		
			$query=$this->db->query("
			SELECT tbl_service_challan_reprint.* FROM tbl_service_challan_reprint
								WHERE MONTH(generated_challan_date) = '".$challan_date_mm."' AND YEAR(generated_challan_date) = YEAR(CURRENT_DATE()) and DATE_FORMAT(generated_challan_date,'%d') between '".$from_date."' and '".$to_date."'");
								//echo $this->db->last_query(); die();
								
								$challan_detail=$query->result_array(); /*
								$challan_date = explode('-', $challan_detail[0]['date_entered']);
								$challan_yy = $challan_date[0];
								$challan_mm = $challan_date[1];
								$challan_dd = $challan_date[2];
								$last_challan_date = $challan_yy .'-'. $challan_mm .'-'. $to_date;
								$record_array = array();
								foreach($challan_detail as $record){
									$check_record = $this->db->get_where('tbl_service_challan_reprint', array('serial_number'=>$record['battery_serial_num_c']))->row_array();
									
									if(!empty($check_record)) continue;
									$record = array(
										'complaint_no' => $record['complaint_no_c'],
										'serial_number' => $record['battery_serial_num_c'],
										'brand' => $record['brand_c'],
										'product' => $record['product_c'],
										'model' => $record['model_c'],
										'scrap_value' => $record['aprox_scrap_value_c'],
										'address' => 'Test',
										'state' => $record['billing_address_state'],
										'city' => $record['billing_address_city'],
										'pincode' => $record['billing_address_postalcode'],
										'contact_person' => 'Mr Saurav',
										'contact_mobile' => '9818540685',
										'dist_firm_name' => $record['name'],
										'GST' => date('YmdHis'),
										'generated_date' => $record['invoice_date'],
										'generated_ip' => $_SERVER['REMOTE_ADDR'],
										'challan_number' => $challan_number,
										'challan_date' => $record['invoice_date'],
										'submitted_by' => $usern,
										'generated_challan_date' => date('Y-m-d'),
										'modified_by' => date('Y-m-d H:i:s'),
										'from_date' => date('Y-m-d H:i:s'),
										'to_date' => $last_challan_date,
										'total_amt' => $record['total_amt'],
										
									);
									$record_array[] = $record;
									$this->db->insert('tbl_service_challan_reprint', $record);
								}*/
								return $challan_detail;
		}
	}
	
	
	public function gets_challan_details($from_date, $to_date, $challan_date_mm, $challan_number, $product_type,$user){
		$challan_date_start_mm = '2018-' .$challan_date_mm . '-01';
		$challan_date_last_mm = '2018-' .$challan_date_mm . '-31';
		$product_types =$product_type; 
		if($product_types == 'TRACTOR')
		{
		$product_types_CAR = "3-W"."','"."CAR & UV"."','"."CV"."','"."TRACTOR";
		}
		else 
		{
			$product_types_CAR = $product_types;
		}
		
		if(!empty($challan_date_mm)){
			
			$query=$this->db->query("
			SELECT i.*,ic.* FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								WHERE MONTH(i.date_entered) = '".$challan_date_mm."' AND YEAR(i.date_entered) = YEAR(CURRENT_DATE()) and DATE_FORMAT(i.date_entered,'%d') between '".$from_date."' and '".$to_date."' AND ic.product_c in('".$product_types_CAR."') and i.billing_account_id = '".$user."' limit 2");
								//echo $this->db->last_query(); die();
								
								$challan_detail=$query->result_array();
								$challan_date = explode('-', $challan_detail[0]['date_entered']);
								$challan_yy = $challan_date[0];
								$challan_mm = $challan_date[1];
								$challan_dd = $challan_date[2];
								$last_challan_date = $challan_yy .'-'. $challan_mm .'-'. $to_date;
								$record_array = array();
								foreach($challan_detail as $record){
									$check_record = $this->db->get_where('tbl_service_challan_reprint', array('serial_number'=>$record['battery_serial_num_c']))->row_array();
									
									if(!empty($check_record)) continue;
									$record = array(
										'complaint_no' => $record['complaint_no_c'],
										'serial_number' => $record['battery_serial_num_c'],
										'brand' => $record['brand_c'],
										'product' => $record['product_c'],
										'model' => $record['model_c'],
										'scrap_value' => $record['aprox_scrap_value_c'],
										'address' => $record['billing_address_street'],
										'state' => $record['billing_address_state'],
										'city' => $record['billing_address_city'],
										'pincode' => $record['billing_address_postalcode'],
										'contact_person' => $record['hub_contact_person_c'],
										'contact_mobile' => $record['hub_contact_phone_c'],
										'dist_firm_name' => $record['name'],
										'GST' =>  $record['gstin_number_c'],
										'generated_date' => $record['invoice_date'],
										'generated_ip' => $_SERVER['REMOTE_ADDR'],
										'challan_number' => $challan_number,
										'challan_date' => $record['invoice_date'],
										'submitted_by' => $usern,
										'generated_challan_date' => date('Y-m-d'),
										'modified_by' => date('Y-m-d H:i:s'),
										'from_date' => date('Y-m-d H:i:s'),
										'to_date' => $last_challan_date,
										'total_amt' => $record['total_amt'],
										
									);
									$record_array[] = $record;
									$this->db->insert('tbl_service_challan_reprint', $record);
								}
								return $record_array;
		}/*
		$query=$this->db->query("
		SELECT i.*,ic.* FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								WHERE MONTH(i.date_entered) = MONTH(CURRENT_DATE()) AND YEAR(i.date_entered) = YEAR(CURRENT_DATE()) and DATE_FORMAT(i.date_entered,'%d') between '".$from_date."' and '".$to_date."'");
								//echo $this->db->last_query(); die();
								
								$challan_detail=$query->result_array();
								$challan_date = explode('-', $challan_detail[0]['date_entered']);
								$challan_yy = $challan_date[0];
								$challan_mm = $challan_date[1];
								$challan_dd = $challan_date[2];
								$last_challan_date = $challan_yy .'-'. $challan_mm .'-'. $to_date;
								$record_array = array();
								foreach($challan_detail as $record){
									$check_record = $this->db->get_where('tbl_service_challan_reprint', array('serial_number'=>$record['battery_serial_num_c']))->row_array();
									
									if(!empty($check_record)) continue;
									$record = array(
										'complaint_no' => $record['complaint_no_c'],
										'serial_number' => $record['battery_serial_num_c'],
										'brand' => $record['brand_c'],
										'product' => $record['product_c'],
										'model' => $record['model_c'],
										'scrap_value' => $record['aprox_scrap_value_c'],
										'address' => 'Test',
										'state' => $record['billing_address_state'],
										'city' => $record['billing_address_city'],
										'pincode' => $record['billing_address_postalcode'],
										'contact_person' => 'Mr Saurav',
										'contact_mobile' => '9818540685',
										'dist_firm_name' => $record['name'],
										'GST' => date('YmdHis'),
										'generated_date' => $record['invoice_date'],
										'generated_ip' => $_SERVER['REMOTE_ADDR'],
										'challan_number' => $challan_number,
										'challan_date' => $record['invoice_date'],
										'submitted_by' => $usern,
										'modified_by' => date('Y-m-d H:i:s'),
										'from_date' => date('Y-m-d H:i:s'),
										'to_date' => $last_challan_date,
										'total_amt' => $record['total_amt'],
										
									);
									$record_array[] = $record;
									$this->db->insert('tbl_service_challan_reprint', $record);
								}
								return $record_array; */
	}
	
	
	function get_challan_details($id){
		$query=$this->db->query("
		SELECT i.*,ic.brand_c,ic.product_c,ic.model_c,ic.battery_serial_num_c,ic.complaint_no_c,i.invoice_date,
		ac.name,ac.date_entered,ac.billing_address_city,ac.	billing_address_state,ac.billing_address_postalcode,ac.billing_address_country, i.total_amt,
		ac.phone_office, ic.aprox_scrap_value_c  
								FROM aos_invoices i
								left Join aos_invoices_cstm ic
								on i.id=ic.id_c
								left join accounts as ac
								on ac.id = i.billing_account_id
								WHERE i.id='".$id."'
								");
								//echo $this->db->last_query(); die("jhbdf");
								
								$challan_detail=$query->result_array();
								$data1 = array(
									 'complaint_no' => $challan_detail[0]['complaint_no_c'],
									 'serial_number' => $challan_detail[0]['battery_serial_num_c'],
									 'brand' => strtoupper($challan_detail[0]['brand_c']),
									 'product' => $challan_detail[0]['product_c'],
									 'model' => $challan_detail[0]['model_c'],
									 'scrap_value' => $challan_detail[0]['aprox_scrap_value_c'],
									 'address' => '',
									 'state' => $challan_detail[0]['billing_address_state'],
									 'city' => $challan_detail[0]['billing_address_city'],
									 'pincode' => $challan_detail[0]['billing_address_postalcode'],
									 'contact_person' => 'Mr Saurav',
									 'contact_mobile' => '9818540685',
									 'dist_firm_name' => $challan_detail[0]['name'],
									 'GST' => date('YmdHis'),
									 'generated_date' => $challan_detail[0]['invoice_date'],
									 'generated_ip' => $_SERVER['REMOTE_ADDR'],
									 'challan_number' => $challan_detail[0]['number'],
									 'challan_date' => $challan_detail[0]['invoice_date'],
									 'submitted_by' => $usern,
									 'modified_by' => date('Y-m-d H:i:s'),
									 'sent_to' => '',
									 'others_detail' => '',
									 'from_date' => '',
									 'to_date' => ''
								);
								$query = $this->db->query("select battery_serial_num_c from aos_invoices_cstm");
								$generate_challanviewprint = $query->result(); 
								$update_data = array('sync_to_dms_c'=>'1');
								
								foreach($generate_challanviewprint as $challan){ 
									if($data1['serial_number'] == $challan->battery_serial_num_c){
										
										$query = $this->db->where('battery_serial_num_c', $challan->battery_serial_num_c); 
												 $this->db->update('aos_invoices_cstm',$update_data);
									}
								}
								$this->db->insert('tbl_service_challan_reprint', $data1);
								return $challan_detail;
							 
							 

	}
	
	
	public function challan_service_reprint(){
		$this->db->select('*');
		$this->db->from('tbl_service_challan_reprint');
		//$this->db->limit(0,30);
		$query = $this->db->get();
		return $query->result();
		//echo $this->db->last_query(); die;
	}
	
	function update_replace_mod($rep, $case)
	{
		
	}
	
	/*Verify Product serial num*/
	function get_serial_case($case)
	{
		$sql=$this->db->query("SELECT callProductSerialNo FROM tbl_service_calls WHERE call_id='".$case."'");
	  //echo $this->db->last_query(); die("jhbdf");
	  return $run=$sql->result();
	}
	
	function block_serial($sr_no, $user)
	{
		function getGUID()
			 {
						if (function_exists('com_create_guid'))
						{
							return com_create_guid();
						}
						else
						{
							mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
							$charid = strtoupper(md5(uniqid(rand(), true)));
							$hyphen = chr(45);// "-"
							$uuid = substr($charid, 0, 8).$hyphen
								.substr($charid, 8, 4).$hyphen
								.substr($charid,12, 4).$hyphen
								.substr($charid,16, 4).$hyphen
								.substr($charid,20,12);
							return $uuid;
						}
			}
					$guid = getGUID();
					
	//	echo $old_sr;
		$date = date("Y-m-d H:i:s");
		$st = "inactive";
		$data = array(
						'id' => $guid,
						'name' => $sr_no,
						'date_entered' => $date,
						'date_modified' => $date,
						'modified_user_id' => $user,
						'created_by' => $user,
						'status' => $st
					);
			
			$this->db->insert('sar_serial_master', $data);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		//}	
	}
	
	function get_serial_data($case, $user)
	{
	  $sql=$this->db->query("SELECT callProductSerialNo, callRegDate, callAssetId, callCreatedBy, callCustId FROM tbl_service_calls WHERE call_id='".$case."' && callCreatedBy='".$user."'");
	  //echo $this->db->last_query(); die("jhbdf");
	  return $run=$sql->result();
	}
	function check_blocking($prod_serial)
	{
		$sql=$this->db->query("SELECT  `name` FROM `sar_serial_master` WHERE `name` ='".$prod_serial."' ");
			//echo $sql; exit;
		return $run=$sql->result();	
	}
	
	/*Verify Product serial num*/
	
	function update_decision($data, $case)
	{
			// $case = $data['call_id']; 
		// echo $case; die;
		//echo "<pre>"; print_r($data); die;
		$this->db->where('call_id', $case);
		$this->db->update('tbl_service_calls',$data);
		if($this->db->affected_rows() > 0)
			{
				 return $data['caLL_case_status'];
			}
			else
			{
				return '';
			}
	}
	
	function block_old_sr($old_sr, $guid, $user)
	{
	//	echo $old_sr;
		$date = date("Y-m-d H:i:s");
		$st = "inactive";
		$data = array(
						'id' => $guid,
						'name' => $old_sr,
						'date_entered' => $date,
						'date_modified' => $date,
						'modified_user_id' => $user,
						'created_by' => $user,
						'status' => $st
					);
	// echo "<pre>"; print_r($data); die("poppopop");
       /* 	
         $sql=$this->db->query("SELECT name FROM sar_serial_master WHERE name='".$old_sr."' && status='".$st."'");
	   echo $this->db->last_query(); die("jhbdf");					
		 echo $run=$sql->num_rows();    
		 die("Available");
        if($run > 0)
        {
			return '1';
		}
        else
		{ */			
			$this->db->insert('sar_serial_master', $data);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		//}	
	}
	function replace_serial_old($prod_serial, $callAssetId, $consumerId, $old_sr, $user)
	{
		$date = date("Y-m-d H:i:s");
		$mode ="1";
		$st = "5";
		$sg = "8";
		
		$rep_arc = array(
							'cust_product_id' =>  $prod_serial
						);
		$rep_arp = array(
							'productSerialNo' =>  $prod_serial
						); 	
		$rep_sar = array(
							'product_serial_no_c' =>  $prod_serial
						);
		$rep_call= array(
							'replacedBy' =>  $user,
							'callStatus' =>  $st,
							'callStage' =>  $sg,
							'dateOfReplacementRemark' =>  $date,
							'callProductSerialNo' =>  $prod_serial,
							'replacementMode' =>  $mode,
							'oldSerialNum' =>  $old_sr,
							'callStage' =>  $stage
						);	
	 	/* echo "Sr".$old_sr;				
        echo "<pre>"; print_r($rep_arc);  
        echo "<pre>"; print_r($rep_arp);  
        echo "<pre>"; print_r($rep_sar); 
        echo "<pre>"; print_r($rep_call); die("Jai");   */
						
		$this->db->where('productSerialNo', $old_sr);
		$this->db->update('tbl_service_products',$rep_arp);	
		//echo $this->db->last_query(); DIE("jABFJBA"); 
		if($this->db->affected_rows() > 0)
		{
			$this->db->where('product_serial_no_c', $old_sr);
			$this->db->update('sar_asset_cstm', $rep_sar);
			//echo $this->db->last_query(); DIE("jABFJBA");
			if($this->db->affected_rows() > 0)
			{
				 //$this->db->where('callCreatedBy', $user);
			    $this->db->where('callProductSerialNo', $old_sr);
				$this->db->update('tbl_service_calls',$rep_call);	
				if($this->db->affected_rows() > 0)
				{
					 //$this->db->where('callCreatedBy', $user);
					 $this->db->where('cust_product_id', $old_sr);
					 $this->db->update('tbl_service_customers',$rep_arc);	
					 return $this->db->affected_rows();
				}
				else
				{
					return 'e1';
				}
			}
			else
			{
				return 'e2';
			}
			
		}
		else
		{
			return 'e3';
		}
			
			/* 	$this->db->where('call_id', $case);
				$this->db->update('sar_asset_cstm',$data);
				if($this->db->affected_rows() > 0)
					{
						 return $data['caLL_case_status'];
					}
					else
					{
						return '';
					} */
	}
	
	function get_case_status($case)
	{
		// $status = "inactive";
		$this->db->select('caLL_case_status');
		$this->db->from('tbl_service_calls');
		// $this->db->order_by("service_call_id", "desc");
		$this->db->where('call_id', $case);
		// $this->db->where('caLL_case_status', $case);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	function get_serial_info($prod_serial)
	{
		$this->db->select('productSerialNo');
		$this->db->from('tbl_service_products');
		// $this->db->order_by("service_call_id", "desc");
		$this->db->where('productSerialNo', $prod_serial);
		// $this->db->where('caLL_case_status', $case);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		else
		{
			return 0;
		}
	}
	
	function update_replace_settle($mode, $others, $date, $case, $user)
	{
		//echo "Hikjdnfkajnfkj"; print_r($data);
		//$case = $data['call_id']; 
		// echo $case; die;
		//print_r($data['caLL_case_status']); die;
		$arr = array(
		             'replacementMode' => $mode,
		             'otherRemarks' => $others,
		             'dateOfReplacementRemark' => $date
		);
		$this->db->where('callCreatedBy', $user);
		$this->db->where('call_id', $case);
		$this->db->update('tbl_service_calls', $arr);
		return $this->db->affected_rows();
	}
	function get_symptom_code()
	{
		$this->db->select('*');
		$this->db->from('tbl_defect_symptoms');
		// $this->db->order_by("service_call_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
	}
	
	function insert_fault_parts($data, $data_pb)
	{
		/* echo "<pre>"; print_r($data); 
		echo "<pre>"; print_r($data_pb);
        echo $data['caseId']; 		
		die; */
		//echo $data['caseid']; die;
		   
		   
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('call_id', $data['caseId']);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
				$this->db->update('tbl_service_calls', $data_pb);
				$this->db->where('call_id', $data['caseId']); 
				if($this->db->affected_rows() > 0)
				{
					 return $data['distributorActionCode'];
				}
				else
				{
					return '';
				}
		}
		else
		{
			$this->db->insert('tbl_service_faults_parts', $data);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}
		}
		   
		   
		  /*  
		  // echo $this->db->affected_rows();
		   
			$this->db->insert('tbl_service_faults_parts', $data);
			if($this->db->affected_rows() > 0)
			{
				$data['caseId']; die("Affected Row");
				$this->db->update('tbl_service_calls', $data_pb);
				$this->db->where('call_id', $data['caseId']); 
				//$this->db->get();
				//echo $this->db->last_query();
				if($this->db->affected_rows() > 0)
				{
					 return $data['distributorActionCode'];
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			} */
		  
	}
	
	function insert_replace_details($battery)
	{
		$this->db->insert('tbl_service_replacement', $battery);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}	
		
	}
	
	function get_product_type($old_sr)
	{
		 $sql = $this->db->query("SELECT  `product` FROM `tblProductSerialNum1` WHERE `serial_num` = '".$old_sr."'"); 
		 //echo $this->db->last_query(); die("Query");
			return $result=$sql->result();
	}
	function get_new_product($prod_serial)
	{
		
		 $sql = $this->db->query("SELECT  `product` FROM `tblProductSerialNum1` WHERE `serial_num` = '".$prod_serial."'"); 
		 //echo $this->db->last_query(); die("Query");
			return $result=$sql->result();
	}
	function check_replcaement($case, $user)
	{
		$sql = $this->db->query("SELECT `replacementMode`  FROM `tbl_service_calls` WHERE `call_id` = '".$case."' && callCreatedBy='".$user."' "); 
		// echo $this->db->last_query(); die("Query");
			return $result=$sql->result();
	}
	 
	function check_replcaement_ag($case, $user)
	{
		$sql = $this->db->query("SELECT `productSerialNo` FROM `tbl_service_replacement` WHERE `submittedByDist`='".$user."' && `caseId` = '".$case."'"); 
		// echo $this->db->last_query(); die("Query");
		return $result=$sql->result();
	}
	 
		public function serial_verify_data($prod_serial)
		{
			 $sql = $this->db->query("
				SELECT * FROM tblProductSerialNum1 tbl1 
				LEFT JOIN tbl_model_master1 tbl2 
				ON tbl1.model = tbl2.MODEL_CODE 
				WHERE tbl1.serial_num = '".$prod_serial."'"); 
			return $result=$sql->result();
		}
		
		function get_product_status($prod_serial)
	    {
			$query = $this->db->query("SELECT name,status FROM `sar_serial_master` WHERE `status` = 'inactive' AND `name` ='".$prod_serial."'");
			   // echo $this->db->last_query();  die;
			return $query->num_rows(); 
	    }
		function get_serial_num($case)
	    {
				$query = $this->db->query("SELECT callProductSerialNo FROM `tbl_service_calls` WHERE `call_id`='".$case."' ");
			  	return $query->result(); 
	    }
		function update_sar_serial($master)
	    {
			$this->db->insert('sar_serial_master', $master);
			if($this->db->affected_rows() > 0)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return '';
			}	
				
	    }
		
		
		public function serial_verify_data_case($caseId){
			$sql = $this->db->query("SELECT cust_code,CONCAT(fname,' ',lname) custnm FROM tbl_service_customers tbl1
								LEFT JOIN tbl_service_calls tbl2
								ON tbl1.cust_code = tbl2.callCustId
								WHERE call_id='".$caseId."'");
			return $result=$sql->result();
			
		}	
function getfault_detail($case,$user)
{
	//echo $case; die("sjdbh");
    $this->db->select('*');
		$this->db->from(' tbl_service_replacement');
		$this->db->where('caseId', $case);
		$this->db->where('submittedByDist', $user);
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}
}	
	
	
	
	public function case_report($data)
	{
	    $save = $this->db->insert('tblservicecasetestreport', $data);
		if($this->db->affected_rows() > 0)
		{
			return $this->db->affected_rows();
		}
		else
		{
			return '';
		}		
	}
	
     function get_case_details($id)
	 {
		$this->db->select('*');
		$this->db->from('tbl_service_calls');
		$this->db->where('service_call_id', $id);
		$this->db->order_by("service_call_id", "desc");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		// $this->db->num_rows();
		
		 if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return '';
		}

	 }
	 function get_cust_data($consumerId)
	 {
		 $sql = "select * from  tbl_service_customers where cust_code='".$consumerId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
	 }
	 function get_dealerName($dealerCode)
	 {
		 $sql = "SELECT `customer_number_c` FROM  `sar_dealer_cstm` WHERE  `id_c` = '".$dealerCode."' ";
					return  $this->db->query($sql)->row();	
	 }
	 function get_cust_prod($consumerId)
	 {
		 $sql = "select * from tbl_service_products where consumerId='".$consumerId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();	
	 }
	 function getTestData($us, $caseId)
	 {
		 $sql = "SELECT * FROM `tblservicecasetestreport` as a left join tbl_service_calls as b on b.call_id = a.caseId where a.submittedByDist= '".$us."' && a.caseId ='".$caseId."'";
				//echo $sql; die;
				return  $this->db->query($sql)->result();
	 }
	 function update_wrrnty_details($caseId)
		{
			$sql = "select * from tbl_service_calls where call_id='".$caseId."'";
			//echo $sql; die;
			return  $this->db->query($sql)->result();	
		}
		public function products_detail()
		{
				$sql = "SELECT * FROM `tblProductSerialNum1` group by `product` ";
					return  $this->db->query($sql)->result();	
		}
}

/* End of file user.php */
/* Location: ./application/models/user.php */
