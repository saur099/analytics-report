
 <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <title>Services | Call Management</title>
  <style>
  button, button#vfy {
    box-shadow: 2px 3px 4px 2px #EEE;
}
.card .card-title {
    color: #000;
    margin-bottom: .75rem;
    text-transform: capitalize;
    font-family: "ubuntu-medium", sans-serif;
    font-size: 1.125rem;
}
button.btn.btn-danger.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
button#submitSearch {
    box-shadow: 3px 4px 1px 1px #EEE;
}
button.btn.btn-gradient-success.mb-2 {
    box-shadow: 3px 3px 2px 1px #eee;
}
button.btn.btn-success.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
	background: linear-gradient(to right, #83f2bea8, #08b21d);
}
button.btn.btn-default.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
table#POITable {
    font-size: 11px;
}
.card-body.saur {
    padding: 0rem;
    margin-left: 0px;
}
div#cons1 {
    margin-bottom: -35px;
}

label {
    font-weight: 600;
}
.badge-success, .preview-list .preview-item .preview-thumbnail .badge.badge-online {
    border: 1px solid #146e13 !important;
}
.badge-success, .preview-list .preview-item .preview-thumbnail .badge.badge-online {
    background-color: #41e823 !important;
}
.badge{
	    font-size: 25px!important;
}
  </style>
  <script>
$(document).ready(function(){
    $("#prod1").click(function(){
        $("#cd1").show();
    });
    $("#submitSearch").click(function(){
        $("#cons1").show();
    });
	
});
function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('POITable').deleteRow(i);
}


function insRow() {
  console.log('hi');
  var x = document.getElementById('addmorePOIbutton');
  var new_row = x.rows[1].cloneNode(true);
  var len = x.rows.length;
  new_row.cells[0].innerHTML = len;

  var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
  inp1.id += len;
  inp1.value = '';
  var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
  inp2.id += len;
  inp2.value = '';
  x.appendChild(new_row);
}
</script> 
     <?php 
			$this->load->view('includes/sidebar.php');  
			$this->load->view('includes/call_center_asset.php');  
   ?>

   
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">

  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
	<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <button type="button" class="btn btn-danger btn-circle">1</button>
            <p>Create Customer</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-danger btn-circle">2</button>
            <p>Create Product</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-danger btn-circle" disabled="disabled">3</button>
               <p>Register Complain</p>
        </div> 
    </div>
</div>

     <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body saur">
				
               <!-----Saurav setup progressbar step process-->
	
<div class="container-fluid css">
        <div class="card-body">

      <!-- Breadcrumbs-->
      <!-- Example DataTables Card-->
    <div class="card border-success mb-3" style="max-width: 70rem;">
     
  <div class="card-body text-info">
  <div class="alert alert-success">
  <strong>Success !!</strong> Your Complain has been registered successfully. Please view details below. 
</div>
  <div class="row">
		  <div class="col-md-4">
		  <center>
				<span class="alerticon">
				<img src="http://bondonshop.com/images/checkmark.gif" height="100" width="120" alt="success" /></span>
				</center>
		  </div>
		  <div class="col-md-8">
				<div class="card complain" style="width: 30rem;">
  <div class="card-body">
    <h4>Complain Number Generated : </h4>
	<h5><span class="badge badge-success">C20180904123134</span></h5>
  </div>
</div>
 
				
		  </div>
  </div>
  </div>
</div>
</div>
</div>
		
			 <div class="col-lg-12 grid-margin stretch-card" id="cons1" >
            <div class="card border-primary mb-3" style="max-width: 72rem;">
  <div class="card-header"><b>Customer Details</b></div>
  <div class="card-body text-primary">
              	
					<div class="row">
					 <div class="col-md-2"><b>Customer Name :</b><input type="text" class="form-control" value="Mr. Tester<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Mobile No :</b><input type="text" class="form-control" value="9293842324<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Consumer Category :</b><input type="text" class="form-control" value="Consumer<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address1 :</b><input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address2 :</b><input type="text" class="form-control" value="bhagalpur, bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					 </div>
			<br/>
			
				<div class="row">	
					<div class="col-md-2"><b>Pincode :</b><input type="text" class="form-control" value="252345<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>				
					<div class="col-md-2"><b>Area :</b><input type="text" class="form-control" value="Patkalpur Loknath-1<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>City :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>District :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>State :</b><input type="text" class="form-control" value="Bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>Email :</b><input type="text" class="form-control" value="test@gmail.com<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					</div>
			     </div>
		
			
			</div>
			</div>
			</div>
			<hr/>
<!-- - ------------------------------------ Consumer Details Ends here ------------------------------------>	  



<!-- - ------------------------------------ Product Details Creation started here ------------------------------------>	 
<div class="card prod">
                <div class="card-body">
		 
 <div class="card-header"><b>Product Details</b></div>		
                    <div class="row">
                        <div id="POItablediv">
						  <table id="POITable" class="table table-bordered"  >
							<tr>
							  <th>JobSheet</th>
							  <th>Product Name</th>
							  <th>Date of Purchase</th>
							  <th>Warranty Start Date</th>
							  <th>Warranty End Date</th>
							  <th>Warranty Status</th>
							  <th>Dealer</th>
							  <th>Product SAP Code</th>
							  <th>Asset Serial no</th>
							  <th>View Jobsheet</th>
							  <th>Feedback</th>
							  <th>Create Jobsheet</th>
							</tr>
							<tr>
							   <td>JS-12873242180</td>
								  <td>SOLAR </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>Santya Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
								  <td><a href="<?php echo base_url(); ?>index.php/mod_call_center/view_jobsheet"><i class="fa fa-eye fa-2x"></i></a></td>
								  <td><a href="<?php echo base_url(); ?>index.php/mod_call_center/js_feedback"><i class="fa fa-play fa-2x" aria-hidden="true"></i></a></td>
								  <td><a href="<?php echo base_url(); ?>index.php/mod_call_center/view_jobsheet"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a></td>
								 
							</tr>
							<tr>
							   <td>JS-12873232412</td>
								  <td>INVERTER </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>INVR Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
								 <td><a href="<?php echo base_url(); ?>index.php/mod_call_center/view_jobsheet"><i class="fa fa-eye fa-2x"></i></a></td>
								  <td><a href="<?php echo base_url(); ?>index.php/mod_call_center/js_feedback"><i class="fa fa-play fa-2x" aria-hidden="true"></i></a></td>
								  <td><a href="<?php echo base_url(); ?>index.php/mod_call_center/view_jobsheet"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a></td>
							</tr>
							
						  </table>
						</div>
                    </div>
		
		
<!--------------------------------------------Create Product Ending------------------------------------------------>					
<!--------------------------------------------Create Complain Starts------------------------------------------------>	
<hr>				
		<div class="card border-primary mb-3" style="max-width: 72rem;">
  <div class="card-header"><b>Complain Details</b></div>
  <div class="card-body text-primary">
  
								<div class="row">
                      <div class="col-md-4">
                       <label>Source of Complaint</label>
                         <input type="text" class="form-control" value="Source of Complaint Reg by Consumer" id="coonsid" readonly>
                           
                        </div>
                      <div class="col-md-4">
                         <label >Call Originator</label>
						  <input type="text" class="form-control" value="Consumer" id="coonsid" readonly>
                            
                        </div>
                      <div class="col-md-4">
                          <label>Type of Call</label>
						   <input type="text" class="form-control" value="Consumer" id="coonsid" readonly>
                            
                      </div>
                      </div>
					  <div class="row">
                      <div class="col-md-4">
                          <label>Sub Type</label>
						   <input type="text" class="form-control" value="Consumer" id="coonsid" readonly>
                           
                          </div>
                      <div class="col-md-4">
                          <label>Complaint Address</label>
						   <input type="text" class="form-control" value="Distributor" id="coonsid" readonly>
                           
                      </div>
                      <div class="col-md-4">
                          <label>Product Type</label>
						   <input type="text" class="form-control" value="Electronics" id="coonsid" readonly>
                            
                      </div>
                    </div>
					<div class="row">
						<div class="col-sm-3">
						<br/>
						 <label>Service Location :  </label>
							<input type="text" class="form-control" value="Consumer" readonly>
							
						</div>
						<div class="col-sm-8">
						<br/>
							<div id="red" class="colors">
							 <label>Consumer Service Location Address:  </label>
								<input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur" id="coonsid" readonly>
							</div>
							
						</div>
					</div>
					
  </div>
</div>	

					 
      
		  
		    
		  
		  
		  
		  
		  
		  
    
<!--------------------------------------------Create Complain Ends------------------------------------------------>					
	  
				  <div class="error"> </div>
                </div>
				
				
				
			</div>


		   
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
	<?php 
			$this->load->view('includes/js-holder.php'); 
   ?>
<link rel="stylesheet" href="<?php echo base_url();?>/dppicker/css/jquery-ui.css"/>
	   <script type="text/javascript" src="<?php echo base_url();?>/dppicker/js/jquery-1.10.2.js"></script>
	   <script type="text/javascript" src="<?php echo base_url();?>/dppicker/js/jquery-ui.js"></script>
	 
	
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>  

<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.js"></script>

 


</body>

</html>


					 