
 <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <title>Services | Call Management</title>
  <style>
  button, button#vfy {
    box-shadow: 2px 3px 4px 2px #EEE;
}
.card .card-title {
    color: #000;
    margin-bottom: .75rem;
    text-transform: capitalize;
    font-family: "ubuntu-medium", sans-serif;
    font-size: 1.125rem;
}
button.btn.btn-danger.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
button#submitSearch {
    box-shadow: 3px 4px 1px 1px #EEE;
}
button.btn.btn-gradient-success.mb-2 {
    box-shadow: 3px 3px 2px 1px #eee;
}
button.btn.btn-success.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
	background: linear-gradient(to right, #83f2bea8, #08b21d);
}
button.btn.btn-default.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
table#POITable {
    font-size: 11px;
}
.card-body.saur {
    padding: 0rem;
    margin-left: -30px;
}
div#cons1 {
    margin-bottom: -35px;
}
.card.prod {
    margin-left: -10px;
}
label {
    font-weight: 600;
}
  </style>
  <script>
$(document).ready(function(){
    $("#prod1").click(function(){
        $("#cd1").show();
    });
    $("#submitSearch").click(function(){
        $("#cons1").show();
    });
	
});
function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('POITable').deleteRow(i);
}


function insRow() {
  console.log('hi');
  var x = document.getElementById('addmorePOIbutton');
  var new_row = x.rows[1].cloneNode(true);
  var len = x.rows.length;
  new_row.cells[0].innerHTML = len;

  var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
  inp1.id += len;
  inp1.value = '';
  var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
  inp2.id += len;
  inp2.value = '';
  x.appendChild(new_row);
}

$(function() {    // Makes sure the code contained doesn't run until
                  //     all the DOM elements have loaded

    $('#colorselector').change(function(){
        $('.colors').hide();
        $('#' + $(this).val()).show();
    });

});
</script> 
     <?php 
			$this->load->view('includes/sidebar.php');  
			$this->load->view('includes/call_center_asset.php');  
   ?>

   
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
	<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <button type="button" class="btn btn-danger btn-circle">1</button>
            <p>Create Customer</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-danger btn-circle">2</button>
            <p>Create Product</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-success btn-circle" disabled="disabled">3</button>
               <p>Register Complain</p>
        </div> 
    </div>
</div>

     <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body saur">
				
               <!-----Saurav setup progressbar step process-->
			
			 <div class="col-lg-12 grid-margin stretch-card" id="cons1" >
            <div class="card border-primary mb-3" style="max-width: 72rem;">
  <div class="card-header"><a id="button" href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i></a> <b>Customer Details</b></div>
  <div class="card-body text-dark" id="cons_togg" style="display: none;">
              	
<script>
$( "#button" ).click(function() {
$( "#cons_togg" ).toggle();
});
</script> 	
					<div class="row">
					 <div class="col-md-2"><b>Customer Name :</b><input type="text" class="form-control" value="Mr. Tester<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Mobile No :</b><input type="text" class="form-control" value="9293842324<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Consumer Category :</b><input type="text" class="form-control" value="Consumer<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address1 :</b><input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address2 :</b><input type="text" class="form-control" value="bhagalpur, bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					 </div>
			<br/>
			
				<div class="row">	
					<div class="col-md-2"><b>Pincode :</b><input type="text" class="form-control" value="252345<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>				
					<div class="col-md-2"><b>Area :</b><input type="text" class="form-control" value="Patkalpur Loknath-1<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>City :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>District :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>State :</b><input type="text" class="form-control" value="Bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>Email :</b><input type="text" class="form-control" value="test@gmail.com<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					</div>
			     </div>
		
			
			</div>
			</div>
			</div>
			<hr/>
<!-- - ------------------------------------ Consumer Details Ends here ------------------------------------>	  



<!-- - ------------------------------------ Product Details Creation started here ------------------------------------>	 
<div class="card prod" id="prod_view">
                <div class="card-body">
		 
 <div class="card-header">
	<a id="prod_button" href="#"><i class="fa fa-chevron-down" aria-hidden="true"></i></a> <b>Product Details</b>
 </div>		
 <script>
 $( "#prod_button" ).click(function() {
	// alert(1);
$( "#item_prod" ).toggle();
});ss
</script>
                    <div class="row" id="item_prod" style="display: none;">
                       <div id="POItablediv">
						  <table id="POITable" class="table" >
							<tr>
							  <th>Sr. No.</th>
							  <th>JobSheet</th>
							  <th>Product Name</th>
							  <th>Date of Purchase</th>
							  <th>Warranty Start Date</th>
							  <th>Warranty End Date</th>
							  <th>Warranty Status</th>
							  <th>Dealer</th>
							  <th>Product SAP Code</th>
							  <th>Asset Serial no</th>
							</tr>
							<tr>
							  <td>1</td>
							   <td>JS-12873242180</td>
								  <td>SOLAR </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>Santya Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
								 
							</tr>
							<tr>
							  <td>2</td>
							   <td>JS-12873232412</td>
								  <td>INVERTER </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>INVR Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
								 
							</tr>
							
						  </table>
						</div>
                    </div>
		
		
<!--------------------------------------------Create Product Ending------------------------------------------------>					
<!--------------------------------------------Create Complain Starts------------------------------------------------>	
<hr>				
		<div class="card border-primary mb-3" style="max-width: 72rem;">
  <div class="card-header"><b>Create Complain</b></div>
  <div class="card-body text-dark">
  
				<div class="row">
                      <div class="col-md-4">
                       <label>Source of Complaint</label>
                         
                            <select id="lngbox" class="form-control"/>
								<option>Call Centre Toll free no</option>
								<option>Social Media </option>
								<option>Mobile App</option>
								<option>Website</option>
								<option>Internal Team (Role Based)</option>
							</select>
                        </div>
                      <div class="col-md-4">
                         <label >Call Originator</label>
                            <select id="lngbox" class="form-control">
								<option>Consumer  </option>
								<option>Dealer</option>
								<option>Distributor</option>
							</select>
                        </div>
                      <div class="col-md-4">
                          <label>Type of Call</label>
                            <select id="lngbox" class="form-control">
								<option>Field Call</option>
								<option>Walk-in</option>
							</select>
                      </div>
                      </div>
					  <div class="row">
                      <div class="col-md-4">
                          <label>Sub Type</label>
                            <select id="lngbox" class="form-control">
								<option>Unsold Stock</option>
								<option>Sold Stock</option>
							</select>
                          </div>
                     <div class="col-sm-4">
						 <label>Service Location :  </label>
							<Select id="colorselector" class="form-control">
							<option value="">Please select service location address</option>
							<option value="red">Consumer</option>
							<option value="yellow">Dealer</option>
							<option value="blue">Distributor</option>
							</Select>
						</div>
                      <div class="col-md-4">
                          <label>Product Type</label>
                            <select id="lngbox" class="form-control">
								<option>Electronics</option>
								<option>Battery</option>
								<option>Omni</option>
							</select>
                      </div>
                    </div>
					<div class="row">
						
						<div class="col-sm-12">
						<br/>
							<div id="red" class="colors" style="display:none">
							 <label>Consumer Service Location Address:  </label>
								<input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur" id="coonsid" readonly>
							</div>
							<div id="yellow" class="colors" style="display:none"> 
							<label>Dealer Service Location Address:  </label>
								<input type="text" class="form-control" placeholder="Please provide dealer service location address">
							</div>
							<div id="blue" class="colors" style="display:none"> 
							<label>Distributor Service Location Address:  </label>
								<input type="text" class="form-control" placeholder="Please provide distributor service location address">
							</div>
						</div>
					</div>
					</div>
					<br>
					<div class="row">
					<div class="col-sm-12">
					<center>
					<a href="<?php echo base_url(); ?>index.php/mod_call_center/success">
					<button class="btn btn-success">Create Complain</button></a></center>
					</div>
					</div>
  </div>
</div>	

<!--------------------------------------------Create Complain Ends------------------------------------------------>					
	  
				  <div class="error"> </div>
                </div>
				
				
				
			</div>


		   
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
	<?php 
			$this->load->view('includes/js-holder.php'); 
   ?>
<link rel="stylesheet" href="<?php echo base_url();?>/dppicker/css/jquery-ui.css"/>
	   <script type="text/javascript" src="<?php echo base_url();?>/dppicker/js/jquery-1.10.2.js"></script>
	   <script type="text/javascript" src="<?php echo base_url();?>/dppicker/js/jquery-ui.js"></script>
	 
	
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>  

<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.js"></script>

 


</body>

</html>

					 
