
 <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <title>Services | Call Management</title>
  <style>
  button, button#vfy {
    box-shadow: 2px 3px 4px 2px #EEE;
}
.card .card-title {
    color: #000;
    margin-bottom: .75rem;
    text-transform: capitalize;
    font-family: "ubuntu-medium", sans-serif;
    font-size: 1.125rem;
}
button#submitSearch {
    box-shadow: 3px 4px 1px 1px #EEE;
}
button.btn.btn-gradient-success.mb-2 {
    box-shadow: 3px 3px 2px 1px #eee;
}
button.btn.btn-success.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
	background: linear-gradient(to right, #83f2bea8, #08b21d);
}
button.btn.btn-default.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
  </style>
  <script>
$(document).ready(function(){
    $("#prod1").click(function(){
        $("#cd1").show();
    });
    $("#submitSearch").click(function(){
        $("#cons1").show();
    });
	
});
function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('POITable').deleteRow(i);
}


function insRow() {
  console.log('hi');
  var x = document.getElementById('addmorePOIbutton');
  var new_row = x.rows[1].cloneNode(true);
  var len = x.rows.length;
  new_row.cells[0].innerHTML = len;

  var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
  inp1.id += len;
  inp1.value = '';
  var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
  inp2.id += len;
  inp2.value = '';
  x.appendChild(new_row);
}
</script> 
     <?php 
			$this->load->view('includes/sidebar.php');  
			$this->load->view('includes/call_center_asset.php');  
   ?>

   
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
	<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <button type="button" class="btn btn-success btn-circle">1</button>
            <p>Create Customer</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle">2</button>
            <p>Create Product</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
               <p>Register Complain</p>
        </div> 
    </div>
</div>

     <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
				<h4 class="card-title">Search Consumer</h4>
                  
                  <form class="form-inline">
                    <label class="sr-only" for="inlineFormInputName2">Name</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" id="mobile" placeholder="Enter Mobile No.">
					<button type="button" class="btn btn-gradient-primary mb-2" id="submitSearch"><i class="fa fa-search" aria-hidden="true"></i> Search Mobile No</button>
                  </form>
				  <div class="error"> </div>
                </div>
				
				<div class="loader"><center><img src="<?php echo base_url();?>assets/images/loader.gif" width="80px" height="80px" alt="loader_logo"> </center></div>
				<div class="consumer_detail"> </div>
				<div class="succss"> </div>
				
				
<!-------------------------------Customer Details View------------------------------------------>				
				
			<div class="card-body saur" id="consumerDetails" style="display:none;">
			 <div class="col-lg-12 grid-margin stretch-card" id="cons1" >
			   <div class="card border-primary mb-3" style="max-width: 72rem;">
				<div class="card-header"><b>Customer Details</b><span class="pull-right"></span></div>
				 <div class="card-body text-dark">
              		<div class="row">
					 <div class="col-md-2"><b>Customer Name :</b><input type="text" class="form-control" value="Mr. Tester<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Mobile No :</b><input type="text" class="form-control" value="9650645067<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Consumer Category :</b><input type="text" class="form-control" value="Consumer<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address1 :</b><input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address2 :</b><input type="text" class="form-control" value="bhagalpur, bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					</div>
			<br/>
			<div class="row">	
					<div class="col-md-2"><b>Pincode :</b><input type="text" class="form-control" value="252345<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>				
					<div class="col-md-2"><b>Area :</b><input type="text" class="form-control" value="Patkalpur Loknath-1<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>City :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>District :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>State :</b><input type="text" class="form-control" value="Bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>Email :</b><input type="text" class="form-control" value="test@gmail.com<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					</div>
			     </div>
			</div>
			</div>
				
<!-------------------------------------Customer Details View Ends------------------------------------------------------------>				
<!-------------------------------------Product Details View Starts------------------------------------------------------------>		
<div class="card-header" id="productDetails" style="display:none;"><b>Product Details</b></div>		
                    <div class="row">
                      <div id="POItablediv">
						<table id="POITable" class="table" >
							<tr>
							  <th>Sr. No.</th>
							  <th>JobSheet</th>
							  <th>Product Name</th>
							  <th>Date of Purchase</th>
							  <th>Warranty Start Date</th>
							  <th>Warranty End Date</th>
							  <th>Warranty Status</th>
							  <th>Dealer</th>
							  <th>Product SAP Code</th>
							  <th>Asset Serial no</th>
							</tr>
							<tr>
							  <td>1</td>
							   <td>JS-12873242180</td>
								  <td>SOLAR </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>Santya Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
							</tr>
							<tr>
							  <td>2</td>
							   <td>JS-12873232412</td>
								  <td>INVERTER </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>INVR Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
				
<!------------------------------------Product Details View Ends------------------------------------------------------------>				
			</div>


		   
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
	<?php 
			$this->load->view('includes/js-holder.php'); 
   ?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>  

<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.js"></script>

  <script type="text/javascript">
$('.consumer_detail').hide();
$('.loader').hide();
  $(document).ready(function(){
		/* $("#dobs").datepicker({
			//showOn: "button",
			buttonImage: "<?php echo base_url();?>images/jscalendar.gif",
			//buttonImageOnly: true,
			altField: "#dob",
			altFormat: "dd-mm-yy",
			changeMonth: true, 
			changeYear: true, 
		}); */

		// Tanuj Create new consumer and get full details 	
		
		$("#submitSearch" ).click(function() { 
		$('.consumer_detail').show();
		$('.loader').show();
			 var mobile=$("#mobile").val();
			 var len=mobile.length;
				if(len==10)
				{
					
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>index.php/mod_call_center/get_consumer_detail",
						data: {mobile:mobile},
						success: function(result)
						{
							//console.log(result);
							$('.loader').hide();
							//alert(result);  exit;
							if(result != 1)
							{
							
								$('.consumer_detail').html(result);
								exit;		
							}
							else
							{
							
								//$('#consumerDetails').show();
								//$('#productDetails').show();
								window.location = 'http://13.228.144.245/livguard_dms/index.php/mod_call_center/create_case2';
								return false;
								
							}
							
							
							
							// alert(result);
							//redirect('');
							
						},
						error:function (error) {
							alert('Some Problem Occurred.Please try again.' + eval(error));
							return false;
						}
					});
				}else{
						$('.loader').hide();
						$('.consumer_detail').hide();
						$('.error').html('<span class="alert alert-danger" >Mobile no. should be minimum 10 Character long.</span>');
						return false;
				}
		});
		$('.error').delay(5000).fadeOut('slow');
    //Tanuj End Create new consumer and get full details 


			
		$("#vfy" ).blur(function(e) {
			e.preventDefault();
			var pinval=$("#pin").val();
			//$('#pinvalue').show();
			$('#pin_code').val(pinval);
			var arr = {"jobType":"get_areadetail", "pincode":pinval};
			$.ajax({
				type: "POST",
				url: "http://13.127.176.185/LivCRM/index.php?entryPoint=Drona",
				data: JSON.stringify(arr),
				dataType: "json",
                async: false,
                contentType: 'application/json; charset=utf-8',	
				success: function(result)
				{
					var option1 = "<option value=''>Please Select Area</option>";
					var option2 = "";
					var option3 = "";
					$.each(result, function(index, obj) {
					option1 = option1 + '<option value="'+ obj.area +'">'+ obj.area+'</option>';
					});	
					$('#area').html(option1);
					$.each(result, function(index, obj) {
					  option2 = '<option value="'+ obj.state +'">'+ obj.state+'</option>';
					});	
					$('#state').html(option2);
					$.each(result, function(index, obj) {
					  option3 =  '<option value="'+ obj.city +'">'+ obj.city+'</option>';
					});	
					$('#city').html(option3);
				}
			});
		});
	});	
	

</script>  


<script>   
	 function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    }    
	
	
	$(document).ready(function()
	{
				$('#mobile-num').keyup(function(){
					var mob = $(this).val();
					//var mob = $(this).length;
					var z=mob.charAt(0);
					var mobileLength=mob.length;
					if(z=="0")
					{
					alert("First Digit can not start with 0");
					$(this).val('');
					}
				});
				
				$('#mobile-num').focusout(function(){
					var mob = $(this).val();
					var mobileLength=mob.length;
					if(mobileLength!=10) {
					alert("Please enter 10 digit valid number");
					}
				});
				
				$('#mobile-num2').change(function(){
					var mob = $(this).val();
					//var mob = $(this).length;
					var z=mob.charAt(0);
					var mobileLength=mob.length;
					if(z=="0")
					{
					alert("First Digit can not start with 0");
					$(this).val('');
					}
					if(mob.length>10){
						alert("Mobile no exceed limit!!!");
						$(this).val('');	
					}
					if(mob.length<10){
						alert("Please enter 10 digit mobile no!!!");
						$(this).val('');	
					}					
				});
				
/* 				$('#mobile-num2').focusout(function(){
					var mob = $(this).val();
					var mobileLength=mob.length;
					if(mobileLength!=10) {
					alert("Please enter 10 digit valid number");
					return false;
					}
				}); */
				
				$('#fname').keypress(function (e) {
					var regex = new RegExp("^[a-zA-Z]+$");
					var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
					if (regex.test(str)) {
					return true;
					}
					else
					{
					e.preventDefault();
					alert('Please input alphabet characters only');
					return false;
					}
				});	
				$('#lname').keypress(function (e) {
					var regex = new RegExp("^[a-zA-Z]+$");
					var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
					if (regex.test(str)) {
					return true;
					}
					else
					{
					e.preventDefault();
					alert('Please input alphabet characters only');
					return false;
					}
				});	
	});
	
</script>  



</body>

</html>

					 
