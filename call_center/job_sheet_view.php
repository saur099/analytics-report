<?php
//echo "<pre>"; print_r($res); die;  // $res

			$this->load->view('includes/top.php');  
   ?>
   <title>Success | Complaint Management</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
   <style>
   th {
    font-size: 12px !important;
}
td {
    font-size: 12px !important;
}
span.card-title.text-danger {
    color: #dc3545!important;
    font-weight: 500;
    font-size: 15px;
}
span.pull-right.complaint_no {
    background: linear-gradient(to right, #ffbf96, #fe7096);
    color: #ffffff;
    padding-top: 5px;
    padding-left: 6px;
    padding-right: 6px;
    box-shadow: 1px 1px 2px 2px #eee;
    font-size: 19px;
}
   </style>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        	  
	<a  href="<?php echo base_url(); ?>/index.php/mod_call_center/success">
	<button type="" class="btn btn-info"><i class="fa fa-backward" aria-hidden="true"></i> Go Back</button></a>
      </ol>
      
  	  
		   <div class="col-md-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">JobSheet View
					  <span class="pull-right">
						<button type="button" class="btn btn-danger ">JS-0213241434</button>
					  </span>
					</h4>
				  <hr>
				  
                 <span class="card-title text-danger">Complaints Module</span>
				  <span class="pull-right complaint_no"><label class="badge badge-gradient-success">Complaint No. : C2018239892384</label></span>
				  
                  </p>
                  <div class="row">
                    <div class="col-md-2">
                       <label>Source of Complaint</label>
                         <input type="text" class="form-control" value="Source of Complaint Reg by Consumer" id="coonsid" readonly>
                           
                        </div>
                      <div class="col-md-2">
                         <label >Call Originator</label>
						  <input type="text" class="form-control" value="Consumer" id="coonsid" readonly>
                            
                        </div>
                      <div class="col-md-2">
                          <label>Type of Call</label>
						   <input type="text" class="form-control" value="Consumer" id="coonsid" readonly>
                            
                      </div>
                      <div class="col-md-2">
                          <label>Sub Type</label>
						   <input type="text" class="form-control" value="Consumer" id="coonsid" readonly>
                           
                          </div>
                      <div class="col-md-2">
                          <label>Complaint Address</label>
						   <input type="text" class="form-control" value="Distributor" id="coonsid" readonly>
                           
                      </div>
                      <div class="col-md-2">
                          <label>Product Type</label>
						   <input type="text" class="form-control" value="Electronics" id="coonsid" readonly>
                            
                      </div>
                  </div>
<!--------------------------------Complain module ends-------------------------------------------------------->	
<hr>	
<!--------------------------------Product module ends-------------------------------------------------------->		
<h6 class="card-title text-danger">Job Sheet Module</h6>
<div class="row">
                        <div id="POItablediv">
						  <table id="POITable" class="table table-bordered"  >
							<tr>
							  <th>JobSheet</th>
							  <th>Product Name</th>
							  <th>Date of Purchase</th>
							  <th>Warranty Start Date</th>
							  <th>Warranty End Date</th>
							  <th>Warranty Status</th>
							  <th>Dealer</th>
							  <th>Product SAP Code</th>
							  <th>Asset Serial no</th>
							  <th>Change Status</th>
							</tr>
							<tr>
							   <td>JS-12873242180</td>
								  <td>SOLAR </td>
								  <td>12-02-2018</td>
								  <td>12-02-2018</td>
								  <td>12-08-2018</td>
								  <td>FOC</td>
								  <td>Santya Enterprises</td>
								  <td>MX0238-STC</td>
								  <td>ASSDSNJ723823</td>
								  <td><select class="form-control" id="exampleFormControlSelect2">
										  <option>Open</option>
										  <option>Closed</option>
										  <option>Pending</option>
										  <option>Processing</option>
										</select>
									</td>
								 
							</tr>
							
						  </table>
						</div>
                    </div>
		
				
<!--------------------------------Product module ends-------------------------------------------------------->	
<hr>	
<!--------------------------------Asset module starts-------------------------------------------------------->		
<h6 class="card-title text-danger">Asset Module</h6>			
	    <div class="row">	
			<div class="col-md-2">Product Segment : <span class="reqd">*</span><input class="form-control" type="text" value="Battery" name="productSegment"  readonly></div>
			<div class="col-md-2">Brand: <span class="reqd">*</span>	<input class="form-control" type="text" name="brand" value="Livguard" id="brand" readonly></div>	
			<div class="col-md-2">Battery Type: <input id="battery" class="form-control" type="text" name="batteryType" value="Flooded" readonly></div>
			<div class="col-md-2">Product Type :<span class="reqd">*</span>	 <input class="form-control" type="text" value="2-W" name="protype" readonly ></div>
			<div class="col-md-2">	Model :	 <input class="form-control" type="text" value="LXTESCT8932" name="model" readonly></div>	
			<div class="col-md-2">Product Serial Number : <input id="productSerialNo" class="form-control" type="text" name="productSerialNo" value="ADBJSB98789798" readonly></div>
		</div>
			<div class="row">
				<div class="col-md-2">Letpl Invoice No :	 <input class="form-control" type="text" id="letpl_invoice_dat" value="298375238975" name="letpl_invoice_no" readonly>	</div>
				<div class="col-md-2">		Letpl Invoice Date : 	 <input class="form-control" type="text" id="letpl_invoice_date" value="20-08-2017" name="letpl_invoice_date" readonly>	</div>
				<div class="col-md-2">Cust Purchase Date :<span class="reqd"> *</span> <input class="form-control" type="text" name="custPurchaseDate" value="20-01-2018" id="custPurchaseDate" readonly="true"/></div>
				<div class="col-md-2">Warranty Start Date : <span class="reqd">*</span>	  <input class="form-control" readonly placeholder="dd-mm-yyyy"  type="text" value="20-01-2018" size="14%" name="warrantyStartDate" id="warrantyStartDate"/></div>
				<div class="col-md-2">Warranty End Date : <span class="reqd">*</span>	  <input class="form-control" readonly placeholder="dd-mm-yyyy"  type="text" value="19-07-2018" size="14%" name="warrantyEndDate" id="warrantyEndDate"/>	</div>
				<div class="col-md-2">Pro Rata Warr. Strt Date : <input class="form-control" type="text" readonly  placeholder="dd-mm-yyyy"  value="" class="second" size="14%" name="proRateWarrStDate" id="proRateWarrStDate"/ ></div>
				
			</div>
			<div class="row">
				<div class="col-md-3">	Pro Rata Warr. End Date :  <input class="form-control" type="text" readonly placeholder="dd-mm-yyyy"  value=""  class="second" size="14%" name="proRateWarrEndDate" id="proRateWarrEndDate"/ ></div>
			<div class="col-md-3">		Warranty Status 	
				<input class="form-control" name="waranyStatus" id="waranyStatus" readonly/>
						<select style="display:none;" class="form-control">
							
							<option value="2">Out of Warranty </option>
						</select>
					</div>
					<div class="col-md-3">		Pro Rata Discount : 	 
					     <input class="form-control" type="text" id="pro_rata_disc" value="100%" name="pro_rata_disc" readonly>	
					</div>
					<div class="col-md-3">			Dealer Name : 	 
                               <input name="dealer" class="form-control" value="Chetan Enterprises" id="dealer" readonly>
								
					</div>
					</div>
<!--------------------------------Asset module ends-------------------------------------------------------->		
<hr>		  
		
<!--------------------------------Consumer module Starts-------------------------------------------------------->	
<h6 class="card-title text-danger">Consumer Module</h6>		
<div class="row">
					 <div class="col-md-2"><b>Customer Name :</b><input type="text" class="form-control" value="Mr. Tester<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Mobile No :</b><input type="text" class="form-control" value="9293842324<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Consumer Category :</b><input type="text" class="form-control" value="Consumer<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address1 :</b><input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-3"><b>Address2 :</b><input type="text" class="form-control" value="bhagalpur, bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					 </div>
				<div class="row">	
					<div class="col-md-2"><b>Pincode :</b><input type="text" class="form-control" value="252345<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>				
					<div class="col-md-2"><b>Area :</b><input type="text" class="form-control" value="Patkalpur Loknath-1<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>City :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>District :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>State :</b><input type="text" class="form-control" value="Bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>Email :</b><input type="text" class="form-control" value="test@gmail.com<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					</div>	
<!--------------------------------Consumer module ends-------------------------------------------------------->
<!--------------------------------Spare Consumption module starts-------------------------------------------------------->
<!--------------------------------Spare Consumption module ends-------------------------------------------------------->
                   
                  </div>
                </div>
              </div>
            </div>
           
		  <hr>
		  
		  
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
   ?>
   
	
  </div>
</body>

</html>

					 