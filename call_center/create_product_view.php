
 <?php
//echo "<pre>"; print_r($res); die;

			$this->load->view('includes/top.php');  
   ?>
   <title>Services | Call Management</title>
  <style>
  button, button#vfy {
    box-shadow: 2px 3px 4px 2px #EEE;
}
.card .card-title {
    color: #000;
    margin-bottom: .75rem;
    text-transform: capitalize;
    font-family: "ubuntu-medium", sans-serif;
    font-size: 1.125rem;
}
button.btn.btn-danger.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
button#submitSearch {
    box-shadow: 3px 4px 1px 1px #EEE;
}
button.btn.btn-gradient-success.mb-2 {
    box-shadow: 3px 3px 2px 1px #eee;
}
button.btn.btn-success.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
	background: linear-gradient(to right, #83f2bea8, #08b21d);
}
button.btn.btn-default.btn-circle {
    width: 40px;
    height: 40px;
    padding: 7px 10px;
    font-size: 18px;
    line-height: 1.33;
    border-radius: 25px;
    box-shadow: 1px 2px 2px 1px #000;
}
table#POITable {
    font-size: 11px;
}
.card-body.saur {
    padding: 0rem;
    margin-left: -30px;
}
div#cons1 {
    margin-bottom: -35px;
}
.card.prod {
    margin-left: -10px;
}
span.add {
    color: cornflowerblue;
    /* background: linear-gradient(to right, #0dec83a8, #26f03f); */
}
span.del {
    color: crimson;
}
button.btn.btn-success {
    background: linear-gradient(to right, #83f2bea8, #08b21d);
    box-shadow: 1px 1px 1px 1px #000;
}
  </style>
  <script>
$(document).ready(function(){
    $("#prod1").click(function(){
        $("#cd1").show();
    });
    $("#submitSearch").click(function(){
        $("#cons1").show();
    });
	
});
function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('POITable').deleteRow(i);
}


function insRow() {
  console.log('hi');
  var x = document.getElementById('addmorePOIbutton');
  var new_row = x.rows[1].cloneNode(true);
  var len = x.rows.length;
  new_row.cells[0].innerHTML = len;

  var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
  inp1.id += len;
  inp1.value = '';
  var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
  inp2.id += len;
  inp2.value = '';
  x.appendChild(new_row);
}
</script> 
     <?php 
			$this->load->view('includes/sidebar.php');  
			$this->load->view('includes/call_center_asset.php');  
   ?>

   
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
	<div class="stepwizard">
    <div class="stepwizard-row">
        <div class="stepwizard-step">
            <button type="button" class="btn btn-danger btn-circle">1</button>
            <p>Create Customer</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-success btn-circle">2</button>
            <p>Create Product</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
               <p>Register Complain</p>
        </div> 
    </div>
</div>

     <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body saur">
				
               <!-----Saurav setup progressbar step process-->
			
			 <div class="col-lg-12 grid-margin stretch-card" id="cons1" >
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><b>Consumer Details </b></h4>
					
					<div class="row">
					 <div class="col-md-2"><b>Customer Name :</b><input type="text" class="form-control" value="Mr. Tester<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Mobile No :</b><input type="text" class="form-control" value="9293842324<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Consumer Category :</b><input type="text" class="form-control" value="Consumer<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Address1 :</b><input type="text" class="form-control" value="14B/C, Patkalpur Loknath, Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Address2 :</b><input type="text" class="form-control" value="bhagalpur, bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					 <div class="col-md-2"><b>Customer Code :</b><input type="text" class="form-control" value="CU023413241223<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					 </div>
			<br/>
			
				<div class="row">	
					<div class="col-md-2"><b>Pincode :</b><input type="text" class="form-control" value="252345<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>				
					<div class="col-md-2"><b>Area :</b><input type="text" class="form-control" value="Patkalpur Loknath-1<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>City :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					<div class="col-md-2"><b>District :</b><input type="text" class="form-control" value="Bhagalpur<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>State :</b><input type="text" class="form-control" value="Bihar<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					<div class="col-md-2"><b>Email :</b><input type="text" class="form-control" value="test@gmail.com<?php //echo strtoupper($row[0]->fname." ".$row[0]->lname); ?>" id="coonsid" readonly></div>
					
					</div>
			     </div>
		
			
			</div>
			</div>
			</div>
			<hr/>
<!-- - ------------------------------------ Consumer Details Ends here ------------------------------------>	  



<!-- - ------------------------------------ Product Details Creation started here ------------------------------------>	 
<div class="card prod">
                <div class="card-body">
				 
 <h4 class="card-title"><b>Create Product </b></h4>
                    <div class="row">
                        <div id="POItablediv">
						  <table id="POITable"  border="1px solid #eee">
							<tr>
							  <th><b>Sr. No.</b></th>
							  <th><b>JobSheet</b></th>
							  <th><b>Product Name</b></th>
							  <th><b>Date of Purchase</b></th>
							  <th><b>Warranty Start Date</b></th>
							  <th><b>Warranty End Date</b></th>
							  <th><b>Warranty Status</b></th>
							  <th><b>Product SAP Code</b></th>
							  <th><b>Asset Serial no</b></th>
							  <th><b>Dealer</b></th>
							  <th><b>Add Product</b></th>
							  <th><b>Delete</b></th>
							</tr>
							<tr>
								  <td>1</td>
								  <td>JS-12873242180</td>
								  <td><select id="lngbox" /><option>Please select</option><option>SOLAR </option><option>INVERTER</option><option>BATTERY </option></select></td>
								  <td><input size=25 type="date"  id="latbox" /></td>
								  <td><input size=25 type="date" id="latbox" /></td>
								  <td><input size=25 type="date" id="latbox" /></td>
								  <td></td>
								  <td></td>
								  <td></td>
								  <td><select id="lngbox" /><option>Please select</option><option>Santya Enterprises</option><option>Chetan Enterprises </option><option>Amulaya Batteries</option><option>INVR Enterprises</option></select></td>
								  <td><button class="btn btn-primary btn-sm"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></button></td>
								  <td><button class="btn btn-danger btn-sm"><i class="fa fa-window-close fa-2x" aria-hidden="true"></i></button></td>
							</tr>
							<div id="cons2" >
								<tr>
								  <td>2</td>
								  <td>JS-12873242180</td>
								  <td><select id="lngbox" /><option>Please select</option><option>SOLAR </option><option>INVERTER</option></select></td>
								  <td><input size=25 type="date"  id="latbox" /></td>
								  <td><input size=25 type="date" id="latbox" /></td>
								  <td><input size=25 type="date" id="latbox" /></td>
								  <td></td>
								  <td></td>
								  <td></td>
								     <td><select id="lngbox" /><option>Please select</option><option>Santya Enterprises</option><option>Chetan Enterprises </option><option>Amulaya Batteries</option><option>INVR Enterprises</option></select></td>
								  <td><button class="btn btn-primary btn-sm"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></button></td>
								  <td><button class="btn btn-danger btn-sm"><i class="fa fa-window-close fa-2x" aria-hidden="true"></i></button></td>
								  
								</tr>
							</div>
						  </table>
						</div>
                    </div>
					<br/>
		<div class="row">	
		<div class="col-sm-12">
			<a href="<?php echo base_url(); ?>index.php/mod_call_center/create_case">
		<center>
				<button type="button" class="btn btn-success">Create product</button>	
			</a>
		</center>
		</div>
		</div>
<!--------------------------------------------Create Product Ending------------------------------------------------>					
	  
				  <div class="error"> </div>
                </div>
				
				
				
			</div>


		   
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
   
	<?php 
			$this->load->view('includes/js-holder.php'); 
   ?>
<link rel="stylesheet" href="<?php echo base_url();?>/dppicker/css/jquery-ui.css"/>
	   <script type="text/javascript" src="<?php echo base_url();?>/dppicker/js/jquery-1.10.2.js"></script>
	   <script type="text/javascript" src="<?php echo base_url();?>/dppicker/js/jquery-ui.js"></script>
	 
	
  </div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>  

<link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-ui.js"></script>

 


</body>

</html>

					 
