<?php
//echo "<pre>"; print_r($res); die;  // $res

			$this->load->view('includes/top.php');  
   ?>
   <title>Success | Complaint Management</title>
     <?php 
			$this->load->view('includes/sidebar.php');  
   ?>
   <script>
$(document).ready(function(){
  $("#Warrantyvoid").hide();
  $("#voidreason").hide();
  $("#nextbtn").hide();
  $("#spare_consumption").hide();
  $("#consumptiontablecheckyes").hide();
  $("#consumptiontablecheckno").hide();
    $("#prod1").click(function(){
        $("#cd1").show();
    });
    $("#submitSearch").click(function(){
        $("#cons1").show();
    });
    $("#submitbtn").click(function(){
    
        $("#formfeedback").hide();
        $("#Warrantyvoid").show();
        
    });
    $("#radiobtnyes").click(function(){
      
      $("#voidreason").show();
      $("#nextbtn").show();
    });
    $("#nextbtn").click(function(){
      $("#nextbutton").hide();
      $("#voidreason").hide();
      $("#spare_consumption").show();
      $("#Warrantyvoid").hide();

    });
    $("#spareconyes").click(function(){
      $("#consumptiontablecheckyes").show();
      $("#consumptiontablecheckno").hide();
});
$("#spareconno").click(function(){
      $("#consumptiontablecheckno").show();
      $("#consumptiontablecheckyes").hide();
});
$("#radiobtnno").click(function(){
 $("#voidreason").hide();
});
    
});
function deleteRow(row) {
  var i = row.parentNode.parentNode.rowIndex;
  document.getElementById('POITable').deleteRow(i);
}


function insRow() {
  console.log('hi');
  var x = document.getElementById('addmorePOIbutton');
  var new_row = x.rows[1].cloneNode(true);
  var len = x.rows.length;
  new_row.cells[0].innerHTML = len;

  var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
  inp1.id += len;
  inp1.value = '';
  var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
  inp2.id += len;
  inp2.value = '';
  x.appendChild(new_row);
}
</script>

<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        	  
	<a  href="<?php echo base_url(); ?>/index.php/mod_call_center/success">
	<button type="" class="btn btn-info"><i class="fa fa-backward" aria-hidden="true"></i> Go Back</button></a>
      </ol>

    
	   <div class="main-panel">        
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              Feedback
            </h3>
            
          </div>
          <div class="row">
            <div class="col-12 grid-margin stretch-card" id="formfeedback">
                 <h4 class="card-title">Enter Happy Code:</h4>      
                  <form class="form-inline">
                    <label class="sr-only" for="inlineFormInputName2">Name</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Enter Happy code.">
					          <input type="button" class="btn btn-primary" id="submitbtn" value="Submit"></button>
                  </form> 
                </div>
                <div class="col-sm-12 form-group" id="Warrantyvoid">
                  <label for="radio_group" class="col-sm-4 control-label"><h4 class="card-title">Warranty Void</h4></label>
                  <div class="col-sm-8">
                    <label class="radio-inline">
                      <input type="radio" name="radio_group" value="1" id="radiobtnyes" label="Yes"  />
                      Yes
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="radio_group" value="0" id="radiobtnno" label="No"  />
                      No
                    </label>
                  </div>
                </div>
                
                  <div class="col-sm-6 form-group" id="voidreason">
                      <label for="published" class="col-sm-6 control-label"><h4 class="card-title">Warranty Void Reason</h4></label>
                      <div class="col-sm-4">
                        <select name="published" id="published" class="valid form-control">
                          <option value="none">Please Select...</option>
                          <option value="1">Water Drop</option>
                          <option value="2">Battery West</option>
                        </select>
                      </div>
                     
                    </div>  
                    <div class="form-group" id="nextbutton">
                        <input type="button"  class="btn btn-gradient-primary mb-2" id="nextbtn" value="Next"></button>
                    </div>
                    
                      <div class="form-group" id="spare_consumption">
                        <label for="radio_group" class="col-sm-6 control-label"><h4 class="card-title">Spare Consumption </h4></label>
                        <div class="col-sm-9">
                          <label class="radio-inline">
                            <input type="radio" name="radio_group" value="1" id="spareconyes" label="Yes"  />
                            Yes
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="radio_group" value="0" id="spareconno" label="No"  />
                            No
                          </label>
                        </div>
                      </div>
                      <div id="consumptiontablecheckyes">
                          <table id="POITable" class="table table-hover" border="1">
                              <tr>
                                <td>Fault Code.</td>
                                <td>Action Taken</td>
                                <td>Spare Consumption</td>
                              </tr>
                              <tr>
                                <td><select id="lngbox" class="form-control"/><option>Please        Select.. </option> 
                                  <option>F001 - Tranform Damage </option> </select></td>

                                <td><select id="lngbox" class="form-control"/><option>Please        Select.. </option> 
                                  <option>F001 - Tranform Replaced </option> </select></td>
                                <td><select id="lngbox" class="form-control"/><option>Please Select.. </option> 
                                  <option>LXTBDD0907 - SOLAR </option> </select></td>
                                
                              </tr>
                               
                              </table>
                      </div>
                      <div id="consumptiontablecheckno">
                          <table id="POITable" class="table table-hover" border="1">
                              <tr>
                                <td>Fault Code.</td>
                                <td>Action Taken</td>
                                 
                              </tr>
                              <tr>
                                <td><select id="lngbox" class="form-control"><option>Please        Select.. </option> 
                                  <option>F001 - Tranform Damage </option> </select></td>

                                <td><select id="lngbox" class="form-control"><option>Please        Select.. </option> 
                                  <option>F001 - Tranform Replaced </option> </select></td>
                                 
                              </tr>
                               
                              </table>
                      </div>
                       </div>
                       </div>
                       </div>
                       </div>
                       </div>
                       </div>	
                       </div>
                      <?php 
			$this->load->view('includes/footer.php');  
			$this->load->view('includes/js-holder.php');  
   ?>
              </div>
</html>

					 