<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Return Stock</title>
	
	<?php    $this->load->view('library');    ?>
	
	
	
  
  

    <script>
        $(document).ready(function() {
		
		    $("#product_name").load("http://52.66.23.135/cis/index.php/return_stock/loadmyproducts" , function(){});
		    //$("#assign_branch").load("http://52.66.23.135/cis/index.php/return_stock/loadmyproducts_serial_number" , function(){});
		  ///  $("#table_distributor").load("http://52.66.23.135/cis/index.php/branch_order/load_scheme_data" , function(){});

            $('#distributor_btn').click(function(){
			  
			   var  product_name = $('#product_name').val();
			   var  assign_branch = $('#assign_branch').val();
			   var  return_reason = $('#return_reason').val();
			  
			   var  dataStr = "product_name="+product_name+"&assign_branch="+assign_branch+"&return_reason="+return_reason;
					  
				
				if(product_name=="") {
				   alert('Select Product');
				   $('#product_name').focus();
				   return false;
				}else{
				   $.ajax({
				      type:"post" ,
					  url: "http://52.66.23.135/cis/index.php/return_stock/add_return_product/" ,
					  data:		dataStr ,
					  async:true ,
						success:function(st){ 
						    
							//'data saved successfully'
							//$("#product_name").load("http://52.66.23.135/cis/index.php/branch_order/loadproducts" , function(){});
							location.reload();
							//$("#table_distributor").load("http://52.66.23.135/cis/index.php/branch_order/load_scheme_data" , function(){});
						}	
				   });
				}	
            });           
        });
    </script>
	


<!--<script>
$(document).ready(function(){

    $('#product_name').change(function(e){
       // Your event handler
	   var pro_name = $('#product_name').val(); 
	   var dataStr =  "pro_name="+pro_name;
	   $.ajax({
				      type:"post" ,
					  url: "http://52.66.23.135/cis/index.php/return_stock/get_serial_number/" ,
					  data:		dataStr ,
					  async:true ,
						success:function(st){ //alert(st);
						   // $("#assign_branch").html(st);
							//'data saved successfully'
							//$("#product_name").load("http://52.66.23.135/cis/index.php/branch_order/loadproducts" , function(){});
							//location.reload();
							//$("#table_distributor").load("http://52.66.23.135/cis/index.php/branch_order/load_scheme_data" , function(){});
						}	
				   });
    });

    // And now fire change event when the DOM is ready
   // $('#product_name').trigger('change');
});

</script>-->

<script>
function delete_return_stock(ids) { 
    var x=confirm("Are you sure to delete record?");
	
  if (x == true) {
	  
	  $.ajax({
				      type:"post" ,
					  url: "http://52.66.23.135/cis/index.php/return_stock/delete_return_stock/" ,
					  data: {"id":ids},
					  async:true ,
						success:function(st){ 
						  //  $("#product_name").load("http://52.66.23.135/cis/index.php/branch_order/loadproducts" , function(){});
							location.reload();
							
						}	
				   });
				   
    
  } else {
    return false;
  }
}
</script>

 </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation');  ?>
		
        <div id="page-wrapper">
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Return Stock
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form">
						
                            <div class="form-group">
                                <label>Product Name</label>
                                <select  id="product_name" class="form-control" name ="product_name">
								
                                </select>
                            </div>
							 <div class="form-group">
                                <label>Serial Number</label>
                                <select id="assign_branch" name ="assign_branch[]" multiple>
								<?php 
								foreach($data as $d) { $con = count($d);
								for($i=0;$i<$con;$i++) {
								?>
								<option value="<?php echo $d[$i]->serial_number;?>"><?php echo $d[$i]->serial_number;?></option>
								<?php } }?>
								
								</select>
                            </div>
							<div class="form-group">
                                <label>Reason For Return</label>
                                <select  id="return_reason" class="form-control" name ="return_reason">
								<option value="">Select Reason</option>
								<option value="Defective_Stock">Defective Stock</option>
								<option value="OutBox_Damage">OutBox Damage</option>
								<option value="Dead_On_Arrival">Dead On Arrival</option>
								<option value="Filter_and_Membrane_Issue">Filter & Membrane Issue</option>
								<option value="Piece_Is_Very_Old">Piece Is Very Old</option>
								<option value="Cabinet_Damage">Cabinet Damage</option>
							    </select>
                            </div>
                            
                            <button type="button" id="distributor_btn" class="btn btn-success">Submit Button</button>
                            <button type="reset" class="btn btn-info">Reset Button</button>
                        </form>
                    </div>

                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<script>
$(document).ready(function(){
 $('#assign_branch').multiselect({
  nonSelectedText: 'Select Serial Number',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'483px'
 });
 });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  
  

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Return Product Details
            </div>
            
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   
					   
					   <script type="text/javascript">
			   $(function(){
			      $('#distributors_data_table').dataTable();
			   });
			   </script>
			  <table id="distributors_data_table" class="table table-striped table-bordered table-hover" id="dataTables-example">
                  			  <thead>
                            <tr>
                               
								<th>Product Name</th>
								<th>Serial Number</th>
                                <th>Return date</th>                               
                                <th>Return Reason</th>                               
                                <th>Status</th>                               
                                <th>Delete</th>                               
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data['get_my_return_stock_details'] as $d) { ?>
									<tr class="odd gradeX">
										<td><?= $d->return_product_name;?></td>
										<td><?=  $d->return_serial_number;?> </td>
										<td><?= $d->return_date_entered;?></td>
										<td><?= str_replace("_"," ",$d->return_reason);?></td>
										<td><?php  if($d->return_status == 0 ) { echo 'Pending'; } ?> </td>
										
			<td><button type="button" class="delete_return_stock btn btn-warning btn-circle" onclick = "delete_return_stock(<?php echo $d->return_stock_id;?>)"><i class="fa fa-times"></i></button></td>
									</tr>
                            <?php } ?>
							
                            </tbody>
			      </table>
				  
					   </div>					
				
                    
                </div>
               


            </div>
            
        </div>
        
    </div>
    
</div>



        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>

