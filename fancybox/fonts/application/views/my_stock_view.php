<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>My Stock</title>
	
	<?php    $this->load->view('library');    ?>
	
   </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation'); $this->load->helper('url');  ?>
		
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                My Stock
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   <script type="text/javascript">
			   $(function(){
			      $('#distributors_data_table').dataTable();
			   });
			   </script>
					   
					   <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
                  			  <thead>
                            <tr>
                                <th>Product Name</th>
								<th>Serial Number</th>
								<th>Model Number</th>
								<th>Dealer Code</th>
								<th>Dealer Name</th>
								<th>Quantity</th>
								<th>Aging</th>
                                <th>Last Scan Date</th>                               
                                							
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  
							
							//$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?= $d->my_product_name;?></td>
										<td><?=  $d->serial_number;?> </td>
										<td><?=  $d->model_number;?> </td>
										<td><?=  $d->my_dealer_code;?> </td>
										<td><?=  $d->my_dealer_name;?> </td>
										<td><?= $d->my_quantity;?></td>
										<td><?= $d->aging;?></td>
										<td><?=  $d->my_last_scan_date;?> </td>
										<!--<td><?php echo anchor('secondary/view_order/'.$result, 'Info', array('class' => '', 'id' => '')); ?></td>-->
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
