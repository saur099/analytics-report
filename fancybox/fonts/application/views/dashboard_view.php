<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Dashboard</title>
    <?= $this->load->view('library'); ?>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable();
        });
    </script>
	
   <script>
        $(document).ready(function() {
            $('#dataTables_order-example').dataTable();
        });
    </script>
	
	<script>
        $(document).ready(function() {
            $('#dataTables_scheme-example').dataTable();
        });
    </script>
	
   <script>
  $(document).ready(function() {
    $("#click2").click(function(){ 
			    
				$('#account_show').show('slow');
				$('#order_show').hide('slow');
				$('#scheme_show').hide('slow');
    });
	
	$("#click1").click(function(){ 
			    
				$('#order_show').show('slow');
				$('#account_show').hide('slow');
				$('#scheme_show').hide('slow');
    });
	$("#click3").click(function(){ 
			    
				$('#scheme_show').show('slow');
				$('#account_show').hide('slow');
				$('#order_show').hide('slow');
    });
	
  });
</script>

</head>

<body>






<div id="wrapper">



<?php include('partial/navigation.php'); ?>









<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header" style="font-size: 23px !important;"><?php $get_session_data = $this->session->userdata('logged_in');

	if($get_session_data['user_type'] == 'Branch')
	{ echo "HUB";} else { echo $data['get_session_data_name'][0]->distributor_name .'('.$get_session_data['username'].')      '. '(Closing Balance : Rs. '.$data['get_session_data_name'][0]->closing_balance.')';}?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->




<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo count($data['account_data']);?></div>
                        <div>My Network</div>
                    </div>
                </div>
            </div>
            <a href="#" id ="click2">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo count($data['scheme_data']);?></div>
                        <div>New Scheme!</div>
                    </div>
                </div>
            </div>
            <a href="#" id = "click3">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><?php echo count($data['order_data']);?></div>
                        <div>New Orders!</div>
                    </div>
                </div>
            </div>
            <a href="#" id ="click1">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-support fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">13</div>
                        <div>Support Tickets!</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- /.row -->






<div class="row" id = "order_show" style="display:none;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               Orders
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables_order-example">
                                         			  <thead>
                            <tr>
                                <th>Name</th>
								<th>Quantity</th>
								<th>Dealer Name</th>
                                <th>City</th>                               
                                <th>State</th>                               
                                <th>Hub Name</th>
                                <th>Details</th>								
                                                            
                            </tr>
                            </thead>
                        <tbody>
							<?php   foreach($data['order_data'] as $d) {  $result = str_replace(array(',', ' '), '_', $d->ids); ?>
									<tr class="odd gradeX">
										<td><?= $d->order_name;?></td>
										<td><?=  $d->quantity;?> </td>
										<td><?= $d->dealer_name;?></td>
										<td><?=  $d->city;?> </td>
										<td><?=  $d->state;?> </td>
										<td><?=  $d->hub_name;?> </td>
										<td><?php echo anchor('secondary/view_order/'.$result, 'Info', array('class' => '', 'id' => '')); ?></td>
										
									</tr>
                            <?php } ?>
                            </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


<!--show accounts-->

<div class="row" id = "account_show" style="display:none;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               My Accounts
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                  			  <thead>
                            <tr>
                                <th>Dealer Code</th>
								<th>Dealer Name</th>
								<th>City</th>
								<th>State</th>
								<th>Postal Code</th>
								<th>Customer Name</th>
								<th>Phone</th>
								<!--<th>Channel Category</th>
								<th>Channel Sub Category</th>-->
								<th>Beat Day</th>
								<th>Next Visit Date</th>
								<th>Hub Name</th>
								<!--<th>Hub Code</th>-->
						    </tr>
                            </thead>
							<tbody>
							<?php   foreach($data['account_data'] as $d) {  ?>
									<tr class="odd gradeX">
										<td><?=  $d->customer_number;?> </td>
										<td><?= $d->dealer_name;?></td>
										<td><?=  $d->billing_city;?> </td>
										<td><?= $d->billing_state;?></td>
										<td><?=  $d->billing_postalcode;?> </td>
										
										<td><?=  $d->full_name;?> </td>
										<td><?=  $d->mobile;?> </td>
										<!--<td><?=  $d->channel_category;?> </td>
										<td><?=  $d->channel_sub_category;?> </td>-->
										<td><?=  $d->beat_day;?> </td>
										<td><?=  $d->next_visit_date;?> </td>
										<td><?=  $d->hub_name;?> </td>
										<!--<td><?=  $d->hub_code;?> </td>-->
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
                </div>
                


            </div>
      
        </div>
    
    </div>
    
</div>

<!--end show accounts-->



<!--show schemes-->

<div class="row" id = "scheme_show" style="display:none;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
               My Scheme
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables_scheme-example">
                  			  <thead>
                            <tr>
                                <th>Scheme Name</th>
								<th>Product Name</th>
								<th>Quantity</th>
                                <th>Duration</th>                               
                                <th>Start Date</th>                               
                                <th>End Date</th>                               
                                <th>Delete</th>                               
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data['scheme_data']->result() as $d) {  $result = str_replace(array(',', ' '), '_', $d->ids); ?>
									<tr class="odd gradeX">
										<td><?= $d->scheme_name;?></td>
										<td><?=  $d->pro_name;?> </td>
										<td><?= $d->quantity;?></td>
										<td><?= str_replace("_"," ",$d->duration);?> </td>
										<td><?=  $d->start_time;?> </td>
										<td><?=  $d->end_time;?> </td>
			<td><button type="button" class="delete_scheme btn btn-warning btn-circle" onclick = "delete_scheme(<?php echo $d->id;?>)"><i class="fa fa-times"></i></button></td>
									</tr>
                            <?php } ?>
							
                            </tbody>
			      </table>
                </div>
                


            </div>
      
        </div>
    
    </div>
    
</div>

<!--end show scheme-->



</div>

</div>

</body>
</html>
