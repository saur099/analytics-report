<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Orders</title>
	<?php    $this->load->view('library');    ?>
	
   </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation'); $this->load->helper('url');  ?>
		
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Order Details
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   
					  
                  			  
							<?php   foreach($data as $d) {  $get_all_ids = str_replace(array(',', ' '), '_', $d->ids); ?>
							 <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
									<tr class="odd gradeX"><td>Name</td><td><?= $d->order_name;?></td></tr>
										
									
									<tr class="odd gradeX"><td>Dealer Name</td>	<td><?= $d->dealer_name;?></td></tr>
									<?php for($i=0;$i<$d->quan;$i++) { 
									$product = explode(",",$d->product_name);
									$quantitys = explode(",",$d->quantity);
									$ids_explode = explode(",",$d->ids);
									$rejection_reason = explode(",",$d->rejection_reason);
									?>
									<tr class="odd gradeX"><td>Product <?php echo $i+1;?></td>	<td colspan="4"><div style="float:left;"><?= $product[$i].' (Quantity :'.$quantitys[$i].')';?></div>&nbsp;&nbsp;</td></tr>
									<?php } ?>
				
									<tr class="odd gradeX"><td>City</td>	<td><?=  $d->city;?> </td></tr>
									<tr class="odd gradeX"><td>State</td>	<td><?=  $d->state;?> </td></tr>
									<tr class="odd gradeX"><td>Hub Name</td>	<td><?=  $d->hub_name;?> </td></tr>
									<!--<tr class="odd gradeX"><td>Rejection Reason</td>	<td><?php if($d->rejection_reason == '') { echo 'No Reason';} else { echo $d->rejection_reason;} ?> </td></tr>-->
									<tr class="odd gradeX"><td>Order Id</td>	<td><?php if($d->order_id == '') { echo 'Pending';} else { echo $d->order_id;} ?> </td></tr>
									
									<tr class="odd gradeX"><td>Order Date</td>	<td><?=  $d->order_date_entered;?> </td></tr>
									<tr class="odd gradeX"><td>Order Confirmation Date</td>	<td><?=  $d->order_confirmation_date;?> </td></tr>
		<!--<tr class="odd gradeX"><td>Dispatch Details</td>	<td><?php if($d->dispatch_by == '') { echo 'Pending';} else { echo $d->dispatch_by;} ?> </td></tr>-->
										
										
									</tr>
                            <?php } ?>
							</table>
							
							<table>
							<tr>
							<input type="hidden" id ='all_element_ids' value="<?php echo $get_all_ids;?>">
							<td><button type="button" class="reject_order btn btn-warning" onclick="reject_order();">Cancel Order</button></td><td>&nbsp;</td><td></td>
							<?php /*if($d->rejection_reason == '') { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<td>&nbsp;</td><td></td>
							<td><button type="button" class="addnew btn btn-success" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary disabled" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<td>&nbsp;</td><td></td>
							<td><button type="button" class="addnew btn btn-success disabled" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } */?>
							
							</tr>
                           </table>
						  
<script type="text/javascript">
function reject_order(ids) { var ids = $('#all_element_ids').val(); 
    var url = 'http://52.66.23.135/cis/index.php/secondary/reject_confirm_order/' +ids;
    CreateFancyBox('button.reject_order', url, '45%', 390);
}

function CreateFancyBox(selector, url, width, height) {
    $(selector).fancybox({
        'href': url,
        'scrolling'         : 'no',
        'titleShow'         : false,
        'titlePosition'     : 'none',
        'openEffect'        : 'elastic',
        'closeEffect'       : 'none',
        'closeClick'        : false,
        'openSpeed'         : 'fast',
        'type'              : 'iframe',
		'padding'           : 0,
        'preload'           : true,
        'width'             : width,
        'height'            : height,
        'fitToView'         : false,
        'autoSize'          : false,
        'helpers'           : { 
            overlay :   {
                'closeClick': false,
            }
        },
        afterClose          : function() { //I search StackOverflow, add this function to reload parent page, it will appear the flash data message notification which I write on controller "add_user"
            parent.location.reload();
        }
    });
}
</script>
<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

   
	

			      
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
