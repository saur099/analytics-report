<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Orders</title>
	
	<?php    $this->load->view('library');    ?>
	
   </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation'); $this->load->helper('url');  ?>
		
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Orders
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   <script type="text/javascript">
			   $(function(){
			      $('#distributors_data_table').dataTable();
			   });
			   </script>
					   
					   <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
                  			  <thead>
                            <tr>
                                <th>Name</th>
								<th>Quantity</th>
								<th>Dealer Name</th>
                                <th>City</th>                               
                                <th>State</th>                               
                                <th>Hub Name</th>
								<th>Order Status</th>
                                <th>Details</th>								
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) { $result = str_replace(array(',', ' '), '_', $d->ids); ?>
									<tr class="odd gradeX">
										<td><?= $d->order_name;?></td>
										<td><?=  $d->quantity;?> </td>
										<td><?= $d->dealer_name;?></td>
										<td><?=  $d->city;?> </td>
										<td><?=  $d->state;?> </td>
										<td><?=  $d->hub_name;?> </td>
										<td><?=  $d->order_status;?> </td>
										<td><?php echo anchor('branch_order/view_order/'.$result, 'Info', array('class' => '', 'id' => '')); ?></td>
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
