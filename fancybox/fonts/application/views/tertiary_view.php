<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tertiary Data</title>
	
	<?php    $this->load->view('library');    ?>
	
   </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation'); $this->load->helper('url');  ?>
		
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Tertiary Orders
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   <script type="text/javascript">
			   $(function(){
			      $('#distributors_data_table').dataTable();
			   });
			   </script>
					   
					   <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
                  			  <thead>
                            <tr>
                                <th>Customer Name</th>
                                <th>Customer City</th>
                                <th>Customer Mobile</th>
								<th>Dealer Name</th>
								<th>Dealer Code</th>
                                <th>Dealer City</th>                               
                                <th>Serial Number</th>                               
                                                            
                            </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  
							
							$result = str_replace(array(',', ' '), '_', $d->ids);
							?>
									<tr class="odd gradeX">
										<td><?= $d->CUSTOMER_NAME;?></td>
										<td><?=  $d->CITY;?> </td>
										<td><?= $d->MOBILE_NUMBER;?></td>
										<td><?=  $d->DEALER_NAME;?> </td>
										<td><?=  $d->DEALER_CODE;?> </td>
										<td><?=  $d->DEALER_CITY;?> </td>
										<td><?=  $d->Serial_No;?> </td>
										
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
