<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Add Dealer
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form">
                            <div class="form-group">
                                <label>Dealer Name</label>
                                <input  type="text" class="form-control" id="distributor_name"/>
                            </div>
                            <div class="form-group">
                                <label>Zone</label>
                                <select  id="zone_id" class="form-control">
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <input id="address" class="form-control"  placeholder="Enter Address">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input class="form-control" id="phone" placeholder="Enter Phone">
                            </div>

                            <button type="button" id="distributor_btn" class="btn btn-success">Submit Button</button>
                            <button type="reset" class="btn btn-info">Reset Button</button>
                        </form>
                    </div>

                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>








