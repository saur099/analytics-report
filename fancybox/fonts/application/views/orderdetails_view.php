<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Orders</title>
	<?php    $this->load->view('library');    ?>
	
   </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation'); $this->load->helper('url');  ?>
		
        <div id="page-wrapper">
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Order Details
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   
					  
                  			  
							<?php  foreach($data['order_details'] as $d) {  
							$get_all_ids = str_replace(array(',', ' '), '_', $d->ids);
							$get_all_stock = str_replace(array(',', ' '), '_', $d->avail_stock);
							?>
							 <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
									<tr class="odd gradeX"><td>Name</td><td><?= $d->order_name;?></td></tr>
										
									<!--<tr class="odd gradeX"><td>Quantity</td>	<td><?=  $d->quantity;?> </td></tr>-->
									<tr class="odd gradeX"><td>Dealer Name</td>	<td><?= $d->dealer_name;?></td></tr>
									<?php for($i=0;$i<$d->quan;$i++) { 
									$product = explode(",",$d->product_name);
									$quantitys = explode(",",$d->quantity);
									$ids_explode = explode(",",$d->ids);
									$rejection_reason = explode(",",$d->rejection_reason);
									$avail_stocks = explode(",",$d->avail_stock);
									
									?>
<tr class="odd gradeX"><td>Product <?php echo $i+1;?></td>	<td colspan="4"><div style="float:left;"><?= $product[$i].' (Quantity :'.$quantitys[$i].')'. '          (Stock Avail:'.$avail_stocks[$i].')';?></div>&nbsp;&nbsp;<div style="margin-left:20%;float:left;"><td>
				
				<button title="Reject Order" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="reject_order btn btn-warning btn-circle" onclick="reject_order(<?php echo $ids_explode[$i];?>,<?php echo $data['distributor_code'];?>);"><i class="fa fa-times"></i>
                </button>&nbsp;&nbsp;
				<?php if($rejection_reason[$i] == '' && $avail_stocks[$i] >= $quantitys[$i]) { ?>
				<button title="Confirm Dispatch" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="addnew btn btn-info btn-circle" onclick="addnew(<?php echo $ids_explode[$i];?>,<?php echo $data['distributor_code'];?>);"><i class="fa fa-check"></i>
                </button>
				<?php } else { ?>
				<button title="Confirm Dispatch" type="button" style="width:26px !important;height:26px !important;padding:3px 0 !important;" class="addnew btn btn-info btn-circle disabled" onclick="addnew(<?php echo $ids_explode[$i];?>);"><i class="fa fa-check"></i>
                </button>
				<?php } ?>
				
				</td></div></td></tr>
									<?php } ?>
									<tr class="odd gradeX"><td>Billing Address</td>	<td><?=  $d->billing_address;?> </td></tr>
									<tr class="odd gradeX"><td>City</td>	<td><?=  $d->city;?> </td></tr>
									<tr class="odd gradeX"><td>State</td>	<td><?=  $d->state;?> </td></tr>
									<tr class="odd gradeX"><td>Hub Name</td>	<td><?=  $d->hub_name;?> </td></tr>
									<!--<tr class="odd gradeX"><td>Rejection Reason</td>	<td><?php if($d->rejection_reason == '') { echo 'No Reason';} else { echo $d->rejection_reason;} ?> </td></tr>-->
									<tr class="odd gradeX"><td>Order Id</td>	<td><?php if($d->order_id == '') { echo 'Pending';} else { echo $d->order_id;} ?> </td></tr>
									<tr class="odd gradeX"><td>Order Date</td>	<td><?=  $d->order_date_entered;?> </td></tr>
									<tr class="odd gradeX"><td>Order Taken By</td>	<td><?=  $d->emp_name;?> </td></tr>
									<tr class="odd gradeX"><td>Employee Code</td>	<td><?=  $d->employee_code;?> </td></tr>
									<tr class="odd gradeX"><td>Designation</td>	<td><?=  $d->emp_type;?> </td></tr>
									<tr class="odd gradeX"><td>Scheme Applicable</td>	<td><?php if($data['scheme_details'][0]->scheme_name == '') { echo 'No Scheme Applicable';} else { echo $data['scheme_details'][0]->scheme_name;} ?> </td></tr>
		<!--<tr class="odd gradeX"><td>Dispatch Details</td>	<td><?php if($d->dispatch_by == '') { echo 'Pending';} else { echo $d->dispatch_by;} ?> </td></tr>-->
										
										
									</tr>
                            <?php } ?>
							</table>
							<?php if($data['dispatch_details']) { ?>
							<div class="panel-heading" style="color: #3c763d;background-color: #dff0d8;border-color: #d6e9c6;">Dispatch Details</div>
							
							<table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
							<tr class="odd gradeX"><td>Product Name</td><td>Dispatch Details</td></tr>
							<?php foreach($data['dispatch_details'] as $dispatch_data) { //echo "<pre>"; print_r($ds);?>
									
				<tr class="odd gradeX"><td><?=$dispatch_data->product_name;?></td><td>Quantity : <?=$dispatch_data->quantity;?></td><td>Dispatch Through : <?=str_replace("_"," ",$dispatch_data->dispatch_through);?></td></tr>
										<?php } ?>
							</table>
							<?php } ?>
							
							<table>
							<!--<tr>
							<td><button type="button" class="reject_order btn btn-warning" onclick="reject_order(<?php echo $d->id;?>);">Reject Order</button></td><td>&nbsp;</td><td></td>
							<?php if($d->rejection_reason == '' && $d->order_id == '' && $d->dispatch_by == '') { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<td>&nbsp;</td><td></td>
							<td><button type="button" class="addnew btn btn-success" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } else { if($d->order_id!= '') { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary disabled" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm(<?php echo $d->id;?>);">Confirm Order</button></td>
							<?php } ?>
							<td>&nbsp;</td><td></td>
							<?php if($d->dispatch_by!= '') { ?>
							<td><button type="button" class="addnew btn btn-success disabled" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew btn btn-success" data-height="410" onclick="addnew(<?php echo $d->id;?>);">Confirm Dispatch</button></td>
							<?php } ?>
							<?php } ?>
							
							</tr>-->
							<tr>
							<input type="hidden" id ='all_element_ids' value="<?php echo $get_all_ids;?>">
							<input type="hidden" id ='all_avail_stocks' value="<?php echo $get_all_stock;?>">
							<input type="hidden" id ='all_quantity' value="<?php echo $d->quantity;?>">
							<input type="hidden" id ='all_product' value="<?php echo $d->product_name;?>">
							
							<?php 
							foreach($data['order_details'] as $dd)
							{ if($dd->order_id == '') {
							?>
							<td><button type="button" class="addnew_confirm btn btn-primary" onclick="addnew_confirm();">Confirm Order</button></td>
							<?php } else { ?>
							<td><button type="button" class="addnew_confirm btn btn-primary disabled" onclick="addnew_confirm();">Confirm Order</button></td>
							<?php } }?>
							</tr>
                           </table>
						  
						  
						  
						  
						  
						  
						  
							
							
<script type="text/javascript">
function addnew(ids,dis_code) { 
    var url = 'http://52.66.23.135/cis/index.php/secondary/confirm_dispatch/' +ids + '_' + dis_code;
    CreateFancyBox('button.addnew', url, '45%', 390);
}

function addnew_confirm() { 
var ids = $('#all_element_ids').val(); 
var avail_stock = $('#all_avail_stocks').val(); 
var quantity = $('#all_quantity').val(); 
var all_product = $('#all_product').val(); 
    var url = 'http://52.66.23.135/cis/index.php/secondary/confirm_order/' +ids;
    CreateFancyBox('button.addnew_confirm', url, '45%', 390);
	
	
	// e.preventDefault(); // avoids calling preview.php
	/*var data = 'ids='+ ids + '&avail_stock='+ avail_stock; 
    $.ajax({
      type: "POST",
      cache: false,
      url: 'http://52.66.23.135/cis/index.php/secondary/confirm_order/', // preview.php
      data: data, // all form fields
      success: function (data) {
        // on success, post (preview) returned data in fancybox
        $.fancybox(data, {
          // fancybox API options
          fitToView: false,
          width: 390,
          height: 390,
               'scrolling'         : 'no',
        'titleShow'         : false,
        'titlePosition'     : 'none',
        'openEffect'        : 'elastic',
        'closeEffect'       : 'none',
        'closeClick'        : false,
        'openSpeed'         : 'fast',
        
		'padding'           : 0,
        'preload'           : true,
        'width'             : 390,
        'height'            : 390,
        'fitToView'         : false,
        'autoSize'          : false,
        'helpers'           : { 
            overlay :   {
                'closeClick': false,
            }
        },
        afterClose          : function() { //I search StackOverflow, add this function to reload parent page, it will appear the flash data message notification which I write on controller "add_user"
            parent.location.reload();
        }
        }); // fancybox
      } // success
    }); // ajax
	
	*/
}

function reject_order(ids,dis_code) { 
    var url = 'http://52.66.23.135/cis/index.php/secondary/reject_order/' +ids + '_' + dis_code;
    CreateFancyBox('button.reject_order', url, '45%', 390);
}

function CreateFancyBox(selector, url, width, height) {
    $(selector).fancybox({
        'href': url,
        'scrolling'         : 'no',
        'titleShow'         : false,
        'titlePosition'     : 'none',
        'openEffect'        : 'elastic',
        'closeEffect'       : 'none',
        'closeClick'        : false,
        'openSpeed'         : 'fast',
        'type'              : 'iframe',
		'padding'           : 0,
        'preload'           : true,
        'width'             : width,
        'height'            : height,
        'fitToView'         : false,
        'autoSize'          : false,
        'helpers'           : { 
            overlay :   {
                'closeClick': false,
            }
        },
        afterClose          : function() { //I search StackOverflow, add this function to reload parent page, it will appear the flash data message notification which I write on controller "add_user"
            parent.location.reload();
        }
    });
}
</script>
<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/jquery.fancybox.pack.js?v=2.1.7"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
   
	

			      
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
