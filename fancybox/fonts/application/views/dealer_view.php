<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dealer</title>
	
	<?php    $this->load->view('library');    ?>
	
    <!--<script>
        $(document).ready(function() {
		
		    $("#zone_id").load("http://52.66.23.135/cis/index.php/dealer/loadZoneComboBox" , function(){});
		    $("#table_distributor").load("http://52.66.23.135/cis/index.php/dealer/load_table_data" , function(){});

            $('#distributor_btn').click(function(){
			   var  distributor_name  = $('#distributor_name').val();
			   var  zone_id = $('#zone_id').val();
			   var  address = $('#address').val();
			   var  phone = $('#phone').val();
			   var  dataStr = "distributor_name="+distributor_name+"&zone_id="+zone_id+"&address="+address
					  +"&phone="+phone;
					  
				 
				if(distributor_name=="") {
				   alert('Enter distributor name');
				   $('#distributor_name').focus();
				   return false;
				}else{
				   $.ajax({
				      type:"post" ,
					  url: "http://52.66.23.135/cis/index.php/distributor/addDistributorAction/" ,
					  data:		dataStr ,
					  async:true ,
						success:function(st){ 
						    alert(st);
							//'data saved successfully'
							$("#zone_id").load("http://52.66.23.135/cis/index.php/dealer/loadZoneComboBox" , function(){});
							$("#table_distributor").load("http://52.66.23.135/cis/index.php/dealer/load_table_data" , function(){});
						}	
				   });
				}	
            });           
        });
    </script>-->
 </head>

 <body>


 <div id="wrapper">
        
		<?php $this->load->view('partial/navigation');  ?>
		
        <div id="page-wrapper">
<br>
            <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Dealers
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
				
				
				       <div id="table_distributor">
					   
					   <script type="text/javascript">
			   $(function(){
			      $('#distributors_data_table').dataTable();
			   });
			   </script>
					   
					   <table id="distributors_data_table" class="table table-striped table-bordered table-hover" >
                  			  <thead>
                            <tr>
                                <th>Dealer Name</th>
								<th>City</th>
								<th>State</th>
								<th>Postal Code</th>
								<th>Customer Number</th>
								<th>Customer Name</th>
								<th>Phone</th>
								<th>Channel Category</th>
								<th>Channel Sub Category</th>
								<th>Hub Name</th>
								<!--<th>Hub Code</th>-->
						    </tr>
                            </thead>
							<tbody>
							<?php   foreach($data as $d) {  ?>
									<tr class="odd gradeX">
										<td><?= $d->dealer_name;?></td>
										<td><?=  $d->billing_city;?> </td>
										<td><?= $d->billing_state;?></td>
										<td><?=  $d->billing_postalcode;?> </td>
										<td><?=  $d->customer_number;?> </td>
										<td><?=  $d->full_name;?> </td>
										<td><?=  $d->mobile;?> </td>
										<td><?=  $d->channel_category;?> </td>
										<td><?=  $d->channel_sub_category;?> </td>
										<td><?=  $d->hub_name;?> </td>
										<!--<td><?=  $d->hub_code;?> </td>-->
									</tr>
                            <?php } ?>
                            </tbody>
			      </table>
					   
					   
					   </div>					
				
                    
                </div>
                <!-- /.table-responsive -->



            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
        </div>
         <!-- /#wrapper -->
 </div>

 </body>
</html>
