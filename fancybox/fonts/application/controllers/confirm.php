<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Confirm extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$this->load->model('orders');
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$data = $this->orders->get_confirm_orders($distributor_code);
		$this->load->view('confirm_order/confirm_order_view', array('data' => $data));

	}
		public function view_confirm_order($id)
	{  
		$this->load->model('orders');
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$data = $this->orders->getorderbyid($id,$distributor_code);
		$this->load->view('confirm_order/confirm_orderdetails_view', array('data' => $data));

	}
	
	public function confirm_dispatch($ids)
	{ 
		$data = $ids;
		$this->load->view('order_dispatch', array('data' => $data));
	}
	public function confirm_order_dispatch()
	{ 
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$dispatch = $data['dispatch'];
		$this->load->model('orders');
		$this->orders->update_order($order_id, $dispatch);
		
	}
	
	public function confirm_order($ids)
	{
		$this->load->model('orders');
		$num0 = (rand(10,100));
        $num1 = date("ymd");
        //$num2 = (rand(100,1000));
       // $num3 = time();
    $data = "OR".'#'.$num0 . $num1;
	$this->orders->update_order_id($ids, $data);
		$this->load->view('confirm_order', array('data' => $data));
	}
	
	public function reject_order($ids)
	{
		$data = $ids;
		$this->load->view('reject_order', array('data' => $data));
	}
	
	public function reject_order_details()
	{ 
		$data = $this->input->post();
		$order_id = $data['order_id'];
		$reject = $data['reject'];
		$this->load->model('orders');
		$this->orders->update_order_rejection_details($order_id, $reject);
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */