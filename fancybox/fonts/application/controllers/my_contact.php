<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_Contact extends CI_Controller {
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }
	
	public function index()
	{
		$get_session_data = $this->session->userdata('logged_in');
	    $login_type = $get_session_data['user_type'];
		
		$this->load->model('contacts');
		$data = $this->contacts->get_my_livpure_contact($get_session_data['username']);
		$this->load->view('my_livpure_contact', array('data' => $data));
		  
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */