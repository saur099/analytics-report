<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tertiary extends CI_Controller {
	
	
	public function __construct() 
    { 
        parent::__construct(); 
            if(!$this->session->userdata['logged_in']['username']) 
            return redirect('login', 'refresh'); 
    }

	
	public function index()
	{
		
		$this->load->model('tertiarys');
		$distributor_code = $this->session->userdata['logged_in']['username'];
		$data = $this->tertiarys->get_tertiary_data($distributor_code);
		
		
		 $this->load->view('tertiary_view', array('data' => $data));

	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */