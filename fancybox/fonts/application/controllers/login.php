<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{ //print_r($this->session->userdata['logged_in']);
		$this->load->view('login_view');
	}
	
	
	public function userLoginCheckAction() {	
        	
	    $user_name = trim($this->input->post('user_name'));
		$user_pass   = trim($this->input->post('user_pass'));
		$user_type = trim($this->input->post('login_as'));
		//$data = $this->input->post('login_as');
		//print_r($data);
		$this->load->model('logins');
		/*$data=array(
			 'user_name'=>$this->input->post('user_name'),
			 'pass'=>$this->input->post('user_pass'),
			 'user_type'=>$this->input->post('login_as')
		 );*/
		
	    $res = $this->logins->userLoginCheckActionData($user_name,$user_pass,$user_type);
		$get_login_count = intval($res[0]->counter);
		//print_r($res);
		if($get_login_count == "1")
			{
				$sess_array = array('user_id'=>$res[0]->user_id,'username'=>$user_name,'user_type' => $user_type);
				$this->session->set_userdata('logged_in', $sess_array);
				echo 'login_success';
			}
			else
			{
				
				echo 'login_un_success';
				
			}
    				
			
	}
	
    public function logout() {	       
	$this->session->sess_destroy();
	?>
	<script type="text/javascript"> window.location= "http://52.66.23.135/cis/index.php/login/";</script>
	<?php 		
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */