function getJc()
{
var cdate = document.getElementById('issue_date_c').value;
var sUrl = 'index.php?module=sur_Survey_Issue&action=get_cdate&selectdate='+cdate;
SUGAR.ajaxUI.showLoadingPanel();
var callback = {
success: function(o) {
if(o.responseText =='Less') 
{
alert("You do not have permission to create issue for future date");
document.getElementById('issue_date_c').value = '';
SUGAR.ajaxUI.hideLoadingPanel();
}
else
{	
SUGAR.ajaxUI.hideLoadingPanel();
}
}
}
YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, null); 
}

function get_retailers (station) {
var sUrl = 'index.php?module=sur_Survey_Issue&action=get_retailer&stations='+station;
SUGAR.ajaxUI.showLoadingPanel();
var callback = {
success: function(o) {
if(o.responseText!='')
{
document.getElementById('retailer_id_c').innerHTML=o.responseText;
SUGAR.ajaxUI.hideLoadingPanel();
}
else
{	
alert("Try Again.");
SUGAR.ajaxUI.hideLoadingPanel();
}
}
}
YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, null);
}

function IssueId()
{
var current_id = document.getElementById("id").value;
var issue_type_value=document.getElementById("issue_type").value;
var dates=document.getElementById("date").value;
var assign_users=document.getElementById("assign_user").value;
var closur_date=document.getElementById("sel").value;
var discription=document.getElementById("text_box").value;
var days=document.getElementById("day_code").value;
var months=document.getElementById("month_code").value;
var years=document.getElementById("year_code").value;
var join_id=months+days+years;
if(issue_type_value==''){
alert("Please Select Issue Type");
return false;
}
 else if(discription=='')
 {
   alert("Please Enter Issue Description");
	return false;
}
else{
var sUrl = 'index.php?module=sur_Survey_Issue&action=Issue_id&Issue_id='+issue_type_value+'&issueID='+join_id+'&getDate='+dates+'&asignUSER='+assign_users+'&closurDATE='+closur_date+'&discription='+discription+'&record_ID='+current_id;
//alert(sUrl);

SUGAR.ajaxUI.showLoadingPanel();
var callback = {
success: function(o) {

if(o.responseText!='')
{
 alert(o.responseText);
window.location.href ='index.php?module=sur_Survey_Issue&action=index&return_module=sur_Survey_Issue&return_action=DetailView';
SUGAR.ajaxUI.hideLoadingPanel();
}
else
{	
alert("Try Again.");
SUGAR.ajaxUI.hideLoadingPanel();
}
}
}
YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, null);
}
}


function edit_save()
{
var prority=document.getElementById("priority").value;
var assign_user=document.getElementById("assign_user").value;
var status=document.getElementById("status").value;
var expected_date=document.getElementById("sel").value;
var closing_date=document.getElementById("sel1").value;
var current_id = document.getElementById("id").value;
var internal=document.getElementById("internal").value;
var external=document.getElementById("external").value;

var sUrl = 'index.php?module=sur_Survey_Issue&action=save_after_edit&priority='+prority+'&user_id='+assign_user+'&status='+status+'&expect_date='+expected_date+'&closingDATE='+closing_date+'&record_ID='+current_id+'&internl_remark='+internal+'&external_remark='+external;
SUGAR.ajaxUI.showLoadingPanel();
var callback = {
success: function(o) {
if(o.responseText==1)
{
window.location.href = 'index.php?module=sur_Survey_Issue&action=admin_Detail&return_module=sur_Survey_Issue&return_action=admin_Detail&record='+current_id;
SUGAR.ajaxUI.hideLoadingPanel();
}
else
{	
alert("Try Again.");
SUGAR.ajaxUI.hideLoadingPanel();
}
}
}
YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, null);
}

function closed_fill()
{
var status=document.getElementById("status").value;
var date = new Date();
var month_no=(date.getMonth()+1);
 if(month_no > 9)
{
 var twoDigitMonth = ((date.getMonth().length+1) === 1)? (date.getMonth()+1) : (date.getMonth()+1);
 var currentDate = date.getDate() + "-" + twoDigitMonth + "-" + date.getFullYear();
if(status=='Closed')
{
document.getElementById('sel1').value=currentDate ; 
}

else                           //edit by K
{
document.getElementById('sel1').value = '' ; 
}                                                   //edit by K
}
 else{
var twoDigitMonth = ((date.getMonth().length+1) === 1)? (date.getMonth()+1) : '0' + (date.getMonth()+1);
var currentDate = date.getDate() + "-" + twoDigitMonth + "-" + date.getFullYear();
if(status=='Closed')
{
document.getElementById('sel1').value=currentDate ; 
}	
else                           //edit by K
{
document.getElementById('sel1').value = '' ; 
} 
  }
}

function toggle (source) {
var chkArray = [];
$(".checkbox:checked").each(function() {
chkArray.push($(this).val());
});

/* we join the array separated by the comma */
var selected;
selected = chkArray.join(",") + "";
document.getElementById('checkedId').value=selected;
}

function test(frm){
var message = [];
//For each checkbox see if it has been checked, record the value;
for (i = 0; i < frm.one.length; i++)
if (frm.one[i].checked){
message = message + frm.one[i].value + "\n";
}

alert(message);
}

function checked_delete()
{
var id_for_delete=document.getElementById('checkedId').value;
if(id_for_delete == '')
{
alert("Please Select Record For Delete ");
exit;
}
var sUrl = 'index.php?module=sur_Survey_Issue&action=checked_delete&id_del='+id_for_delete;
SUGAR.ajaxUI.showLoadingPanel();
var callback = {
success: function(o) {
if(o.responseText==1)
{
window.location.href = 'index.php?module=sur_Survey_Issue&action=index&parentTab=Survey';

SUGAR.ajaxUI.hideLoadingPanel();
}

else
{	
alert("Try Again.");
SUGAR.ajaxUI.hideLoadingPanel();
}
}
}
YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, null);
}

function test1(source) {
totalSum=[];
checkboxes = document.getElementsByName('chk');
for(var i=0, n=checkboxes.length;i<n;i++) {
checkboxes[i].checked = source.checked;
totalSum += checkboxes[i].value + ',';
}
document.getElementById('checkedId').value=totalSum;
}